<?php
/*
Template Name: flow
*/
?>
<?php get_header();?>



<div id="flow" class="content-area underlayer">
  <main id="main" class="site-main" role="main">

    <nav id="breadcrumbs">
      <ul class="cf">
        <li><a href="/"><span>ホーム</span></a></li>
        <li>入居までの流れ</li>
      </ul>
    </nav>

    <article class="ul-flex">
      <section class="ul-contents">
        <h2 class="ul-title mincho">入居までの流れ</h2>

        <section class="flow01">
          <div class="flow-box">
            <h3 class="ul-title02 mincho"><span>1</span>物件のお問い合わせ</h3>
            <p>実際に内覧を希望する物件が決まりましたら、物件ページからお問い合わせ頂くか、お電話にてお問い合わせください。<br>
              メールにて、最新の募集図面をお送りさせて頂きますので、到着までしばらくお待ち下さい。<br>
              また、募集状況につきましては、常に最新の状況をHPに掲載するように心掛けておりますが、貸主様が定める募集条件に変更が生じたり、別のお客様から申し込みが入ったことで、ご案内できないこともありますので、予めご了承ください。</p>
          </div>

          <div class="flow-box">
            <h3 class="ul-title02 mincho"><span>2</span>内覧に関して</h3>
            <p>当社の営業スタッフと日時を相談頂き確定しましたら、当日は物件エントランスにてお待ち合わせとさせて頂きますので、当社に来店する必要もありません。<br>
              また、1日の内覧件数は原則3部屋までとさせて頂き、同じ物件なら30分程度、別の物件ですと移動が必要なので、1時間〜1時間半程のお時間頂戴します。<br>
              人気な物件ですと予約が取りにくいこともあり、ご希望の日時にご案内できないこともありますので、予めご了承ください。</p>
          </div>

          <div class="flow-box">
            <h3 class="ul-title02 mincho"><span>3</span>申し込みに関して</h3>
            <p>希望の物件が見つかったときは、入居申込書を提出することで物件を確保することができます。<br>
              引き合いが多い物件ですと、数時間の差で2番手・3番手扱いとなってしまうことが頻繁に起きますので、できる限り早い段階での申し込みが望ましいです。</p>
            <div class="note">
              <h4 class="mincho">＜申し込みに必要な書類＞</h4>
              <p>入居申込書を提出後2~3日以内に、下記の申し込み書類を提出頂きます。</p>
              <h5>・個人名義で契約</h5>
              <ul class="list-s">
                <li>身分証両面写し（免許証・保険証・パスポートのいづれか）</li>
                <li>所得証明写し（源泉徴収票・確定申告書・課税証明書）</li>
              </ul>
              <h5>・法人名義で契約</h5>
              <ul class="list-s">
                <li>登記簿謄本写し※3ヶ月以内発行</li>
                <li>印鑑証明書写し※3ヶ月以内発行</li>
                <li>会社概要（HPや事業内容のわかる資料など）</li>
                <li>決算書（貸借対照表・損益計算書・キャッシュフロー計算書）※1~3期分</li>
                <li>代表者・入居者それぞれの身分証両面写し（免許証・保険証・パスポートのいづれか）</li>
              </ul>
              <h5>・提出方法</h5>
              <ul class="list-s">
                <li>メールの場合：PDFデータにて</li>
                <li>FAXの場合：</li>
                <li>郵送の場合：〒1040061 東京都中央区銀座3-10-19美術家会館402号</li>
              </ul>
            </div>
          </div>

          <div class="flow-box">
            <h3 class="ul-title02 mincho"><span>4</span>審査に関して</h3>
            <p>必要書類の提出が完了しましたら審査に移りますが、結果が出る目安は以下の通りです。</p>
            <ul class="list-l">
              <li>・個人契約：当日〜1週間（審査項目が少ない：勤務先・年収）</li>
              <li>・法人契約：2日〜10日（審査項目が多い：会社の規模・売上・代表者審査）</li>
            </ul>
          </div>

          <div class="flow-box">
            <h3 class="ul-title02 mincho"><span>5</span>確認連絡に関して</h3>
            <p>物件により審査基準が異なるため、一概には言えませんが、一般的に下記の確率で確認連絡が入ります。</p>
            <ul class="list-l">
              <li>・契約者：50%の確率で連絡が入りますが、書類審査で完了することもあります。</li>
              <li>・連帯保証人：100%に近い確率で連絡が入るので、事前に伝えておきましょう。</li>
              <li>・緊急連絡先と勤務先：稀ですが、連絡が入るときは事前にお伝えします。</li>
            </ul>
          </div>

          <div class="flow-box">
            <h3 class="ul-title02 mincho"><span>6</span>契約に関して</h3>
            <p>無事に審査完了となりましたら、契約締結（重要事項説明・署名捺印）を弊社にて執り行わせて頂き、お時間は40分〜60分程頂戴致します。</p>
            <div class="note">
              <h4 class="mincho">＜契約に必要な書類＞</h4>
              <p>契約時に持参頂く書類は以下の通りです。</p>
              <h5>・個人契約</h5>
              <ul class="list-s">
                <li>住民票原本※3ヶ月以内発行</li>
                <li>身分証ファーストコピー</li>
                <li>認印</li>
              </ul>
              <h5>・法人契約</h5>
              <ul class="list-s">
                <li>登記簿謄本※3ヶ月以内発行</li>
                <li>会社・代表者の印鑑証明書※3ヶ月以内発行</li>
                <li>入居者の住民票※3ヶ月以内発行</li>
                <li>法人実印</li>
              </ul>
            </div>
          </div>
        </section><!-- .flow01 -->
      </section><!-- .ul-contents -->

      <?php get_sidebar(); ?>
    </article><!-- .ul-flex -->

  </main><!-- #main -->
</div><!-- #about -->

<?php get_footer(); ?>
