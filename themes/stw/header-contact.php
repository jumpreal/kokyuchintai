<!doctype html>
<html lang="ja">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <?php wp_head(); ?>

    <link rel="stylesheet" type="text/css" href="<?php bloginfo('template_directory'); ?>/style.css">
    <link rel="stylesheet" type="text/css" href="<?php bloginfo('template_directory'); ?>/assets/css/common.css">
    <link rel="stylesheet" type="text/css" href="<?php bloginfo('template_directory'); ?>/assets/css/style2017.css">
    <link rel="stylesheet" type="text/css" href="<?php bloginfo('template_directory'); ?>/assets/css/corners2017.css">
    <link rel="stylesheet" type="text/css" href="<?php bloginfo('template_directory'); ?>/assets/css/body.css">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri();?>/assets/css/jquery.range.css">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri();?>/assets/css/modal-search.css">
    <link rel="stylesheet" type="text/css" href="<?php bloginfo('template_directory'); ?>/assets/font/flaticon.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/v4-shims.css">
    <link rel="stylesheet" type="text/css" href="<?php bloginfo('template_directory'); ?>/assets/css/style-child.css">
    <link rel="stylesheet" type="text/css" href="<?php bloginfo('template_directory'); ?>/assets/css/swiper.min.css">

    <link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/highlight.js/8.7/styles/default.min.css">

    <!--
<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_directory'); ?>/assets/css/style-stw.css">
<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_directory'); ?>/assets/css/style-stwsp.css">
-->




    <link rel="shortcut icon" href="<?php bloginfo('template_directory'); ?>/assets/images/common/favicon.ico">
    <link rel="apple-touch-icon" href="<?php bloginfo('template_directory'); ?>/assets/images/common/apple-touch-icon.ico">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="<?php bloginfo('template_directory'); ?>/assets/js/common.js"></script>
    <script defer src="https://use.fontawesome.com/releases/v5.8.1/js/all.js" integrity="sha384-g5uSoOSBd7KkhAMlnQILrecXvzst9TdC09/VM+pjDTCM+1il8RHz5fKANTFFb+gQ" crossorigin="anonymous"></script>

</head>

<body <?php body_class(''); ?>>
    <div id="fb-root"></div>
    <script async defer crossorigin="anonymous" src="https://connect.facebook.net/ja_JP/sdk.js#xfbml=1&version=v7.0" nonce="F0KOgt9i"></script>


    <header class="l-header">
        <div class="l-headerContact l-wrap header flex-w jc-between">
            <div class="flex-w ai-center">
                <h1 class="l-headerLogo l-headerCont flex-w ai-center">
                    <a href="<?php bloginfo('url'); ?>" class="l-headerLogo_link"><img src="<?php bloginfo('template_directory'); ?>/assets/images/common/logo.png" alt="高級賃貸東京" class="l-headerLogo_img"></a>
                </h1>
            </div>
            <div class="l-headerContact_flow flex-nw">
                <div class="l-headerContact_flowItem __active">ご入力</div>
                <div class="l-headerContact_flowItem">完了</div>
            </div>
        </div>

    </header>




    <div id="page" class="site">


        <?php



    /*

     * If a regular post or page, and not the front page, show the featured image.

     * Using get_queried_object_id() here since the $post global may not be set before a call to the_post().

     */
/*
    if ( ( is_single() || ( is_page() && ! twentyseventeen_is_frontpage() ) ) && has_post_thumbnail( get_queried_object_id() ) ) :

        echo '<div class="single-featured-image-header">';

        echo get_the_post_thumbnail( get_queried_object_id(), 'twentyseventeen-featured-image' );

        echo '</div><!-- .single-featured-image-header -->';

    endif;
*/
    ?>


        <div class="l-wrap main">
