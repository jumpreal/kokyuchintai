<?php
/*
Template Name: マイページ - 検索条件保存
Template Post Type: post,page
*/
$sc = ( isset($_COOKIE['searchSaveList']) && !empty($_COOKIE['searchSaveList']) ) ? json_decode(stripcslashes($_COOKIE['searchSaveList'])) : null;
$sc_con = ( is_null($sc) ) ? 0 : count($sc);
?>
<?php get_header(); ?>


<!--<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_directory'); ?>/assets/css/page.css">-->


<div id="<?php echo $post->post_name;?>" class="p-mypage">
    <main id="main" class="site-main" role="main">

        <div class="o-fv __page mb-0">
            <img src="https://koukyu-chintai.com/wp-content/uploads/cropped-cropped-main-1-2000x1200-1-1024x614.jpg" alt="" class="o-fvImg" loading="lazy">
            <div class="o-fvCatch">
                <h2 class="o-fvCatch_title o-title __large __white __nobold">
                    <?php the_title(); ?>
                </h2>
            </div>
        </div>

        <section class="l-wrap __maxwsmall">

            <div class="p-mypageTab o-tab __basic01 mb-6">
                <a href="<?php bloginfo('url'); ?>/myhistory" class="p-mypageTab_item history  o-tabItem" ><i class="o-icon flaticon-history"></i>最近見た物件<span class="p-mypageTab_notification o-partsNotification stock __sizes"><?php echo stw_count_his();?></span></a>
                <a href="<?php bloginfo('url'); ?>/mylist" class="p-mypageTab_item save o-tabItem" class=""><i class="o-icon flaticon-love"></i>検討リスト<span class="p-mypageTab_notification o-partsNotification stock __sizes"><?php echo @get_mylist();?></span></a>
                <a class="p-mypageTab_item terms __active o-tabItem" ><i class="o-icon flaticon-download"></i>保存した検索条件<span class="p-mypageTab_notification o-partsNotification stock __sizes searchCount"><?php echo $sc_con;?></span></a>
            </div>

            <section class="p-postCont">
                <p class="u-text-ac"><?php the_content(); ?></p>
            </section>

            <section class="p-mypageCont __terms o-partsTerms __area l-cont __section js-tabItem" class="">

                <?php if ( !is_null($sc) ):?>
                    <?php foreach ( $sc as $ci => $onedata ):?>
                        <div class="o-partsTerms_item">
                            <div class="o-partsTerms_head flex-nw ai-center">
                                <p class="o-partsTerms_title"><?php echo $onedata->name;?></p>
                                <span class="o-partsTerms_date"><?php echo $onedata->date;?></span>
                                <a href="" class="o-partsTerms_delete" data-ci="<?php echo $ci; // 削除をjsで行う。JSONのキー?>">削除する</a>
                                <div>
                                    <a href="<?php echo $onedata->url;?>" class="o-partsTerms_searchbtn o-btn __basic02 __mid"><i class="o-icon flaticon-search"></i>この条件で検索する</a>
                                </div>
                            </div>
                            <!--
                            <div class="o-partsTerms_cont __white flex-nw ai-center">
                                <div class="o-partsTemrs_info">
                                    <div class="o-partsTerms_search1"><i class="o-partsTerms_search1Icon o-icon flaticon-location"></i><span class="o-partsTerms_search1Title">中野区00市</span></div>
                                    <div class="o-partsTerms_search2">
                                        <p class="o-partsTerms_search2Title">値段指定なし</p>
                                        <ul class="flex-w">
                                            <li class="o-partsTerms_search2Desc">1LDK</li>
                                        </ul>
                                    </div>
                                </div>
                                <div>
                                    <a href="" class="o-partsTerms_searchbtn o-btn __basic02 __mid"><i class="o-icon flaticon-search"></i>この条件で検索する</a>
                                </div>
                            </div>
                            -->
                        </div>
                    <?php endforeach;?>
                <?php else:?>
                   <p>保存された検索条件はありません。</p>
                <?php endif;?>
            </section>



        </section>



        <?php
// echo do_shortcode('[contact-form-7 id="18" title="コンタクトフォーム 1"]'); ?>


    </main><!-- #main -->
</div><!-- #about -->

<?php get_footer('top'); ?>
