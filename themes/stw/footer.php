<?php
// TOPオプションからひっぱる
 get_template_part('template/bukken', 'condition');
?>

<section class="l-contCta l-cont __section">
    <div class="l-wrap __postcont l-row flex-nw-no-lg">
        <div class="l-contCta_left l-rowLeft bigimg flex-nw">
            <img class="l-contCta_img" src="https://koukyu-chintai.com/wp-content/uploads/cropped-cropped-main-1-2000x1200-1-1024x614.jpg" alt="">
            <img class="l-contCta_logo" src="" alt="">
        </div>
        <section class="l-contCta_desc l-rowRight bigimg flex-nw jc-center ai-center">
            <div>
                <p class="l-contCta_catch o-title __min __accent01 __mb-3">プロにお任せ</p>
                <h3 class="l-contCta_title o-title __large mb-3">あなたの理想の物件を教えてください。<br>
                    私達がお探しいたします。</h3>
                <p class="l-contCta_text o-text __p mb-1">物件を探すのは一苦労です。<br>
                    お忙しいあなたの代わりに、プロが条件に合う物件をお探しするサービスです。</p>
                <ul class="o-list bar __min __nolink mb-3">
                    <li class="l-contCta_list">掘り出し物件に出会える可能性が高くなります。</li>
                    <li class="l-contCta_list">プロがぴったりの物件を提案いたします。</li>
                    <li class="l-contCta_list">新着物件を優先的にご紹介。</li>
                </ul>


                <a href="" class="l-contCta_btn o-btn __basic02 __mid">条件を指定してプロに任せる</a>
            </div>
        </section>
    </div>
</section>

<?php
// TOPオプションからひっぱる
 get_template_part('template/bukken', 'popular');
?>


<?php
// 最近見た物件　ウィジェットから。
 get_template_part('template/bukken', 'recent');
?>


<?php
// 新着情報からひっぱる
 get_template_part('template/bukken', 'post');
?>





<section class="l-contCompany">
    <div class="l-wrap __postcont l-row flex-nw-md">
        <div class="l-contCompany_left l-rowLeft bigimg flex-nw">
            <img class="l-contCompany_img" src="https://koukyu-chintai.com/wp-content/uploads/cropped-cropped-main-1-2000x1200-1-1024x614.jpg" alt="">
            <img class="l-contCompany_logo" src="" alt="">
        </div>
        <section class="l-contCompany_desc l-rowRight bigimg flex-nw jc-center ai-center">
            <div>
                <h3 class="l-contCompany_title o-title __large mb-3">厳選した高級物件をあなたに</h3>
                <p class="l-contCompany_text o-text __p mb-1">高級賃貸東京は、一般的な不動産屋さんのように、「店舗で一緒に物件を探す」ことを基本的には行いません。</p>

                <p class="l-contCompany_text o-text __p mb-1">なぜかと言うと、店舗での物件探しでは、対面になる分、お客様がご自身で検討できる時間が限られてしまうからです。</p>

                <p class="l-contCompany_text o-text __p mb-1">弊社では、メールなどで希望に沿った物件情報を複数お送りいたしますので、お客様のペースでじっくり比較・検討いただいた上で内覧・お申し込みをしていただくことができます。

                    <p class="l-contCompany_text o-text __p mb-3">このスタイルで営業しているため、店舗での物件紹介は原則しておりません。</p>
                    <a href="" class="l-contCompany_link o-link __mid __text type01">高級賃貸東京が選ばれる理由<span class="o-linkItem"></span></a>
            </div>
        </section>
    </div>
</section>

<section class="l-contSppliers mb-6">
    <div class="l-wrap flex-nw">
        <h3 class="l-contSppliers_title o-title __p">取引先企業様</h3>
        <div class="l-contSppliers_list flex-nw ai-center">
            <div class="l-contSppliers_item"><img src="https://koukyu-chintai.com/wp-content/uploads/cropped-cropped-main-1-2000x1200-1-1024x614.jpg" alt="" class="l-contSppliers_logo"></div>
        </div>
    </div>
</section>
<footer class="l-footer">
    <div class="l-footerWrap l-wrap __maxw02">
        <div class="l-footerNav flex-w jc-between">
            <div class="flex-nw-md jc-between">
                <nav class="l-footerNav_cont service">
                    <h4 class="l-footerNav_title o-title __title __en">SERVICE</h4>
                    <?php
                wp_nav_menu( array(
                    'theme_location' => 'footer_service',
                    'container' => 'false',  //コンテナのデフォルト <div> を <nav> に変更
                    'menu_class' => 'l-footerNav_list o-list type01',  //デフォルトの menu-{メニュー名} を変更
                ) );
                ?>
                </nav>
                <nav class="l-footerNav_cont contact">
                    <h4 class="l-footerNav_title o-title __title __en">CONTACT</h4>
                    <?php
                wp_nav_menu( array(
                    'theme_location' => 'footer_contact',
                    'container' => 'false',  //コンテナのデフォルト <div> を <nav> に変更
                    'menu_class' => 'l-footerNav_list o-list __mid type01',  //デフォルトの menu-{メニュー名} を変更
                ) );
                ?>
                </nav>
                <nav class="l-footerNav_cont company">
                    <h4 class="l-footerNav_title o-title __title __en">COMPANY</h4>
                    <?php
                wp_nav_menu( array(
                    'theme_location' => 'footer_company',
                    'container' => 'false',  //コンテナのデフォルト <div> を <nav> に変更
                    'menu_class' => 'l-footerNav_list o-list type01',  //デフォルトの menu-{メニュー名} を変更
                ) );
                ?>
                </nav>
            </div>

            <div class="l-footerContact">
                <div class="l-footerContact_tel o-text __title __bold __en">0120-916-546<span class="l-footerContact_hour">祝休のぞく 10~18時</span></div>
                <a href="" class="l-footerContact_btn o-btn __basic01">お問い合わせ</a>
            </div>



        </div>
        <div class="l-footerSub flex-nw jc-between ai-center">
            <p class="l-footerSub_copy">©2019 高級賃貸東京 All rights reserved.</p>
            <div class="flex-nw">
                <a class="l-footerSub_sns fb" href="" target="_blank" rel="nofollow"><i class="fab fa-facebook-square"></i>
                </a>
                <a class="l-footerSub_sns tw" href="" target="_blank" rel="nofollow"><i class="fab fa-twitter"></i></a>
            </div>
        </div>
    </div>
</footer>




</div><!-- .site-content-contain -->

</div><!-- #page -->





<?php wp_footer(); ?>
<?php get_template_part('template/modal-searchform');?>
<script src="<?php bloginfo('template_directory'); ?>/assets/js/swiper.min.js"></script>


<!--スマホメニュー-->
<script>

    (function($) {
        $(function() {
            var $header = $('#sp-header');
            // Nav Fixed
            $(window).scroll(function() {
                if ($(window).scrollTop() > 350) {
                    $header.addClass('fixed');
                } else {
                    $header.removeClass('fixed');
                }
            });
            // Nav Toggle Button
            $('#js-nav').click(function() {
                this.toggleClass('open');
            });

            $('.l-headerBtn').click(function() {
                $('.l-headerBtn_bar').toggleClass('open');
            });
        });

    })(jQuery);
</script>
<script>
    //基本的なカルーセルタイプ
    var swiper = new Swiper('.__carouselType01', {
        slidesPerView: 1,
        spaceBetween: 10,
        // init: false,
        pagination: {
            el: '.swiper-pagination',
            clickable: true,
        },
        breakpoints: {
            640: {
                slidesPerView: 1,
                spaceBetween: 20,
            },
            768: {
                slidesPerView: 1,
                spaceBetween: 20,
            },
            1024: {
                slidesPerView: 2,
                spaceBetween: 20,
            },
            1424: {
                slidesPerView: 3,
                spaceBetween: 20,
            },
            1860: {
                slidesPerView: 4,
                spaceBetween: 20,
            },
        }
    });

    //オススメ物件
    var swiper = new Swiper('.__carouselType02', {
        slidesPerView: 'auto',
        // init: false,
        pagination: {
            el: '.swiper-pagination',
            clickable: true,
        },
        breakpoints: {
            640: {
                slidesPerView: 2,
            },
            768: {
                slidesPerView: 1,
            },
        }
    });
</script>
<script>
    window.WebFontConfig = {
        google: {
            families: ['Lato:400,700,900']
        },
        active: function() {
            sessionStorage.fonts = true;
        }
    };
    (function() {
        var wf = document.createElement('script');
        wf.src = 'https://ajax.googleapis.com/ajax/libs/webfont/1.6.26/webfont.js';
        wf.type = 'text/javascript';
        wf.async = 'true';
        var s = document.getElementsByTagName('script')[0];
        s.parentNode.insertBefore(wf, s);
    })();
</script>
</body>

</html>
