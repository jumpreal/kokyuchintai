<?php

$connected_parent = get_posts($args);
// print_r($connected_parent);
$bukken_mansitu_fg = get_field('bukken_status', $parent_post_ID);

if(count($connected_parent)>0){
    foreach($connected_parent as $ck=>$cv){
        $room_post_ID = $cv->ID;
        $room_meta=get_post_meta($room_post_ID);
        $str=$room_meta["kakaku"][0];
        $num = preg_replace('/[^0-9]/', '',$str);
        $tani=preg_replace('/'.$num.'/','',$str);
        $sr = get_field('kakakushikikin', $room_post_ID) ."/".get_field('kakakureikin', $room_post_ID);
        $kanrihi = (get_field('management_fee', $room_post_ID) ? number_format(get_field('management_fee', $room_post_ID)) . "円" : "なし" );
        $link=wpfp_link_id($room_post_ID);

        if($post_id !== $room_post_ID){//自分自身を覗いて表示
            echo '<table class="rooms">';
            echo '<tr><th>部屋</th>';
                echo '<td>'.get_field('room_floor', $room_post_ID).'階</td>';
                echo '<th>間取</th><td>'.xls_custom_madorisu_print($room_post_ID).'</td>';
            echo '</tr>';
            echo '<tr>';
                echo '<th>専有面積</th><td>'.get_field('senyumenseki', $room_post_ID).'㎡</td>';
                echo '<th>賃料</th><td>'.get_mansitu_text($room_post_ID, $bukken_mansitu_fg, number_unit($num).$tani.'円', '現在募集はございません').'</td>';
            echo '</tr>';
            echo '<tr>';
                echo '<th>管理費</th><td>'.get_mansitu_text($room_post_ID, $bukken_mansitu_fg, $kanrihi, '-').'</td>';
                echo '<th>敷金/礼金</th><td>'. get_mansitu_text($room_post_ID, $bukken_mansitu_fg, $sr, '-').'</td>';
            echo '</tr>';
            echo '<tr>';
                echo '<th>入居可能</th><td>'. get_mansitu_text($room_post_ID, $bukken_mansitu_fg, get_xls_custom_nyukyogenkyo_print($room_post_ID), '-').'</td>';
                echo '<th>詳細</th><td><a href="'.get_permalink($room_post_ID).'">詳細</a></td>';
            echo '</tr>';
            echo '<tr>';
                echo '<th>お問合せ</th><td><a href="'.get_permalink($room_post_ID).'#toiawasesaki">お問合せ</a></td>';
                echo '<th>ﾏｲﾘｽﾄ</th><td>'.$link.'</td>';
            echo '</tr>';
            echo '</table>';
        }
    }
}			