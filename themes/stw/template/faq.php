<?php if( have_rows('qanda', 'option') ): ?>
<h2 class="o-rsingleCont_title flex-nw ai-center">よくあるご質問<span class="o-rsingleCont_en">FAQ</span></h2>
<div class="o-rsingleFaq_cont o-rsingleCont_wrap">



    <?php while( have_rows('qanda', 'option') ): the_row(); ?>

    <div class="o-rsingleFaq_item">
        <h3 class="o-rsingleFaq_question o-title __mid flex-nw ai-center" id="js-accordion"><?php the_sub_field('question');?><i class="o-rsingleFaq_questionIcon o-icon fa fa-angle-down"></i></h3>
        <div class="o-rsingleFaq_answer">
            <h4 class="o-rsingleFaq_answerTitle o-title __mid"><?php the_sub_field('answer_title');?></h4>
            <p class="o-rsingleFaq_answerText o-text __mid"><?php the_sub_field('answer_honbun');?></p>
        </div>
    </div>

    <?php endwhile; ?>



</div>
<?php else:?>
<?php endif; ?>
