<! ---------------------------------
全体 - 検索モーダル
---------------------------------- >

<div class="o-modal __search search_modal" id="searchModal">
    <div class="o-modalBg modal__bg js-modal-close"></div>
    <a href="" class="o-modalClose js-modal-close"><span class="o-modalClose_bar"></span><span class="o-modalClose_bar"></span></a>

    <div class="o-modalOverwrap">


        <div class="o-modalWrap o-search modal modal__content">
            <header class="o-modalHead flex-nw-no-sm jc-between ai-center">
                <h3 class="o-title __title mb-1">条件を絞って探す</h3>
                <?php
                $sc = ( isset($_COOKIE['searchSaveList']) && !empty($_COOKIE['searchSaveList']) ) ? json_decode(stripcslashes($_COOKIE['searchSaveList'])) : null;
                $sc_con = ( is_null($sc) ) ? 0 : count($sc);
                ?>
                <a id="js-saveModal" class="o-modalHead_save o-btn __min __sizem __basic07"><i class="o-icon flaticon-download"></i>保存した条件から探す(<span class="searchCount">
                        <?php echo $sc_con;?></span>件)</a>
            </header>

            <div class="o-modalCont">
                <!-- <h4 class="o-modalSteptitle"> -->
                <h4 class="o-modalSteptitles" style="align-content: center;padding: 2rem 8rem;margin-left: -8rem;margin-right: -8rem;font-size: 1.3rem;">エリア・沿線を選択</h4>
                <div class="o-searchArea">
                    <div class="flex-nw">
                        <a id="js-areaModal" class="o-searchArea_item"><i class="o-icon o-searchArea_itemIcon flaticon-location"></i>エリア</a>
                        <a id="js-stationModal" class="o-searchArea_item"><i class="o-icon o-searchArea_itemIcon flaticon-train"></i>沿線・駅</a>
                        <!--<a href="" class="o-searchArea_item"><i class="o-icon o-searchArea_itemIcon flaticon-map"></i>地図</a>-->
                    </div>
                    <div id="selectedSearch" class="o-searchInfo_selected __area mt-1">
                        <?php
                    $get_ksiks = array();
                    $ksik = ( isset($_GET['ksik']) && !empty($_GET['ksik']) ) ? $_GET['ksik'] : null;
                    if ( !is_null($ksik) ){
                        $get_ksiks = str_getareaname($ksik);
                    }
                    $get_ekis = array();
                    $eki = ( isset($_GET['eki']) && !empty($_GET['eki']) ) ? $_GET['eki'] : null;
                    if ( !is_null($eki) ){
                        $get_ekis = stw_getekiname($eki);
                    }
                    ?>
                        <?php foreach ( $get_ksiks as $ogk ):?>
                        <span class="o-searchInfo_selectedItem d-area">
                            <?php echo $ogk['narrow_area_name'];?></span>
                        <?php endforeach;?>
                        <?php foreach ( $get_ekis as $oge ):?>
                        <span class="o-searchInfo_selectedItem d-tation">
                            <?php echo $oge['roseneki'];?></span>
                        <?php endforeach;?>
                        <!--  <li class="o-searchArea_selectedItem">都営浅草線 高輪台駅</li>-->
                    </div>
                </div>
            </div>
            <?php
// $field = get_field_object('colors');
// $value = $field['value'];
// $label = $field['choices'][ $value ]; // ラベルを取得
// echo $label; // ラベルを表示
?>
            <div class="o-modalCont">
                <!--  <h4 class="o-modalSteptitle">-->
                <h4 class="o-modalSteptitles" style="align-content: center;padding: 2rem 8rem;margin-left: -8rem;margin-right: -8rem;font-size: 1.3rem;">探したい賃貸の条件を指定</h4>
                <div class="o-searchMoney o-modalBox">
                    <h5 class="o-modalSubtitle">賃料<span class="o-modalSubtitle_unit">(万円)</span></h5>
                    <?php $kalc=(isset($_GET['kalc'])&&!empty($_GET['kalc']))?$_GET['kalc']:'0';$kahc=(isset($_GET['kahc'])&&!empty($_GET['kahc']))?$_GET['kahc']:'200';?>
                    <input type="hidden" class="o-searchMoney_slider __nowidth range-slider" id="mdlChinryoSlider" value="<?php echo $kalc.','.$kahc;?>" style="display:none;" />
                    <form class="o-searchMoney_select mb-3 flex-nw o-formCheckbtn" action="">

                        <?php
                            // 検索条件を結果画面で反映させる処理
                            // 設備のGETパラメータ取得。
                            $set_s =  ( isset($_GET['set']) && !empty($_GET['set']) ) ? $_GET['set'] : null;
                            // setの値をキー値にした配列に再構成
                            $sumi_sets = array();
                            if ( is_array($set_s) ){
                                foreach ( $set_s as $one ){
                                    $sumi_sets[$one] = 1;
                                }
                            } else {
                                $sumi_sets[$set_s] = 1;
                            }

                            if( have_rows('サーチフォーム上設備', 'option') ):

                                $i = 1;

                                while( have_rows('サーチフォーム上設備', 'option') ): the_row();

                                    //name属性を設定
                                    if(get_sub_field('共用or部屋', 'option') == '共用') {
                                        $setubi_name = 'set2';
                                    } else {
                                        $setubi_name = 'set3';
                                    }

                                    //値を取得
                                    if(get_sub_field('共用or部屋', 'option') == '共用') {
                                        if(get_sub_field('共用設備', 'option') !== '選択してください'){
                                            $field = get_sub_field_object('共用設備', 'option');
                                            // print_r($field);
                                            $setubi_value = $field['value'];
                                            $setubi_label = $field['choices'][$setubi_value];
                                        }else{

                                        }
                                    }else{
                                        if(get_sub_field('共用設備', 'option') !== '選択してください'){
                                            $field = get_sub_field_object('部屋設備', 'option');
                                            $setubi_value = $field['value'];
                                            $setubi_label = $field['choices'][$setubi_value];
                                        }else{

                                        }
                                    }

                                    // 検索パラメータに存在する場合はcheckedにする
                                    $checked = ( isset($sumi_sets[$setubi_value]) ) ? 'checked' : '';
                                    ?>

                        <input name="<?php echo $setubi_name;?>" value="<?php echo $setubi_value; ?>" class="o-formCheckbtn_input" id="toggle<?php echo $i;?>" type="checkbox" <?php echo $checked;?>>
                        <label class="o-formCheckbtn_label o-searchMoney_label" for="toggle<?php echo $i;?>">
                            <?php echo $setubi_label; ?>
                        </label>

                        <?php

                                    $i++;

                                endwhile;

                            endif;



                            ?>

                    </form>
                </div>
                <div class="o-modalBox flex-nw-no-md l-row">
                    <div class="o-modalBox_cont o-formSize l-rowLeft rentsearch">
                        <h5 class="o-modalSubtitle">面積</h5>
                        <div class="flex-nw ai-center">
                            <div class="o-formSelect_wrap">
                                <?php $mel=(isset($_GET['mel'])&&!empty($_GET['mel']))?$_GET['mel']:'0';?>
                                <select class="o-formSelect" name="mel" id="mdlMensekiMin">
                                    <?php $mel_value = array('0','10','15','20','25','30','35','40','50','60','70','80','90','100','200','300','400','500','600','700','800','900','1000');?>
                                    <?php foreach ( $mel_value as $one ):?>
                                    <option value="<?php echo $one;?>" <?php echo ($one==$mel) ? 'selected' :''; ?>>
                                        <?php echo ( '0' == $one ) ? '下限なし' : $one.'m²';?>
                                    </option>
                                    <?php endforeach;?>
                                </select>
                            </div>
                            〜
                            <div class="o-formSelect_wrap">
                                <?php $meh=(isset($_GET['meh'])&&!empty($_GET['meh']))?$_GET['meh']:'0';?>
                                <select class="o-formSelect" name="meh" id="mdlMensekiMax">
                                    <?php $meh_value = array('10','15','20','25','30','35','40','50','60','70','80','90','100','200','300','400','500','600','700','800','900','1000','0');?>
                                    <?php foreach ( $meh_value as $one ):?>
                                    <option value="<?php echo $one;?>" <?php echo ($one==$meh) ? 'selected' :''; ?>>
                                        <?php echo ( '0' == $one ) ? '上限なし' : $one.'m²';?>
                                    </option>
                                    <?php endforeach;?>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="o-modalBox_cont l-rowRight rentsearch">
                        <div class="flex-nw l-row">

                            <div class="o-formSize l-rowLeft rentsearch2">
                                <h5 class="o-modalSubtitle">駅徒歩</h5>

                                <div class="o-formSelect_wrap">
                                    <?php $hof=(isset($_GET['hof'])&&!empty($_GET['hof']))?$_GET['hof']:'0';?>
                                    <select class="o-formSelect" name="eki" id="mdlHof">
                                        <?php $hof_value = array('0','1','3','5','10','15');?>
                                        <?php foreach ( $hof_value as $one ):?>
                                        <option value="<?php echo $one;?>" <?php echo ($one==$hof) ? 'selected' : '' ;?>>
                                            <?php echo ('0' == $one ) ? '指定なし': $one . '分以内'; ?>
                                        </option>
                                        <?php endforeach;?>
                                    </select>
                                </div>
                            </div>
                            <div class="l-rowRight rentsearch2">

                                <h5 class="o-modalSubtitle">入居可能日</h5>

                                <div class="o-formCheckbtn">
                                    <?php $nyukyo=(isset($_GET['nyukyo'])&&!empty($_GET['nyukyo']))?$_GET['nyukyo']:'0';?>
                                    <input class="o-formCheckbtn_input" id="mdlNyukyo" value="2" type="checkbox" <?php echo ( '2' == $nyukyo ) ? 'checked' : '';?>>
                                    <label class="o-formCheckbtn_label" for="mdlNyukyo">即入居可</label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="o-modalBox flex-nw-no-md l-row">
                    <div class="o-modalBox_cont o-searchRoom l-rowLeft rentsearch">
                        <h5 class="o-modalSubtitle">間取り</h5>
                        <form id="mdlMadori" class="o-searchRoom_select o-formCheckbtn flex-w" action="">
                            <?php
                        $mad_list = array(
                            array('id'=>'modalMadori110','value'=>'110','name'=>'1R'),
                            array('id'=>'modalMadori120','value'=>'120','name'=>'1K'),
                            array('id'=>'modalMadori130','value'=>'130','name'=>'1DK'),
                            array('id'=>'modalMadori150','value'=>'150','name'=>'1LDK'),
                            array('id'=>'modalMadori155','value'=>'155','name'=>'1SLDK'),
                            array('id'=>'modalMadori230','value'=>'230','name'=>'2DK'),
                            array('id'=>'modalMadori250','value'=>'250','name'=>'2LDK'),
                            array('id'=>'modalMadori255','value'=>'255','name'=>'2SLDK'),
                            array('id'=>'modalMadori350','value'=>'350','name'=>'3LDK'),
                            array('id'=>'modalMadori355','value'=>'355','name'=>'3SLDK'),
                            array('id'=>'modalMadori450','value'=>'450','name'=>'4LDK'),
                            array('id'=>'modalMadori455','value'=>'455','name'=>'4SLDK'),
                            array('id'=>'modalMadori550','value'=>'550','name'=>'5LDK'),
                            array('id'=>'modalMadori555','value'=>'555','name'=>'5SLDK')
                        );
                        $mad = ( isset($_GET['mad']) && !empty($_GET['mad']) ) ? $_GET['mad'] : '0';
                        ?>
                            <?php foreach ( $mad_list as $one ):?>
                            <?php
                            $mad_chkd = '';
                            if ( is_array($mad) ){
                                foreach ( $mad as $mado ){
                                    if ( $one['value'] == $mado ) $mad_chkd = 'checked';
                                }
                            } else {
                                if ( $one['value'] == $mad ) $mad_chkd = 'checked';
                            }
                            ?>
                            <input class="o-formCheckbtn_input" id="<?php echo $one['id'];?>" name="madori" type="checkbox" value="<?php echo $one['value'];?>" <?php echo $mad_chkd;?>>
                            <label class="o-formCheckbtn_label  o-searchRoom_label" for="<?php echo $one['id'];?>">
                                <?php echo $one['name'];?></label>
                            <?php endforeach; ?>

                        </form>

                    </div>
                    <div class="o-modalBox l-rowRight rentsearch">
                        <h5 class="o-modalSubtitle">設備</h5>

                        <a id="js-setsubiModal" class="o-modalChild_btn o-btn __basic11 mb-3">設備を選ぶ</a>

                        <h5 class="o-modalSubtitle">こだわり条件</h5>

                        <a id="js-kodawariModal" class="o-modalChild_btn o-btn __basic11">こだわり条件を選ぶ</a>
                    </div>
                </div>

            </div>

            <?php if ( have_rows('popular_search', 'option') ):?>
            <div class="o-modalCont">
                <!--  <h4 class="o-modalSteptitle">-->
                <h4 class="o-modalSteptitles" style="align-content: center;padding: 2rem 8rem;margin-left: -8rem;margin-right: -8rem;font-size: 1.3rem;">他にもこのような検索条件から選ばれてます</h4>
                <div class="o-searchTerms">
                    <form id="mdlPopularSearch" class="o-searchTerms_select o-formChecklist flex-w" action="">
                        <ul>
                            <?php $popcon=0; while ( have_rows('popular_search', 'option') ): the_row();?>
                            <!--
                        <input class="o-formChecklist_input" name="popular" id="popser<?php echo $popcon;?>" type="radio" value="<?php echo get_sub_field('display_popular_search_link');?>">
                        <label class="o-formChecklist_label" for="popser<?php echo $popcon;?>"><?php the_sub_field('display_popular_search_name'); ?></label>
                        -->
                            <li><a href="<?php echo get_sub_field('display_popular_search_link');?>" target="_blank">
                                    <?php the_sub_field('display_popular_search_name'); ?></a></li>
                            <?php $popcon++; endwhile;?>
                        </ul>
                    </form>
                </div>
            </div>
            <?php endif;?>

            <div class="o-modalFixed">

                <div class="o-searchBtn o-modalBtn">
                    <div class="o-searchBtn_wrap o-modalBtn_wrap flex-nw-no-md jc-center">
                        <a href="" class="o-modalBtn_search o-searchBtn_search o-btn __basic02 __p search searchRun" data-searchform="mdl"><i class="o-icon flaticon-search"></i>この条件で検索する</a>
                        <div class="o-searchBtn_function o-modalBtn_subwrap flex-w jc-center ai-center">
                            <a href="" class="o-searchBtn_save o-btn __min __basic03 __sizes modalSearchSave" data-searchform="mdl"><i class="o-icon flaticon-download"></i>検索条件を保存</a>
                            <a href="" class="o-searchBtn_reset o-btn __min __basic04 __sizem">条件をリセット</a>
                        </div>
                    </div>
                </div>
                <!--<div class="o-searchKeyword o-modalKeyword">
                    <div class="o-searchKeyword_wrap o-modalKeyword_wrap flex-nw jc-between ai-center">
                        <h4 class="o-modalKeyword_title o-title">キーワード検索</h4>
                        <?php include(dirname(__FILE__).'/../searchform.php' ); ?>
                    </div>
                </div>-->


            </div>


        </div>
        <!--modal__content-->
    </div>
</div>
<!--search_modal-->

<! ---------------------------------
検索 - エリア選択
---------------------------------- >
<div class="o-modal __search __child search_modal" id="areaModal">
    <div class="o-modalBg modal__bg js-areamodal-close"></div>
    <a href="" class="o-modalClose js-areamodal-close"><span class="o-modalClose_bar"></span><span class="o-modalClose_bar"></span></a>

    <div class="o-modalOverwrap">
        <div class="o-modalWrap o-search modal modal__content">
            <header class="o-modalHead flex-nw-md jc-between ai-center">
                <h3 class="o-title __title">エリア検索</h3>
            </header>

            <div class="o-modalCont">
                <h4 class="o-modalTitle">東京都</h4>
                <form class="o-formCheckbtn flex-w flex-column3-md-2" id="areaForm" action="">
                    <!-- jsによって以下の形式でdomが追加される。
                    <input class="o-formCheckbtn_input" id="area103" name="area" type="checkbox" checked>
                    <label class="o-modalChild_label o-formCheckbtn_label " for="area103">港区</label>
                 -->
                </form>
            </div>
        </div>


    </div>
    <div class="o-modalFixed">
        <div class="o-modalBtn">

            <div class="o-searchBtn_wrap o-modalBtn_wrap flex-nw-no-md jc-center">
                <a id="areaRun" class="o-modalChild_btnSelect o-btn __basic02 __title search">このエリアで決定する</a>
            </div>
        </div>

    </div>
</div>
<!--search_modal-->


<! ---------------------------------
検索 - 沿線・駅 選択
---------------------------------- >
<div class="o-modal __search __child search_modal" id="stationModal">
    <div class="o-modalBg modal__bg js-stationmodal-close"></div>
    <a href="" class="o-modalClose js-stationmodal-close"><span class="o-modalClose_bar"></span><span class="o-modalClose_bar"></span></a>

    <div class="o-modalOverwrap">
        <div class="o-modalWrap o-search modal modal__content">
            <header class="o-modalHead flex-nw-md jc-between ai-center">
                <h3 class="o-title __title">沿線・駅 選択</h3>
            </header>

            <div id="rosEkis">
                <!--  input dom for ajax
                <h4 class="o-modalTitle">総武線</h4>
                    <input class="o-formCheckbtn_input" id="station1" type="checkbox" checked>
                    <label class="o-modalChild_label o-formCheckbtn_label " for="station1">~~~~駅</label>
            -->
            </div>

        </div>
    </div>
    <div class="o-modalFixed">

        <div class="o-modalBtn">
            <div class="o-searchBtn_wrap o-modalBtn_wrap flex-nw-no-md jc-center">
                <a id="stationRun" class="o-modalChild_btnSelect o-btn __basic02 __title search">この沿線・駅で決定する</a>
            </div>
        </div>
    </div>
</div>
<!--search_modal-->


<! ---------------------------------
 検索子モーダル - 設備
---------------------------------- >
<div class="o-modal __search __child search_modal" id="setsubiModal">
    <div class="o-modalBg modal__bg js-setsubimodal-close"></div>
    <a href="" class="o-modalClose js-setsubimodal-close"><span class="o-modalClose_bar"></span><span class="o-modalClose_bar"></span></a>

    <div class="o-modalOverwrap">
        <div class="o-modalWrap o-search modal modal__content">
            <header class="o-modalHead flex-nw-md jc-between ai-center">
                <h3 class="o-title __title">設備を絞る</h3>
            </header>

            <div class="o-modalCont">
                <h4 class="o-modalTitle">共用設備</h4>
                <form class="o-formCheckbtn flex-w flex-column3-md-2" action="">
                    <?php global $work_setsubi2, $work_setsubi3;?>
                    <?php
                    $sets_arr = array();
                    $set = ( isset($_GET['set']) && !empty($_GET['set']) ) ? $_GET['set'] : '0';
                    // 検索高速化の為にindexに値を入れた形に変形
                    if ( is_array($set) ){
                        foreach ( $set as $one_set ){
                            $sets_arr[$one_set] = 1;
                        }
                    } else {
                        $sets_arr[$set] = 1;
                    }
                    $sets2_arr = array();
                    $set2 = ( isset($_GET['set2']) && !empty($_GET['set2']) ) ? $_GET['set2'] : '0';
                    // 検索高速化の為にindexに値を入れた形に変形
                    if ( is_array($set2) ){
                        foreach ( $set2 as $one_set2 ){
                            $sets2_arr[$one_set2] = 1;
                        }
                    } else {
                        $sets_arr2[$set2] = 1;
                    }
                    ?>

                    <?php foreach($work_setsubi2 as $i => $ws2o ):?>
                    <input class="o-formCheckbtn_input" id="kyoyosetsubi<?php echo $i;?>" name="setubi" type="checkbox" value="<?php echo $ws2o['code'];?>" <?php echo (isset($sets_arr[$ws2o['code']]))?'checked':'';?>>
                    <label class="o-modalChild_label o-formCheckbtn_label flex-item" for="kyoyosetsubi<?php echo $i;?>">
                        <?php echo $ws2o['name'];?></label>
                    <?php endforeach;?>
                </form>
            </div>

            <div class="o-modalCont">
                <h4 class="o-modalTitle">部屋設備</h4>
                <form class="o-formCheckbtn flex-w flex-column3-md-2" action="">
                    <?php foreach($work_setsubi3 as $i => $ws3o ):?>
                    <input class="o-formCheckbtn_input" id="heyasetsubi<?php echo $i;?>" name="setubi2" type="checkbox" value="<?php echo $ws3o['code'];?>" <?php echo (isset($sets2_arr[$ws3o['code']]))?'checked':'';?>>
                    <label class="o-modalChild_label o-formCheckbtn_label flex-item" for="heyasetsubi<?php echo $i;?>">
                        <?php echo $ws3o['name'];?></label>
                    <?php endforeach;?>
                </form>
            </div>




        </div>
    </div>
    <div class="o-modalFixed">

        <div class="o-modalBtn">
            <div class="o-searchBtn_wrap o-modalBtn_wrap flex-nw-no-md jc-center">
                <a id="" class="o-modalChild_btnSelect o-btn __basic02 __title search setubi_run">この設備で決定する</a>
            </div>
        </div>
    </div>
</div>
<!--search_modal-->

<! ---------------------------------
検索子モーダル - こだわり条件
---------------------------------- >
<div class="o-modal __search __child search_modal" id="kodawariModal">
    <div class="o-modalBg modal__bg js-kodawarimodal-close"></div>
    <a href="" class="o-modalClose js-kodawarimodal-close"><span class="o-modalClose_bar"></span><span class="o-modalClose_bar"></span></a>

    <div class="o-modalOverwrap">
        <div class="o-modalWrap o-search modal modal__content">
            <header class="o-modalHead flex-nw-md jc-between ai-center">
                <h3 class="o-title __title">こだわり条件</h3>
            </header>

            <div class="o-modalCont">
                <h4 class="o-modalTitle">こだわり条件</h4>
                <form class="o-formCheckbtn flex-w flex-column3-md-2" action="">
                    <?php global $work_setsubi4;?>
                    <?php
                    $koda_arr = array();
                    $koda = ( isset($_GET['koda']) && !empty($_GET['koda']) ) ? $_GET['koda'] : '0';
                    // 検索高速化の為にindexに値を入れた形に変形
                    if ( is_array($koda) ){
                        foreach ( $koda as $one_koda ){
                            $koda_arr[$one_koda] = 1;
                        }
                    } else {
                        $koda_arr[$koda] = 1;
                    }
                    ?>
                    <?php foreach ( $work_setsubi4 as $i => $ws4o ):?>
                    <input class="o-formCheckbtn_input" id="kodawari<?php echo $i;?>" name="kodawari" type="checkbox" value="<?php echo $ws4o['code'];?>" <?php echo (isset($koda_arr[$ws4o['code']]))?'checked':'';?>>
                    <label class="o-modalChild_label o-formCheckbtn_label flex-item" for="kodawari<?php echo $i;?>">
                        <?php echo $ws4o['name'];?></label>
                    <?php endforeach;?>
                </form>
            </div>



        </div>
    </div>
    <div class="o-modalFixed">

        <div class="o-modalBtn">
            <div class="o-searchBtn_wrap o-modalBtn_wrap flex-nw-no-md jc-center">
                <a id="" class="o-modalChild_btnSelect o-btn __basic02 __title search kodawari_run">この条件で決定する</a>
            </div>
        </div>
    </div>
</div>
<!--search_modal-->


<! ---------------------------------
検索子モーダル - 検索条件を保存
---------------------------------- >
<div class="o-modal __small search_modal" id="searchSaveModal">
    <div class="o-modalBg modal__bg js-searchsavemodal-close"></div>
    <a href="" class="o-modalClose js-searchsavemodal-close"><span class="o-modalClose_bar"></span><span class="o-modalClose_bar"></span></a>

    <div class="o-modalOverwrap">
        <div class="o-modalWrap o-search modal modal__content">
            <header class="o-modalHead flex-nw-md jc-between ai-center">
                <h3 class="o-title __title">検索条件に名前をつけてを保存してください</h3>
            </header>

            <input type="text" class="o-formInput __type01 __sizel mb-3" id="searchSaveName" placeholder="こちらに名前を入力してください。 (例)豊洲の賃貸検索">
            <div class="o-partsTerms_item o-formSet">
                <p class="o-formSet_title">検索条件のURL</p>
                <p id="searchSaveURL"></p>
            </div>

        </div>
        <div class="o-modalBtn_wrap flex-nw-no-md jc-center">
            <a href="" class="o-modalChild_btnSelect o-btn __basic02 __sizel __p" id="searchSaveRun">この名前で検索条件を保存する</a>
        </div>
    </div>
</div>
<!--search_modal-->


<! ---------------------------------
検索子モーダル - 保存した条件から探す
---------------------------------- >
<?php
$sc_mdl = ( isset($_COOKIE['searchSaveList']) && !empty($_COOKIE['searchSaveList']) ) ? json_decode(stripcslashes($_COOKIE['searchSaveList'])) : null;
$sc_mdl_con = ( is_null($sc) ) ? 0 : count($sc);

?>
<div class="o-modal __search __child search_modal" id="saveModal">
    <div class="o-modalBg modal__bg js-savemodal-close"></div>
    <a href="" class="o-modalClose js-savemodal-close"><span class="o-modalClose_bar"></span><span class="o-modalClose_bar"></span></a>

    <div class="o-modalOverwrap">
        <div class="o-modalWrap o-search modal modal__content">
            <header class="o-modalHead flex-nw-md jc-between ai-center">
                <h3 class="o-title __title">保存した検索条件</h3>
            </header>

            <div class="o-modalCont">
                <?php if ( !is_null($sc_mdl) ):?>
                <?php foreach ( $sc_mdl as $ci => $onedata ):?>
                <section class="p-mypageTerms o-partsTerms __area l-cont __section">
                    <div class="o-partsTerms_item">
                        <div class="o-partsTerms_head flex-nw ai-center">
                            <p class="o-partsTerms_title">
                                <?php echo $onedata->name;?>
                            </p>
                            <span class="o-partsTerms_date">
                                <?php echo $onedata->date;?></span>
                            <a href="" class="o-partsTerms_delete" data-ci="<?php echo $ci;?>">削除する</a>
                        </div>
                        <div class="o-partsTerms_cont __gray __modal">
                            <div class="o-partsTerms_info mb-3">
                                <!--
                                <div class="o-partsTerms_search1"><i class="o-partsTerms_search1Icon o-icon flaticon-location"></i><span class="o-partsTerms_search1Title">中野区00市</span></div>
                                <div class="o-partsTerms_search2">
                                    <p class="o-partsTerms_search2Title">値段指定なし</p>
                                    <ul class="flex-w">
                                        <li class="o-partsTerms_search2Desc">1LDK</li>
                                    </ul>
                                </div>
                            </div>
                            -->
                                <div>
                                    <a href="<?php echo $onedata->url;?>" class="o-partsTerms_searchbtn o-btn __basic02 __mid"><i class="o-icon flaticon-search"></i>この条件で検索する</a>
                                </div>
                            </div>
                        </div>
                </section>
                <?php endforeach;?>
                <?php else:?>
                <p>保存した検索条件はありません。</p>
                <?php endif;?>
            </div>
        </div>
    </div>
    <!--search_modal-->
</div>

<! ---------------------------------
single-room.php - 部屋の画像モーダル
---------------------------------- >

<div class="o-modal __roomimg search_modal" id="roomimgModal">
    <div class="o-modalBg modal__bg js-roomimgmodal-close"></div>
    <a href="" class="o-modalClose js-roomimgmodal-close"><span class="o-modalClose_bar"></span><span class="o-modalClose_bar"></span></a>

    <div class="o-modalOverwrap">
        <div class="o-modalWrap o-search modal modal__content">
            <header class="o-modalHead flex-nw-md jc-between ai-center">
                <h3 class="o-title __title">部屋の画像一覧。</h3>
            </header>

            <div class="o-modalCont">
                <?php get_room_images(get_the_ID());?>
            </div>

        </div>
    </div>
</div>


<! ---------------------------------
single-room.php - ビルの画像モーダル
---------------------------------- >

<div class="o-modal __roomimg search_modal" id="fudoimgModal">
    <div class="o-modalBg modal__bg js-roomimgmodal-close"></div>
    <a href="" class="o-modalClose js-roomimgmodal-close"><span class="o-modalClose_bar"></span><span class="o-modalClose_bar"></span></a>

    <div class="o-modalOverwrap">
        <div class="o-modalWrap o-search modal modal__content">
            <header class="o-modalHead flex-nw-md jc-between ai-center">
                <h3 class="o-title __title">ビル物件の画像一覧</h3>
            </header>

            <div class="o-modalCont">
                <?php get_bukken_images(get_the_ID());?>
            </div>

        </div>
    </div>
</div>

<! ---------------------------------
single-room.php - 初期費用概算見積もり
---------------------------------- >
<?php
global $post;

$goukei_kingaku = 0;

$kakaku = get_field('kakaku', $post->ID);
$kakakukyouekihi = (get_field('kakakukyouekihi', $post->ID) !== '-') ? get_field('kakakukyouekihi', $post->ID) : 0;
$kakakushikikin = (int)str_replace(array("ヶ月分","原則賃料の","ヶ月"), "", get_field('kakakushikikin', $post->ID));
$kakakureikin = (float)str_replace("ヶ月", "", get_field('kakakureikin', $post->ID));
$change_key = get_field('change_key', $post->ID);
$tyuukai_tesuuryou = get_field('tyuukai_tesuuryou', $post->ID)=='無料' ? 0 : str_replace(array("無料", "ヶ月（税別）"), "", get_field('tyuukai_tesuuryou', $post->ID));


$goukei_kingaku += $kakaku;
$goukei_kingaku += $kakakukyouekihi;
$goukei_kingaku += $kakaku * $kakakushikikin;
$goukei_kingaku += $kakaku * $kakakureikin;
$goukei_kingaku += $kakaku * $tyuukai_tesuuryou * 1.10;
// $goukei_kingaku += $change_key;

?>
<div class="o-modal __estimate __child search_modal" id="estimateModal">
    <div class="o-modalBg modal__bg js-estimatemodal-close"></div>
    <a href="" class="o-modalClose js-estimatemodal-close"><span class="o-modalClose_bar"></span><span class="o-modalClose_bar"></span></a>

    <div class="o-modalOverwrap">
        <div class="o-modalWrap o-search modal modal__content">

            <div class="o-modalEstimate_price o-modalCont">
                <p class="o-modalEstimate_priceTitle o-title __mid __bold">初期費用概算見積もり</p>
                <p class="o-modalEstimate_priceNum o-title __en __bold __huge __accent01"><?php echo number_format($goukei_kingaku) ;?><span class="o-modalEstimate_priceNum_unit o-title __min">円（税込）〜</span></p>
            </div>

            <table class="o-table __p __sizem o-modalCont">
                <tr>
                    <th>家賃</th>
                    <td><?php echo number_format($kakaku);?>円</td>
                </tr>
                <tr>
                    <th>管理費</th>
                    <td><?php echo number_format($kakakukyouekihi);?>円</td>
                </tr>
                <tr>
                    <th>敷金（<?php echo get_field('kakakushikikin', $post->ID);?>）</th>
                    <td><?php echo number_format($kakaku * $kakakushikikin);?>円</td>
                </tr>
                <tr>
                    <th>礼金（<?php echo get_field('kakakureikin', $post->ID);?>）</th>
                    <td><?php echo number_format($kakaku * $kakakureikin);?>円</td>
                </tr>
                <tr>
                    <th>保証会社利用料</th>
                    <td>お問い合わせください</td>
                </tr>
                <tr>
                    <th>仲介手数料</th>
                    <td><?php echo number_format($kakaku * $tyuukai_tesuuryou * 1.10);?>円（税込）</td>
                </tr>
                <tr style="display:none;">
                    <th>鍵交換費用</th>
                    <td><?php echo number_format($change_key);?>円</td>
                </tr>
            </table>

            <div class="o-modalCont">
                <a id="" class="o-modalChild_btnSelect o-btn __basic08 __title search js-estimatemodal-close">閉じる</a>
            </div>

            <div class="o-modalEstimate_attention o-modalCont">
                <p class="o-modalEstimate_attentionItem o-text __min mb-1" style="">仲介手数料のみ消費税がかかるため税込表記になっています。</p>
                <p class="o-modalEstimate_attentionItem o-text __min " style="">その他、追加で費用が発生する可能性がございます。<br>（例：火災保険料、鍵交換費用、24時間サポート費用、退去時クリーニング費用 等）</p>


                <p class="o-modalEstimate_attentionItem o-text __min mb-1" style="display:none;">
                    ※その他、足し上げられていない項目がある場合がございます。<br>（例：保証会社費用、月額保証料、24時間サポート費用、町会費用、口座引落手数料、駐車場・駐輪場費用 等）
                </p>
                <p class="o-modalEstimate_attentionItem o-text __min " style="display:none;">※初期費用概算は、不動産会社が登録している情報を元に計算しています。<br>
                    実際とは異なる場合がございますので、詳細はお問い合わせください。</p>
            </div>


        </div>
    </div>
</div>
<!--search_modal-->





<div class="hide_form">
    <?php if(is_active_sidebar('sidebar-2')) : ?>
    <?php dynamic_sidebar('sidebar-2'); ?>
    <?php endif;?>
    <form id="modalSearchForm" action="<?php echo home_url();?>" method="GET">
        <input type="hidden" name="bukken" value="jsearch">
        <input type="hidden" name="shu" value="2">
        <input type="hidden" name="kalc" id="msKalc" value="0">
        <input type="hidden" name="kahc" id="msKahc" value="0">
        <input type="hidden" name="hof" id="msHof" value="0">
        <input type="hidden" name="mel" id="msMel" value="0">
        <input type="hidden" name="meh" id="msMeh" value="0">
        <input type="hidden" name="ken" id="msKen" value="13">
        <div id="msSik">
            <?php $ksik = ( isset($_GET['ksik']) && !empty($_GET['ksik']) ) ? $_GET['ksik'] : '0';?>
            <?php if ( is_array($ksik) ):?>
            <?php foreach ( $ksik as $ms_sikone):?>
            <input class="hd-area" type="hidden" name="ksik[]" value="<?php echo $ms_sikone;?>">
            <?php endforeach;?>
            <?php elseif ( 0 != $ksik):?>
            <input class="hd-area" type="hidden" name="ksik[]" value="<?php echo $ksik;?>">
            <?php endif;?>
            <!--
        <input type="hidden" name="ksik[]" id="msSik" value="0">
        -->
        </div>
        <div id="msEki">
            <?php $eki = ( isset($_GET['eki']) && !empty($_GET['eki']) ) ? $_GET['eki'] : '0';?>
            <?php if ( is_array($eki) ):?>
            <?php foreach ( $eki as $ms_ekione):?>
            <input class="hd-eki" type="hidden" name="eki[]" value="<?php echo $ms_ekione;?>">
            <?php endforeach;?>
            <?php elseif ( 0 != $eki):?>
            <input class="hd-eki" type="hidden" name="eki[]" value="<?php echo $eki;?>">
            <?php endif;?>
            <!--  <input class="hd-eki" type="hidden" name="eki[]" value="0"> -->
        </div>
        <input type="hidden" name="nyukyo" id="msNyukyo" value="0">
        <div id="msSet">
            <!--  <input type="hidden" name="set[]" value="0"> -->
        </div>
        <div id="msSet2">
            <!--  <input type="hidden" name="set2[]" value="0"> -->
        </div>
        <div id="msKoda">
            <?php if ( is_array($koda) ):?>
            <?php foreach ( $koda as $ms_kodaone):?>
            <input class="hd-kodawari" type="hidden" name="koda[]" value="<?php echo $ms_kodaone;?>">
            <?php endforeach;?>
            <?php elseif ( 0 != $koda):?>
            <input class="hd-kodawari" type="hidden" name="koda[]" value="<?php echo $koda;?>">
            <?php endif;?>
            <!--  <input type="hidden" name="koda[]" value="0"> -->
        </div>
        <div id="msMad">
            <!--  <input type="hidden" name="mad[]" value="0"> -->
        </div>
    </form>
</div>

<script src="<?php echo get_template_directory_uri();?>/assets/js/jquery.range-min.js"></script>
<script src="<?php echo get_template_directory_uri();?>/assets/js/modal-search.js"></script>
<script src="<?php echo get_template_directory_uri();?>/assets/js/jquery.cookie.js"></script>
