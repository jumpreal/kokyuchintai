<?php
$sc = ( isset($_COOKIE['searchSaveList']) && !empty($_COOKIE['searchSaveList']) ) ? json_decode(stripcslashes($_COOKIE['searchSaveList'])) : null;
$sc_con = ( is_null($sc) ) ? 0 : count($sc);
?>
<div class="l-sideSearch_wrap">
               <a href="<?php echo get_page_link(910);?>" class="o-searchSave"><i class="o-icon __min flaticon-check-mark"></i>保存した条件から探す(<span class="searchCount"><?php echo $sc_con;?></span>)</a>
                    <!--1124px までのpcで表示-->
                    <div class="l-sideSearch_cont __pc o-searchCont l-wrap">
                        <div class="o-searchStep area">
                            <!--  <p class="o-searchTitle o-title __mid __withsub">--><p class="o-searchTitles o-title __mid __withsub" style="position:relative;text-align:center;margin-bottom: 1.6rem;">エリア・沿線を選択</p>
                            <div class="o-searchArea">
                                <div class="flex-nw">
                                    <a id="js-areaModal_side" class="o-searchArea_item"><i class="o-icon o-searchArea_itemIcon flaticon-location"></i>エリア</a>
                                    <a id="js-stationModal_side" class="o-searchArea_item"><i class="o-icon o-searchArea_itemIcon flaticon-train"></i>沿線・駅</a>
                                    <!--<a href="" class="o-searchArea_item"><i class="o-icon o-searchArea_itemIcon flaticon-map"></i>地図</a>-->
                                </div>
                                <div id="selectedSearch" class="o-searchInfo_selected __area">
                                    <?php
                                    $get_ksiks = array();
                                    $ksik = ( isset($_GET['ksik']) && !empty($_GET['ksik']) ) ? $_GET['ksik'] : null;
                                    if ( !is_null($ksik) ){
                                        $get_ksiks = str_getareaname($ksik);
                                    }
                                    $get_ekis = array();
                                    $eki = ( isset($_GET['eki']) && !empty($_GET['eki']) ) ? $_GET['eki'] : null;
                                    if ( !is_null($eki) ){
                                        $get_ekis = stw_getekiname($eki);
                                    }
                                    ?>
                                    <?php foreach ( $get_ksiks as $ogk ):?>
                                        <span class="o-searchInfo_selectedItem d-area"><?php echo $ogk['narrow_area_name'];?></span>
                                    <?php endforeach;?>
                                    <?php foreach ( $get_ekis as $oge ):?>
                                        <span class="o-searchInfo_selectedItem d-tation"><?php echo $oge['roseneki'];?></span>
                                    <?php endforeach;?>

                                    <!--  <li class="o-searchArea_selectedItem">都営浅草線 高輪台駅</li>-->
                                </div>
                            </div>
                        </div>
                        <div class="o-searchStep conditions">
                            <!-- <p class="o-searchTitle o-title __mid __withsub"> --><p class="o-searchTitles o-title __mid __withsub" style="position:relative;text-align:center;margin-bottom: 1.6rem;">探したい賃貸の条件を指定</p>
                            <div class="o-searchMoney o-searchBox">
                                <h5 class="o-searchSubtitle">賃料<span class="o-searchSubtitle_unit">(万円)</span></h5>
                                <?php $kalc=(isset($_GET['kalc'])&&!empty($_GET['kalc']))?$_GET['kalc']:'0';$kahc=(isset($_GET['kahc'])&&!empty($_GET['kahc']))?$_GET['kahc']:'200';?>
                                <input type="hidden" class="o-searchMoney_slider range-slider" id="sideChinryoSlider" value="<?php echo $kalc.','.$kahc;?>" style="display:none;" />
                                <?php if( have_rows('サーチフォーム上設備', 'option') ):$i = 1;?>
                                <span class="wpcf7-form-control-wrap chintai">
                                    <span class="wpcf7-form-control wpcf7-checkbox __2">
                                    <?php while( have_rows('サーチフォーム上設備', 'option') ): the_row();?>
                                        <?php
                                        // 検索条件を結果画面で反映させる処理
                                        // 設備のGETパラメータ取得。
                                        $set_s =  ( isset($_GET['set']) && !empty($_GET['set']) ) ? $_GET['set'] : null;
                                        // setの値をキー値にした配列に再構成
                                        $sumi_sets = array();
                                        if ( is_array($set_s) ){
                                            foreach ( $set_s as $one ){
                                                $sumi_sets[$one] = 1;
                                            }
                                        } else {
                                            $sumi_sets[$set_s] = 1;
                                        }
                                        //name属性を設定
                                        if(get_sub_field('共用or部屋', 'option') == '共用') {
                                            $setubi_name = 'set2';
                                        } else {
                                            $setubi_name = 'set3';
                                        }

                                        //値を取得
                                        if(get_sub_field('共用or部屋', 'option') == '共用') {
                                            if(get_sub_field('共用設備', 'option') !== '選択してください'){
                                                $field = get_sub_field_object('共用設備', 'option');
                                                // print_r($field);
                                                $setubi_value = $field['value'];
                                                $setubi_label = $field['choices'][$setubi_value];
                                            }else{

                                            }
                                        }else{
                                            if(get_sub_field('共用設備', 'option') !== '選択してください'){
                                                $field = get_sub_field_object('部屋設備', 'option');
                                                $setubi_value = $field['value'];
                                                $setubi_label = $field['choices'][$setubi_value];
                                            }else{

                                            }
                                        }
                                        // 検索パラメータに存在する場合はcheckedにする
                                        $checked = ( isset($sumi_sets[$setubi_value]) ) ? 'checked' : '';

                                        ?>
                                        <span class="wpcf7-list-item <?php echo ($i == 0) ? 'first':'';?>">
                                            <label><input type="checkbox" name="<?php echo $setubi_name;?>" value="<?php echo $setubi_value;?>" <?php echo $checked;?>><span class="wpcf7-list-item-label"><?php echo $setubi_label;?></span></label>
                                        </span>
                                     <?php $i++; endwhile;?>
                                     </span>
                                 </span>
                                 <?php endif;?>
                            </div>
                            <div class="o-searchBox">
                                <div class="o-searchBox_cont o-formSize">
                                    <h5 class="o-searchSubtitle">面積</h5>
                                    <div class="flex-nw ai-center">
                                        <div class="o-formSelect_wrap u-w100">
                                            <span class="wpcf7-form-control-wrap menseki-kagen">
                                                <?php $mel=(isset($_GET['mel'])&&!empty($_GET['mel']))?$_GET['mel']:'0';?>
                                                <select name="mel" class="wpcf7-form-control wpcf7-select o-formSelect" id="sideMensekiMin" aria-invalid="false">
                                                    <?php $mel_value = array('0','10','15','20','25','30','35','40','50','60','70','80','90','100','200','300','400','500','600','700','800','900','1000');?>
                                                    <?php foreach ( $mel_value as $one ):?>
                                                        <option value="<?php echo $one;?>" <?php echo ($one==$mel) ? 'selected' :''; ?>><?php echo ( '0' == $one ) ? '下限なし' : $one.'m²';?></option>
                                                    <?php endforeach;?>
                                                </select></span></div>
                                        <div>〜</div>
                                        <div class="o-formSelect_wrap u-w100">
                                            <span class="wpcf7-form-control-wrap menseki-jogen">
                                                <?php $meh=(isset($_GET['meh'])&&!empty($_GET['meh']))?$_GET['meh']:'0';?>
                                                <select name="meh" class="wpcf7-form-control wpcf7-select o-formSelect" id="sideMensekiMax" aria-invalid="false">
                                                    <?php $meh_value = array('10','15','20','25','30','35','40','50','60','70','80','90','100','200','300','400','500','600','700','800','900','1000','0');?>
                                                    <?php foreach ( $meh_value as $one ):?>
                                                        <option value="<?php echo $one;?>" <?php echo ($one==$meh) ? 'selected' :''; ?>><?php echo ( '0' == $one ) ? '上限なし' : $one.'m²';?></option>
                                                    <?php endforeach;?>
                                                </select></span></div>

                                    </div>

                                </div>
                                <div class="o-searchBox_cont">
                                    <div class="flex-nw l-row">
                                        <div class="o-formSize l-rowLeft rentsearch2">
                                            <h5 class="o-searchSubtitle">駅徒歩</h5>
                                            <div class="o-formSelect_wrap u-w100">
                                                <span class="wpcf7-form-control-wrap toho">
                                                    <?php $hof=(isset($_GET['hof'])&&!empty($_GET['hof']))?$_GET['hof']:'0';?>
                                                    <select name="eki" class="wpcf7-form-control wpcf7-select o-formSelect" id="sideHof" aria-invalid="false">
                                                        <?php $hof_value = array('0','1','3','5','10','15');?>
                                                        <?php foreach ( $hof_value as $one ):?>
                                                            <option value="<?php echo $one;?>" <?php echo ($one==$hof) ? 'selected' : '';?>><?php echo ('0' == $one ) ? '指定なし': $one . '分以内'; ?></option>
                                                        <?php endforeach;?>
                                                    </select></span></div>

                                        </div>
                                        <!-- 2020/12/20 入居可能日を即入居可のチェックボックスに変更。 -->
                                        <div class="l-rowRight rentsearch2">
                                            <h5 class="o-searchSubtitle">入居可能日</h5>
                                            <div class="o-formCheckbtn u-w100">
                                                <span class="wpcf7-form-control wpcf7-checkbox nyukyobi">
                                                    <span class="u-w100">
                                                        <label>
                                                            <?php $nyukyo=(isset($_GET['nyukyo'])&&!empty($_GET['nyukyo']))?$_GET['nyukyo']:'0';?>
                                                            <!-- 2020/12/20 スタイルが当たらないので、style属性でチェック欄が出るように仮修正 -->
                                                            <input type="checkbox" style="appearance:auto;" value="2" id="sideNyukyo" <?php echo ( '2' == $nyukyo ) ? 'checked' : '';?>>
                                                            <span class="wpcf7-list-item-label">
                                                                即入居可
                                                            </span>
                                                        </label>
                                                    </span>
                                                </span>
                                            </div>
                                        </div>
                                        <!--  2020/12/20 入居可能日修正前、selectboxで出していたもの。
                                        <div class="l-rowRight rentsearch2">
                                            <h5 class="o-searchSubtitle">入居可能日</h5>
                                            <div class="o-formSelect_wrap u-w100">
                                                <span class="wpcf7-form-control-wrap nyukyobi">
                                                    <?php //$nyukyo=(isset($_GET['nyukyo'])&&!empty($_GET['nyukyo']))?$_GET['nyukyo']:'0';?>
                                                    <select name="nyukyobi" class="wpcf7-form-control wpcf7-select o-formSelect nyukyo" id="sideNyukyo" aria-invalid="false">
                                                        <option value="0" <?php //echo ('0'==$nyukyo)?'selected':'';?>>指定なし</option>
                                                    <?php //$nyukyokanou_arr = str_get_nyukyokanoubi();?>
                                                    <?php //foreach ($nyukyokanou_arr as $od ):?>
                                                        <option value="<?php //echo $od['val'];?>"  <?php //echo ($od['val']==$nyukyo)?'selected':'';?>><?php //echo $od['txt'];?></option>
                                                    <?php //endforeach;?>
                                                    </select>
                                                </span>
                                            </div>
                                        </div>
                                        -->

                                    </div>
                                </div>
                            </div>
                            <div class="o-searchBox">
                                <div class="o-searchBox_cont o-searchRoom">
                                    <h5 class="o-searchSubtitle">間取り</h5>
                                    <p>
                                        <span class="wpcf7-form-control-wrap madori">
                                            <span id="sideMadori" class="wpcf7-form-control wpcf7-checkbox __4">
                                            <?php
                                            $mad_list = array(
                                                array('id'=>'modalMadori110','value'=>'110','name'=>'1R'),
                                                array('id'=>'modalMadori120','value'=>'120','name'=>'1K'),
                                                array('id'=>'modalMadori130','value'=>'130','name'=>'1DK'),
                                                array('id'=>'modalMadori150','value'=>'150','name'=>'1LDK'),
                                                array('id'=>'modalMadori155','value'=>'155','name'=>'1SLDK'),
                                                array('id'=>'modalMadori230','value'=>'230','name'=>'2DK'),
                                                array('id'=>'modalMadori250','value'=>'250','name'=>'2LDK'),
                                                array('id'=>'modalMadori255','value'=>'255','name'=>'2SLDK'),
                                                array('id'=>'modalMadori350','value'=>'350','name'=>'3LDK'),
                                                array('id'=>'modalMadori355','value'=>'355','name'=>'3SLDK'),
                                                array('id'=>'modalMadori450','value'=>'450','name'=>'4LDK'),
                                                array('id'=>'modalMadori455','value'=>'455','name'=>'4SLDK'),
                                                array('id'=>'modalMadori550','value'=>'550','name'=>'5LDK'),
                                                array('id'=>'modalMadori555','value'=>'555','name'=>'5SLDK')
                                            );
                                            $mad = ( isset($_GET['mad']) && !empty($_GET['mad']) ) ? $_GET['mad'] : '0';
                                            ?>
                                            <?php foreach ( $mad_list as $one ):?>
                                                <?php
                                                $mad_chkd = '';
                                                if ( is_array($mad) ){
                                                    foreach ( $mad as $mado ){
                                                        if ( $one['value'] == $mado ) $mad_chkd = 'checked';
                                                    }
                                                } else {
                                                    if ( $one['value'] == $mad ) $mad_chkd = 'checked';
                                                }
                                                ?>
                                                <span class="wpcf7-list-item first">
                                                  <label>
                                                    <input type="checkbox" name="madori" value="<?php echo $one['value'];?>" <?php echo $mad_chkd;?>>
                                                      <span class="wpcf7-list-item-label"><?php echo $one['name'];?></span>
                                                  </label>
                                                </span>
                                          <?php endforeach;?>
                                            </span>
                                        </span>
                                    </p>
                                </div>
                                <div class="o-searchBox_cont">
                                    <h5 class="o-searchSubtitle">設備</h5>
                                    <p> <a id="js-setsubiModal_side" class="o-searchBtn o-btn __basic11 __sizem __min mb-3">設備を選ぶ</a></p>
                                    <h5 class="o-searchSubtitle">こだわり条件</h5>
                                    <p> <a id="js-kodawariModal_side" class="o-searchBtn o-btn __basic11  __sizem __min mb-3">こだわり条件を選ぶ</a>
                                    </p>
                                </div>
                            </div>

                        <div class="o-searchStep o-searchBtn">
                            <div class="o-searchBtn_wrap jc-center">
                                <a href="" class="o-searchBtn_search o-btn __basic02 __mid search mb-1 searchRun" data-searchform="side"><i class="o-icon flaticon-search"></i>この条件で検索する</a>
                                <div class="o-searchBtn_function flex-w jc-center ai-center">
                                    <a href="" class="o-searchBtn_save o-btn __tiny __basic11 __sizes modalSearchSave" data-searchform="side"><i class="o-icon flaticon-download"></i>検索条件を保存</a>
                                    <a href="" class="o-searchBtn_reset o-btn __tiny __basic04 __sizem">条件をリセット</a>
                                </div>
                            </div>
                        </div>

                            <?php if ( have_rows('popular_search', 'option') ):?>
                            <div class="o-searchStep popular">
                                <!--  <p class="o-searchTitle o-title __mid __withsub">--><p class="o-searchTitles o-title __mid __withsub" style="position:relative;text-align:center;margin-bottom: 1.6rem;">他にもこのような検索条件から選ばれてます</p>
                                <div class="o-searchTerms">
                                    <form id="sidePopularSearch" class="o-searchTerms_select o-formChecklist" action="">
                                    <ul>
                                    <?php $popcon=0; while ( have_rows('popular_search', 'option') ): the_row();?>
                                    <!--
                                        <input class="o-formChecklist_input" name="popular" id="sidepopser<?php echo $popcon;?>" type="radio" value="<?php echo get_sub_field('display_popular_search_link');?>">
                                        <label class="o-formChecklist_label" for="sidepopser<?php echo $popcon;?>"><?php the_sub_field('display_popular_search_name'); ?></label>
                                    -->
                                        <li><a href="<?php echo get_sub_field('display_popular_search_link');?>" target="_blank"><?php the_sub_field('display_popular_search_name'); ?></a></li>

                                    <?php $popcon++; endwhile;?>
                                    </ul>
                                    </form>
                                </div>
                            </div>
                            <?php endif;?>


                        </div>

                    </div>

                    <!-- 1124px以下で表示 -->
                    <div class="l-sideSearch_cont __sp">
                        <div class="l-sideSearch_wrap l-wrap flex-nw-no-md">
                            <div class="l-sideSearch_function __area flex-nw js-modal1">
                                <p class="l-sideSearch_functionTitle"><i class="o-icon __large __nobold flaticon-location"></i>検索エリア</p>
                                <p class="l-sideSearch_functionDesc">指定なし</p>
                            </div>

                            <div class="l-sideSearch_function __terms flex-nw js-modal2">
                                <p class="l-sideSearch_functionTitle"><i class="o-icon __title __nobold flaticon-construction"></i>賃貸の条件</p>
                                <p class="l-sideSearch_functionDesc">指定なし指定なし指定なし</p>
                            </div>

                            <div class="l-sideSearch_btn">
                                <div class="l-sideSearch_btnWrap flex-w jc-center">
                                    <a href="" id="searchRun" class="l-sideSearch_btn_search o-btn __basic02 __p search mb-1"><i class="o-icon flaticon-search"></i>この条件で検索する</a>
                                    <div class="l-sideSearch_btnFunction flex-w jc-center ai-center">
                                        <a href="" class="l-sideSearch_btnSave o-btn __min __basic07 __sizem modalSearchSave" data-searchform="side"><i class="o-icon flaticon-download"></i>検索条件を保存</a>
                                        <a href="" class="l-sideSearch_btnReset o-btn __min __basic04 __sizem">条件をリセット</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
