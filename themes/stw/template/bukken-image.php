<?php
/**
 * http://kokyuchintai-tokyo.com/wp/wp-admin/post.php?post=702&action=edit
 * ACFから画像群を呼び出し
 */

    //物件外観写真
    $bukken_gaikan_images = get_field('bukken_gaikan_images');
    if(is_array($bukken_gaikan_images))
    foreach($bukken_gaikan_images as $k => $v){
        // print_r($v);
        echo '<img class="box3image" src="' . $v['sizes']['thumbnail'] . '">';
    }

    //棟その他
    $bukken_other_images = get_field('bukken_other_images');
    if(is_array($bukken_other_images))
    foreach($bukken_other_images as $k => $v){
        // print_r($v);
        echo '<img class="box3image" src="' . $v['sizes']['thumbnail'] . '">';
    }

    //リビング
    $bukken_living_images = get_field('bukken_living_images');
    if(is_array($bukken_living_images))
    foreach($bukken_living_images as $k => $v){
        // print_r($v);
        echo '<img class="box3image" src="' . $v['sizes']['thumbnail'] . '">';
    }

    //寝室
    $bukken_bedroom_images = get_field('bukken_bedroom_images');
    if(is_array($bukken_bedroom_images))
    foreach($bukken_bedroom_images as $k => $v){
        // print_r($v);
        echo '<img class="box3image" src="' . $v['sizes']['thumbnail'] . '">';
    }

    //浴室
    $bukken_bathroom_images = get_field('bukken_bathroom_images');
    if(is_array($bukken_bathroom_images))
    foreach($bukken_bathroom_images as $k => $v){
        // print_r($v);
        echo '<img class="box3image" src="' . $v['sizes']['thumbnail'] . '">';
    }

    //室内・その他
    $bukken_room_images = get_field('bukken_room_images');
    if(is_array($bukken_room_images))
    foreach($bukken_room_images as $k => $v){
        // print_r($v);
        echo '<img class="box3image" src="' . $v['sizes']['thumbnail'] . '">';
    }

    //間取り
    $bukken_madori_images = get_field('bukken_madori_images');
    if(is_array($bukken_madori_images))
    foreach($bukken_madori_images as $k => $v){
        // print_r($v);
        echo '<img class="box3image" src="' . $v['sizes']['thumbnail'] . '">';
    }

