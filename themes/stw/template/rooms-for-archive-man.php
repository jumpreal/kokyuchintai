<?php
/**
 * 物件に表示する部屋のループ処理PC（満室時）
 */
?>
<section class="o-roomVacancy">
    <div class="o-rsingleAction __notexist">
        <div class="o-rsingleAction_price o-text __p __red u-text-ac">
            現在募集がございません </div>

        <div class="o-rsingleAction_btn o-rsingleAction_cont">
            <a href="<?php echo home_url().'/fudo/'.$post_id;?>#awasete" class="o-rsingleAction_btnVisit o-btn __basic02 __mid">条件が近い物件を確認する</a>
            <div class="flex-no-md-nw">
            <a href="https://koukyu-chintai.com/kuusitu_inquiry?post_id=<?php echo $post_id;?>" class="o-rsingleAction_btnEmpty o-btn __basic03 flex-nw jc-center ai-center fb-50">
                空室状況を<br>問い合わせる
            </a>
            <a href="https://koukyu-chintai.com/bukken_inquiry?post_id=<?php echo $post_id;?>" class="o-rsingleAction_btnQuestion o-btn __basic03 flex-nw jc-center ai-center fb-50">
                物件について<br>質問する
            </a>
            </div>
        </div>
        <div class="o-rsingleAction_tel u-text-ac">
            <p class="o-rsingleAction_telTitle">電話で問い合わせする</p>
            <p class="o-rsingleAction_telTel">0120-916-546</p>
            <div class="o-rsingleAction_telInfo flex-nw jc-center ai-center">問い合わせ物件番号 :
                <?php echo $post_id;?>
            </div>
        </div>
    </div>
</section>


<?php

echo $js=<<<EOF
<script>
// 初期表示
jQuery(function(){
    if ('1' != jQuery('#check').val()) {
        jQuery('.hide_area').hide();
    }
});
// 表示/非表示
var speed = 500; //表示アニメのスピード（ミリ秒）
var stateDeliv = 1;
function hideToggle(hidearea) {
    hidearea.toggle(speed);
}
</script>
EOF;
