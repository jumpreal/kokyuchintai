<?php
/**
 * 物件一覧 / 検索結果 / マップ検索 のSPで表示する追尾ナビ
 */
?>
<aside class="o-partsFixnav __type01 o-animateMove fadein">
    <div class="o-partsFixnav_wrap flex-nw">
        <a href="" class="js-modal o-partsFixnav_item __search">
            <i class="o-partsFixnav_icon o-icon __p flaticon-search"></i>検索条件を変更
        </a>
        <a href="" class="o-partsFixnav_item __save">
            <i class="o-partsFixnav_icon o-icon __large flaticon-download"></i>検索条件を保存
        </a>
        <a href="tel:0120916546" class="o-partsFixnav_item">
            <i class="o-partsFixnav_icon o-icon __mid fas fa-phone"></i>
        </a>
        <a href="/contact/" class="o-partsFixnav_item">
            <i class="o-partsFixnav_icon o-icon __mid fas fa-envelope"></i>
        </a>

    </div>
</aside>
