<?php
if (DEBUGFLG) echo "<p>" . __FILE__ . "の" . __LINE__ . "行目</p>";
//TOPオプションからひっぱる
?>
    <section class="l-contTerm l-cont __section">
        <h2 class="o-title __title mb-3">人気の検索条件</h2>
        <div class="l-contTerm_cont flex-nw">

<?php if( have_rows('popular_condition', 'option') ): ?>
<?php while( have_rows('popular_condition', 'option') ): the_row();

    $display_popular_condition_name = get_sub_field('display_popular_condition_name');
    $display_popular_condition_link = get_sub_field('display_popular_condition_link');
    $display_popular_condition_image = get_sub_field('display_popular_condition_image');
    $eye_image = $display_popular_condition_image['url'];
    ?>

        <div class="l-contTerm_item o-bnr __5 overimg">
            <div class="o-bnrWrap">
                <div class="o-bnrThumb">
                <?php if ($eye_image !== '') :

                else :
                    $eye_image = "https://koukyu-chintai.com/wp-content/uploads/cropped-cropped-main-1-2000x1200-1-1024x614.jpg";
                endif ; ?>
                <img src="<?php echo $eye_image; ?>" alt="" class="o-bnrImg">

                </div>
                <h3 class="o-bnrTitle o-title __mid __bold"><?php echo $display_popular_condition_name; ?></h3>
                <a href="<?php echo $display_popular_condition_link; ?>" class="o-link __over"></a>
            </div>
        </div>

<?php endwhile; ?>
<?php endif; ?>


    </div>
</section>