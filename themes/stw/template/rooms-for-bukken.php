<?php
/**
 * 物件に表示する部屋のループ処理PC
 */
// echo __FILE__;

$connected_parent = get_posts($args);
$bukken_mansitu_fg = get_field('bukken_status');

if(count($connected_parent)>0){

$i = 1;
foreach($connected_parent as $ck=>$cv){
    $room_post_ID = $cv->ID;
    $room_meta=get_post_meta($room_post_ID);
    $str=$room_meta["kakaku"][0];
    $num = preg_replace('/[^0-9]/', '',$str);
    $tani=preg_replace('/'.$num.'/','',$str);
    $sr = get_field('kakakushikikin', $room_post_ID) ."/".get_field('kakakureikin', $room_post_ID);

    // $bosyu_juuko = get_field('madori_img', $room_post_ID);
    // if(is_array($bosyu_juuko))
    // $madori_img = $bosyu_juuko['url'];

    $bosyu_juuko = get_field('room_madori_images', $room_post_ID);
// print_r($bosyu_juuko);
    $madori_img = $bosyu_juuko[0]['sizes']['large'];

    if($madori_img !== ''){
        $room_image = $madori_img;
    }else{
        $room_image = "nowprinting.jpg";
    }
    $room_image = stw_get_singlefudo_madori_image($room_post_ID);
    
    $chinryo = get_mansitu_text($room_post_ID, $bukken_mansitu_fg, number_unit($num).$tani, '現在募集はございません');

    //部屋詳細へのリンク
    $room_detail_link = get_permalink($room_post_ID);

    //部屋への問合せリンク
    $room_inquiry_link = get_permalink($room_post_ID).'#toiawasesaki';

    //管理費
    $kanrihi = (get_field('kakakukyouekihi', $room_post_ID) ? number_format(get_field('kakakukyouekihi', $room_post_ID)) . "円" : "なし" );


    // echo $madori_img;

    // echo '<tr>';

    // //マイリスト登録用リンク生成
    // $link=wpfp_link_id($room_post_ID);

    // if($i > 3) {
    //     echo '<tr class="hide_area">';
    // }else{
    //     echo '<tr>';
    // }
    //     echo '<td><img style="width:150px;" src="' . $madori_img . '"></td>';
    //     //echo '<table class="rooms"><tr><th>部屋番号</th><td>'.$cv->post_title.'</td>';
    //     echo '<td>'. get_field('room_floor', $room_post_ID) .'階</td>';
    //     //echo '<td>'.$cv->post_title.'</td>';
    //     echo '<td>'. xls_custom_madorisu_print($room_post_ID) .'</td>';
    //     echo '<td>'. get_field('senyumenseki', $room_post_ID) .'㎡</td>';
    //     echo '<td>'. get_mansitu_text($room_post_ID, $bukken_mansitu_fg, number_unit($num).$tani.'円', '現在募集はございません') . '</td>';
    //     echo '<td>'. get_mansitu_text($room_post_ID, $bukken_mansitu_fg, $kanrihi, '-') .'</td>';
    //     echo '<td>'. get_mansitu_text($room_post_ID, $bukken_mansitu_fg, $sr, '-') .'</td>';
    //     // echo '<td>'. get_field('nyukyogenkyo', $room_post_ID).'</td>';
    //     echo '<td>'. get_mansitu_text($room_post_ID, $bukken_mansitu_fg, get_xls_custom_nyukyogenkyo_print($room_post_ID), '-') .'</td>';
    //     echo '<td><a href="'.get_permalink($room_post_ID).'">詳細</a></td>';
    //     echo '<td><a href="'.get_permalink($room_post_ID).'#toiawasesaki">お問合せ</a></td>';
    //     echo '<td>'.$link.'</td>';
    // echo '</tr>';

    if($i > 6) {
        $hide_class = 'hide_area';
    }else{
        $hide_class = '';
    }
?>

<!--<section class="o-rsingleRoom_item o-roomVacancy <?php echo $hide_class;?> fb-33"> 部屋・物件詳細の場合-->
    <section class="o-roomVacancy <?php echo $hide_class;?>">
    <div class="o-roomVacancy_floor"><?php the_field('room_floor', $room_post_ID);?>Fのお部屋</div>
    <div class="o-roomVacancy_thumb"><img class="o-roomVacancy_img" src="<?php echo $room_image;?>" alt=""></div>
    <div class="o-roomVacancy_cont">
        <div class="o-roomVacancy_price"><?php echo $chinryo;?></div>
        <div class="o-roomVacancy_cost flex-nw ai-center">
            <div class="o-roomVacancy_costItem __deposit">
                <span class="o-roomVacancy_costItem_title">敷金</span>
                <p class="o-roomVacancy_costItem_period">
                    <?php
                    if(get_field('bukken_status', $post_id) !== "2"){ //満室でなはい時表示
                        the_field('kakakushikikin', $room_post_ID);
                    }else{
                        echo '-';
                    }
                    ?>
                </p>
            </div>
            <div class="o-roomVacancy_costItem __gratuity">
                <span class="o-roomVacancy_costItem_title">礼金</span>
                <p class="o-roomVacancy_costItem_period">
                <?php
                    if(get_field('bukken_status', $post_id) !== "2"){ //満室でなはい時表示
                        the_field('kakakureikin', $room_post_ID);
                    }else{
                        echo '-';
                    }
                ?>
                </p>
            </div>
            <div class="o-roomVacancy_costItem __manege">
                <span class="o-roomVacancy_costItem_title">管理費</span>
                <p class="o-roomVacancy_costItem_period">
                <?php
                    if(get_field('bukken_status', $post_id) !== "2"){ //満室でなはい時表示
                        echo $kanrihi;
                    }else{
                        echo '-';
                    }
                    ?>
                </p>
            </div>

            <?php if(get_field('freerent', $room_post_ID)){ ?>
                    <div class="o-rsingleAction_costItem __freerent">
                    <span class="o-rsingleAction_costItem_title">フリーレント</span>
                    <p class="o-rsingleAction_costItem_period">
                        <?php echo str_replace("[フリーレント]", "", get_field('freerent', $room_post_ID));?>
                    </p>
                    </div>
            <?php } ?>


        </div>


        <div class="o-roomVacancy_space flex-nw jc-start ai-center">
            <div class="o-roomVacancy_spaceItem __area"><i class="o-roomVacancy_tag o-icon __mid flaticon-buildings"></i><?php the_field('senyumenseki', $room_post_ID);?>㎡</div>
            <div class="o-roomVacancy_spaceItem __layout"><i class="o-roomVacancy_tag flaticon-blueprint o-icon __mid"></i><?php echo xls_custom_madorisu_print($room_post_ID);?></div>

            <a href="<?php echo $room_detail_link;?>" class="o-link __over"></a>
        </div>

        <?php if(is_user_logged_in()){ //ログインしている人にだけ出ます。?>
        <div>
            現況：<?php echo get_xls_custom_nyukyogenkyo_print($room_post_ID); ?><br>
            共益・管理費：<?php the_field('kakakukyouekihi', $room_post_ID);?><br>
            仲介手数料：<?php the_field('tyuukai_tesuuryou', $room_post_ID);?>
        </div>
        <?php } ?>

        <?php if(get_field('bukken_status', $post_id) !== "2"){ //満室でなはい時表示 ?>
        <a href="<?php echo $room_inquiry_link;?>" class="o-roomVacancy_contact o-btn __basic01 __mid">詳細</a>
        <?php } ?>

    </div>
    </section>
<?php
    $i++;
}//foreach



if(count($connected_parent)>3){
    echo '<input id="check" type="checkbox" onclick="hideToggle(jQuery(\'.hide_area\'));">もっと見る';

echo $js=<<<EOF
<script>
// 初期表示
jQuery(function(){
    if ('1' != jQuery('#check').val()) {
        jQuery('.hide_area').hide();
    }
});
// 表示/非表示
var speed = 500; //表示アニメのスピード（ミリ秒）
var stateDeliv = 1;
function hideToggle(hidearea) {
    hidearea.toggle(speed);
}
</script>
EOF;
}
}





