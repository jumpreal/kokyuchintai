<?php
/**
 * 合わせて検討したい物件
 * awasete
 */
$awasete = get_field('awasete');
// print_r($awasete);
if(isset($awasete)){
?>

<?php
$bukken_id="";
    $awasete_bukken_fg = false;
    foreach($awasete as $p){
        $bukken_id = $p->ID;
        if(get_field('bukken_status', $bukken_id) == 2){
            $awasete_bukken_fg = true;
        }
    }

    if($awasete_bukken_fg == false){
?>


<section class="l-contPopular l-cont __section __garter">
    <h2 class="o-title __title mb-3" id="awasete">合わせて検討したい物件</h2>

    <div class="o-carousel swiper-container __carouselType01">
        <div class="l-contPopular_cont o-room o-carouselWrap swiper-wrapper flex-nw">

<?php
$bukken_id="";
    foreach($awasete as $p){
        $bukken_id = $p->ID;
        include('bukken-card.php');
    }
?>


</div>
        <div class="o-carouselPagenation swiper-pagination"></div>
    </div>
</section>

<?php
    }//$awasete_bukken_fg
}
?>