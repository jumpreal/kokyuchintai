<section class="l-contBlog l-cont __section __garter">
<h2 class="o-title __title mb-3">役立つ高級賃貸の知識</h2>
<div class="l-contBlog_cont flex-w">

<?php
    // posts_per_pageで取得件数の指定、orderbyでソート順を新着順にしています。
    $args = array('post_type'=>'post', 'posts_per_page' => 4, 'orderby' => 'date');
    $query = new WP_Query($args);
?>
<?php if( $query->have_posts() ) : ?>

    <?php while ($query->have_posts()) : $query->the_post(); ?>
        <article class="l-contBlog_post o-post __4">
            <div class="o-postWrap">
                <div class="o-postThumb">
                <?php if (has_post_thumbnail()) :
                    // アイキャッチ画像のIDを取得
                    $thumbnail_id = get_post_thumbnail_id();

                    // mediumサイズの画像内容を取得（引数にmediumをセット）
                    $eye_image_array = wp_get_attachment_image_src( $thumbnail_id , 'medium' );
                    $eye_image = $eye_image_array[0];
                else :
                    $eye_image = "https://koukyu-chintai.com/wp-content/uploads/cropped-cropped-main-1-2000x1200-1-1024x614.jpg";
                endif ; ?>
                <img class="l-contCompany_img" src="<?php echo $eye_image; ?>" alt="">
                </div>
                <div class="o-postHeader">
                    <ul class="o-postData flex-nw jc-between">
                    <?php
                    $category = get_the_category();
                    $cat_id   = $category[0]->cat_ID;
                    $cat_name = $category[0]->cat_name;
                    ?>
                        <li class="o-postCat"><a href=""><?php echo $cat_name; ?></a></li>
                        <li class="o-postDate"><a href=""><?php the_time('Y/m/d'); ?></a></li>
                    </ul>
                    <h3 class="o-postTitle"><?php the_title(); ?></h3>
                </div>

                <a href="<?php the_permalink();?>" class="o-link __over"></a>
            </div>
        </article>

   <?php endwhile; ?>

<?php endif; ?>
<?php wp_reset_postdata(); ?>

</div>
    <a href="<?php bloginfo('url'); ?>/category/staffblog/" class="l-contNewestate_link o-link __mid __text type01">新着一覧を見る<span class="o-linkItem"></span></a>
</section>
