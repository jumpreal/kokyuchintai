<?php
/**
 * footer-top.php
 * footer-single.php
 * page-mylist.php
 */
?>

<?php
if(get_rooms_count($bukken_id)>0){
?>
<section class="o-roomItem o-carouselItem swiper-slide">
                <?php $fvflg = ( in_array($bukken_id,$fvpids) ) ? true : false;?>
                <a class="o-roomSave" data-fvflg="<?php echo ($fvflg) ? 'sumi' : 'mi';?>" data-pid="<?php echo $bukken_id;?>" href="<?php echo home_url();?>/fudo/<?php echo $bukken_id;?>/?wpfpaction=add&postid=<?php echo $bukken_id;?>">
                    <i class="o-icon <?php echo ($fvflg) ? 'flaticon-heart __checked' : 'flaticon-heart';?>"></i>
                </a>
                <p class="o-roomStatus"><?php echo get_rooms_count($bukken_id);?>部屋募集中</p>
                <div class="o-roomCont o-roomThumb">


<?php

    $p_type = get_post($bukken_id)->post_type;
    $parent_post_ID = get_parent_ID($bukken_id);

    if($p_type == 'room'){
        $oya_title = get_the_title($parent_post_ID);
        $permalink = get_permalink($parent_post_ID);
        $oya_title_with_roomNo="";
        // $oya_title_with_link = '<a href="' . $permalink . '">' . $oya_title . '</a>';

        if(get_field('room_floor', $bukken_id)!=""){ $oya_title_with_roomNo = $oya_title . '／' . get_field('room_floor', $bukken_id).'階';}
        $title = $oya_title_with_roomNo;
    }else{
        $title = get_the_title($bukken_id);
    }


        // if($connected_parent[0]->post_type == 'room'){
        //     $oya_title=$connected_parent[0]->post_title;
        //     $oya_title_with_roomNo="";
        //     $parent_post_ID = $connected_parent[0]->ID;
        //     $oya_meta=get_post_meta($parent_post_ID);
        //     $bukken_mansitu_fg = get_field('bukken_status', $parent_post_ID);
        //     $room_mansitu_fg = get_field('nyukyogenkyo', $post_id);
        //     $ko_meta=get_post_meta($post_id);
        //     $permalink = get_permalink($parent_post_ID);


        // }else{
        //     $title = get_the_title($bukken_id);
        // }
?>


                        <?php if (has_post_thumbnail($bukken_id)) :
                            // アイキャッチ画像のIDを取得
                            $thumbnail_id = get_post_thumbnail_id($bukken_id);

                            // mediumサイズの画像内容を取得（引数にmediumをセット）
                            $eye_image_array = wp_get_attachment_image_src( $thumbnail_id , 'medium' );
                            $eye_image = $eye_image_array[0];
                        else :
                            $rtn_arr = stw_get_singlefudo_images($bukken_id,2);
                            if ( 0 < count($rtn_arr) ){
                                $eye_image = stw_generate_imgurl($rtn_arr[0]->image_path);
                            } else {
                                $eye_image = "https://koukyu-chintai.com/wp-content/uploads/cropped-cropped-main-1-2000x1200-1-1024x614.jpg";
                            }
                        endif ; ?>
                            <img class="o-roomThumb_img" src="<?php echo $eye_image; ?>">

                    <div class="o-roomSummary">
                    <h3 class="o-roomTitle o-title __white">
                    <?php echo $title;?></h3>
                        <p class="o-roomPrice o-title __en">
                        <?php
                        //賃料
                        if(get_field('chinryos', $bukken_id)){
                            the_field('chinryos', $bukken_id);
                        }else{
                            echo get_kakaku_list_for_fudo($bukken_id);
                        }
                        ?>
                        </p>
                        <div class="o-roomSpace flex-nw jc-start ai-center">
                            <div class="o-roomSpace_meter"><i class="o-roomTag_icon o-icon __min __white flaticon-buildings"></i>
                            <?php //面積 ?>
                            <?php echo get_senyumenseki_list_for_fudo($bukken_id);?>
                            </div>
                            <div class="o-roomSpace_room"><i class="flaticon-blueprint o-icon __min __white"></i>
                            <?php
                                if(get_field('bukken_madori_list', $bukken_id)){
                                    the_field('bukken_madori_list', $bukken_id);
                                }else{
                                    echo get_madori_list_for_fudo($bukken_id);
                                }
                            ?>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="o-roomCont o-roomDesc flex-nw jc-between">

                    <div class="o-roomInfo">
                        <p class="o-roomAddress flex-nw jc-between">
                        <span>
                            <?php my_custom_shozaichi_print($bukken_id);?>
                            <?php echo get_post_meta($bukken_id, 'shozaichimeisho', true);?>
                        </span>
                        <?php
                        $map_link = "https://www.google.co.jp/maps/place/" . urlencode(get_my_custom_shozaichi_print($bukken_id) . get_post_meta($bukken_id, 'shozaichimeisho', true) . get_post_meta($bukken_id, 'shozaichimeisho2', true));

                        ?>
                        <a href="<?php echo $map_link; ?>" target="_blank" class="o-roomMap o-link __tiny">MAPで表示</a> </p>
                        <ul class="o-roomStation">
                            <li class="o-roomStation_list">
                            <?php my_custom_koutsu1_print($bukken_id); ?>
                            </li>
                            <li class="o-roomStation_list">
                            <?php my_custom_koutsu2_print($bukken_id); ?>
                            </li>
                            <li class="o-roomStation_list">
                            <?php my_custom_koutsu3_print($bukken_id); ?>
                            </li>
                        </ul>
                        <div class="o-roomTag flex-w" style="display:none;">
                            <i class="o-roomTag_icon o-icon __min flaticon-tag"></i>
                            <a href="" class="o-roomTag_link o-link __nocolor">ペット相談可</a>
                        </div>
                    </div>
                    <div class="o-roomBuilding">
                        <ul>
                            <li class="o-roomBuilding_item">
                                <?php
                                        //築年月
                                $tatemonochikunenn = get_post_meta($bukken_id, 'tatemonochikunenn', true);
                                if(!empty($tatemonochikunenn)){

                                    if(strpos($tatemonochikunenn, '月')){
                                        $tatemonochikunenn_data = $tatemonochikunenn . "01";
                                        $tatemonochikunenn_data = str_replace(array("年","月"), "/", $tatemonochikunenn_data);
                                    }else{
                                        $tatemonochikunenn_data = $tatemonochikunenn . "/01";
                                    }
                                }

                                // echo "<p>" . $tatemonochikunenn . "</p>";

                                // echo get_sintiku_mark($tatemonochikunenn_data);
                                //築年数
                                echo get_tikunensu($tatemonochikunenn_data);
                                ?>
                            </li>
                            <li class="o-roomBuilding_item">
                            <?php
                            //建物構造
                            my_custom_tatemonokozo_print_br($bukken_id);
                            ?>
                            </li>
                            <li class="o-roomBuilding_item">
                            <?php
                                if(get_post_meta($bukken_id, 'tatemonokaisu1', true)!="") echo '地上'.get_post_meta($bukken_id, 'tatemonokaisu1', true)."階";
                                if(get_post_meta($bukken_id, 'tatemonokaisu2', true)!="") echo '<br>地下'.get_post_meta($bukken_id, 'tatemonokaisu2', true)."階";
                            ?>
                            </li>
                        </ul>
                            <a href="<?php echo home_url() . "/" . get_field('kuusitu_inquiry_link', 'option');?>?post_id=<?php echo $bukken_id;?>" class="o-roomContact o-btn __basic01">詳細</a>
                    </div>

                </div>

                <a href="<?php echo get_permalink($bukken_id);?>" class="o-link __over"></a>

            </section>


<?php
}
?>
