<?php
/**
 * 物件・部屋詳細のSPで表示する追尾ナビ
 */
?>
<aside class="o-partsFixnav __type02">
    <div class="o-partsFixnav_wrap flex-nw">
        <a href="" class="js-modal o-partsFixnav_item __search animate fadein">
            <i class="o-partsFixnav_icon o-icon __p flaticon-search"></i>検索条件を変更
        </a>
        <a href="" class="o-partsFixnav_item __save animate fadein">
            <i class="o-partsFixnav_icon o-icon __large flaticon-download"></i>検討リストへ追加
        </a>
        <a id="js-estimateModal2" class="o-partsFixnav_item __estimate animate fadein">
            <i class="o-partsFixnav_icon o-icon __large flaticon-calculator"></i>概算見積もり
        </a>
    </div>
</aside>
