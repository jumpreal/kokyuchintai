<?php
if (DEBUGFLG) echo "<p>" . __FILE__ . "の" . __LINE__ . "行目</p>";
//TOPオプションからひっぱる
?>


<section class="l-contPopular l-cont __section __garter">
    <h2 class="o-title __title mb-3">よく見られている人気物件。</h2>

    <div class="o-carousel swiper-container __carouselType01">
        <div class="l-contPopular_cont o-room o-carouselWrap swiper-wrapper flex-nw">

<?php
global $fvpids;
$posts = get_field('top用人気物件', 'option');//print_r($posts);
if( $posts ): ?>

    <?php foreach( $posts as $post): // variable must be called $post (IMPORTANT) ?>
                <?php //print_r($post); ?>
        <?php setup_postdata($post); ?>
                <?php $post_id = get_the_ID();?>
                <section class="o-roomItem o-carouselItem swiper-slide">
                <?php $fvflg = ( in_array($post_id,$fvpids) ) ? true : false;?>
                    <a class="o-roomSave" data-fvflg="<?php echo ($fvflg) ? 'sumi' : 'mi';?>" data-pid="<?php echo $post_id;?>"><i class="o-icon <?php echo ($fvflg) ? 'flaticon-heart __checked' : 'flaticon-heart';?>"></i></a>
                <p class="o-roomStatus"><?php echo get_rooms_count($post_id);?>部屋募集中</p>
                <div class="o-roomCont o-roomThumb">

                        <?php if (has_post_thumbnail($post_id)) :
                            // アイキャッチ画像のIDを取得
                            $thumbnail_id = get_post_thumbnail_id();

                            // mediumサイズの画像内容を取得（引数にmediumをセット）
                            $eye_image_array = wp_get_attachment_image_src( $thumbnail_id , 'medium' );
                            $eye_image = $eye_image_array[0];
                        else :
                            $rtn_arr = stw_get_singlefudo_images($post_id,2);
                            $rtn_arr0 = stw_get_singlefudo_images($post_id);
                            if ( 0 < count($rtn_arr) ){
                                if(!in_array(basename($rtn_arr[0]->image_path),     get_ban_image_file_name())){
                                $eye_image = stw_generate_imgurl($rtn_arr[0]->image_path);
                                }else{
                                    $eye_image = stw_generate_imgurl($rtn_arr0[1]->image_path);
                                }
                            } else {
                                $eye_image = "https://koukyu-chintai.com/wp-content/uploads/cropped-cropped-main-1-2000x1200-1-1024x614.jpg";
                            }
                        endif ; ?>
                            <img class="o-roomThumb_img" src="<?php echo $eye_image; ?>">

                    <div class="o-roomSummary">
                    <h3 class="o-roomTitle o-title __white"><?php the_title(); ?></h3>
                        <p class="o-roomPrice o-title __en">
                        <?php
                        //賃料
                        if(get_field('chinryos', $post_id)){
                            the_field('chinryos', $post_id);
                        }else{
                            echo get_kakaku_list_for_fudo($post_id);
                        }
                        ?>
                        </p>
                        <div class="o-roomSpace flex-nw jc-start ai-center">
                            <div class="o-roomSpace_meter"><i class="o-roomTag_icon o-icon __min __white flaticon-buildings"></i>
                            <?php //面積 ?>
                            <?php echo get_senyumenseki_list_for_fudo($post_id);?>
                            </div>
                            <div class="o-roomSpace_room"><i class="flaticon-blueprint o-icon __min __white"></i>
                            <?php
                                if(get_field('bukken_madori_list', $post_id)){
                                    the_field('bukken_madori_list', $post_id);
                                }else{
                                    echo get_madori_list_for_fudo($post_id);
                                }
                            ?>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="o-roomCont o-roomDesc flex-nw jc-between">

                    <div class="o-roomInfo">
                        <p class="o-roomAddress flex-nw jc-between">
                        <span>
                        <?php my_custom_shozaichi_print($post_id);?>
                            <?php echo get_post_meta($post_id, 'shozaichimeisho', true);?>                        </span>
                        <?php
                        $map_link = "https://www.google.co.jp/maps/place/" . urlencode(get_my_custom_shozaichi_print($post_id) . get_post_meta($post_id, 'shozaichimeisho', true) . get_post_meta($post_id, 'shozaichimeisho2', true));

                        ?>
                        <a href="<?php echo $map_link; ?>" target="_blank" class="o-roomMap o-link __tiny">MAPで表示</a> </p>
                        <ul class="o-roomStation">
                            <li class="o-roomStation_list">
                            <?php my_custom_koutsu1_print($post_id); ?>
                            </li>
                            <li class="o-roomStation_list">
                            <?php my_custom_koutsu2_print($post_id); ?>
                            </li>
                            <li class="o-roomStation_list">
                            <?php my_custom_koutsu3_print($post_id); ?>
                            </li>
                        </ul>
                        <div class="o-roomTag flex-w" style="display:none;">
                            <i class="o-roomTag_icon o-icon __min flaticon-tag"></i>
                            <a href="" class="o-roomTag_link o-link __nocolor">ペット相談可</a>
                        </div>
                    </div>
                    <div class="o-roomBuilding">
                        <ul>
                            <li class="o-roomBuilding_item">
                                <?php
                                        //築年月
                                $tatemonochikunenn = get_post_meta($post_id, 'tatemonochikunenn', true);
                                if(!empty($tatemonochikunenn)){

                                    if(strpos($tatemonochikunenn, '月')){
                                        $tatemonochikunenn_data = $tatemonochikunenn . "01";
                                        $tatemonochikunenn_data = str_replace(array("年","月"), "/", $tatemonochikunenn_data);
                                    }else{
                                        $tatemonochikunenn_data = $tatemonochikunenn . "/01";
                                    }
                                }

                                // echo "<p>" . $tatemonochikunenn . "</p>";

                                // echo get_sintiku_mark($tatemonochikunenn_data);
                                //築年数
                                echo get_tikunensu($tatemonochikunenn_data);
                                ?>
                            </li>
                            <li class="o-roomBuilding_item">
                            <?php
                            //建物構造
                            my_custom_tatemonokozo_print_br($post_id);
                            ?>
                            </li>
                            <li class="o-roomBuilding_item">
                            <?php
                                if(get_post_meta($post_id, 'tatemonokaisu1', true)!="") echo '地上'.get_post_meta($post_id, 'tatemonokaisu1', true)."階";
                                if(get_post_meta($post_id, 'tatemonokaisu2', true)!="") echo '<br>地下'.get_post_meta($post_id, 'tatemonokaisu2', true)."階";
                            ?>
                            </li>
                        </ul>

                        <!--  <a href="<?php //echo home_url() . "/" . get_field('kuusitu_inquiry_link', 'option');?>?post_id=<?php //echo $post_id;?>" class="o-roomContact o-btn __basic01">詳細</a>-->
                        <a href="<?php echo get_the_permalink($post_id);?>" class="o-roomContact o-btn __basic01">詳細</a>
                    </div>

                </div>
                <a href="<?php the_permalink(); ?>" class="o-link __over"></a>
            </section>

            <?php endforeach; ?>
    <?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
<?php endif; ?>


        </div>
        <div class="o-carouselPagenation swiper-pagination"></div>
    </div>
</section>
