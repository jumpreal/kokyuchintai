<?php

/**

 * 地図表示 GoogleMaps Places

 *

 * @since Fudousan Plugin ver1.7.12

 * For single-fudo.php apply_filters( 'fudou_single_googlemaps', $post_id , $kaiin , $kaiin2 , $title );

 *

 * @param int $post_id Post ID.

 * @param int $kaiin.

 * @param int $kaiin2.

 * @param str $title.

 * @return text

 */
$lat = get_post_meta($post_id,'bukkenido',true);
$lng = get_post_meta($post_id,'bukkenkeido',true);

if($lat && $lng){
    apply_filters( 'fudou_single_googlemaps', $post_id , $kaiin , $kaiin2 , $title );
}else{
    apply_filters( 'fudou_single_googlemaps_from_address', $post_id , $kaiin , $kaiin2 , $title );
}

?>