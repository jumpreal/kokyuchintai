<section class="l-cont recommend">
    <h2 class="o-title __large mb-3">オススメ物件</h2>
    <div class="o-carousel swiper-container __carouselType02 __overflow">
        <div class="o-room o-carouselWrap swiper-wrapper flex-nw">


            <?php

$posts = get_field('top用おすすめ物件', 'option');

if( $posts ): ?>

            <?php foreach( $posts as $post): // variable must be called $post (IMPORTANT) ?>
            <?php setup_postdata($post); ?>
                <?php $bukken_id  = $post->ID;?>

            <section class="o-roomItem __nomb o-carouselItem swiper-slide">
            <?php global $fvpids;?>
            <?php $fvflg = ( in_array($bukken_id,$fvpids) ) ? true : false;?>
                <a class="o-roomSave" data-fvflg="<?php echo ($fvflg) ? 'sumi' : 'mi';?>" data-pid="<?php echo $bukken_id;?>"><i class="o-icon <?php echo ($fvflg) ? 'flaticon-heart __checked' : 'flaticon-heart';?>"></i></a>
            <p class="o-roomStatus"><?php echo get_rooms_count($bukken_id);?>部屋募集中</p>
                <div class="o-roomCont o-roomThumb">
                <?php
                $bukken_gaikan_kyouyou = get_field('bukken_gaikan_kyouyou', $post->ID);
                if($bukken_gaikan_kyouyou[0]['sizes']['large']){
                    $fudoimg_url = $bukken_gaikan_kyouyou[0]['sizes']['large'];
                }else{
                    $res_arr = stw_get_singlefudo_images($post->ID,2);
                    if ( 0 < count($res_arr) ){
                        $fudoimg_url = stw_generate_imgurl($res_arr[0]->image_path);
                    }else {
                        $fudoimg_url = "https://koukyu-chintai.com/wp-content/uploads/cropped-cropped-main-1-2000x1200-1-1024x614.jpg";
                    }
                }
                ?>
                    <img class="o-roomThumb_img" src="<?php echo $fudoimg_url;?>">

                    <div class="o-roomSummary">
                        <p class="o-roomTitle o-title __white"><?php echo get_the_title($bukken_id); ?></p>
                        <p class="o-roomPrice o-title __en">
                        <?php
                        //賃料
                        if(get_field('chinryos', $bukken_id)){
                            the_field('chinryos', $bukken_id);
                        }else{
                            echo get_kakaku_list_for_fudo($bukken_id);
                        }
                        ?>
                            <span class="o-roomPrice_unit">円</span></p>
                            <div class="o-roomSpace flex-nw jc-start ai-center">
                        <div class="o-roomSpace_meter"><i class="o-roomTag_icon o-icon __min __white flaticon-buildings"></i>
                        <?php //面積 ?>
                        <?php echo get_senyumenseki_list_for_fudo($bukken_id);?>
                        </div>
                        <div class="o-roomSpace_room"><i class="flaticon-blueprint o-icon __min __white"></i>
                        <?php
                            if(get_field('bukken_madori_list', $bukken_id)){
                                the_field('bukken_madori_list', $bukken_id);
                            }else{
                                echo get_madori_list_for_fudo($bukken_id);
                            }
                        ?>
                        </div>
                        </div>
                    </div>

                </div>
                <div class="o-roomCont o-roomDesc flex-nw jc-between">
                <div class="o-roomInfo">
                    <p class="o-roomAddress flex-nw jc-between">
                    <span>
                    <?php echo get_my_custom_shozaichi_print($bukken_id) . get_post_meta($bukken_id, 'shozaichimeisho', true);?>
                    </span>
                    <?php
                    $map_link = "https://www.google.co.jp/maps/place/" . urlencode(get_my_custom_shozaichi_print($bukken_id) . get_post_meta($bukken_id, 'shozaichimeisho', true) . get_post_meta($bukken_id, 'shozaichimeisho2', true));

                    ?>
                    <a href="<?php echo $map_link; ?>" target="_blank" class="o-roomMap o-link __tiny">MAPで表示</a> </p>
                    <ul class="o-roomStation">
                        <li class="o-roomStation_list">
                        <?php my_custom_koutsu1_print($bukken_id); ?>
                        </li>
                        <li class="o-roomStation_list">
                        <?php my_custom_koutsu2_print($bukken_id); ?>
                        </li>
                        <li class="o-roomStation_list">
                        <?php my_custom_koutsu3_print($bukken_id); ?>
                        </li>
                    </ul>
                    <div class="o-roomTag flex-w" style="display:none;">
                        <i class="o-roomTag_icon o-icon __min flaticon-tag"></i>
                        <a href="" class="o-roomTag_link o-link __nocolor">ペット相談可</a>
                    </div>
                </div>
                <div class="o-roomBuilding">
                <ul>
                    <li class="o-roomBuilding_item">
                        <?php
                                //築年月
                        $tatemonochikunenn = get_post_meta($bukken_id, 'tatemonochikunenn', true);
                        if(!empty($tatemonochikunenn)){

                            if(strpos($tatemonochikunenn, '月')){
                                $tatemonochikunenn_data = $tatemonochikunenn . "01";
                                $tatemonochikunenn_data = str_replace(array("年","月"), "/", $tatemonochikunenn_data);
                            }else{
                                $tatemonochikunenn_data = $tatemonochikunenn . "/01";
                            }
                        }

                        // echo "<p>" . $tatemonochikunenn . "</p>";

                        // echo get_sintiku_mark($tatemonochikunenn_data);
                        //築年数
                        echo get_tikunensu($tatemonochikunenn_data);
                        ?>
                    </li>
                    <li class="o-roomBuilding_item">
                    <?php
                    //建物構造
                    my_custom_tatemonokozo_print($bukken_id);
                    ?>
                    </li>
                    <li class="o-roomBuilding_item">
                    <?php
                        if(get_post_meta($bukken_id, 'tatemonokaisu1', true)!="") echo '地上'.get_post_meta($bukken_id, 'tatemonokaisu1', true)."階";
                        if(get_post_meta($bukken_id, 'tatemonokaisu2', true)!="") echo '<br>地下'.get_post_meta($bukken_id, 'tatemonokaisu2', true)."階";
                    ?>
                    </li>
                </ul>

                <?php
                    $link = get_permalink($bukken_id) . "#contact_form";
                ?>
                <a href="<?php echo $link;?>" class="o-roomContact o-btn __basic01">詳細</a>
            </div>

                </div>
                <a href="<?php echo get_permalink($bukken_id);?>" class="o-link __over"></a>
            </section>


            <?php endforeach; ?>
            <?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
            <?php endif; ?>


        </div>
        <div class="o-carouselPagenation swiper-pagination"></div>
    </div>
</section>
