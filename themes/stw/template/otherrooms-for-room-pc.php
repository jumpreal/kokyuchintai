<?php

$connected_parent = get_posts($args);
// print_r($connected_parent);
$bukken_mansitu_fg = get_field('bukken_status', $parent_post_ID);

if(count($connected_parent)>0){

    echo '<p>「'.$oya_title.'」の他の部屋</p>';
    echo '<table class="rooms marg">';

    echo '<tr><th>部屋</th><th>間取</th><th>専有面積</th><th>賃料</th><th>管理費</th><th>敷金/礼金</th><th>入居可能</th><th>詳細</th></tr>';
    
    foreach($connected_parent as $k => $v){
        $room_post_ID = $v->ID;
        $room_meta=get_post_meta($room_post_ID);
        $str=$room_meta["kakaku"][0];
        $num = preg_replace('/[^0-9]/', '',$str);
        $tani=preg_replace('/'.$num.'/','',$str);
        $sr = get_field('kakakushikikin', $room_post_ID) ."/".get_field('kakakureikin', $room_post_ID);

        $kanrihi = (get_field('management_fee', $room_post_ID) ? number_format(get_field('management_fee', $room_post_ID)) . "円" : "なし" );

        if($post_id !== $room_post_ID){//自分自身を覗いて表示
            echo '<tr>';
            //echo '<table class="rooms"><tr><th>部屋番号</th><td>'.$cv->post_title.'</td>';
            echo '<td>'. get_field('room_floor', $room_post_ID) .'階</td>';
            //echo '<td>'.$cv->post_title.'</td>';
            echo '<td>'. xls_custom_madorisu_print($room_post_ID) .'</td>';
            echo '<td>'. get_field('senyumenseki', $room_post_ID) .'㎡</td>';
            echo '<td>'. get_mansitu_text($post_id, $bukken_mansitu_fg, number_unit($num).$tani.'円', '現在募集はございません') . '</td>';
            echo '<td>'. get_mansitu_text($post_id, $bukken_mansitu_fg, $kanrihi, '-') .'</td>';
            echo '<td>'. get_mansitu_text($post_id, $bukken_mansitu_fg, $sr, '-') .'</td>';
            echo '<td>'. get_mansitu_text($room_post_ID, $bukken_mansitu_fg, get_xls_custom_nyukyogenkyo_print($room_post_ID), '-') .'</td>';
            echo '<td><a href="'.get_permalink($room_post_ID).'">詳細</a></td>';
            echo '</tr>';
        }
    }

    echo '</table>';
}

