<?php
/*
Template Name: company
*/
?>
<?php get_header();?>



<div id="company" class="content-area underlayer">
  <main id="main" class="site-main" role="main">

    <nav id="breadcrumbs">
      <ul class="cf">
        <li><a href="/"><span>ホーム</span></a></li>
        <li>会社概要</li>
      </ul>
    </nav>
    
    <article class="ul-flex">
      <div class="ul-contents">
        <h2 class="ul-title mincho">会社概要</h2>
        
        <table class="ul-table">
          <tbody>
            <tr>
              <th>会社名</th>
              <td>株式会社 リテック</td>
            </tr>
            <tr>
              <th>代表取締役</th>
              <td>北原達哉</td>
            </tr>
            <tr>
              <th>E-mail</th>
              <td>info@retech-inc.jp</td>
            </tr>
            <tr>
              <th>店舗</th>
              <td>〒1040061<br>東京都中央区銀座3-10-19 美術家会館402号</td>
            </tr>
            <tr>
              <th>営業時間</th>
              <td>10時〜19時</td>
            </tr>
            <tr>
              <th>設立</th>
              <td>2018年12月</td>
            </tr>
            <tr>
              <th>資本金</th>
              <td>100万円</td>
            </tr>
            <tr>
              <th>取引先金融機関</th>
              <td>三井住友銀行・三菱UFJ銀行・みずほ銀行</td>
            </tr>
            <tr>
              <th>宅地建物取引業免許番号</th>
              <td>（ ）号</td>
            </tr>
            <tr>
              <th>取引先</th>
              <td>三井不動産・ 三菱地所ハウスネット・ 住友不動産・ 東急</td>
            </tr>
            <tr>
              <th>加盟団体</th>
              <td></td>
            </tr>
          </tbody>
        </table>
      </div><!-- .ul-contents -->
      
      <?php get_sidebar(); ?>
    </article><!-- .ul-flex -->
    
  </main><!-- #main -->
</div><!-- #about -->

<?php get_footer(); ?>
