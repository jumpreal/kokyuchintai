<?php
/*
Template Name: 問い合わせ - 物件リクエスト
Template Post Type: post,page
*/
?>

<?php get_header('contact');?>



<div id="<?php echo $post->post_name;?>" class="p-mypage">
    <main id="main" class="site-main" role="main">

        <div class="o-fv __page mb-3">
            <?php
            // アイキャッチ画像が設定されているかチェック
            if(has_post_thumbnail()){
                // アイキャッチ画像を表示する
                the_post_thumbnail( 'thumbnail', array('class' => 'o-fvImg') );
            }else{
                // 代替画像を表示する
                echo '<img src="https://koukyu-chintai.com/wp-content/uploads/cropped-cropped-main-1-2000x1200-1-1024x614.jpg" width="640" height="384" alt="No Image" class="o-fvImg" loading="lazy" />';
            }
            ?>
            <div class="o-fvCatch">
                <h2 class="o-fvCatch_title o-title __large __white __nobold">
                    <?php the_title(); ?>
                </h2>
            </div>
        </div>

        <section class="l-wrap __maxwsmall mb-6">
            <section class="p-postCont p-otherCont request">

                <?php the_content(); ?>

            </section>

        </section>

    </main><!-- #main -->
</div>

<?php get_footer('contact'); ?>
