<?php
/*
Template Name: 問い合わせ - 通常のお問い合わせ
Template Post Type: post,page
*/
?>

<?php get_header('contact');?>



<div id="<?php echo $post->post_name;?>" class="p-mypage">
    <main id="main" class="site-main" role="main">

        <div class="o-fv __page mb-3" style="background-image:url(
                                             <?php
                                             // アイキャッチ画像が設定されているかチェック
                                             if(has_post_thumbnail()){
                                                 // アイキャッチ画像を表示する
                                                 the_post_thumbnail_url( get_the_ID(),'full' );
                                             }else{
                                                 // 代替画像を表示する
                                                 the_field('contact-mvimg', 'option');
                                             }
                                             ?>
                                             )">
            <div class="o-fvCatch">
                <h2 class="o-fvCatch_title o-title __large __white __nobold">
                    <?php the_title(); ?>
                </h2>
            </div>
        </div>

        <section class="l-wrap __maxwmini mb-6">
            <section class="p-postCont p-otherCont p-contact">
                <?php the_content(); ?>

            </section>

        </section>

    </main><!-- #main -->
</div>

<?php get_footer('contact'); ?>
