<?php
/**
 * single-fudoから読み込み
 * single-roomからも活用
 */
	$fudo_gaikan_images = get_field('bukken_gaikan_kyouyou', $parent_post_ID);
	// print_r($fudo_gaikan_images[0]['sizes']);
	if($fudo_gaikan_images != ''){
		$fudoimg_data=$fudo_gaikan_images[0]['sizes']['large'];
		// echo $fudoimg_data;
	}else{
		$fudoimg_data="https://koukyu-chintai.com/wp-content/uploads/cropped-cropped-main-1-2000x1200-1-1024x614.jpg";
	}

	$room_images = get_field('room_situnai_senyu', $post_id);
	// print_r($room_images[0]['sizes']);
	if($room_images != ''){
		$roomimg_data=$room_images[0]['sizes']['full'];
		// echo $roomimg_data;
	}else{
		$roomimg_data="https://koukyu-chintai.com/wp-content/uploads/cropped-cropped-main-1-2000x1200-1-1024x614.jpg";
	}

		

	//賃料
	if(get_field('chinryos', $parent_post_ID)){
		$chinryo_side = get_field('chinryos', $parent_post_ID);
	}else{
		$chinryo_side = get_kakaku_list_for_fudo($parent_post_ID);
	}	

	//間取りリスト
	// if(get_field('bukken_madori_list', $parent_post_ID)){
	// 	$madori_list = get_field('bukken_madori_list', $parent_post_ID);
	// }else{
		$madori_list = get_madori_list_for_fudo($parent_post_ID);
	// }	

	// MAPリンク
	$map_link = "https://www.google.co.jp/maps/place/" . urlencode(get_my_custom_shozaichi_print($parent_post_ID) . get_post_meta($parent_post_ID, 'shozaichimeisho', true) . get_post_meta($parent_post_ID, 'shozaichimeisho2', true));

	//築年月
	$tatemonochikunenn = get_post_meta($parent_post_ID, 'tatemonochikunenn', true);
	if(!empty($tatemonochikunenn)){
		
		if(strpos($tatemonochikunenn, '月')){
			$tatemonochikunenn_data = $tatemonochikunenn . "01";
			$tatemonochikunenn_data = str_replace(array("年","月"), "/", $tatemonochikunenn_data);
		}else{
			$tatemonochikunenn_data = $tatemonochikunenn . "/01";
		}
	}

	//階数
	$tikunensu = get_tikunensu($tatemonochikunenn_data);	
	
	if(get_post_meta($parent_post_ID, 'tatemonokaisu1', true)!="") $kaisu = '地上'.get_post_meta($parent_post_ID, 'tatemonokaisu1', true)."階";
	if(get_post_meta($parent_post_ID, 'tatemonokaisu2', true)!="") $kaisu .= '<br>地下'.get_post_meta($parent_post_ID, 'tatemonokaisu2', true)."階";	

	//問合せリンク
	$contact_link = get_permalink($parent_post_ID) . "#contact_form'";

	//お気に入り追加リンク
	$favorite_link =  get_permalink($post_id) . "?wpfpaction=add&postid=" . $post_id;	

	//空室状況を問い合せるリンク
	$kuusitu_inquiry_link = get_permalink(1560) . "?postid=" . $post_id;	
	
	//物件について質問するリンク
	$kuusitu_inquiry_link = get_permalink(1563) . "?postid=" . $post_id;	
