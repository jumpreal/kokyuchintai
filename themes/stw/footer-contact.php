<footer class="l-footer">
    <div class="l-footerWrap l-wrap __maxw02">
        <div class="l-footerSub flex-nw jc-between ai-center">
            <p class="l-footerSub_copy">©2019 高級賃貸東京 All rights reserved.</p>
            <div class="flex-nw">
                <a class="l-footerSub_sns fb" href="" target="_blank" rel="nofollow"><i class="fab fa-facebook-square"></i>
                </a>
                <a class="l-footerSub_sns tw" href="" target="_blank" rel="nofollow"><i class="fab fa-twitter"></i></a>
            </div>
        </div>
    </div>
</footer>

</div><!-- .site-content-contain -->

</div><!-- #page -->


<?php get_template_part('template/modal-searchform');?>
<?php get_template_part('template/modal-roomimg');?>
<script src="<?php bloginfo('template_directory'); ?>/assets/js/swiper.min.js"></script>


<!--スマホメニュー-->
<script>
    (function($) {
        $(function() {
            var $header = $('#sp-header');
            // Nav Fixed
            $(window).scroll(function() {
                if ($(window).scrollTop() > 350) {
                    $header.addClass('fixed');
                } else {
                    $header.removeClass('fixed');
                }
            });
            // Nav Toggle Button
            $('#js-nav').click(function() {
                this.toggleClass('open');
            });

        });
    })(jQuery);
</script>
<script>
    //基本的なカルーセルタイプ
    var swiper = new Swiper('.__carouselType01', {
        slidesPerView: 1,
        spaceBetween: 10,
        // init: false,
        pagination: {
            el: '.swiper-pagination',
            clickable: true,
        },
        breakpoints: {
            640: {
                slidesPerView: 1,
                spaceBetween: 20,
            },
            768: {
                slidesPerView: 1,
                spaceBetween: 20,
            },
            1024: {
                slidesPerView: 2,
                spaceBetween: 20,
            },
            1424: {
                slidesPerView: 3,
                spaceBetween: 20,
            },
            1860: {
                slidesPerView: 4,
                spaceBetween: 20,
            },
        }
    });

    //オススメ物件
    var swiper = new Swiper('.__carouselType02', {
        slidesPerView: 'auto',
        centeredSlides: true,
        // init: false,
        pagination: {
            el: '.swiper-pagination',
            clickable: true,
        },
        breakpoints: {
            640: {
                slidesPerView: 1,
            },
            820: {
                slidesPerView: 1,
            },
        }
    });
</script>
<script>
    window.WebFontConfig = {
        google: {
            families: ['Lato:400,700,900']
        },
        active: function() {
            sessionStorage.fonts = true;
        }
    };
    (function() {
        var wf = document.createElement('script');
        wf.src = 'https://ajax.googleapis.com/ajax/libs/webfont/1.6.26/webfont.js';
        wf.type = 'text/javascript';
        wf.async = 'true';
        var s = document.getElementsByTagName('script')[0];
        s.parentNode.insertBefore(wf, s);
    })();
</script>
<script>
    (function($) {
        $(function() {

            var navFlg = false;
            $('.l-headerBtn, .l-headerHnav_searchbtn').on('click',function(){
                $('.l-headerBtn_bar').toggleClass('open');
                $('.l-headerHnav').fadeToggle();
                if(!navFlg){
                    $('.gnav__menu__item').each(function(i){
                        $(this).delay(i*100).animate({
                            'margin-left' : 0,
                            'opacity' : 1,
                        },400,'easeOutQuint');
                    });
                    navFlg = true;
                }
                else{
                    $('.l-headerHnav_cont').css({
                        'margin-left' : 100,
                        'opacity' : 0,
                    });
                    navFlg = false;
                }
            });
        });

    })(jQuery);
</script>

</body>

</html>
