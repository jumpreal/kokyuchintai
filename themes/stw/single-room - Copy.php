<?php
/**

 * The Template for displaying room single posts.

 *

 * Template: shingle-room.php

 * 

 */

?>
<?php get_header(); ?>
<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_directory'); ?>/assets/css/single-room.css">




<div class="wrap" id="single-room">

	<header class="page-header">


	</header><!-- .page-header -->

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

<?php
require_once WP_PLUGIN_DIR . '/fudou/inc/inc-archive-fudo.php';
//	require_once locate_template('inc/inc-archive-fudo.php', true);

	//カウント

		$metas_co = 0;

		if($sql !=''){

			//$sql = $wpdb->prepare($sql,'');

			$metas = $wpdb->get_row( $sql );

			if( !empty( $metas ) ){

				$metas_co = $metas->co;

			}

		}else{

			$metas_co = 0;

		}



?>





<?php
if (have_posts()) : while (have_posts()) : the_post();
?>
        <h3><?php the_title(); ?></h3>
        <p class="txtR"><span class="day"><?php the_time('Y年n月j日'); ?></span></p>


		<?php the_content(); ?>

            <!-- STW追加　画像＆テキスト繰り返しフィールド -->

            <?php
echo get_field('room_no')."号室";
echo get_field('floors')."階";
echo "所在地：".get_field('location_details');

//--LiteBox（ギャラリーで編集）
$room_img=get_field('room_img');
if(count($room_img)>0){
echo '<div class="list_picsam">';
foreach($room_img as $rk=>$rv){
echo '<figure>
<a href="'.$rv["sizes"]["large"].'" rel="lightbox['.$post->ID.']"  title="'.$rv["title"].'"><img class="box3image" src="'.$rv["sizes"]["thumbnail"].'" alt="'.$rv["alt"].'" title="'.$rv["title"].'"/></a>
 <figcaption>'.$rv["caption"].'</figcaption>
</figure>
';

}
echo '</div>';
}


$room_in=get_field('room_information');
var_dump($room_in);
if(is_array($room_in)){
	foreach($room_in[0] as $rk=>$rv){
		echo $rv;
	}
}

			
			 if(have_rows('room_no')): ?>

            <?php while(have_rows('room_no')): the_row(); ?>

              <?php $text = get_field('room_no'); if( $text ){ echo $text; }?>

            <?php endwhile; ?>

            <?php endif; ?>

            <!-- STW追加　画像＆テキスト繰り返しフィールド -->

        

<?php endwhile; else: ?>
      <p class="txtC">記事が見つかりませんでした。</p>

<?php endif; ?>

<hr>
<?php
$args = array(
    'connected_type' => 'room_relation', // function.php で登録した紐付けグループの名称
    'connected_items' => get_queried_object() // 紐付けられた記事の情報の取得
);
?>
<?php $loop = new WP_Query( $args ); ?>
<?php if($loop -> have_posts()): ?>
<div id="list_simplepage">
    <?php while($loop -> have_posts()): $loop->the_post(); the_id();?>
        
        
        
        <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
		<?php 
		
							if(!empty($metas)) {



						foreach ( $metas as $meta ) {
							$meta_id=$loop->post->ID;

							$meta_data = get_post($meta_id); 

							$meta_title =  $meta_data->post_title;

							require 'archive-fudo-loop-stw.php';

						}

					}else{

						echo "物件がありませんでした。<br /><br />";



					}
		
//		the_content(); ?>
    <?php endwhile; ?>
</div>
<?php endif; ?>
<?php wp_reset_postdata(); ?>

<?php 

			//SSL

			$fudou_ssl_site_url = get_option('fudou_ssl_site_url');





			//物件問合せ先

			echo '<div id="toiawasesaki">';



			if( $fudou_ssl_site_url != ''){

				//Tweet, Like, Google +1 and Share

				if ( function_exists('disp_social') ) 

					add_filter('the_content', 'disp_social',1);

				//WP Social Bookmarking Light

				if ( function_exists('wp_social_bookmarking_light_the_content') ) 

					add_filter('the_content', 'wp_social_bookmarking_light_the_content');

			}



			$fudo_annnai = get_option('fudo_annnai');

			/*

			 * 物件問合せ先 Filter

			 * 

			 * Version: 1.7.12

			 */

			$fudo_annnai = apply_filters( 'fudo_single_annnai', $fudo_annnai, $post_id );

			$fudo_annnai = apply_filters( 'the_content', $fudo_annnai );
			$be=array(']]>',"\n");
			$af=array(']]&gt;',"<br>");

			$fudo_annnai = str_replace($be,$af,$fudo_annnai);

			echo $fudo_annnai;



			echo '</div>';





			do_action( 'single-fudo5', $post_id );





			if( $kaiin == 1 ) {

			}else{



				if ( $kaiin2 ){



					//SSL

					if( $fudou_ssl_site_url !=''){

						//SSL問合せフォーム

						echo '<div id="ssl_botton" align="center">';

						echo '<a href="'.$fudou_ssl_site_url.'/wp-content/plugins/fudou/themes/contact.php?post_type=fudo&p='.$post_id.'&KeepThis=true&TB_iframe=true&height=500&width=620" ' . $thickbox_class . '>';

						echo '<img src="'.get_option('siteurl').'/wp-content/plugins/fudou/img/ask_botton.jpg" alt="物件お問合せ" title="物件お問合せ" /></a>';

						echo '</div>';

						echo '<br />';

					}else{



						//問合せフォーム

						echo '<div id="contact_form">';



						//Tweet, Like, Google +1 and Share

						if ( function_exists('disp_social') ) 

							add_filter('the_content', 'disp_social',1);

						//WP Social Bookmarking Light

						if ( function_exists('wp_social_bookmarking_light_the_content') ) 

							add_filter('the_content', 'wp_social_bookmarking_light_the_content');



						$fudo_form = get_option('fudo_form');



						/*

						 * 問合せフォーム Filter

						 * 

						 * Version: 1.7.12

						 */

						$fudo_form = apply_filters( 'fudo_single_form', $fudo_form, $post_id );



						$fudo_form = apply_filters( 'the_content', $fudo_form );

						$fudo_form = str_replace( ']]>', ']]&gt;', $fudo_form );

						echo $fudo_form;

						echo '</div>';

					}



				} //kaiin2

			} //kaiin



?>




	  </main><!-- #main -->
<?php do_action( 'fudou_share_buttons_do' ); ?>   
	</div><!-- #primary -->
shingle-room.php
	<?php get_sidebar(); ?>
</div><!-- .wrap -->
<!-- //For Auto -->        
<?php get_footer(); ?>