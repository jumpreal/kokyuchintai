<form role="search" method="get" class="o-form search flex-nw search-form" action="<?php echo esc_url( home_url( '/' ) ); ?>">
    <input type="hidden" name="post_name" value="post">
    <input type="text" value="" name="s" class="o-formInput text __reset" placeholder="例) 銀座 駅近" />
    <button type="submit" class="o-formBtn o-btn"><i class="o-icon flaticon-search"></i></button>
</form>
