<?php

/**
 * 物件・部屋のフッターのときのみ表示
 * 合わせて検討したい物件
 * 関連している物件
 *
 *
 */
if(is_singular('fudo')){
    get_template_part('template/bukkenroom', 'extrainfo');
}

/**
 * 関連
 *
 *
 *
 */
/*get_template_part('template/bukken', 'related');
なぜか過去に見た物件が表示されているのでコメントアウト*/


// TOPオプションからひっぱる
/**
 * 人気の条件
 */
 get_template_part('template/bukken', 'condition');
?>

    <section class="l-contCta l-cont __section">
        <div class="l-wrap __postcont l-row flex-nw-no-xl">
            <div class="l-contCta_left l-rowLeft bigimg flex-nw" style="background-image:url(<?php the_field('cta-img', 'option'); ?>);">
                &nbsp;
            </div>
            <section class="l-contCta_desc l-rowRight bigimg flex-nw jc-center ai-center">
                <div>
                    <p class="l-contCta_catch o-title __min __accent01 __mb-3">
                        <?php the_field('cta-sub', 'option'); ?>
                    </p>
                    <h3 class="l-contCta_title o-title __large mb-3">
                        <?php the_field('cta-title', 'option'); ?>
                    </h3>

                    <?php the_field('cta-desc', 'option'); ?>

                    <a href="<?php echo home_url(); ?>/request/" class="l-contCta_btn o-btn __basic02 __mid">
                        <?php the_field('cta-btn', 'option'); ?></a>
                </div>
            </section>
        </div>
    </section>

<div class="l-contOuter">
<?php
// TOPオプションからひっぱる
/**
 * よく見られている物件
 */
 get_template_part('template/bukken', 'popular');
?>

</div>

<section class="l-contRecent l-cont __section">
    <h2 class="l-contRecent_title o-title __title mb-1">最近見た物件</h2>
    <div class="l-contRecent_cont o-room __compact flex-nw">
        <?php
        // 最近見た物件　ウィジェットから。
        get_template_part('template/bukken', 'recent');
        ?>

    </div>
</section>

<?php
// 新着情報からひっぱる
/**
 * 役立つ高級賃貸の知識
 */
 get_template_part('template/bukken', 'post');
?>





<section class="l-contCompany">
    <div class="l-wrap __postcont l-row flex-nw-no-lg">
        <div class="l-contCompany_left l-rowLeft bigimg flex-nw">

            <img class="l-contCompany_img" src="<?php the_field('company-img', 'option'); ?>" alt="">
            <img class="l-contCompany_logo" src="<?php the_field('company-logo', 'option'); ?>" alt="">
        </div>
        <section class="l-contCompany_desc l-rowRight bigimg flex-nw jc-center ai-center">
            <div>
                <h3 class="l-contCompany_title o-title __large mb-3">
                    <?php the_field('company-title', 'option'); ?>
                </h3>
                <?php the_field('company-desc', 'option'); ?>
                <a href="<?php the_field('company-url', 'option'); ?>" class="l-contCompany_link o-link __mid __text type01">
                    <?php the_field('company-link', 'option'); ?><span class="o-linkItem"></span></a>
            </div>
        </section>
    </div>
</section>

<?php if(have_rows('sppliers-list','option')): ?>
<section class="l-contSppliers">
    <div class="l-wrap flex-nw-no-md">
        <h3 class="l-contSppliers_title o-title __p">取引先企業様</h3>
        <div class="l-contSppliers_list flex-nw ai-center">
            <?php while(have_rows('sppliers-list','option')): the_row(); ?>
            <div class="l-contSppliers_item">
                <a href="<?php the_sub_field('sppliers-list-url'); ?>" rel="nofollow" target="_blank">
                    <img src="<?php the_sub_field('sppliers-list-img'); ?>" alt="" class="l-contSppliers_logo">
                </a>
            </div>
            <?php endwhile; ?>
        </div>
    </div>
</section>
<?php endif; ?>

<footer class="l-footer mt-6">
    <div class="l-footerWrap l-wrap __maxw02">
        <div class="l-footerNav flex-w jc-between">
            <div class="l-footerNav_wrap flex-nw-no-md jc-between">
                <nav class="l-footerNav_cont service">
                    <h4 class="l-footerNav_title o-title __title __en">SERVICE</h4>
                    <?php
                wp_nav_menu( array(
                    'theme_location' => 'footer_service',
                    'container' => 'false',  //コンテナのデフォルト <div> を <nav> に変更
                    'menu_class' => 'l-footerNav_list o-list type01',  //デフォルトの menu-{メニュー名} を変更
                ) );
                ?>
                </nav>
                <nav class="l-footerNav_cont contact">
                    <h4 class="l-footerNav_title o-title __title __en">CONTACT</h4>
                    <?php
                wp_nav_menu( array(
                    'theme_location' => 'footer_contact',
                    'container' => 'false',  //コンテナのデフォルト <div> を <nav> に変更
                    'menu_class' => 'l-footerNav_list o-list __mid type01',  //デフォルトの menu-{メニュー名} を変更
                ) );
                ?>
                </nav>
                <nav class="l-footerNav_cont company">
                    <h4 class="l-footerNav_title o-title __title __en">COMPANY</h4>
                    <?php
                wp_nav_menu( array(
                    'theme_location' => 'footer_company',
                    'container' => 'false',  //コンテナのデフォルト <div> を <nav> に変更
                    'menu_class' => 'l-footerNav_list o-list type01',  //デフォルトの menu-{メニュー名} を変更
                ) );
                ?>
                </nav>
            </div>

            <div class="l-footerContact">
                <div class="l-footerContact_tel o-text __title __bold __en">0120-916-546<span class="l-footerContact_hour">祝休のぞく 10~18時</span></div>
                <a href="" class="l-footerContact_btn o-btn __basic01">お問い合わせ</a>
            </div>



        </div>
        <div class="l-footerSub flex-nw jc-between ai-center">
            <p class="l-footerSub_copy">©2019 高級賃貸東京 All rights reserved.</p>
            <div class="flex-nw">
                <a class="l-footerSub_sns fb" href="" target="_blank" rel="nofollow"><i class="fab fa-facebook-square"></i>
                </a>
                <a class="l-footerSub_sns tw" href="" target="_blank" rel="nofollow"><i class="fab fa-twitter"></i></a>
            </div>
        </div>
    </div>
</footer>

</div><!-- .site-content-contain -->

</div><!-- #page -->


<?php get_template_part('template/modal-searchform');?>
<?php get_template_part('template/modal-roomimg');?>
<script src="<?php bloginfo('template_directory'); ?>/assets/js/swiper.min.js"></script>
<script src='http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.5/jquery-ui.min.js'></script>


<!--スマホメニュー-->
<script>
    (function($) {
        $(function() {
            var $header = $('#sp-header');
            // Nav Fixed
            $(window).scroll(function() {
                if ($(window).scrollTop() > 350) {
                    $header.addClass('fixed');
                } else {
                    $header.removeClass('fixed');
                }
            });
            // Nav Toggle Button
            $('#js-nav').click(function() {
                this.toggleClass('open');
            });



        });

    })(jQuery);
</script>
<script>
    //基本的なカルーセルタイプ
    var swiper = new Swiper('.__carouselType01', {
        slidesPerView: 1.1,
        spaceBetween: 10,
        // init: false,
        pagination: {
            el: '.swiper-pagination',
            clickable: true,
        },
        breakpoints: {
            640: {
                slidesPerView: 1.8,
                spaceBetween: 20,
            },
            820: {
                slidesPerView: 2.3,
                spaceBetween: 20,
            },
            1024: {
                slidesPerView: 2.3,
                spaceBetween: 20,
            },
            1424: {
                slidesPerView: 3,
                spaceBetween: 20,
            },
            1860: {
                slidesPerView: 4,
                spaceBetween: 20,
            },
        }
    });

    //オススメ物件
    var swiper = new Swiper('.__carouselType02', {
        slidesPerView: 1.1,
        spaceBetween: 10,
        // init: false,
        pagination: {
            el: '.swiper-pagination',
            clickable: true,
        },
        breakpoints: {
            640: {
                slidesPerView: 1.8,
                spaceBetween: 20,
            },
            820: {
                slidesPerView: 2.3,
                spaceBetween: 20,
            },
            1024: {
                slidesPerView: 2.3,
                spaceBetween: 20,
            },
            1424: {
                slidesPerView: 2,
                spaceBetween: 20,
            },
            2000: {
                slidesPerView: 3,
                spaceBetween: 20,
            },
        }
    });
</script>
<script>
    window.WebFontConfig = {
        google: {
            families: ['Lato:400,700,900']
        },
        active: function() {
            sessionStorage.fonts = true;
        }
    };
    (function() {
        var wf = document.createElement('script');
        wf.src = 'https://ajax.googleapis.com/ajax/libs/webfont/1.6.26/webfont.js';
        wf.type = 'text/javascript';
        wf.async = 'true';
        var s = document.getElementsByTagName('script')[0];
        s.parentNode.insertBefore(wf, s);
    })();
</script>

<script>
    (function($) {
        $(function() {

            var navFlg = false;
            $('.l-headerBtn, .l-headerHnav_searchbtn').on('click', function() {
                $('.l-headerBtn_bar').toggleClass('open');
                $('.l-headerHnav').fadeToggle();
                if (!navFlg) {
                    $('.l-headerHnav_conta').each(function(i) {
                        $(this).delay(i * 100).animate({
                            'margin-left': 0,
                            'opacity': 1,
                        }, 400, 'easeOutQuint');
                    });
                    navFlg = true;
                } else {
                    $('.l-headerHnav_conta').css({
                        'margin-left': 100,
                        'opacity': 0,
                    });
                    navFlg = false;
                }
            });
        });

    })(jQuery);
</script>

</body>

</html>
