<?php
/**
 * The template for displaying search results pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>

<?php
    global $wp_query;
    $total_results = $wp_query->found_posts;
    $search_query = get_search_query();
?>

<div class="o-fv __archive" style="background-image:url(
                                   <?php
                                   // アイキャッチ画像が設定されているかチェック
                                   if(has_post_thumbnail()){
                                       // アイキャッチ画像を表示する
                                       the_post_thumbnail_url( get_the_ID(),'full' );
                                   }else{
                                       // 代替画像を表示する
                                       the_field('search-mvimg', 'option');
                                   }
                                   ?>
                                   )">
    <?php the_custom_header_markup(); ?>
    <div class="o-fvCatch">
        <h2 class="o-fvCatch_title o-title __large mb-1">
            「<?php echo $search_query; ?>」の検索結果一覧</h2>
        <div class="o-fvCatch_result">
            <span class="o-fvCatch_num o-text __bold __en">
                <?php echo $total_results; ?></span>件 見つかりました。
        </div>
    </div>
    <div class="o-fvBtn flex-nw jc-end ai-center">
        <a href="" class="o-fvBtn_item __save o-btn __basic02"><i class="o-icon __mid flaticon-download"></i>この条件を保存する</a>
        <a id="js-saveModal" class="o-fvBtn_item __term o-btn __basic10"><i class="o-icon __mid __white flaticon-check-mark"></i>保存した条件から探す</a>
    </div>
</div>


<div id="primary" class="content-area">
    <main id="main" class="site-main" role="main">
        <div class="l-main">
            <div class="l-contRoomlist">
                <div class="l-wrap flex-nw jc-between">
                    <div class="o-search side l-sideSearch l-rowLeft search">
                        <?php include('template/side-search.php');?>

                    </div>
                    <div class="l-rowRight searchmain __result">
                        <section class="l-cont roomlist">
                            <div class="l-contRoomlist_term mb-3" style="display:none;">
                                <div class="l-contRoomlist_termWrap __area flex-nw">
                                    <div class="l-contRoomlist_termTitle">エリア</div>
                                    <div class="l-contRoomlist_termDesc">
                                        <div class="l-contRoomlist_termDesc_wrap flex-w">
                                            <span class="l-contRoomlist_termDesc_item"> エリアで指定した内容が並びます。</span>
                                            <span class="l-contRoomlist_termDesc_item"> エリアで指定した内容が並びます。</span>
                                            <span class="l-contRoomlist_termDesc_item"> エリアで指定した内容が並びます。</span>
                                            <span class="l-contRoomlist_termDesc_item"> エリアで指定した内容が並びます。</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="l-contRommlist_termWrap __term flex-nw">
                                    <div class="l-contRoomlist_termTitle">こだわり条件</div>
                                    <div class="l-contRoomlist_termDesc">
                                        <div class="l-contRoomlist_termDesc_wrap flex-w">
                                            <span class="l-contRoomlist_termDesc_item"> エリアで指定した内容が並びます。</span>
                                            <span class="l-contRoomlist_termDesc_item"> エリアで指定した内容が並びます。</span>
                                            <span class="l-contRoomlist_termDesc_item"> エリアで指定した内容が並びます。</span>
                                            <span class="l-contRoomlist_termDesc_item"> エリアで指定した内容が並びます。</span>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="l-contRoomlist_function flex-nw  mb-3" style="display:none;">
                                <div class="l-contRoomlist_functionSelect">
                                    <select class="l-contRoomlist_functionSelect_form o-formSelect __white __sizem __bold __mid" name="example2" size="1"  onChange="location.href=value;">
                                        <?php echo $p_navi;?>
                                    </select>
                                </div>
                                <div class="o-pagenationSelect l-contRoomlist_functionPagenation flex-nw">



                                    <?php //--並べ替えのナビ
                                    echo $page_navigation; ?>

                                </div>
                            </div>

                            <div class="l-contPopular_contflex-nw">
                                <div class="o-room __withroom flex-w l-row">


                                <?php
                                    if( $total_results >0 ):
                                    if(have_posts()):
                                    while(have_posts()): the_post();
                                        $post_type =  $post->post_type;

                                    ?>

                                                                <?php
                                    $bukken_id =  get_the_ID();
                                    include('template/bukken-card-for-search.php');
                                    ?>


                                <?php endwhile; endif; else: ?>



                                <?php echo $search_query; ?> に一致する情報は見つかりませんでした。

                                <?php endif; ?>

                                </div>
                            </div>
                        </section>
                    </div>
                </div>
            </div>
        </div>

    </main><!-- #main -->
</div><!-- #primary -->

<?php get_footer('top'); ?>

<?php include('template/fixednav1.php');?>
