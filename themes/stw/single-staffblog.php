<?php get_header();?>


<div id="staffblog" class="l-cont __main content-area underlayer">
    <main id="main" class="site-main" role="main">

        <div class="l-contSingle flex-nw-no-xl jc-between l-wrap __maxw02 __type02 l-row">
            <div class="p-postMain l-rowLeft post">
                <?php while (have_posts()) : the_post(); ?>

                <article class="p-postSingle">

                    <header class="p-postHeader">
                        <ul class="p-postHeader_meta flex-nw jc-between ai-center">
                            <li class="p-postHeader_cat">
                                <?php the_category(); ?>
                            </li>
                            <li class="p-postHeader_date">
                                <?php the_time('Y.m.d')?>
                            </li>
                        </ul>
                        <h2 class="p-postHeader_title">
                            <?php the_title(); ?>
                        </h2>


                        <ul class="p-postSns __header flex-w ai-center">
                            <?php get_template_part( 'sns-share' ); ?>
                        </ul>

                    </header>


                    <section class="p-postCont">

                        <div class="p-postThumb">
                            <?php the_post_thumbnail('full'); ?>
                        </div>

                        <?php the_content(); ?>




<?php
/**
 * 記事下紹介物件
 * 記事編集ページでACFにて設定
 */
// echo "<p>" . __FILE__ . "の" . __LINE__ . "行目</p>";
$bukken_for_sintyaku = get_field('bukken_for_sintyaku');
// print_r($bukken_for_sintyaku);
if($bukken_for_sintyaku){
?>
<?php
    // foreach($bukken_for_sintyaku as $val){
        $bukken_id = $bukken_for_sintyaku;
        // include('template/bukken-card-for-staffblog.php');
    // }

?>
<?php
}
?>




                    </section>
                    <ul class="p-postSns __footer flex-w ai-center">
                        <?php get_template_part( 'sns-share' ); ?>
                    </ul>

                    <div class="p-postFooter_meta flex-w jc-start">
                        <div class="p-postFooter_cat">
                            <?php the_category(); ?>
                        </div>
                        <div class="p-postFooter_date">
                            <?php the_tags('',''); ?>
                        </div>
                    </div>

                    <div class="p-postAuthor flex-nw" style="display:none;">
                        <div class="p-postAuthor_img">
                            <?php echo get_avatar( get_the_author_meta('ID'), 120 ); ?>
                        </div>
                        <div class="p-postAuthor_desc">
                            <p class="p-postAuthor_title">
                                <?php the_author_meta('display_name'); ?>
                            </p>
                            <p class="p-postAuthor_desc">
                                <?php the_author_meta('user_description'); ?>
                            </p>
                        </div>
                    </div>
                </article>
                <?php endwhile;?>


                <div class="p-postRelated">
                    <?php //rp4wp_children();?>
                </div>

            </div>
            <div class="p-postSide l-rowRight post">
                <?php get_sidebar(); ?>
            </div>
        </div>

    </main><!-- #main -->
</div><!-- #about -->

<?php get_footer('single'); ?>
