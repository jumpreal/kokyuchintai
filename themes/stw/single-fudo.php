<?php

/**

 * The Template for displaying fudou single posts.

 *

 * Template: single-fudo2017.php

 *

 * @package WordPress5.0

 * @subpackage Fudousan Plugin

 * @since Twenty Seventeen

 * Version: 5.0.2

 */



    /**** functions ****/

//    require_once WP_PLUGIN_DIR . '/fudou/inc/inc-single-fudo.php';

require_once get_template_directory().'/inc/inc-single-fudo-stw.php';




    global $is_fudouktai,$is_fudoukaiin;

    global $is_fudoukouku;



    $post_id = isset( $_GET['p'] ) ? myIsNum_f( $_GET['p'] ) : '';

    if( empty($post_id) ){

        $post_id = $post->ID;

    }





    //会員2

    $kaiin = 0;

    if( !is_user_logged_in() && get_post_meta($post_id, 'kaiin', true) > 0 ) $kaiin = 1;

    //ユーザー別会員物件リスト

    $kaiin2 = users_kaiin_bukkenlist( $post_id, get_option( 'kaiin_users_rains_register' ), get_post_meta( $post_id, 'kaiin', true ) );



    //Disable AMP canonical

    if( $kaiin == 1 ){

        add_filter( 'amp_frontend_show_canonical', '__return_false' );

    }





    //title変更 会員

    if ( !my_custom_kaiin_view('kaiin_title',$kaiin,$kaiin2) ){

        //WordPress ～4.3

        add_filter( 'wp_title', 'add_post_type_wp_title_ka' );

        //WordPress 4.4～

        add_filter( 'pre_get_document_title', 'add_post_type_wp_title_ka' );

        //All-in-One-SEO-Pack

        add_filter( 'aioseop_title', 'add_post_type_wp_title_ka' );

    }

    function add_post_type_wp_title_ka( $title = '' ) {

        /**

         * Filter the separator for the document title Fudou.

         * @since 4.4.0

         * @param string $sep Document title separator. Default '-'.

         */

        $sep = apply_filters( 'document_title_separator_fudou', '-' );

        $title =  '会員物件 ' . $sep . ' '. get_bloginfo( 'name', 'display' );

        return $title;

    }



    $post_id_array = get_post( $post_id );

    $title    = $post_id_array->post_title;

    $excerpt  = $post_id_array->post_excerpt;

    $content  = $post_id_array->post_content;

    $modified = $post_id_array->post_modified;



    //newup_mark

    $newup_mark = get_option('newup_mark');

    if($newup_mark == '') $newup_mark=14;



    $post_modified_date =  vsprintf("%d-%02d-%02d", sscanf($modified, "%d-%d-%d"));

    $post_date =  vsprintf("%d-%02d-%02d", sscanf($post_id_array->post_date, "%d-%d-%d"));



    $newup_mark_img =  '';

    if( $newup_mark != 0 && is_numeric($newup_mark) ){



        if( ( abs(strtotime($post_modified_date) - strtotime(date("Y/m/d"))) / (60 * 60 * 24) ) < $newup_mark ){

            if($post_modified_date == $post_date ){

                $newup_mark_img = '<div class="new_mark">new</div>';

            }else{

                $newup_mark_img = '<div class="new_mark">up</div>';

            }

        }

    }





    //会員登録 URL生成 ver1.9.4



        //SSL URL

        $fudou_ssl_site_url = get_option('fudou_ssl_site_url');



        if( defined('FUDOU_SSL_MODE') && FUDOU_SSL_MODE == 1 && $fudou_ssl_site_url !='' ){

            $site_register_url = $fudou_ssl_site_url;

        }else{

            $site_register_url = get_option('siteurl');

        }



        //会員登録URL

        $kaiin_register_url = $site_register_url . '/wp-content/plugins/fudoukaiin/wp-login.php?action=register';



        //Add thickbox class

        if ( wp_is_mobile() ) {

            $thickbox_class = '';

        }else{

            $kaiin_register_url .= '&KeepThis=true&TB_iframe=true&width=400&height=500';

            $thickbox_class = ' class="thickbox"';

        }



        /*

         * 会員登録 URLフィルター

         * ver 1.9.1

         */

        $kaiin_register_url    = apply_filters( 'fudou_kaiin_register_url', $kaiin_register_url );



        // 会員登録 URLタグ

        $kaiin_register_url_tag = '<a href="' . $kaiin_register_url . '" ' . $thickbox_class . ' rel="nofollow">';



        /*

         * 会員登録 URLタグフィルター

         * ver 1.9.3

         */

        $kaiin_register_url_tag     = apply_filters( 'fudou_kaiin_register_url_tag', $kaiin_register_url_tag );



    // .会員登録 URL生成





    /*

     * VIP 物件詳細ページ非表示 404

     *

     * apply_filters( 'fudou_kaiin_vip_single_seigen', $view, $post_id );

     *

     * single_fudoXXXX.php

     *

     * @param bool $view    true:表示  false: 非表示(404)

     * Version: 1.7.5

     */

    if( ! apply_filters( 'fudou_kaiin_vip_single_seigen', true, $post_id ) ){

        status_header( 404 );

        //header( "location: 404.php" );

        wp_redirect( home_url( '/404.php' ) );

        exit();

    }



    status_header( 200 );

    get_header();

    the_post();

?>


<!--<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_directory'); ?>/assets/css/single.css">-->
<?php
    //これを読み込み様々な値をもってくる
    $parent_post_ID = $post_id;
    require('bukken-hensu.php');

    $img1 = @$fudo_gaikan_images[0]['sizes']['large'];
    $img2 = @$fudo_gaikan_images[1]['sizes']['large'];
    $img3 = @$fudo_gaikan_images[2]['sizes']['large'];
    if ( is_null($img1) || is_null($img2) || is_null($img3) ){
        $gai_images = stw_get_singlefudo_images($post_id,2);
        $img1_path = '';
        if ( 0 < count($gai_images) ) {
            $img1_path = $gai_images[0]->image_path;
            $img1 = stw_generate_imgurl($img1_path);
        }
        $h_images = stw_get_singlefudo_images($post_id);
        $h_images_count_without_ban = count($h_images);

        if ( 0 < count($h_images) ){
            foreach ($h_images as $oh ){
                if ( is_null($img1) ){
                    if(!in_array(basename($oh->image_path), get_ban_image_file_name())){
                    $img1 = stw_generate_imgurl($oh->image_path);
                    continue;
                    }else{
                        $h_images_count_without_ban--;
                        continue;
                    }
                }
                if ( is_null($img2) && $oh->image_path != $img1_path ){
                    if(!in_array(basename($oh->image_path), get_ban_image_file_name())){
                    $img2 = stw_generate_imgurl($oh->image_path);
                    continue;
                    }else{
                        $h_images_count_without_ban--;
                        continue;
                    }
                }
                if ( is_null($img3) && $oh->image_path != $img1_path ){
                    if(!in_array(basename($oh->image_path), get_ban_image_file_name())){
                    $img3 = stw_generate_imgurl($oh->image_path);
                    continue;
                    }else{
                        $h_images_count_without_ban--;
                        continue;
                    }
                }
            }
            /*
            $img1 = ( isset($h_images[0]) ) ? stw_generate_imgurl($h_images[0]->image_path) : '';
            $img2 = ( isset($h_images[1]) ) ? stw_generate_imgurl($h_images[1]->image_path) : '';
            $img3 = ( isset($h_images[2]) ) ? stw_generate_imgurl($h_images[2]->image_path) : '';
            */
        }
    }
?>
<section class="o-rsingleFv l-cont mb-1">
    <div class="o-rsingleFv_wrap flex-nw l-row">
      <?php if ( !is_null($img1) ):?>
        <div class="o-rsingleFv_main l-rowLeft room flex-nw">
            <img src="<?php echo $img1;?>" alt="" class="o-rsingleFv_mainimg">
        </div>
      <?php endif;?>
        <div class="o-rsingleFv_sub l-rowRight room flex-w fd-column">
            <?php if ( !is_null($img2) ):?>
                <img src="<?php echo $img2;?>" alt="" class="o-rsingleFv_subimg">
            <?php endif;?>
            <?php if ( !is_null($img3) ):?>
                <img src="<?php echo $img3;?>" alt="" class="o-rsingleFv_subimg">
            <?php endif;?>
        </div>
        <a id="js-fudoimgModal" class="o-link __over"></a>
    </div>
    <div class="o-rsingleFunction flex-nw">
        <a id="js-fudoimgModal2" class="o-rsingleImgbtn"><?php echo (0 == get_bukken_image_count($post_id)) ? $h_images_count_without_ban : get_bukken_image_count($post_id);?>枚の画像</a>
        <?php $fvflg = ( in_array($post_id,$fvpids) ) ? true : false;?>
        <a class="o-rsingleSave" data-fvflg="<?php echo ($fvflg) ? 'sumi' : 'mi';?>" data-pid="<?php echo $post_id;?>" href="<?php echo $favorite_link;?>">
            <i class="o-icon <?php echo ($fvflg) ? 'flaticon-heart __checked' : 'flaticon-heart';?>"></i>
        </a>
    </div>
    <div class="o-rsingleStatus flex-nw ai-center">
        <p class="o-rsingleStatus_title">
            <?php
            if ( get_field('bukken_status') == 2 ){//物件の満室fgがON
                echo "満室";
            }else{
                echo get_rooms_count($post_id) . "部屋募集中";
            }
            ?>

        </p>
    </div>

</section>

<aside class="o-rsingleUpdate flex-nw jc-end o-text __min">
    <p class="o-rsingleUpdate_published">掲載 :
        <?php the_date('Y/m/d');?>
    </p>
    <p class="o-rsingleUpdate_modified">更新 :
        <?php the_modified_date('Y/m/d') ?>
    </p>
</aside>

<main class="o-rsingleMain l-cont __section l-row flex-nw-no-xl">
    <div class="l-rowLeft roomdesc l-row flex-nw-no-big">

        <div class="o-rsingleSummary l-rowLeft roomnav js-fixedTop">


                <ul class="o-rsingleSummary_list js-fixedStart" id="o-rsingleSummary_list">

                    <li class="o-rsingleSummary_item item1"><a href="#section01" class="o-rsingleSummary_link section1">概要</a></li>
                    <li class="o-rsingleSummary_item item2"><a href="#section02" class="o-rsingleSummary_link section2">部屋一覧</a></li>
                    <li class="o-rsingleSummary_item item3"><a href="#section03" class="o-rsingleSummary_link section3">特徴</a></li>
                    <li class="o-rsingleSummary_item item4"><a href="#section04" class="o-rsingleSummary_link section4">物件情報</a></li>
                    <li class="o-rsingleSummary_item item5"><a href="#section05" class="o-rsingleSummary_link section5">備考欄</a></li>
                    <li class="o-rsingleSummary_item item6"><a href="#section06" class="o-rsingleSummary_link section6">周辺情報</a></li>
                    <li class="o-rsingleSummary_item item7"><a href="#section07" class="o-rsingleSummary_link section7">FAQ</a></li>
                    <li class="o-rsingleSummary_item item8"><a href="#section08" class="o-rsingleSummary_link section8">コメント</a></li>

                </ul>


        </div>
        <div class="l-rowRight roomnav">

            <!-- 概要 -->
            <section class="o-rsingleOverview o-rsingleCont" id="section01">
                <h1 class="o-rsingleTitle">
                    <?php the_title(); ?>
                </h1>
                <div class="o-rsingleSpace flex-nw jc-start ai-center">
                    <div class="o-rsingleSpace_item __area"><i class="o-rsingleTag_icon o-icon __mid flaticon-buildings"></i>
                        <?php echo get_senyumenseki_list_for_fudo($post_id);?>
                    </div>
                    <div class="o-rsingleSpace_item __layout"><i class="flaticon-blueprint o-icon __mid"></i>
                        <?php echo $madori_list;?>
                    </div>
                </div>
                <div class="o-rsingleDesc flex-nw jc-between">

                    <div class="o-rsingleInfo">
                        <p class="o-rsingleAddress flex-nw jc-between"><span>
                        <?php my_custom_shozaichi_print($post_id);?>
                                <?php echo get_post_meta($post_id, 'shozaichimeisho', true);?>
                                <?php echo get_post_meta($post_id, 'shozaichimeisho2', true);?>
                                </span><a href="<?php echo $map_link;?>" class="o-rsingleMap o-link __min" target="_blank">MAPで表示</a> </p>
                        <ul class="o-rsingleStation">
                            <li class="o-rsingleStation_list">
                                <?php my_custom_koutsu1_print($post_id);?>
                            </li>
                            <li class="o-rsingleStation_list">
                                <?php my_custom_koutsu2_print($post_id);?>
                            </li>
                            <li class="o-rsingleStation_list">
                                <?php my_custom_koutsu3_print($post_id);?>
                            </li>
                        </ul>
                        <?php the_recommend_speciality($post_id); ?>
                    </div>
                    <div class="o-rsingleBuilding">
                        <ul>
                            <li class="o-rsingleBuilding_item">
                                <?php echo $tikunensu;?>
                            </li>
                            <li class="o-rsingleBuilding_item">
                                <?php my_custom_tatemonokozo_print_br($post_id);?>
                            </li>
                            <li class="o-rsingleBuilding_item">
                                <?php echo $kaisu;?>
                            </li>
                        </ul>
                    </div>

                </div>
            </section>

            <!-- 概要 -->
            <section class="o-rsingleMemo o-rsingleCont" id="section01">
                <?php
                // 社内用メモ
                // 管理者のみに表示
                $user_id = get_current_user_id();
                $user_info = get_userdata( $user_id );
                if(is_user_logged_in()){
                    $user_roles = implode(', ', $user_info->roles);

                    if(in_array("administrator", $user_info->roles)){
                        echo "<div class='o-rsingleMemo_cont'>";
                        echo "<p class='mb-1'>管理権限保持者にのみこのブロックは表示されています。</p>";
                        echo "<p class='o-rsingleMemo_text'><strong class='o-rsingleMemo_title'>社内用メモ：</strong><span class='o-rsingleMemo_desc'>" . get_post_meta($post_id, 'shanaimemo', true) . "</span></p>";
                        echo "<p class='o-rsingleMemo_text'><strong class='o-rsingleMemo_title'>報酬形態：</strong><span class='o-rsingleMemo_desc'>" . get_post_meta($post_id, 'houshoukeitai', true) . "</span></p>";

                        echo "<p class='o-rsingleMemo_text'><strong class='o-rsingleMemo_title'>元付け 名称：</strong><span class='o-rsingleMemo_desc'>" . get_post_meta($post_id, 'motozukemei', true) . "</span></p>";
                        echo "<p class='o-rsingleMemo_text'><strong class='o-rsingleMemo_title'>元付け 電話番号：</strong><span class='o-rsingleMemo_desc'>" . get_post_meta($post_id, 'motozuketel', true) . "</span></p>";
                        echo "<p class='o-rsingleMemo_text'><strong class='o-rsingleMemo_title'>元付け FAX番号：</strong><span class='o-rsingleMemo_desc'>" . get_post_meta($post_id, 'motozukefax', true) . "</span></p>";
                        echo "<p class='o-rsingleMemo_text'><strong class='o-rsingleMemo_title'>社内備考１：</strong><span class='o-rsingleMemo_desc'>" . get_post_meta($post_id, 'shanaibiko', true) . "</span></p>";
                        echo "<p class='o-rsingleMemo_text'><strong class='o-rsingleMemo_title'>社内備考２：</strong><span class='o-rsingleMemo_desc'>" . get_post_meta($post_id, 'shanaibiko2', true) . "</span></p>";
                        echo "</div>";
                    }
                }
                ?>
            </section>


            <!-- スマホのみ表示  -->
            <?php
            $ua=$_SERVER['HTTP_USER_AGENT'];
            $browser = ((strpos($ua,'iPhone')!==false)||(strpos($ua,'iPod')!==false)||(strpos($ua,'Android')!==false));
            if ($browser == true){
                $browser = 'sp';
            }
            if($browser == 'sp'){
            ?>
            <div class="o-rsingleAction o-rsingleCont">
                <div class="o-rsingleAction __building">
                    <div class="o-rsingleAction_wrap">
                        <div class="o-rsingleAction_price o-rsingleAction_cont">
                            <?php echo $chinryo_side;?> <span class="o-rsingleAction_unit"> 円/月</span></div>

                        <div class="o-rsingleAction_function o-rsingleAction_cont flex-nw jc-center ai-center">

                            <a href="<?php echo $favorite_link;?>" class="o-rsingleAction_functionItem __save flex-nw ai-center" data-fvflg="<?php echo ($fvflg) ? 'sumi' : 'mi';?>" data-pid="<?php echo $post_id;?>">
                                <i class="o-icon __min flaticon-love"></i><span class="o-rsingleAction_functionText"><?php echo ($fvflg) ? '検討リストから削除する' : '検討リストに追加する';?></span>
                            </a>
                        </div>
                        <div class="o-rsingleAction_btn o-rsingleAction_cont">
                            <a href="<?php echo home_url() . "/" . get_field('kengaku_nairan_link', 'option');?>" class="o-rsingleAction_btnVisit o-btn __basic01">見学・内覧する</a>
                            <div class="flex-nw">
                                <a href="<?php echo home_url() . "/" . get_field('kuusitu_inquiry_link', 'option');?>?post_id=<?php echo $post_id;?>" class="o-rsingleAction_btnEmpty o-btn __basic06 flex-nw jc-center ai-center fb-50">空室状況を<br>問い合わせる</a>
                                <a href="<?php echo home_url() . "/" . get_field('bukken_inquiry_link', 'option');?>" class="o-rsingleAction_btnQuestion o-btn __basic06 flex-nw jc-center ai-center fb-50">物件について<br>質問する</a>
                            </div>
                        </div>
                        <div class="o-rsingleAction_tel u-text-ac">
                            <p class="o-rsingleAction_telTitle">電話で問い合わせする</p>
                            <p class="o-rsingleAction_telTel"><?php the_field('free_dial', 'option');?></p>
                            <div class="o-rsingleAction_telInfo flex-nw jc-center ai-center">問い合わせ物件番号 : <?php echo $post_id;//get_post_meta($post_id, 'shikibesu', true);?></div>
                        </div>
                    </div>
                    <div class="o-rsingleAction_sns flex-nw jc-center ai-center"></div>
                </div>

            </div>
            <?php }else{ ?>
            <?php } ?>


            <!-- 同じ物件のお部屋 -->
            <section class="o-rsingleRoom o-rsingleCont o-partsMore" id="section02">
                <h2 class="o-rsingleCont_title flex-nw ai-center">この物件のお部屋<span class="o-rsingleCont_en">ROOMS</span></h2>
                <div class="o-rsingleRoom_building o-bnr overimg mb-3" style="display:none;">
                    <div class="o-bnrWrap">
                        <div class="o-rsingleRoom_thumb o-bnrThumb">
                            <img src="https://koukyu-chintai.com/wp-content/uploads/cropped-cropped-main-1-2000x1200-1-1024x614.jpg" alt="" class="o-rsingleRoom_img o-bnrImg">
                        </div>
                        <h3 class="o-rsingleRoom_title o-bnrTitle o-title __title __bold">
                            <?php the_title();?>
                        </h3>
                        <a href="" class="o-link __over"></a>
                    </div>
                </div>


                <?php
                //--roomに関連の親情報を集める
                $args = array(
                    'connected_type' => 'room_relation',
                    'connected_items' => $post_id,
                    'posts_per_page' => 20,

                    'meta_query' => array(
                        // 'relation' => 'AND',
                        // 'meta_nyukyogenkyo' => array(
                        //         'relation' => 'OR',
                        //         array(
                        //             'relation' => 'OR',
                        //             array(
                        //             'key' => 'nyukyogenkyo',
                        //             'type' => 'numeric',
                        //             'value' => 3),//即入居可
                        //             array(
                        //                 'key' => 'nyukyogenkyo',
                        //                 'type' => 'numeric',
                        //                 'value' => 2),//居住中
                        //             array(
                        //                 'key' => 'nyukyogenkyo',
                        //                 'type' => 'numeric',
                        //                 'value' => 11),//空室
                        //             array(
                        //                 'key' => 'nyukyogenkyo',
                        //                 'type' => 'numeric',
                        //                 'value' => 1),//空室
                        //             array(
                        //                 'key' =>  'nyukyogenkyo',
                        //                 'value' => '*'
                        //             )
                        //         ),
                        //     ),
                            'meta_kakaku' => array(
                                'key'=>'kakaku',
                                'type' => 'numeric',
                                'compare' => 'EXISTS',
                                )
                    ),
                      'orderby' => array( 'nyukyogenkyo' => 'DESC','meta_kakaku' => 'ASC'),
                    //   'order' => 'DESC'
                );
                ?>

                <div class="o-rsingleRoom_list flex-w o-partsMore">
                    <?php include('template/rooms-for-bukken-for-detail.php');?>
                </div>

            </section>

            <!-- 特徴 -->
            <?php if( have_rows('bukken-tokutyou') ): ?>

            <section class="o-rsingleSpecial o-rsingleCont" id="section03">
                <h2 class="o-rsingleCont_title flex-nw ai-center">物件の特徴<span class="o-rsingleCont_en">CHARACTERISTIC</span></h2>


                <?php while( have_rows('bukken-tokutyou') ): the_row();

                $image = get_sub_field('bukken-tokutyou-img');
                // $image_src = $image['sizes']['large'];

                    ?>

                <div class="o-rsinglePost">
                    <div class="o-rsinglePost_thumb">
                        <img class="o-rsinglePost_img" src="<?php echo $image;?>" alt="">
                    </div>
                    <section class="o-rsinglePost_cont o-rsingleCont_wrap">
                        <div class="p-postCont __singleroom o-partsMore">
                            <input id="o-partsMore_triggerSpecial" class="o-partsMore_trigger" type="checkbox">
                            <label class="o-partsMore_btn" for="o-partsMore_triggerSpecial"><i class="o-partsMore_icon o-icon fas fa-angle-down"></i></label>
                            <div class="p-postCont __singleroom o-partsMore_cont">
                                <p class="p-postCont_text">
                                    <?php the_sub_field('bukken-tokutyou-text');?>

                                </p>
                            </div>
                        </div>
                    </section>
                </div>

                <?php endwhile; ?>

            </section>

            <?php endif; ?>

            <!-- 共有・室内設備 -->
            <section class="o-rsingleFacility o-rsingleCont" id="section04">
                <h2 class="o-rsingleCont_title flex-nw ai-center">建物設備・特徴<span class="o-rsingleCont_en">Facility & Characteristic</span></h2>

                <section class=" o-rsingleCont_wrap">
                    <h3 class="o-title __mid __bold mb-1 flex-nw ai-center"><i class="o-icon __title __nobold flaticon-construction"></i>共有設備</h3>
                    <ul class="o-list __mid __basic01 square flex-w ai-center">
                        <?php my_custom_kyouyu_setsubi_print($post_id); ?>
                    </ul>
                </section>

                <?php
                if(!empty(get_field('setsubisonota2', $post_id))){
                ?>
                
                <section class=" o-rsingleCont_wrap">
                    <h3 class="o-title __mid __bold mb-1 flex-nw ai-center"><i class="o-icon __title __nobold flaticon-construction"></i>その他の条件・設備</h3>
                    <ul class="o-list __mid __basic01 square flex-w ai-center">
                        <?php echo nl2br(get_field('setsubisonota2', $post_id)); ?>
                    </ul>
                </section>
                
                <?php
                }
                ?>


            </section>

            <!-- 物件情報 -->
            <section class="o-rsingleBuilding o-rsingleCont" id="section05">
                <h2 class="o-rsingleCont_title flex-nw ai-center">物件情報<span class="o-rsingleCont_en">BUILDING</span></h2>
                <section class="o-rsingleCont_wrap">
                    <table class="o-table building __mid __sizem">
                        <tr>
                            <th>物件名</th>
                            <td>
                                <?php the_title();?>
                            </td>
                        </tr>
                        <tr>
                            <th>築年数</th>
                            <td>
                                <?php

                            $tatemonochikunenn = get_post_meta($post_id, 'tatemonochikunenn', true);
                            if(!empty($tatemonochikunenn)){

                                if(strpos($tatemonochikunenn, '月')){
                                    $tatemonochikunenn_data = $tatemonochikunenn . "01";
                                    $tatemonochikunenn_data = str_replace(array("年","月"), "/", $tatemonochikunenn_data);
                                }else{
                                    $tatemonochikunenn_data = $tatemonochikunenn . "/01";
                                }
                            }

                            echo $tatemonochikunenn;
                            echo get_sintiku_mark($tatemonochikunenn_data);

                            ?>
                            </td>
                        </tr>
                        <tr>
                            <th>建物階数</th>
                            <td>
                                <?php
                            if(get_post_meta($post_id, 'tatemonokaisu1', true)!="") echo '地上'.get_post_meta($post_id, 'tatemonokaisu1', true)."階";
                            if(get_post_meta($post_id, 'tatemonokaisu2', true)!="") echo '<br>地下'.get_post_meta($post_id, 'tatemonokaisu2', true)."階";
                            ?>
                            </td>
                        </tr>
                        <tr>
                            <th>建物構造</th>
                            <td>
                                <?php my_custom_tatemonokozo_print($post_id);?></td>
                        </tr>
                        <tr>
                            <th>総戸数</th>
                            <td>
                                <?php echo get_post_meta($post_id, 'bukkensoukosu', true);?>戸
                            </td>
                        </tr>
                        <tr>
                            <th>交通</th>
                            <td>
                                <?php my_custom_koutsu_all($post_id);?>
                            </td>
                        </tr>
                        <tr>
                            <th>間取り</th>
                            <td>
                                <?php echo $madori_list;?>
                            </td>
                        </tr>
                        <tr>
                            <th>専有面積</th>
                            <td>
                                <?php echo get_senyumenseki_list_for_fudo($post_id);?>
                            </td>

                        <?php if(get_field('bukken_status') !== "2"){ //満室でなはい時表示　?>
                        <tr>
                            <th>賃料</th>
                            <td>
                            <?php
                            // the_field('bukken_status');
                                if(get_field('chinryos')){
                                    the_field('chinryos');
                                }else{
                                    echo get_kakaku_list_for_fudo($post_id);
                                }
                            ?>
                            </td>
                        </tr>
                        <tr>
                            <th>共益・管理費</th>
                            <td>
                                <?php
                                    echo @get_management_fee_list_for_fudo($post_id);
                                ?>
                            </td>
                        </tr>
                        <tr>
                            <th>仲介手数料</th>
                            <td>
                                <?php
                                    // echo @get_tyuukai_tesuuryou_list_for_fudo($post_id);
                                    the_field('chukai_tesuryou', $post_id);
                                ?>
                            </td>
                        </tr>
                        <?php }//満室 ?>
                        <tr>
                            <th>取引態様</th>
                            <td><?php my_custom_torihikitaiyo_print($post_id); ?></td>
                        </tr>
                        <?php if(get_post_meta($parent_post_ID,'kanrininn',true) !='') { ?>
                        <tr>
                            <th>管理人</th>
                            <td><?php my_custom_kanrininn_print($post_id); ?></td>
                        </tr>
                        <?php } ?>
                        <?php //if(get_field('bukken_status') !== "2"){ //満室でなはい時表示　?>
                        <tr>
                            <th>駐車場</th>
                            <td>
                                <?php the_custom_chushajokubun_print($post_id); ?>
                            </td>
                        </tr>
                        <?php //}//満室?>
                        <?php if(get_custom_gakku($post_id)){ ?>
                        <tr>
                            <th>学区域</th>
                            <td>
                                <?php the_custom_gakku($post_id); ?>
                            </td>
                        </tr>
                        <?php } ?>
                        <tr>
                            <th>所在地</th>
                            <td>
                                <?php my_custom_shozaichi_print($post_id);?>
                                <?php echo get_post_meta($post_id, 'shozaichimeisho', true);?>
                                <?php echo get_post_meta($post_id, 'shozaichimeisho2', true);?>
                            </td>
                        </tr>
                    </table>
                    <!-- 地図
                     include('template/googlemap.php');?>
                    <!-- // 地図 -->

                </section>
            </section>


            <!-- 備考情報 -->
            <?php if(get_field('bukken-remarks-title',$post_id)){ ?>
            <section class="o-rsingleRemarks o-rsingleCont" id="section06">
                <h2 class="o-rsingleCont_title flex-nw ai-center">備考欄<span class="o-rsingleCont_en">REMARKS</span></h2>
                <div class="o-rsinglePost">
                    <section class="o-rsinglePost_cont o-rsingleCont_wrap">

                        <div class="p-postCont __singleroom o-partsMore">
                            <input id="o-partsMore_triggerRemarks" class="o-partsMore_trigger" type="checkbox">
                            <label class="o-partsMore_btn" for="o-partsMore_triggerRemarks"><i class="o-partsMore_icon o-icon fas fa-angle-down"></i></label>
                            <div class="p-postCont __singleroom o-partsMore_cont">

                                <h2 class="o-title __title" style="display:none;">
                                    <?php the_field('bukken-remarks-title',$post_id);?>
                                </h2>
                                <p class="p-postCont_text">
                                    <?php the_field('bukken-remarks',$post_id);?>
                                </p>
                            </div>
                        </div>


                    </section>
                </div>
            </section>
            <?php } ?>


            <!-- 周辺情報 -->
            <?php if(my_custom_koutsu1_for_nearby($post_id)){ ?>
            <section class="o-rsingleLocation o-rsingleCont" id="section07">
                <h2 class="o-rsingleCont_title flex-nw ai-center">周辺情報<span class="o-rsingleCont_en">LOCATION</span></h2>
                <div class="o-rsinglePost">
                    <div class="o-rsingleRoom_building o-bnr overimg">
                        <div class="o-bnrWrap">
                            <div class="o-rsingleRoom_thumb o-bnrThumb">
                                <img src="https://koukyu-chintai.com/wp-content/uploads/cropped-cropped-main-1-2000x1200-1-1024x614.jpg" alt="" class="o-rsingleRoom_img o-bnrImg">
                            </div>
                            <h3 class="o-rsingleRoom_title o-bnrTitle o-title __title __bold">
                                <?php
                            $koutsurosen_data = get_post_meta($post_id, 'koutsurosen1', true);
                            $koutsueki_data = get_post_meta($post_id, 'koutsueki1', true);
                            echo my_custom_koutsu1_for_nearby_distance($post_id)."の周辺情報";
                            ?>
                            </h3>
                        </div>
                    </div>
                    <section class="o-rsinglePost_cont o-rsingleCont_wrap">
                        <div class="p-postCont __singleroom o-partsMore">
                            <input id="o-partsMore_trigger" class="o-partsMore_trigger" type="checkbox">
                            <label class="o-partsMore_btn" for="o-partsMore_trigger"><i class="o-partsMore_icon o-icon fas fa-angle-down"></i></label>
                            <div class="o-partsMore_cont">
                                    <?php echo my_custom_koutsu1_for_nearby($post_id);?>
                            </div>
                        </div>
                    </section>
                </div>
            </section>
            <?php } ?>
            <!-- よくあるご質問 -->

            <section class="o-rsingleFaq o-rsingleCont" id="section08">
                <?php include('template/faq.php');?>

            </section>

            <!-- コメント -->
            <section class="o-rsingleComment o-rsingleCont" id="section09">
                <h2 class="o-rsingleCont_title flex-nw ai-center">コメント<span class="o-rsingleCont_en">COMMENT</span></h2>

                <div class="o-rsingleComment_cont __main o-rsingleCont_wrap">
                    <?php  if( FUDOU_TRA_COMMENT )     comments_template( '', true ); ?>
                </div>

                <div class="o-rsingleComment_cont __posting o-rsingleCont_wrap">
                    <?php $args = array(
                // 'title_reply' => '',
                'title_reply' => $post->post_title.'へコメント投稿',
                'label_submit' => '投稿する',
                'comment_notes_before' => '<p class="o-rsingleComment_postTitle">コメントを投稿する<i class="o-rsingleComment_postTitle_icon o-icon far fa-angle-down"></i></p>',
                'comment_notes_after'  => '',
                'fields' => array(
                        'author'    => '<div class="o-rsingleComment_form flex-nw"><span class="o-rsingleComment_formTitle">お名前</span><input type="text" name="author" class="o-rsingleComment_formInput o-formInput __sizem"></div>',
                        ),
                'logged_in_as'=>false,
                'comment_field' => '<div class="o-rsingleComment_form flex-nw"><span class="o-rsingleComment_formTitle">コメント内容</span><input type="textarea" name="comment" class="o-rsingleComment_formInput o-formInput __sizem"></div>',
                );
            comment_form( $args );
            ?>
                </div>
            </section>

            <!-- 注意書き -->
            <section class="o-rsingleCaution o-rsingleCont">
                <div class="o-rsingleCaution_wrap">
                    <h2 class="o-rsingleCaution_title o-title __p mb-1">注意書き</h2>
                    <ul class="o-rsingleCaution_list o-list __basic01 caution __mid">
                        <li class="o-rsingleCaution_item o-listItem">建物周辺情報はGoogleMapを使用しており、物件の表示位置が実際の場所と多少異なる場合があります。</li>
                        <li class="o-rsingleCaution_item o-listItem">掲載情報が現況と異なる場合は、現況を優先させていただきます。</li>
                        <li class="o-rsingleCaution_item o-listItem">部屋によって敷金・礼金の内容が異なる場合がございますので、詳しい情報は各部屋ページにてご確認ください。</li>
                        <li class="o-rsingleCaution_item o-listItem">分譲賃貸は部屋によってオーナーが異なる為、内装や設備が変更されている場合がございます。</li>
                        <li class="o-rsingleCaution_item o-listItem">ペットの飼育やSOHO利用に関しては、建物自体が許可を出していても、所有者の意向により禁止とされている場合もございますのでご注意ください。</li>
                        <li class="o-rsingleCaution_item o-listItem">駐車場ありの場合、空き状況等詳細につきましてはお問い合わせください。</li>
                        <li class="o-rsingleCaution_item o-listItem">物件の棟ページに掲載されてる室内写真は、該当物件の代表的な室内写真またはモデルルームの写真を掲載しております。</li>
                    </ul>
                </div>
            </section>

            <!-- 機能ボタン群 -->
            <section class="o-rsingleBtn o-rsingleCont">
                <div class="o-rsingleBtn_wrap flex-nw jc-center">
                    <a href="" class="o-rsingleBtn_item __prev o-btn __basic02 __mid" onclick="javascript:window.history.back(-1);return false;"><i class="o-icon __mid mr-1 fas fa-angle-left"></i>前のページに戻る</a>
                    <a id="js-saveModal" href="#" class="o-rsingleBtn_item o-btn __basic02 __mid"><i class="o-icon __mid flaticon-history"></i>過去の検索履歴</a>
                    <a href="" class="o-rsingleBtn_item o-btn __basic02 __mid js-modal"><i class="o-icon __mid flaticon-search"></i>検索条件を変更する</a>
                </div>
            </section>

        </div>
    </div>
    <div class="l-rowRight roomdesc js-fixedTop">
        <div class="o-rsingleAction __building js-fixedStart">
            <div class="o-rsingleAction_wrap">
                <div class="o-rsingleAction_price o-rsingleAction_cont">
                    <?php
                    if ( get_field('bukken_status') == 2 ){//物件の満室fgがON
                        echo '現在募集がございません';
                    }else{
                        echo $chinryo_side;
                    }

                    ?> </div>

                <div class="o-rsingleAction_function o-rsingleAction_cont flex-nw jc-center ai-center">

                    <a href="<?php echo $favorite_link;?>" class="o-rsingleAction_functionItem __save flex-nw ai-center" data-fvflg="<?php echo ($fvflg) ? 'sumi' : 'mi';?>" data-pid="<?php echo $post_id;?>">
                        <i class="o-icon __min <?php echo ($fvflg) ? 'flaticon-heart __checked' : 'flaticon-heart';?>"></i><span class="o-rsingleAction_functionText"><?php echo ($fvflg) ? '検討リストから削除する' : '検討リストに追加する';?></span>
                    </a>
                </div>
                <div class="o-rsingleAction_btn o-rsingleAction_cont">

                <?php
                if ( get_field('bukken_status') == 2 ){//物件の満室fgがON
                ?>
                <a href="#awasete" class="o-rsingleAction_btnVisit o-btn __basic01">条件が近い物件を確認する</a>
                <?php }else{ ?>
                    <a href="<?php echo home_url() . "/" . get_field('kengaku_nairan_link', 'option');?>?post_id=<?php echo $post_id;?>" class="o-rsingleAction_btnVisit o-btn __basic01">見学・内覧する</a>
                <?php } ?>
                    <div class="flex-nw">
                        <a href="<?php echo home_url() . "/" . get_field('kuusitu_inquiry_link', 'option');?>?post_id=<?php echo $post_id;?>" class="o-rsingleAction_btnEmpty o-btn __basic06 flex-nw jc-center ai-center fb-50">空室状況を<br>問い合わせる</a>
                        <a href="<?php echo home_url() . "/" . get_field('bukken_inquiry_link', 'option');?>?post_id=<?php echo $post_id;?>" class="o-rsingleAction_btnQuestion o-btn __basic06 flex-nw jc-center ai-center fb-50">物件について<br>質問する</a>
                    </div>
                </div>
                <div class="o-rsingleAction_tel u-text-ac">
                    <p class="o-rsingleAction_telTitle">電話で問い合わせする</p>
                    <p class="o-rsingleAction_telTel"><?php the_field('free_dial', 'option');?></p>
                    <div class="o-rsingleAction_telInfo flex-nw jc-center ai-center">問い合わせ物件番号 : <?php echo $post_id;//get_post_meta($post_id, 'shikibesu', true);?></div>
                </div>
            </div>
            <div class="o-rsingleAction_sns flex-nw jc-center ai-center"></div>
        </div>
        <div class="js-fixedEnd"></div>

    </div>
</main>

<div id="primary" class="content-area">
    <main id="main" class="site-main" role="main">

        <article class="fudo-ul-flex">
            <section class="fudo-s-ul-contents">

                <?php do_action( 'single-fudo0', $post_id ); ?>

                <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>




                    <?php if ( post_password_required() ){ //パスワード保護あり ?>


                    <div id="list_simplepage2">
                        <div id="post-<?php the_ID(); ?>">
                            <div class="list_simple_box entry-content">
                                <?php the_content();?>
                            </div><!-- .list_simple_box -->
                        </div>
                    </div><!-- #list_simplepage2 -->


                    <?php }else{ //パスワード保護無し ?>


                    <!-- ここから物件詳細情報 -->
                    <div id="list_simplepage2">
                        <div id="post-<?php the_ID(); ?>">
                            <?php do_action( 'single-fudo1', $post_id ); ?>
                            <div class="list_simple_box">
                                <?php
                    //Gutenberg 利用
                    if ( get_option( 'fudo_use_gutenberg' ) == 'true' ) {     //Gutenberg 利用の場合
                    ?>
                                <?php }else{
                        //抜粋
                        if ( my_custom_kaiin_view('kaiin_excerpt',$kaiin,$kaiin2) && $excerpt ){
                            //echo '<div class="entry-excerpt">' . $excerpt . '</div>';
                        }
                    ?>
                                <?php } ?>


                                <!-- フリーテキスト欄 -->
                                <?php
            //Gutenberg 利用
            if ( get_option( 'fudo_use_gutenberg' ) == 'true' ) {     //Gutenberg 利用の場合
            ?>
                                <?php }else{ ?>
                                <!-- .entry-content -->
                                <div class="entry-content">
                                    <?php the_content();?>
                                </div>
                                <!-- .entry-content -->
                                <?php } ?>
                                <!-- // フリーテキスト欄 -->

                                <?php if( $kaiin == 1 ) { ?>
                                <div class="entry-content">
                                    <?php if( $is_fudoukaiin && get_option('kaiin_users_can_register') == 1 ){ ?>
                                    <br />
                                    この物件は、「会員様にのみ限定公開」している物件です。<br />
                                    非公開物件につき、詳細情報の閲覧には会員ログインが必要です。<br />
                                    非公開物件を閲覧・資料請求するには会員登録が必要です。<br />

                                    <?php if( get_option('kaiin_moushikomi') != 1 ){ ?>

                                    まだ会員登録をしていない方は、簡単に会員登録ができますので是非ご登録ください。<br />
                                    <br />

                                    <div align="center">
                                        <?php echo $kaiin_register_url_tag;?><img src="<?php echo get_option('siteurl'); ?>/wp-content/plugins/fudou/img/kaiin_botton.jpg" alt="会員登録" /></a>
                                    </div>

                                    <?php } ?>

                                    <br />
                                    <br />

                                    <?php }else{ ?>

                                    <br />
                                    この物件は、閲覧できません。<br />
                                    <br />
                                    <br />

                                    <?php }



                        echo '</div><!-- .entry-content -->';


                    }else{



                        //ユーザー別会員物件リスト

                        if ( $kaiin2 === false ) {



                            echo '<div class="entry-content">';



                            if( get_post_meta( $post_id, 'kaiin', true ) > 1 ){

                                //VIP

                                echo '<br />';

                                echo 'この物件は、閲覧できません。<br />';

                                echo '<br />';

                            }else{

                                echo '<br />';

                                echo 'この物件は、「閲覧条件に合った物件のみ公開」している物件です。<br />';

                                echo '条件変更をする事で閲覧ができますので、閲覧条件の登録・変更をしてください。<br />';

                                echo '<br />';

                                echo '<div align="center">';

                                echo '<div id="maching_mail"><a href="'.WP_PLUGIN_URL.'/fudoumail/fudou_user.php?KeepThis=true&TB_iframe=true&height=500&width=680" ' . $thickbox_class . '>';

                                echo '閲覧条件・メール設定</a></div>';



                                echo '</div>';

                                echo '<br />';

                            }



                            echo '</div><!-- .entry-content -->';





                        }else{

                ?>



                                    <?php

                            //Gutenberg 利用

                            if ( get_option( 'fudo_use_gutenberg' ) == 'true' ) {     //Gutenberg 利用の場合

                            ?>

                                    <!-- .entry-content -->

                                    <div class="entry-content">

                                        <?php the_content();?>

                                    </div>

                                    <!-- .entry-content -->

                                    <?php }else{ ?>

                                    <?php } ?>





                                    <!-- ここから左ブロック -->

                                    <div class="list_picsam">

                                        <?php



                                //画像

                                if (!defined('FUDOU_IMG_MAX')){

                                    $fudou_img_max = 10;

                                }else{

                                    $fudou_img_max = FUDOU_IMG_MAX;

                                }



                                //サムネイル画像

                                $img_path = get_option('upload_path');

                                if ($img_path == '')    $img_path = 'wp-content/uploads';



                                // for( $imgid=1; $imgid<=10; $imgid++ ){



                                //     $fudoimg_data = get_post_meta($post_id, "fudoimg$imgid", true);

                                //     $fudoimgcomment_data = get_post_meta($post_id, "fudoimgcomment$imgid", true);

                                //     $fudoimg_alt = $fudoimgcomment_data . my_custom_fudoimgtype_print(get_post_meta($post_id, "fudoimgtype$imgid", true));



                                //     if($fudoimg_data !="" ){



                                //         /*

                                //          * Add url fudoimg_data Pre

                                //          *

                                //          * Version: 1.7.12

                                //          **/

                                //         $fudoimg_data = apply_filters( 'pre_fudoimg_data_add_url', $fudoimg_data, $post_id );



                                //         //Check URL

                                //         if ( checkurl_fudou( $fudoimg_data )) {

                                //             echo '<a href="' . $fudoimg_data . '" rel="lightbox lytebox['.$post_id.']" title="'.$fudoimg_alt.'">';

                                //             echo '<img class="box3image" src="' . $fudoimg_data . '" alt="' . $fudoimg_alt . '" title="' . $fudoimg_alt . '" /></a>';



                                //         }else{

                                //             //Check attachment

                                //             $sql  = "SELECT P.ID,P.guid";

                                //             $sql .= " FROM $wpdb->posts AS P";

                                //             $sql .= " WHERE P.post_type ='attachment' AND P.guid LIKE '%/$fudoimg_data' ";

                                //             //$sql = $wpdb->prepare($sql,'');

                                //             $metas = $wpdb->get_row( $sql );

                                //             $attachmentid = '';

                                //             if( !empty($metas) ){

                                //                 $attachmentid  =  $metas->ID;

                                //             }



                                //             /*

                                //              * fudoimg_data to attachmentid

                                //              *

                                //              * Version: 1.9.2

                                //              **/

                                //             $attachmentid = apply_filters( 'fudoimg_data_to_attachmentid', $attachmentid, $fudoimg_data, $post_id );



                                //             if($attachmentid !=''){

                                //                 //thumbnail、medium、large、full

                                //                 $fudoimg_data1 = wp_get_attachment_image_src( $attachmentid, 'thumbnail');

                                //                 $fudoimg_url = $fudoimg_data1[0];



                                //                 //light-box v1.7.9 SSL

                                //                 $large_guid_url = wp_get_attachment_image_src( $attachmentid, 'large' );

                                //                 if( $large_guid_url[0] ){

                                //                     $guid_url = $large_guid_url[0];

                                //                 }else{

                                //                     $guid_url = get_the_guid( $attachmentid );

                                //                     if( is_ssl() ){

                                //                         $guid_url= str_replace( 'http://', 'https://', $guid_url );

                                //                     }

                                //                 }



                                //                 echo '<a href="' . $guid_url . '" rel="lightbox lytebox['.$post_id.']" title="'.$fudoimg_alt.'">';

                                //                 if($fudoimg_url !=''){

                                //                     echo '<img class="box3image" src="' . $fudoimg_url.'" alt="'.$fudoimg_alt.'" title="'.$fudoimg_alt.'" /></a>';

                                //                 }else{

                                //                     echo '<img class="box3image" src="' . $guid_url . '" alt="'.$fudoimg_alt.'" title="'.$fudoimg_alt.'"  /></a>';

                                //                 }

                                //             }else{



                                //                 /*

                                //                  * Add url fudoimg_data

                                //                  *

                                //                  * Version: 1.7.12

                                //                  *

                                //                  **/

                                //                 $fudoimg_data = apply_filters( 'fudoimg_data_add_url', $fudoimg_data, $post_id );



                                //                 if ( checkurl_fudou( $fudoimg_data )) {

                                //                     echo '<a href="' . $fudoimg_data . '" rel="lightbox lytebox['.$post_id.']" title="'.$fudoimg_alt.'">';

                                //                     echo '<img class="box3image" src="' . $fudoimg_data . '" alt="' . $fudoimg_alt . '" title="' . $fudoimg_alt . '" /></a>';

                                //                 }else{

                                //                     echo '<img class="box3image" src="' . WP_PLUGIN_URL . '/fudou/img/nowprinting.jpg" alt="' . $fudoimg_data . '" />';

                                //                 }

                                //             }

                                //         }



                                //     }else{

                                //         if( $imgid==1 )

                                //         echo '<img class="box3image" src="'.WP_PLUGIN_URL.'/fudou/img/nowprinting.jpg" alt="" />';

                                //     }

                                //     echo "\n";

                                // }

                                /**
                                 * 物件の画像をタブごとにならべる
                                 * thumbnail部分などを変えてlightbox化などすればよい
                                 */

                                //get_template_part('template/bukken-image');


                                //携帯QR

                                if ( $is_fudouktai ){

                                    $yoursubject = '%e7%89%a9%e4%bb%b6%e3%82%b5%e3%82%a4%e3%83%88%e3%81%aeURL'; //物件サイトのURL

                                    echo "\n";

                                    echo '<a href="mailto:?subject='.$yoursubject.'&body='. urlencode( get_permalink($post_id) ) .'">';

                                    $options = '';

                                    $culum3 = false;

                                    if (function_exists('unpc_get_theme_options'))

                                        $options = unpc_get_theme_options();



                                    if ( is_array( $options ) ){

                                        $current_layout = $options['theme_layout'];

                                        if ( in_array( $current_layout, array( 'sidebar-content-sidebar' ) ) )

                                            $culum3 = true;

                                    }



                                    if ( $culum3 ){

                                        echo '<img src="//chart.googleapis.com/chart?chs=100x100&amp;cht=qr&amp;chl=' . urlencode( get_permalink($post_id) ) . '" alt="クリックでURLをメール送信" title="クリックでURLをメール送信" /></a>';

                                    }else{

                                        echo '<img src="//chart.googleapis.com/chart?chs=130x130&amp;cht=qr&amp;chl=' . urlencode( get_permalink($post_id) ) . '" alt="クリックでURLをメール送信" title="クリックでURLをメール送信" /></a>';

                                    }

                                }

                            ?>

                                    </div>



                                    <!-- ここから右ブロック -->

                                    <div class="list_detail">

                                        <?php do_action( 'single-fudo2', $post_id ); ?>
                                        <?php
//--roomに関連の親情報を集める
    // $args = array(
    //     'connected_type' => 'room_relation',
    //     'connected_items' => $post_id,
    //     'posts_per_page' => 20,
    //     // 'post_type' => 'room',
    //     // 'meta_key' => 'nyukyogenkyo',
    //     // 'order' => 'DESC'
    //     'meta_query' => array(
    //         'relation' => 'AND',
    //         'meta_nyukyogenkyo' => array(
    //             'key' => 'nyukyogenkyo',
    //             'type' => 'numeric',
    //     'compare' => 'EXISTS'),
    //         'meta_kakaku' => array(
    //             'key'=>'kakaku',
    //             'type' => 'numeric',
    //             'compare' => 'EXISTS',
    //             )
    //     ),
    //       'orderby' => array( 'meta_nyukyogenkyo' => 'DESC','meta_kakaku' => 'ASC'),
    //       'order' => 'DESC'
    // );
?>




                                        <?php do_action( 'single-fudo10', $post_id ); ?>



                                        <?php // social do_action( 'single-fudo13', $post_id ); ?>




                                        <?php //do_action( 'single-fudo14', $post_id ); ?>


                                        <?php //do_action( 'single-fudo15', $post_id ); ?>







                                        <?php } ?>

                                        <?php } ?>
                                        <!-- //ユーザー別会員物件リスト -->

                                        <?php
/**
 * カスタムフィールドで選択したものを出す場合
 */
// $nearby = get_field('nearby');
// if(isset($nearby->post_content)){
// // print_r($nearby);
//     echo $nearby->post_content;
// }


// // LINE1を表示　共通項目
// $line1 = get_field('line1', 'option');

// $okikae_mae = array('%%%bukken_name%%%', '%%%distance%%%');

// $bukken_name = get_post_meta($post_id,'bukkenmei',true);
// $distance = my_custom_koutsu1_for_nearby_distance($post_id);

// $okikae_go = array($bukken_name, $distance);

// $line1 = str_replace($okikae_mae, $okikae_go, $line1);

// echo $line1;

// /**
//  * 路線１から駅IDを取得し、それをもとに、周辺情報カスタムポストのslugを表示
//  */
// my_custom_koutsu1_for_nearby($post_id);


//
?>

                                        <?php //get_recommend_bukken($post_id); ?>


                                    </div><!-- .list_simple_box -->
                                    <!-- .byline -->

                                    <?php //fudou_entry_meta( $post_id ); ?>


                                </div><!-- #post-## -->


                                <?php do_action( 'single-fudo17', $post_id ); ?>



                                <?php
//--コメント欄表示
 //comments_template(); ?>

                                <?php


            do_action( 'single-fudo6', $post_id );

?>


                                <?php //edit_post_link( '[編集]', '<span class="edit-link">', '</span>' ); ?>



                                <?php
/*
  $parent_id = $post->ancestors[count($post->ancestors) - 1]; // 最上の親ページのIDを取得
  //echo get_post($parent_id)->post_name; // 最上の親ページのスラッグを取得して表示
  echo $parent_title = get_post($parent_id)->post_title; // 最上の親ページのタイトルを取得して表示
  echo get_permalink($parent_id); // 最上の親ページの URL を表示
*/
?>



                            </div><!-- #list_simplepage2 -->

                </article>


                <?php } //パスワード保護 ?>







            </section><!-- .fudo-ul-contents -->

            <?php //get_sidebar(); ?>
        </article><!-- .fudo-ul-flex -->

    </main><!-- #main -->

</div><!-- #primary -->

<?php get_footer('single'); ?>


<?php include('template/fixednav2.php');?>
