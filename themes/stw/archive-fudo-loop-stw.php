<?php
/**
 * The Template for displaying fudou archive posts.
 *
 * Template: archive-fudo-loop.php
 *
 * @package WordPress5.0
 * @subpackage Fudousan Plugin
 * Version: 5.0.2
 *
 * 検索結果にroomを出るようにした。
 * そのため、表示上問題がでる。
 * 物件IDをもとに、その部屋情報をテーブルで表示しているが、
 * そこに子供のIDが入ってきてしまう。
 * inc-archive-fudo_sql.php
 * $fudo_room="(P.post_type ='fudo' OR P.post_type ='room')";
 *  $kakaku = "kakaku";//default
 */
    echo "\n<!-- stw_01 -->\n";

    //会員2
    $kaiin = 0;

    // 2020-12-06 16:06:49 鈴木　コメントアウト
    // if( !is_user_logged_in() && get_post_meta($meta_id, 'kaiin', true) > 0 ) $kaiin = 1;
    // //ユーザー別会員物件リスト
    // $kaiin2 = users_kaiin_bukkenlist( $meta_id, $kaiin_users_rains_register, get_post_meta( $meta_id, 'kaiin', true ) );

    //用途不明
    // $_post = get_post( intval($meta_id) );

    //newup_mark
    // 2020-12-06 16:07:26コメントアウト
    // $post_modified_date =  vsprintf("%d-%02d-%02d", sscanf($_post->post_modified, "%d-%d-%d"));
    // $post_date =  vsprintf("%d-%02d-%02d", sscanf($_post->post_date, "%d-%d-%d"));
    // $newup_mark_img=  '';
    // if( $newup_mark != 0 && is_numeric($newup_mark) ){
    //     if( ( abs(strtotime($post_modified_date) - strtotime(date("Y/m/d"))) / (60 * 60 * 24) ) < $newup_mark ){
    //         if($post_modified_date == $post_date ){
    //             $newup_mark_img = '<div class="new_mark">new</div>';
    //         }else{
    //             $newup_mark_img =  '<div class="new_mark">up</div>';
    //         }
    //     }
    // }



//----------room用の情報取得--------------------------------------------

//--room用に全情報を集める
//meta_idを調べ、post_type=roomの場合はその親（物件）IDをみつけ、上書きする
if(!empty(get_fudoID_from_room($meta_id))){
    $meta_id = get_fudoID_from_room($meta_id);
}

$meta_data = get_post( $meta_id );

$meta_title =  $meta_data->post_title;

// $rooms_get_post=get_post($meta_id);

// $get_post_meta=get_post_meta($meta_id);
//echo "<pre>".var_dump($meta_id)."</pre>";

//--roomに関連の親情報を集める
// $args = array(
//     'connected_type' => 'room_relation',
//     'connected_items' => $meta_id,
//     'meta_query' => array(
//         'relation' => 'AND',
//         'meta_nyukyogenkyo' => array(
//             'key' => 'nyukyogenkyo',
//             'type' => 'numeric',
//     'compare' => 'EXISTS'),
//         'meta_kakaku' => array(
//             'key'=>'kakaku',
//             'type' => 'numeric',
//             'compare' => 'EXISTS',
//             )
//     ),
//       'orderby' => array( 'meta_nyukyogenkyo' => 'DESC','meta_kakaku' => 'ASC'),
//       'order' => 'DESC'
// );
// $connected_parent = get_posts($args);

//---------------------------------------------------------------------


    //成約 class ver1.9.7
    // 2020-12-06 16:38:55 コメントアウト
    // if( get_post_meta( $meta_id, 'seiyakubi', true ) != '' ){
    //     $seiyaku_class = ' seiyaku';
    // }else{
    //     $seiyaku_class = '';
    // }
    //会員 class ver1.9.7
    // if( get_post_meta( $meta_id, 'kaiin', true ) > 0 ){
    //     $kaiin_class = ' kaiin';
    // }else{
    //     $kaiin_class = '';
    // }

    //
    $post_id = $meta_id;

    //サムネイル画像
    //ギャラリー利用の場合
    //https://hirashimatakumi.com/blog/126.html

    $fudo_gaikan_images = get_field('bukken_gaikan_images', $post_id);
    // print_r($fudo_gaikan_images[0]['sizes']);
    if($fudo_gaikan_images != ''){
        $fudoimg_data=$fudo_gaikan_images[0]['sizes']['large'];
        echo $fudoimg_data;
    }else{
        // スクレイピングした画像から、外観の画像を取得
        $h_images = stw_get_singlefudo_images($post_id,2);

        $h_images2 = stw_get_singlefudo_images($post_id);
        //echo '<pre>';var_dump($h_images);echo '</pre>';
        if ( isset($h_images[0]) && !in_array(basename($h_images[0]->image_path), get_ban_image_file_name()) ){
            // 画像があればその画像を出す。
            $fudoimg_data = stw_generate_imgurl($h_images[0]->image_path);
        } elseif( isset($h_images2[0]) ) {
            // 画像があればその画像を出す。
            $fudoimg_data = stw_generate_imgurl($h_images2[0]->image_path);
        } else {
            // なければ、デフォルトの画像を出す。
            $fudoimg_data="https://koukyu-chintai.com/wp-content/uploads/cropped-cropped-main-1-2000x1200-1-1024x614.jpg";
        }
    }

    //賃料
    if(get_field('chinryos', $post_id)){
        $chinryo = get_field('chinryos', $post_id);
    }else{
        $chinryo = get_kakaku_list_for_fudo($post_id);
    }

    //間取りリスト
    if(get_field('bukken_madori_list', $post_id)){
        $madori_list = get_field('bukken_madori_list', $post_id);
    }else{
        $madori_list = get_madori_list_for_fudo($post_id);
    }

    // MAPリンク
    $map_link = "https://www.google.co.jp/maps/place/" . urlencode(get_my_custom_shozaichi_print($post_id) . get_post_meta($post_id, 'shozaichimeisho', true) . get_post_meta($post_id, 'shozaichimeisho2', true));

    //築年月
    $tatemonochikunenn = get_post_meta($post_id, 'tatemonochikunenn', true);
    if(!empty($tatemonochikunenn)){

        if(strpos($tatemonochikunenn, '月')){
            $tatemonochikunenn_data = $tatemonochikunenn . "01";
            $tatemonochikunenn_data = str_replace(array("年","月"), "/", $tatemonochikunenn_data);
        }else{
            $tatemonochikunenn_data = $tatemonochikunenn . "/01";
        }
    }

    //階数
    $tikunensu = get_tikunensu($tatemonochikunenn_data);

    if(get_post_meta($post_id, 'tatemonokaisu1', true)!="") $kaisu = '地上'.get_post_meta($post_id, 'tatemonokaisu1', true)."階";
    if(get_post_meta($post_id, 'tatemonokaisu2', true)!="") $kaisu .= '<br>地下'.get_post_meta($post_id, 'tatemonokaisu2', true)."階";

    //問合せリンク
    $contact_link = get_permalink($post_id) . "#contact_form'";

    // 検討リスト入り判別フラグ
    $fvflg = ( in_array($post_id,$fvpids) ) ? true : false;
?>



<!--物件表示-->
<section class="l-contArchive l-cont __section">
    <div class="l-contArchive_cont o-room flex-nw">
        <div class="o-room __withroom flex-nw-no-md l-row">
            <section class="o-roomItem l-rowLeft withroom">
                <a class="o-roomSave" data-fvflg="<?php echo ($fvflg) ? 'sumi' : 'mi';?>" data-pid="<?php echo $post_id;?>"><i class="o-icon <?php echo ($fvflg) ? 'flaticon-heart __checked' : 'flaticon-heart';?>"></i></a>
                <p class="o-roomStatus"><?php echo get_rooms_count($post_id);?>部屋募集中</p>
                <div class="o-roomCont o-roomThumb">
                    <img class="o-roomThumb_img" src="<?php echo $fudoimg_data;?>">

                    <div class="o-roomSummary">
                        <h3 class="o-roomTitle o-title __white"><?php echo $meta_title; ?></h3>
                        <p class="o-roomPrice o-title __en"><?php echo $chinryo;?> <span class="o-roomPrice_unit"></span></p>
                        <div class="o-roomSpace flex-nw jc-start ai-center">
                            <div class="o-roomSpace_meter"><i class="o-roomTag_icon o-icon __min __white flaticon-buildings"></i><?php echo get_senyumenseki_list_for_fudo($post_id);?></div>
                            <div class="o-roomSpace_room"><i class="flaticon-blueprint o-icon __min __white"></i><?php echo $madori_list;?></div>
                        </div>
                    </div>

                </div>
                <div class="o-roomCont o-roomDesc flex-nw jc-between">

                    <div class="o-roomInfo">
                        <p class="o-roomAddress flex-nw jc-between"><span><?php echo get_post_meta($post_id, 'shozaichimeisho', true);?>
                        <?php echo get_post_meta($post_id, 'shozaichimeisho2', true);?>
                        </span><a href="<?php echo $map_link;?>" target="_blank" class="o-roomMap o-link __tiny">MAPで表示</a> </p>
                        <ul class="o-roomStation">
                            <li class="o-roomStation_list"><?php my_custom_koutsu1_print($post_id);?></li>
                            <li class="o-roomStation_list"><?php my_custom_koutsu2_print($post_id);?>
                            </li>
                            <li class="o-roomStation_list"><?php my_custom_koutsu3_print($post_id);?></li>
                        </ul>
                        <div class="o-roomTag flex-w" style="display:none;">
                            <i class="o-roomTag_icon o-icon __min flaticon-tag"></i>
                            <a href="" class="o-roomTag_link o-link __nocolor">ペット相談可</a>
                        </div>
                    </div>
                    <div class="o-roomBuilding">
                        <ul>
                            <li class="o-roomBuilding_item"><?php echo $tikunensu;?></li>
                            <li class="o-roomBuilding_item"><?php my_custom_tatemonokozo_print($post_id);?></li>
                            <li class="o-roomBuilding_item"><?php echo $kaisu;?></li>
                        </ul>
                        <a href="<?php echo $contact_link;?>" class="o-roomContact o-btn __basic01">詳細</a>
                    </div>

                </div>
                <a href="<?php echo get_permalink($post_id);?>" class="o-link __over"></a>
            </section>


            <?php
            /**
                        * この物件に属する部屋情報の呼び出し
                        * 入居可能フラグで降順にし、価格の昇順で表示
                        */
            $args = array(
                'connected_type' => 'room_relation',
                'connected_items' => $post_id,
                'meta_query' => array(
                    'relation' => 'AND',
                    'meta_nyukyogenkyo' => array(
                        'relation' => 'OR',
                        array(
                            'relation' => 'OR',
                            array(
                                'key' => 'nyukyogenkyo',
                                'type' => 'numeric',
                                'value' => 3),//即入居可
                            array(
                                'key' => 'nyukyogenkyo',
                                'type' => 'numeric',
                                'value' => 2),//居住中
                            array(
                                'key' => 'nyukyogenkyo',
                                'type' => 'numeric',
                                'value' => 11),//空室
                            array(
                                'key' =>  'nyukyogenkyo',
                                'value' => '*'
                            )
                        ),
                    ),
                    'meta_kakaku' => array(
                        'key'=>'kakaku',
                        'type' => 'numeric',
                        'compare' => 'EXISTS',
                    )
                ),
                'orderby' => array( 'meta_nyukyogenkyo' => 'DESC','meta_kakaku' => 'ASC'),
                'order' => 'DESC'
            );
            ?>


            <?php
            // 物件のステータスが満室状態だったら、部屋の代わりに別のものを出す。
            if ( '2' == get_post_meta($post_id,'bukken_status')[0] ) {?>

            <div class="o-roomVacant l-rowRight withroom __notexist">
                <h3 class="o-roomVacant_title">募集中のお部屋</h3>
                <div class="o-roomVacant_cont">

                    <?php include locate_template('./template/rooms-for-archive-man.php'); ?>
                </div>
            </div>
            <?php } else { ?>
            <div class="o-roomVacant l-rowRight withroom">
                <h3 class="o-roomVacant_title">募集中のお部屋</h3>
                <div class="o-roomVacant_cont">
                    <div class="o-roomVacant_wrap">
                        <?php
                            // 満室以外だったら、部屋を出す。
                            include locate_template('./template/rooms-for-archive.php');
                        ?>
                    </div>
                </div>
            </div>
            <?php } ?>





        </div>
    </div>
</section>


