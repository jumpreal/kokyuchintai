<?php

/**

 * The Template for displaying fudou single posts.

 *

 * Template: single-fudo2017.php

 *

 * @package WordPress5.0

 * @subpackage Fudousan Plugin

 * @since Twenty Seventeen

 * Version: 5.0.2

 */



/**** functions ****/

//    require_once WP_PLUGIN_DIR . '/fudou/inc/inc-single-fudo.php';

require_once get_template_directory().'/inc/inc-single-fudo-stw.php';




global $is_fudouktai,$is_fudoukaiin;

global $is_fudoukouku;



$post_id = isset( $_GET['p'] ) ? myIsNum_f( $_GET['p'] ) : '';

if( empty($post_id) ){

    $post_id = $post->ID;

}





//会員2

$kaiin = 0;

if( !is_user_logged_in() && get_post_meta($post_id, 'kaiin', true) > 0 ) $kaiin = 1;

//ユーザー別会員物件リスト

$kaiin2 = users_kaiin_bukkenlist( $post_id, get_option( 'kaiin_users_rains_register' ), get_post_meta( $post_id, 'kaiin', true ) );



//Disable AMP canonical

if( $kaiin == 1 ){

    add_filter( 'amp_frontend_show_canonical', '__return_false' );

}





//title変更 会員

if ( !my_custom_kaiin_view('kaiin_title',$kaiin,$kaiin2) ){

    //WordPress ～4.3

    add_filter( 'wp_title', 'add_post_type_wp_title_ka' );

    //WordPress 4.4～

    add_filter( 'pre_get_document_title', 'add_post_type_wp_title_ka' );

    //All-in-One-SEO-Pack

    add_filter( 'aioseop_title', 'add_post_type_wp_title_ka' );

}

function add_post_type_wp_title_ka( $title = '' ) {

    /**

         * Filter the separator for the document title Fudou.

         * @since 4.4.0

         * @param string $sep Document title separator. Default '-'.

         */

    $sep = apply_filters( 'document_title_separator_fudou', '-' );

    $title =  '会員物件 ' . $sep . ' '. get_bloginfo( 'name', 'display' );

    return $title;

}



$post_id_array = get_post( $post_id );

$title    = $post_id_array->post_title;

$excerpt  = $post_id_array->post_excerpt;

$content  = $post_id_array->post_content;

$modified = $post_id_array->post_modified;



//newup_mark

$newup_mark = get_option('newup_mark');

if($newup_mark == '') $newup_mark=14;



$post_modified_date =  vsprintf("%d-%02d-%02d", sscanf($modified, "%d-%d-%d"));

$post_date =  vsprintf("%d-%02d-%02d", sscanf($post_id_array->post_date, "%d-%d-%d"));



$newup_mark_img =  '';

if( $newup_mark != 0 && is_numeric($newup_mark) ){



    if( ( abs(strtotime($post_modified_date) - strtotime(date("Y/m/d"))) / (60 * 60 * 24) ) < $newup_mark ){

        if($post_modified_date == $post_date ){

            $newup_mark_img = '<div class="new_mark">new</div>';

        }else{

            $newup_mark_img = '<div class="new_mark">up</div>';

        }

    }

}





//会員登録 URL生成 ver1.9.4



//SSL URL

$fudou_ssl_site_url = get_option('fudou_ssl_site_url');



if( defined('FUDOU_SSL_MODE') && FUDOU_SSL_MODE == 1 && $fudou_ssl_site_url !='' ){

    $site_register_url = $fudou_ssl_site_url;

}else{

    $site_register_url = get_option('siteurl');

}



//会員登録URL

$kaiin_register_url = $site_register_url . '/wp-content/plugins/fudoukaiin/wp-login.php?action=register';



//Add thickbox class

if ( wp_is_mobile() ) {

    $thickbox_class = '';

}else{

    $kaiin_register_url .= '&KeepThis=true&TB_iframe=true&width=400&height=500';

    $thickbox_class = ' class="thickbox"';

}



/*

         * 会員登録 URLフィルター

         * ver 1.9.1

         */

$kaiin_register_url    = apply_filters( 'fudou_kaiin_register_url', $kaiin_register_url );



// 会員登録 URLタグ

$kaiin_register_url_tag = '<a href="' . $kaiin_register_url . '" ' . $thickbox_class . ' rel="nofollow">';



/*

         * 会員登録 URLタグフィルター

         * ver 1.9.3

         */

$kaiin_register_url_tag     = apply_filters( 'fudou_kaiin_register_url_tag', $kaiin_register_url_tag );



// .会員登録 URL生成





/*

     * VIP 物件詳細ページ非表示 404

     *

     * apply_filters( 'fudou_kaiin_vip_single_seigen', $view, $post_id );

     *

     * single_fudoXXXX.php

     *

     * @param bool $view    true:表示  false: 非表示(404)

     * Version: 1.7.5


     */

if( ! apply_filters( 'fudou_kaiin_vip_single_seigen', true, $post_id ) ){

    status_header( 404 );

    //header( "location: 404.php" );

    wp_redirect( home_url( '/404.php' ) );

    exit();

}



status_header( 200 );

get_header();

the_post();

?>


<!--<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_directory'); ?>/assets/css/single.css">-->
<?php
//--roomに関連の親情報を集める
$args = array(
    'connected_type' => 'room_relation',
    'connected_items' => $post_id
);
$connected_parent = get_posts($args);

$oya_title=$connected_parent[0]->post_title;
$oya_title_with_roomNo="";
$parent_post_ID = $connected_parent[0]->ID;
$oya_meta=get_post_meta($parent_post_ID);
$bukken_mansitu_fg = get_field('bukken_status', $parent_post_ID);
$room_mansitu_fg = get_field('nyukyogenkyo', $post_id);
$ko_meta=get_post_meta($post_id);
$permalink = get_permalink($parent_post_ID);

$oya_title_with_link = '<a href="' . $permalink . '">' . $oya_title . '</a>';
// if(get_post_meta($post_id, 'room_floor', true)!=""){ echo '／'.get_post_meta($post_id, 'room_floor', true).'階';}
if(get_field('room_floor')!=""){ $oya_title_with_roomNo = $oya_title_with_link . '／' . get_field('room_floor').'階';}
// echo  $newup_mark_img;

//これを読み込み様々な値をもってくる

require('bukken-hensu.php');


$img1 = @$fudo_gaikan_images[0]['sizes']['large'];
$img2 = @$fudo_gaikan_images[1]['sizes']['large'];
$img3 = @$fudo_gaikan_images[2]['sizes']['large'];
if ( is_null($img1) || is_null($img2) || is_null($img3) ){
    $gai_images = stw_get_singlefudo_images($parent_post_ID,2);
    $img1_path = '';
    if ( 0 < count($gai_images) ) {
        $img1_path = $gai_images[0]->image_path;
        $img1 = stw_generate_imgurl($img1_path);
    }
    $h_images = stw_get_singlefudo_images($parent_post_ID);
    $h_images_count_without_ban = count($h_images);
    // echo $h_images_count_without_ban;exit;
    if ( 0 < count($h_images) ){
        foreach ($h_images as $oh ){
            if ( is_null($img1) ){
                if(!in_array(basename($oh->image_path), get_ban_image_file_name())){
                $img1 = stw_generate_imgurl($oh->image_path);
                continue;
                }else{
                    $h_images_count_without_ban--;
                }
            }
            if ( is_null($img2) && $oh->image_path != $img1_path ){
                if(!in_array(basename($oh->image_path), get_ban_image_file_name())){
                $img2 = stw_generate_imgurl($oh->image_path);
                continue;
                }else{
                    $h_images_count_without_ban--;
                    continue;
                }
            }
            if ( is_null($img3) && $oh->image_path != $img1_path ){
                if(!in_array(basename($oh->image_path), get_ban_image_file_name())){
                $img3 = stw_generate_imgurl($oh->image_path);
                continue;
                }else{
                    $h_images_count_without_ban--;
                    continue;
                }
            }
        }
    }
}

//　間取り画像が一枚目にでるバージョン
// $img1 = $room_images[0]['sizes']['large'];
// $img2 = $room_images[1]['sizes']['large'];
// $img3 = $room_images[2]['sizes']['large'];
// if ( is_null($img1) || is_null($img2) || is_null($img3) ){
//     $mad_images = stw_get_singlefudo_images($post_id,1);
//     $img1_path = '';
//     if ( 0 < count($mad_images) ) {
//         $img1_path = $mad_images[0]->image_path;
//         $img1 = stw_generate_imgurl($img1_path);
//     }
//     $h_images = stw_get_singlefudo_images($post_id);
//     if ( 0 < count($h_images) ){
//         foreach ($h_images as $oh ){
//             if ( is_null($img1) ){
//                 $img1 = stw_generate_imgurl($oh->image_path);
//                 continue;
//             }
//             if ( is_null($img2) && $oh->image_path != $img1_path ){
//                 $img2 = stw_generate_imgurl($oh->image_path);
//                 continue;
//             }
//             if ( is_null($img3) && $oh->image_path != $img1_path ){
//                 $img3 = stw_generate_imgurl($oh->image_path);
//                 continue;
//             }
//         }
//     }
// }


    $mad_images = stw_get_singlefudo_images($post_id,1);
    $madori_image = '';
    if ( 0 < count($mad_images) ) {
        $madori_image = $mad_images[0]->image_path;
        $madori_image_for_sidebar = stw_generate_imgurl($madori_image);
    }
?>

<section class="o-rsingleFv l-cont mb-1">
    <div class="o-rsingleFv_wrap flex-nw l-row">
        <div class="o-rsingleFv_main l-rowLeft room flex-nw">
            <img src="<?php echo $img1;?>" alt="" class="o-rsingleFv_mainimg">
        </div>
        <div class="o-rsingleFv_sub l-rowRight room flex-w fd-column">
            <img src="<?php echo $img2;?>" alt="" class="o-rsingleFv_subimg">
            <img src="<?php echo $img3;?>" alt="" class="o-rsingleFv_subimg">
        </div>
        <a id="js-roomimgModal" class="o-link __over"></a>
    </div>
    <div class="o-rsingleFunction flex-nw">
        <a id="js-roomimgModal2" class="o-rsingleImgbtn">
            <?php echo ( 0 == get_room_image_count($post_id)) ? $h_images_count_without_ban : get_room_image_count($post_id);?>枚の画像</a>
        <?php $fvflg = ( in_array($parent_post_ID,$fvpids) ) ? true : false;?>
        <a class="o-rsingleSave" data-fvflg="<?php echo ($fvflg) ? 'sumi' : 'mi';?>" data-pid="<?php echo $parent_post_ID;?>" href="<?php echo $favorite_link;?>">
            <i class="o-icon flaticon-heart <?php echo ($fvflg) ? 'flaticon-heart __checked' : 'flaticon-heart';?>"></i>
        </a>
    </div>
    <div class="o-rsingleStatus flex-nw ai-center">

        <p class="o-rsingleStatus_title">
        <?php
            if ( get_field('bukken_status', $parent_post_ID) == 2 ){//物件の満室fgがON
        ?>
            満室
        <?php }else{ ?>
            募集中
        <?php } ?>
        </p>
        <?php
            if ( get_field('bukken_status', $parent_post_ID) <> 2 ){//物件の満室fgがON
        ?>
        <p class="o-rsingleStatus_desc"><span class="o-roomsingleStatus_when">入居現況：
                <?php
                echo get_xls_custom_nyukyogenkyo_print($post_id);
                ?>
        </p>
        <?php } ?>
    </div>

</section>

<aside class="o-rsingleUpdate flex-nw jc-end o-text __min">
    <p class="o-rsingleUpdate_published">掲載 :
        <?php the_date('Y/m/d');?>
    </p>
    <p class="o-rsingleUpdate_modified">更新 :
        <?php the_modified_date('Y/m/d') ?>
    </p>
</aside>

<main class="o-rsingleMain l-cont __section l-row flex-nw-no-xl ">
    <div class="l-rowLeft roomdesc l-row flex-nw-no-big">
        <div class="o-rsingleSummary l-rowLeft roomnav js-fixedTop">


            <ul class="o-rsingleSummary_list js-fixedStart" id="o-rsingleSummary_list">
                <li class="o-rsingleSummary_item item1"><a href="#section01" class="o-rsingleSummary_link section1">概要</a></li>
                <li class="o-rsingleSummary_item item2"><a href="#section02" class="o-rsingleSummary_link section2">特徴</a></li>
                <li class="o-rsingleSummary_item item3"><a href="#section03" class="o-rsingleSummary_link section3">設備</a></li>
                <li class="o-rsingleSummary_item item4"><a href="#section04" class="o-rsingleSummary_link section4">物件情報</a></li>
                <li class="o-rsingleSummary_item item5"><a href="#section05" class="o-rsingleSummary_link section5">備考欄</a></li>
                <li class="o-rsingleSummary_item item6"><a href="#section06" class="o-rsingleSummary_link section6">他の部屋</a></li>
                <li class="o-rsingleSummary_item item7"><a href="#section07" class="o-rsingleSummary_link section7">周辺情報</a></li>
                <li class="o-rsingleSummary_item item8"><a href="#section08" class="o-rsingleSummary_link section8">FAQ</a></li>
                <li class="o-rsingleSummary_item item9"><a href="#section09" class="o-rsingleSummary_link section9">コメント</a></li>

            </ul>

        </div>
        <div class="l-rowRight roomnav">

            <!-- 概要 -->
            <section class="o-rsingleOverview o-rsingleCont" id="section01">
                <h1 class="o-rsingleTitle">
                    <?php echo $oya_title_with_roomNo;?>
                </h1>
                <div class="o-rsingleSpace flex-nw jc-start ai-center">
                    <div class="o-rsingleSpace_item __area"><i class="o-rsingleTag_icon o-icon __mid flaticon-buildings"></i>
                        <?php if( $ko_meta["senyumenseki"][0] ){ ?>

                        <?php echo $ko_meta["senyumenseki"][0];?>㎡

                        <?php } ?>
                    </div>
                    <div class="o-rsingleSpace_item __layout"><i class="flaticon-blueprint o-icon __mid"></i>
                        <?php echo xls_custom_madorisu_print($post_id); ?>
                    </div>
                    <?php if(get_field('heyamuki') && get_field('heyamuki') !== "部屋・建物向き選択"){ ?>
                    <div class="o-rsingleSpace_item __direction"><i class="flaticon-safari o-icon __mid"></i>
                        <?php the_field('heyamuki'); ?>向き</div>
                    <?php } ?>
                </div>
                <div class="o-rsingleDesc flex-nw jc-between">

                    <div class="o-rsingleInfo">
                        <p class="o-rsingleAddress flex-nw jc-between"><span>
                                <?php my_custom_shozaichi_print($post_id);?>
                                <?php echo get_post_meta($parent_post_ID, 'shozaichimeisho', true);?>
                                <?php echo get_post_meta($parent_post_ID, 'shozaichimeisho2', true);?>
                                </span><a href="<?php echo $map_link;?>" target="_blank" class="o-rsingleMap o-link __min">MAPで表示</a> </p>
                        <ul class="o-rsingleStation">
                            <li class="o-rsingleStation_list">
                                <?php my_custom_koutsu1_print($parent_post_ID);?>
                            </li>
                            <li class="o-rsingleStation_list">
                                <?php my_custom_koutsu2_print($parent_post_ID);?>
                            </li>
                            <li class="o-rsingleStation_list">
                                <?php my_custom_koutsu3_print($parent_post_ID);?>
                            </li>
                        </ul>
                        <div class="o-rsingleTag flex-w" style="display:none;">
                            <i class="o-rsingleTag_icon o-icon __min flaticon-tag"></i>
                            <a href="" class="o-rsingleTag_link o-link __nocolor">ペット相談可</a>
                            <a href="" class="o-rsingleTag_link o-link __nocolor">ペット相談可</a>
                        </div>
                    </div>
                    <div class="o-rsingleBuilding">
                        <ul>
                            <li class="o-rsingleBuilding_item">
                                <?php echo $tikunensu;?>
                            </li>
                            <li class="o-rsingleBuilding_item">
                                <?php my_custom_tatemonokozo_print_br($parent_post_ID);?>
                            </li>
                            <li class="o-rsingleBuilding_item">
                                <?php echo $kaisu;?>
                            </li>
                        </ul>
                    </div>

                </div>
            </section>

            <!-- スマホのみ表示  -->
            <?php
            $ua=$_SERVER['HTTP_USER_AGENT'];
            $browser = ((strpos($ua,'iPhone')!==false)||(strpos($ua,'iPod')!==false)||(strpos($ua,'Android')!==false));
            if ($browser == true){
                $browser = 'sp';
            }
            if($browser == 'sp'){
            ?>
            <div class="o-rsingleAction __building o-rsingleCont">
                <div class="o-rsingleAction_wrap">
                    <?php if(get_field('bukken_status', $parent_post_ID) !== "2"){ //満室でなはい時表示　?>

                    <div class="o-rsingleAction_price o-rsingleAction_cont">
                        <?php echo $kakaku;?> <span class="o-rsingleAction_unit"> 円/月</span></div>
                    <div class="o-rsingleAction_cost o-rsingleAction_cont flex-nw ai-center withfreerent">
                        <div class="o-rsingleAction_costItem __deposit">
                            <span class="o-rsingleAction_costItem_title">敷金</span>
                            <p class="o-rsingleAction_costItem_period">
                                <?php the_field('kakakushikikin');?>
                            </p>
                        </div>
                        <div class="o-rsingleAction_costItem __gratuity">
                            <span class="o-rsingleAction_costItem_title">礼金</span>
                            <p class="o-rsingleAction_costItem_period">
                                <?php the_field('kakakureikin');?>
                            </p>
                        </div>
                        <div class="o-rsingleAction_costItem __manege">
                            <span class="o-rsingleAction_costItem_title">管理費</span>
                            <p class="o-rsingleAction_costItem_period">
                                <?php
                                  if(number_format(get_field('kakakukyouekihi')) == 0){
                                      echo '0円';
                                  }else{
                                      echo @number_format(get_field('kakakukyouekihi'));
                                  }
                                ?>
                            </p>
                        </div>
                        <?php if(get_field('freerent', $post_id)){ ?>
                        <div class="o-rsingleAction_costItem __freerent">
                            <span class="o-rsingleAction_costItem_title">フリーレント</span>
                            <p class="o-rsingleAction_costItem_period">
                                <?php the_field('freerent', $post_id);?>
                            </p>
                        </div>
                        <?php } ?>
                    </div>
                    <div class="o-rsingleAction_function o-rsingleAction_cont flex-nw jc-center ai-center">
                        <a id="js-estimateModal" class=" o-rsingleAction_functionItem __estimate flex-nw ai-center fb-50"><i class="o-icon __min flaticon-calculator"></i><span class="o-rsingleAction_functionText">初期費用<br>概算見積もり</span></a>
                        <a href="<?php echo $favorite_link;?>" class="o-rsingleAction_functionItem __save flex-nw ai-center fb-50" data-fvflg="<?php echo ($fvflg) ? 'sumi' : 'mi';?>" data-pid="<?php echo $parent_post_ID;?>">
                            <i class="o-icon __min flaticon-love <?php echo ($fvflg) ? 'flaticon-heart __checked' : 'flaticon-heart';?>"></i>
                            <span class="o-rsingleAction_functionText">
                                <?php echo ($fvflg) ? '検討リストから削除する' : '検討リストに追加する';?></span>
                        </a>
                    </div>
                    <div class="o-rsingleAction_btn o-rsingleAction_cont">
                        <a href="<?php echo home_url() . '/' . get_field('kengaku_nairan_link', 'option' );?>?post_id=
                            <?php echo $post_id;?>" class="o-rsingleAction_btnVisit o-btn __basic01">見学・内覧する</a>
                        <div class="flex-nw">
                            <a href="<?php echo home_url() . '/' . get_field('kuusitu_inquiry_link', 'option' );?>?post_id=
                                <?php echo $post_id;?>" class="o-rsingleAction_btnEmpty o-btn __basic06 flex-nw jc-center ai-center fb-50">空室状況を<br>問い合わせる</a>
                            <a href="<?php echo home_url() . '/' . get_field('bukken_inquiry_link', 'option' );?>?post_id=
                                <?php echo $post_id;?>" class="o-rsingleAction_btnQuestion o-btn __basic06 flex-nw jc-center ai-center fb-50">物件について<br>質問する</a>
                        </div>
                    </div>
                    <div class="o-rsingleAction_tel u-text-ac">
                        <p class="o-rsingleAction_telTitle">電話で問い合わせする</p>
                        <p class="o-rsingleAction_telTel">
                            <?php the_field('free_dial', 'option');?>
                        </p>
                        <div class="o-rsingleAction_telInfo flex-nw jc-center ai-center">問い合わせ物件番号 :
                            <?php echo $post_id;//get_post_meta($parent_post_ID, 'shikibesu', true);?>
                        </div>
                    </div>

                    <?php } else { //満室の場合は?>

                    <div class="o-rsingleAction_price o-rsingleAction_cont">現在満室のため募集しておりません。</div>

                    <?php } // 満室 ?>
                </div>
                <div class="o-rsingleAction_sns flex-nw jc-center ai-center"></div>

            </div>

            <?php }else{ ?>
            <?php } // スマホのみ表示?>


            <?php if(get_field('room_memo_for_admin', $post_id)){ ?>
            <section class="o-rsingleMemo o-rsingleCont" id="section01">
                <div class='o-rsingleMemo_cont'>
                    <?php the_field('room_memo_for_admin', $post_id); ?>
                    <?php echo get_source_url($post_id); ?>
                </div>
            </section>
            <?php } ?>

            <!-- 部屋詳細 -->
            <?php
                echo get_field('heyasyousai', $post_id);
            ?>

            <!-- 特徴 -->
            <?php if( have_rows('bukken-tokutyou', $parent_post_ID) ): ?>
            <section class="o-rsingleSpecial o-rsingleCont" id="section02">
                <h2 class="o-rsingleCont_title flex-nw ai-center">物件の特徴<span class="o-rsingleCont_en">CHARACTERISTIC</span></h2>


                <?php while( have_rows('bukken-tokutyou', $parent_post_ID) ): the_row();

                $image = get_sub_field('bukken-tokutyou-img');
                // $image_src = $image['sizes']['large'];

                ?>

                <div class="o-rsinglePost">
                    <div class="o-rsinglePost_thumb">
                        <img class="o-rsinglePost_img" src="<?php echo $image;?>" alt="">
                    </div>
                    <section class="o-rsinglePost_cont o-rsingleCont_wrap">
                        <h2 class="o-title __title">
                            <?php the_sub_field('bukken-tokutyou-title');?>
                        </h2>


                        <div class="p-postCont __singleroom o-partsMore">
                            <input id="o-partsMore_triggerSpecial" class="o-partsMore_trigger" type="checkbox">
                            <label class="o-partsMore_btn" for="o-partsMore_triggerSpecial"><i class="o-partsMore_icon o-icon fas fa-angle-down"></i></label>

                            <div class="p-postCont __singleroom o-partsMore_cont">
                                <p class="p-postCont_text">
                                    <?php the_sub_field('bukken-tokutyou-text');?>
                                </p>
                            </div>
                        </div>
                    </section>
                </div>

                <?php endwhile; ?>


            </section>
            <?php endif; ?>

            <!-- 共有・室内設備 -->
            <section class="o-rsingleFacility o-rsingleCont" id="section03">

                <h2 class="o-rsingleCont_title flex-nw ai-center">条件・設備<span class="o-rsingleCont_en">Requirement & Facility</span></h2>
            <?php if(!empty(my_custom_keiyakujouken_print($post_id))){ ?>
                <section class="o-rsingleCont_wrap" style="display:none;">
                    <h3 class="o-title __mid __bold mb-1 flex-nw ai-center"><i class="o-icon __p __nobold flaticon-drops"></i>契約形態</h3>
                    <ul class="o-list __mid __basic01 square flex-w ai-center">
                        <?php echo my_custom_keiyakujouken_print($post_id); ?>
                    </ul>
                </section>
            <?php } ?>

                <section class="o-rsingleCont_wrap">
                    <h3 class="o-title __mid __bold mb-1 flex-nw ai-center"><i class="o-icon __large __nobold flaticon-sofa"></i>室内設備</h3>
                    <ul class="o-list __mid __basic01  square flex-w ai-center">
                        <?php my_custom_setsubi_print($post_id); ?>
                    </ul>
                </section>
                <section class="o-rsingleCont_wrap">
                    <h3 class="o-title __mid __bold mb-1 flex-nw ai-center"><i class="o-icon __title __nobold flaticon-construction"></i>共有設備</h3>
                    <ul class="o-list __mid __basic01  square flex-w ai-center">
                        <?php my_custom_kyouyu_setsubi_print($parent_post_ID); ?>
                    </ul>
                </section>

                <?php
                if(!empty(get_field('setsubisonota3', $post_id))){
                ?>
                
                <section class=" o-rsingleCont_wrap">
                    <h3 class="o-title __mid __bold mb-1 flex-nw ai-center"><i class="o-icon __title __nobold flaticon-construction"></i>その他の条件・設備</h3>
                    <ul class="o-list __mid __basic01 square flex-w ai-center">
                        <?php echo nl2br(get_field('setsubisonota3', $post_id)); ?>
                    </ul>
                </section>
                
                <?php
                }
                ?>
                
            </section>

            <!-- 物件情報 -->
            <section class="o-rsingleBuilding o-rsingleCont" id="section04">
                <h2 class="o-rsingleCont_title flex-nw ai-center">物件情報<span class="o-rsingleCont_en">BUILDING</span></h2>
                <section class="o-rsingleCont_wrap">
                    <table class="o-table building __mid __sizem">
                        <tr>
                            <th>物件名</th>
                            <td>
                                <?php echo $oya_title;?>
                            </td>
                        </tr>
                        <tr>
                            <th>築年数</th>
                            <td>
                                <?php

                                $tatemonochikunenn = get_post_meta($parent_post_ID, 'tatemonochikunenn', true);
                                if(!empty($tatemonochikunenn)){

                                    if(strpos($tatemonochikunenn, '月')){
                                        $tatemonochikunenn_data = $tatemonochikunenn . "01";
                                        $tatemonochikunenn_data = str_replace(array("年","月"), "/", $tatemonochikunenn_data);
                                    }else{
                                        $tatemonochikunenn_data = $tatemonochikunenn . "/01";
                                    }
                                }

                                echo $tatemonochikunenn;
                                echo get_sintiku_mark($tatemonochikunenn_data);

                                ?>
                            </td>
                        </tr>
                        <tr>
                            <th>建物階数</th>
                            <td>
                                <?php
                                if(get_post_meta($parent_post_ID, 'tatemonokaisu1', true)!="") echo '地上'.get_post_meta($parent_post_ID, 'tatemonokaisu1', true)."階";
                                if(get_post_meta($parent_post_ID, 'tatemonokaisu2', true)!="") echo '<br>地下'.get_post_meta($parent_post_ID, 'tatemonokaisu2', true)."階";
                                ?>
                            </td>
                        </tr>
                        <tr>
                            <th>建物構造</th>
                            <td>
                                <?php my_custom_tatemonokozo_print($parent_post_ID);?>
                            </td>
                        </tr>
                        <tr>
                            <th>総戸数</th>
                            <td>
                                <?php echo get_post_meta($parent_post_ID, 'bukkensoukosu', true);?>戸</td>
                        </tr>
                        <tr>
                            <th>交通</th>
                            <td>
                                <?php my_custom_koutsu_all($parent_post_ID);?>
                            </td>
                        </tr>
                        <tr>
                            <th>間取り</th>
                            <td>
                                <?php echo xls_custom_madorisu_print($post_id); ?>
                            </td>
                        </tr>
                        <tr>
                            <th>専有面積</th>
                            <td>
                                <?php if( $ko_meta["senyumenseki"][0] ){ ?>

                                <?php echo $ko_meta["senyumenseki"][0];?>㎡

                                <?php } ?>
                            </td>
                            <?php if(get_field('bukken_status', $parent_post_ID) == "1"){ //満室でなはい時表示　?>
                        <tr>
                            <th>賃料</th>
                            <td>
                                <?php
                                $kakaku = get_field('kakaku');
                                $kakaku = number_format($kakaku);
                                echo $kakaku;
                                ?>円</td>
                        </tr>
                        <tr>
                            <th>共益・管理費</th>
                            <td>
                                <?php
                            if(get_field('kakakukyouekihi')){
                                echo @number_format(get_field('kakakukyouekihi')) . '円';
                            }
                            ?>
                            </td>
                        </tr>
                        <?php if(get_field('kakakuhoshoukin', $post_id)){ ?>
                        <tr>
                            <th>保証金</th>
                            <td>
                                <?php the_field('kakakuhoshoukin', $post_id); ?>
                            </td>
                        </tr>
                        <?php } ?>

                        <tr>
                            <th>仲介手数料</th>
                            <td>
                                <?php
                                    // echo @get_tyuukai_tesuuryou_list_for_fudo($parent_post_ID);
                                    the_field('tyuukai_tesuuryou', $post_id)
                                ?>
                            </td>
                        </tr>

                        <!-- 
                        <?php 
                        
                        if(get_field('chukai_muryou', $parent_post_ID)){//物件の仲介手数料無料 ?>
                            <tr>
                                <th>仲介手数料</th>
                                <td>
                            <?php 
                            if(get_field('tyuukai_tesuuryou')!='' && get_field('tyuukai_tesuuryou')!='仲介手数料選択'){ 
                                    the_field('tyuukai_tesuuryou');
                                 }else{
                                    echo '無料';
                            }
                            ?>
                                </td>
                            </tr>
                        <?php }else{ ?>
                            <?php if(get_field('tyuukai_tesuuryou')!='' && get_field('tyuukai_tesuuryou')!='仲介手数料選択'){ ?>
                            <tr>
                                <th>仲介手数料</th>
                                <td>
                                <?php
                                    the_field('tyuukai_tesuuryou');
                                // if(get_field('tyuukai_tesuuryou')!='仲介手数料選択'){
                                //     the_field('tyuukai_tesuuryou');
                                // }else{
                                //     echo '1ヶ月（税別）';
                                // }
                                ?>
                                </td>
                            </tr>
                            <?php } ?>
                        <?php } ?>
                        -->
                        <tr>
                            <th>保証会社</th>
                            <td>
                                <?php the_field('hosyougaisya'); ?>
                            </td>
                        </tr>
                        <tr>
                            <th>火災保険</th>
                            <td>
                                <?php the_field('kakakuhoken'); ?>
                            </td>
                        </tr>
                        <?php if(get_field('freerent', $post_id) !== "-" && !is_null(get_field('freerent', $post_id)) && !empty(get_field('freerent', $post_id))){ ?>
                        <tr>
                            <th>フリーレント</th>
                            <td>
                                <?php echo str_replace("[フリーレント]", "", get_field('freerent', $post_id)); ?>
                            </td>
                        </tr>
                        <?php } ?>
                        <?php if(get_field('keyakukeitai')!=='契約形態選択' && !empty(get_field('keyakukeitai'))){ ?>
                        <tr style="display:none;">
                            <th>契約形態</th>
                            <td>
                                <?php the_field('keyakukeitai'); ?>
                            </td>
                        </tr>
                        <?php } ?>
                        <?php if(get_field('kakakukoushin')!=='更新料選択' && !empty(get_field('kakakukoushin'))){ ?>
                        <tr>
                            <th>更新料</th>
                            <td>
                                <?php
                                if(is_numeric(get_field('kakakukoushin'))){
                                    echo number_format(get_field('kakakukoushin')) . "円";
                                }else{
                                    if(get_field('kakakukoushin')!==''){
                                        the_field('kakakukoushin');
                                    }
                                }
                                ?>
                            </td>
                        </tr>
                        <?php } ?>
                        <?php if(get_field('kakakushikibiki', $post_id) && get_field('kakakushikibiki', $post_id) !== '-'){ ?>
                        <tr>
                            <th>償却・敷引金</th>
                            <td>
                                <?php the_field('kakakushikibiki', $post_id); ?>
                            </td>
                        </tr>
                        <?php } ?>

                        <?php }//満室 ?>

                        <tr>
                            <th>取引態様</th>
                            <td>
                                <?php my_custom_torihikitaiyo_print($parent_post_ID); ?>
                            </td>
                        </tr>
                        <?php if(get_post_meta($parent_post_ID,'kanrininn',true) !='') { ?>
                        <tr>
                            <th>管理人</th>
                            <td>
                                <?php
                            my_custom_kanrininn_print($parent_post_ID); ?>
                            </td>
                        </tr>
                        <?php } ?>

                        <?php //if(get_field('bukken_status', $parent_post_ID) !== "" && get_field('bukken_status', $parent_post_ID) !== "2"){ //満室でなはい時表示　?>
                        <tr>
                            <th>駐車場</th>
                            <td>
                                <?php the_custom_chushajokubun_print($parent_post_ID); ?>
                            </td>
                        </tr>
                        <?php //}//満室 ?>
                        <?php if(get_post_meta($parent_post_ID,'shuuhenshougaku',true) !== ''){ ?>
                        <tr>
                            <th>学区域</th>
                            <td>
                                <?php the_custom_gakku($parent_post_ID); ?>
                            </td>
                        </tr>
                        <?php } ?>
                        <tr>
                            <th>所在地</th>
                            <td>
                                <?php my_custom_shozaichi_print($parent_post_ID);?>
                                <?php echo get_post_meta($parent_post_ID, 'shozaichimeisho', true);?>
                                <?php echo get_post_meta($parent_post_ID, 'shozaichimeisho2', true);?>
                            </td>
                        </tr>
                    </table>

                    <!-- 地図
                        include('template/googlemap.php');?>
                    <!-- // 地図 -->

                </section>
            </section>

            <?php if(get_field('bukken-remarks', $post_id)){ ?>
            <!-- 備考情報 -->
            <section class="o-rsingleRemarks o-rsingleCont" id="section05">
                <h2 class="o-rsingleCont_title flex-nw ai-center">備考欄<span class="o-rsingleCont_en">REMARKS</span></h2>
                <div class="o-rsinglePost">
                    <section class="o-rsinglePost_cont o-rsingleCont_wrap">


                        <div class="p-postCont __singleroom o-partsMore">
                            <input id="o-partsMore_triggerRemarks" class="o-partsMore_trigger" type="checkbox">
                            <label class="o-partsMore_btn" for="o-partsMore_triggerRemarks"><i class="o-partsMore_icon o-icon fas fa-angle-down"></i></label>

                            <div class="p-postCont __singleroom o-partsMore_cont">
                                <h2 class="o-title __title" style="display:none;">
                                    <?php the_field('bukken-remarks-title',$parent_post_ID);?>
                                </h2>
                                <p class="p-postCont_text">
                                    <?php the_field('bukken-remarks',$post_id);?>
                                </p>
                            </div>
                        </div>


                    </section>
                </div>
            </section>
            <?php } ?>

                <?php
            //--roomに関連の親情報を集める
            $args = array(
                'connected_type' => 'room_relation',
                'connected_items' => $parent_post_ID,
                'posts_per_page' => 30,
                'post__not_in' => array($post_id),
                'meta_query' => array(
                    'relation' => 'AND',
                    'meta_nyukyogenkyo' => array(
                        'relation' => 'OR',
                        array(
                            'relation' => 'OR',
                            array(
                            'key' => 'nyukyogenkyo',
                            'type' => 'numeric',
                            'value' => 3),//即入居可
                            array(
                            'key' => 'nyukyogenkyo',
                            'type' => 'numeric',
                            'value' => 2),//居住中
                            array(
                                'key' => 'nyukyogenkyo',
                                'type' => 'numeric',
                                'value' => 11),//空室
                            array(
                                'key' => 'nyukyogenkyo',
                                'type' => 'numeric',
                                'value' => 1),//満室
                            array(
                                'key' =>  'nyukyogenkyo',
                                'value' => '*'
                            )
                        ),
                    ),
                    'meta_kakaku' => array(
                        'key'=>'kakaku',
                        'type' => 'numeric',
                        'compare' => 'EXISTS',
                    )
                ),
                'orderby' => array( 'nyukyogenkyo' => 'DESC','meta_kakaku' => 'ASC'),
                'order' => 'DESC'
            );

            $connected_parent = get_posts($args);
            $bukken_mansitu_fg = get_field('bukken_status');
            
            if(count($connected_parent)>0){

            ?>
            <!-- 同じ物件のお部屋 -->
            <section class="o-rsingleRoom o-rsingleCont" id="section06">
                <h2 class="o-rsingleCont_title flex-nw ai-center">同じ物件のお部屋<span class="o-rsingleCont_en">ROOMS</span></h2>
                <div class="o-rsingleRoom_building o-bnr overimg mb-3">
                    <div class="o-bnrWrap">
                        <div class="o-rsingleRoom_thumb o-bnrThumb">
                            <img src="https://koukyu-chintai.com/wp-content/uploads/cropped-cropped-main-1-2000x1200-1-1024x614.jpg" alt="" class="o-rsingleRoom_img o-bnrImg">
                        </div>
                        <h3 class="o-rsingleRoom_title o-bnrTitle o-title __title __bold">
                            <?php echo $oya_title;?>の他のお部屋</h3>
                        <a href="" class="o-link __over"></a>
                    </div>
                </div>


                <div class="o-rsingleRoom_list flex-w o-partsMore">
                    <?php include('template/rooms-for-room-for-detail.php');?>
                </div>

            </section>
            <?php } ?>
            
            <!-- 周辺情報 -->
            <?php if(my_custom_koutsu1_for_nearby($parent_post_ID)){ ?>
            <section class="o-rsingleLocation o-rsingleCont" id="section07">
                <h2 class="o-rsingleCont_title flex-nw ai-center">周辺情報<span class="o-rsingleCont_en">LOCATION</span></h2>
                <div class="o-rsinglePost">
                    <div class="o-rsingleRoom_building o-bnr overimg">
                        <div class="o-bnrWrap">
                            <div class="o-rsingleRoom_thumb o-bnrThumb">
                                <img src="https://koukyu-chintai.com/wp-content/uploads/cropped-cropped-main-1-2000x1200-1-1024x614.jpg" alt="" class="o-rsingleRoom_img o-bnrImg">
                            </div>
                            <h3 class="o-rsingleRoom_title o-bnrTitle o-title __title __bold">
                                <?php
                            $koutsurosen_data = get_post_meta($parent_post_ID, 'koutsurosen1', true);
                            $koutsueki_data = get_post_meta($parent_post_ID, 'koutsueki1', true);
                            echo my_custom_koutsu1_for_nearby_distance($parent_post_ID)."の周辺情報";
                            ?>
                            </h3>
                        </div>
                    </div>
                    <section class="o-rsinglePost_cont o-rsingleCont_wrap">
                        <div class="p-postCont __singleroom o-partsMore">
                            <input id="o-partsMore_trigger" class="o-partsMore_trigger" type="checkbox">
                            <label class="o-partsMore_btn" for="o-partsMore_trigger"><i class="o-partsMore_icon o-icon fas fa-angle-down"></i></label>
                            <div class="o-partsMore_cont">
                                <p class="p-postCont_text">
                                    <?php echo my_custom_koutsu1_for_nearby($parent_post_ID);?>
                                </p>
                            </div>
                        </div>
                    </section>
                </div>
            </section>
            <?php }elseif(my_custom_koutsu2_for_nearby($post_id)){ ?>
                <section class="o-rsingleLocation o-rsingleCont" id="section07">
                <h2 class="o-rsingleCont_title flex-nw ai-center">周辺情報<span class="o-rsingleCont_en">LOCATION</span></h2>
                <div class="o-rsinglePost">
                    <div class="o-rsingleRoom_building o-bnr overimg">
                        <div class="o-bnrWrap">
                            <div class="o-rsingleRoom_thumb o-bnrThumb">
                                <img src="https://koukyu-chintai.com/wp-content/uploads/cropped-cropped-main-1-2000x1200-1-1024x614.jpg" alt="" class="o-rsingleRoom_img o-bnrImg">
                            </div>
                            <h3 class="o-rsingleRoom_title o-bnrTitle o-title __title __bold">
                                <?php
                            $koutsurosen_data = get_post_meta($parent_post_ID, 'koutsurosen2', true);
                            $koutsueki_data = get_post_meta($parent_post_ID, 'koutsueki2', true);
                            echo my_custom_koutsu1_for_nearby_distance($parent_post_ID)."の周辺情報";
                            ?>
                            </h3>
                        </div>
                    </div>
                    <section class="o-rsinglePost_cont o-rsingleCont_wrap">
                        <div class="p-postCont __singleroom o-partsMore">
                            <input id="o-partsMore_trigger" class="o-partsMore_trigger" type="checkbox">
                            <label class="o-partsMore_btn" for="o-partsMore_trigger"><i class="o-partsMore_icon o-icon fas fa-angle-down"></i></label>
                            <div class="o-partsMore_cont">
                                <p class="p-postCont_text">
                                    <?php echo my_custom_koutsu2_for_nearby($post_id);?>
                                </p>
                            </div>
                        </div>
                    </section>
                </div>
            </section>
            <?php } ?>

            <!-- よくあるご質問 -->
            <section class="o-rsingleFaq o-rsingleCont" id="section08">
                <?php include('template/faq.php');?>

            </section>

            <!-- コメント -->
            <section class="o-rsingleComment o-rsingleCont" id="section09">
                <h2 class="o-rsingleCont_title flex-nw ai-center">コメント<span class="o-rsingleCont_en">COMMENT</span></h2>
                <div class="o-rsingleComment_cont __main o-rsingleCont_wrap">
                    <?php  if( FUDOU_TRA_COMMENT )     comments_template( '', true ); ?>
                </div>

                <div class="o-rsingleComment_cont __posting o-rsingleCont_wrap">
                    <?php $args = array(
    'title_reply' => '',
    // 'title_reply' => $post->post_title.'へコメント投稿',
    'label_submit' => '投稿する',
    'comment_notes_before' => '<p class="o-rsingleComment_postTitle">コメントを投稿する<i class="o-rsingleComment_postTitle_icon o-icon far fa-angle-down"></i></p>',
    'comment_notes_after'  => '',
    'fields' => array(
        'author'    => '<div class="o-rsingleComment_form flex-nw"><span class="o-rsingleComment_formTitle">お名前</span><input type="text" name="author" class="o-rsingleComment_formInput o-formInput __sizem"></div>',
    ),
    'logged_in_as'=>false,
    'comment_field' => '<div class="o-rsingleComment_form flex-nw"><span class="o-rsingleComment_formTitle">コメント内容</span><input type="textarea" name="comment" class="o-rsingleComment_formInput o-formInput __sizem"></div>',
);
                comment_form( $args );
                ?>
                </div>
            </section>

            <!-- 注意書き -->
            <section class="o-rsingleCaution o-rsingleCont">
                <div class="o-rsingleCaution_wrap">
                    <h2 class="o-rsingleCaution_title o-title __p mb-1">注意書き</h2>
                    <ul class="o-rsingleCaution_list o-list __basic01 caution __mid">
                        <li class="o-rsingleCaution_item o-listItem">建物周辺情報はGoogleMapを使用しており、物件の表示位置が実際の場所と多少異なる場合があります。</li>
                        <li class="o-rsingleCaution_item o-listItem">掲載情報が現況と異なる場合は、現況を優先させていただきます。</li>
                        <li class="o-rsingleCaution_item o-listItem">部屋によって敷金・礼金の内容が異なる場合がございますので、詳しい情報は各部屋ページにてご確認ください。</li>
                        <li class="o-rsingleCaution_item o-listItem">分譲賃貸は部屋によってオーナーが異なる為、内装や設備が変更されている場合がございます。</li>
                        <li class="o-rsingleCaution_item o-listItem">ペットの飼育やSOHO利用に関しては、建物自体が許可を出していても、所有者の意向により禁止とされている場合もございますのでご注意ください。</li>
                        <li class="o-rsingleCaution_item o-listItem">駐車場ありの場合、空き状況等詳細につきましてはお問い合わせください。</li>
                        <li class="o-rsingleCaution_item o-listItem">物件の棟ページに掲載されてる室内写真は、該当物件の代表的な室内写真またはモデルルームの写真を掲載しております。</li>
                    </ul>
                </div>
            </section>

            <!-- 機能ボタン群 -->
            <section class="o-rsingleBtn o-rsingleCont">
                <div class="o-rsingleBtn_wrap flex-nw jc-center">
                    <a href="" class="o-rsingleBtn_item __prev o-btn __basic02 __mid" onclick="javascript:window.history.back(-1);return false;"><i class="o-icon __mid mr-1 fas fa-angle-left"></i>前のページに戻る</a>
                    <a id="js-saveModal" href="#" class="o-rsingleBtn_item o-btn __basic02 __mid"><i class="o-icon __mid flaticon-history"></i>過去の検索履歴</a>
                    <a href="" class="o-rsingleBtn_item o-btn __basic02 __mid js-modal"><i class="o-icon __mid flaticon-search"></i>検索条件を変更する</a>
                </div>
            </section>

        </div>
    </div>



    <div class="l-rowRight roomdesc js-fixedTop">
        <div class="o-rsingleAction __building js-fixedStart">
            <div class="o-rsingleAction_wrap">
                <?php if(get_field('bukken_status', $parent_post_ID) !== "2"){ //満室でなはい時表示　?>
                
                <div>
                <img src="<?php echo $madori_image_for_sidebar;?>">
                </div>
                <div class="o-rsingleAction_price o-rsingleAction_cont">
                    <?php echo $kakaku;?> <span class="o-rsingleAction_unit"> 円/月</span></div>
                <div class="o-rsingleAction_cost o-rsingleAction_cont flex-nw ai-center withfreerent">
                    <div class="o-rsingleAction_costItem __deposit">
                        <span class="o-rsingleAction_costItem_title">敷金</span>
                        <p class="o-rsingleAction_costItem_period">
                            <?php the_field('kakakushikikin');?>
                        </p>
                    </div>
                    <div class="o-rsingleAction_costItem __gratuity">
                        <span class="o-rsingleAction_costItem_title">礼金</span>
                        <p class="o-rsingleAction_costItem_period">
                            <?php the_field('kakakureikin');?>
                        </p>
                    </div>
                    <div class="o-rsingleAction_costItem __manege">
                        <span class="o-rsingleAction_costItem_title">管理費</span>
                        <p class="o-rsingleAction_costItem_period">
                            <?php
                        if(number_format(get_field('kakakukyouekihi')) == 0){
                            echo '0円';
                        }else{
                        echo @number_format(get_field('kakakukyouekihi'));
                        }
                    ?>
                        </p>
                    </div>
                    <?php if(get_field('freerent', $post_id)){ ?>
                    <div class="o-rsingleAction_costItem __freerent">
                        <span class="o-rsingleAction_costItem_title">フリーレント</span>
                        <p class="o-rsingleAction_costItem_period">
                            <?php echo str_replace("[フリーレント]", "", get_field('freerent', $post_id));?>
                        </p>
                    </div>
                    <?php } ?>
                </div>
                <div class="o-rsingleAction_function o-rsingleAction_cont flex-nw jc-center ai-center">
                    <a id="js-estimateModal" class=" o-rsingleAction_functionItem __estimate flex-nw ai-center fb-50"><i class="o-icon __min flaticon-calculator"></i><span class="o-rsingleAction_functionText">初期費用<br>概算見積もり</span></a>
                    <a href="<?php echo $favorite_link;?>" class="o-rsingleAction_functionItem __save flex-nw ai-center fb-50" data-fvflg="<?php echo ($fvflg) ? 'sumi' : 'mi';?>" data-pid="<?php echo $parent_post_ID;?>">
                        <i class="o-icon __min flaticon-love <?php echo ($fvflg) ? 'flaticon-heart __checked' : 'flaticon-heart';?>"></i>
                        <span class="o-rsingleAction_functionText">
                            <?php echo ($fvflg) ? '検討リストから削除する' : '検討リストに追加する';?></span>
                    </a>
                </div>
                <div class="o-rsingleAction_btn o-rsingleAction_cont">
                    <a href="<?php echo home_url() . "/" . get_field('kengaku_nairan_link', 'option' );?>?post_id=
                        <?php echo $post_id;?>" class="o-rsingleAction_btnVisit o-btn __basic01">見学・内覧する</a>
                    <div class="flex-nw">
                        <a href="<?php echo home_url() . "/" . get_field('kuusitu_inquiry_link', 'option' );?>?post_id=
                            <?php echo $post_id;?>" class="o-rsingleAction_btnEmpty o-btn __basic06 flex-nw jc-center ai-center fb-50">空室状況を<br>問い合わせる</a>
                        <a href="<?php echo home_url() . "/" . get_field('bukken_inquiry_link', 'option' );?>?post_id=
                            <?php echo $post_id;?>" class="o-rsingleAction_btnQuestion o-btn __basic06 flex-nw jc-center ai-center fb-50">物件について<br>質問する</a>
                    </div>
                </div>
                <div class="o-rsingleAction_tel u-text-ac">
                    <p class="o-rsingleAction_telTitle">電話で問い合わせする</p>
                    <p class="o-rsingleAction_telTel">
                        <?php the_field('free_dial', 'option');?>
                    </p>
                    <div class="o-rsingleAction_telInfo flex-nw jc-center ai-center">問い合わせ物件番号 :
                        <?php echo $post_id;//get_post_meta($parent_post_ID, 'shikibesu', true);?>
                    </div>
                </div>

                <?php } else { //満室の場合は?>

                <div class="o-rsingleAction_price o-rsingleAction_cont">現在満室のため募集しておりません。</div>

                <?php } // 満室 ?>
            </div>
            <div class="o-rsingleAction_sns flex-nw jc-center ai-center"></div>

        </div>
        <div class="js-fixedEnd"></div>
    </div>


</main>



<?php get_footer('single'); ?>


<?php include('template/fixednav2.php');?>
