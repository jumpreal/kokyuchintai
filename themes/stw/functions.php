<?php
require_once get_template_directory() . '/inc/modal-search-utils.php';
require_once get_template_directory() . '/inc/favorite-posts-ajax.php';
require_once get_template_directory() . '/inc/cron-functions.php';
define("DEBUGFLG", false);
add_filter( 'wp_resource_hints', 'twentyseventeen_resource_hints', 10, 2 );

function twentyseventeen_resource_hints( $urls, $relation_type ) {

    if ( wp_style_is( 'twentyseventeen-fonts', 'queue' ) && 'preconnect' === $relation_type ) {

        $urls[] = array(

            'href' => 'https://fonts.gstatic.com',

            'crossorigin',

        );

    }



    return $urls;

}

add_filter( 'wp_resource_hints', 'twentyseventeen_resource_hints', 10, 2 );



/**

* Register widget area.

*

* @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar

*/

function twentyseventeen_widgets_init() {

    register_sidebar( array(

        'name'          => __( 'Blog Sidebar', 'twentyseventeen' ),

        'id'            => 'sidebar-1',

        'description'   => __( 'Add widgets here to appear in your sidebar on blog posts and archive pages.', 'twentyseventeen' ),

        'before_widget' => '<section id="%1$s" class="widget %2$s">',

        'after_widget'  => '</section>',

        'before_title'  => '<h2 class="widget-title">',

        'after_title'   => '</h2>',

    ) );



    register_sidebar( array(

        'name'          => __( 'Footer 1', 'twentyseventeen' ),

        'id'            => 'sidebar-2',

        'description'   => __( 'Add widgets here to appear in your footer.', 'twentyseventeen' ),

        'before_widget' => '<section id="%1$s" class="widget %2$s">',

        'after_widget'  => '</section>',

        'before_title'  => '<h2 class="widget-title">',

        'after_title'   => '</h2>',

    ) );



    register_sidebar( array(

        'name'          => __( 'Footer 2', 'twentyseventeen' ),

        'id'            => 'sidebar-3',

        'description'   => __( 'Add widgets here to appear in your footer.', 'twentyseventeen' ),

        'before_widget' => '<section id="%1$s" class="widget %2$s">',

        'after_widget'  => '</section>',

        'before_title'  => '<h2 class="widget-title">',

        'after_title'   => '</h2>',

    ) );



    /*----------------------------------------------------------
     TOPビジュアルエリア　ウィジェット追加（STW森追加）
     ------------------------------------------------------------*/
    register_sidebar( array(
        'name'          => __( 'TOPビジュアルエリア'),
        'id'            => 'widget-1',
        'description'   => __( 'トップページメイン画像表示' ),
        'before_widget' => '<section id="%1$s" class="widget %2$s">',
        'after_widget'  => '</section>',
        'before_title'  => '<h2 class="widget-title">',
        'after_title'   => '</h2>',
    ) );

    /*----------------------------------------------------------
     TOPピックアップ　ウィジェット追加（STW森追加）
     ------------------------------------------------------------*/
    register_sidebar( array(
        'name' => 'TOPピックアップ',
        'id' => 'widget-2',
        'description' => 'トップページピックアップ物件表示',
        'before_widget' => '<section id="%1$s" class="widget %2$s">',
        'after_widget'  => '</section>',
        'before_title'  => '<h2 class="widget-title">',
        'after_title'   => '</h2>',
    ) );


    //---------------------------------------------------------------------------
    //　サイドバーの設定
    //　nameとidを変更すれば幾つでも登録できる。
    //　ダッシュボードの外観>ウィジェットから中身を設定可能。
    //---------------------------------------------------------------------------
    register_sidebar( array(
        'name' => 'SidebarSW-1',
        'id' => 'sidebarsw-1',
        'description' => 'Widget area of sidebar',
        'before_widget' => '<div id="%1$s" class="widget %2$s sidebar">',
        'after_widget' => '</div>',
        'before_title'  => '<h4 class="text-center widgettitle">',
        'after_title'   => '</h4>'
    ) );

}
add_action( 'widgets_init', 'twentyseventeen_widgets_init' );


//---------------------------------------------------------------------------
//
//　不動産のテンプレートを色々キャンセルして独自テンプレに集約
//
//---------------------------------------------------------------------------

//--post関係
remove_filter( 'template_include', 'get_post_type_top_template_fudou' );
remove_filter( 'template_include', 'get_post_type_single_template_fudou' );
remove_filter( 'template_include', 'get_post_type_single_template_fudou' );
remove_filter( 'template_include', 'get_post_type_archive_template_fudou' , 11);

//--元のウィジェットのTOP用クラスを停止させる
remove_filter( 'widgets_init', 'fudo_widgetInit_top_r');
remove_filter( 'widgets_init', 'fudo_widget_top_r');
//--トップのウィジェットはこっちで編集
require_once locate_template('inc/fudo-top-widget.php', true);


//オリジナルテーマ内 archive-fudo.php を読み込むように再設定。
function fudo_body_org_class( $class ) {
    $class[0] = 'archive archive-fudo';
    return $class;
}
function get_post_type_archive_org_template( $template = '' ) {
    global $wp_query;
    $cat = $wp_query->get_queried_object();
    $cat_name = isset( $cat->taxonomy ) ? $cat->taxonomy : '';

    global $template; // テンプレートファイルのパスを取得
    $temp_name = basename($template); // パスの最後の名前（ファイル名）を取得

    if($temp_name !== 'page-map.php'){
        if ( isset( $_GET['bukken'] ) || isset( $_GET['bukken_tag'] )
            || $cat_name == 'bukken' || $cat_name =='bukken_tag' ) {
                status_header( 200 );
                $template = locate_template( array('archive-fudo.php', 'archive.php') );
                add_filter( 'body_class', 'fudo_body_org_class' );
            }
    }
    return $template;
}
add_filter( 'template_include', 'get_post_type_archive_org_template' , 11 );


//標準切替キャンセル (不動産プラグイン v1.5.0 ～)
remove_action( 'wp_enqueue_scripts', 'add_header_css_js_fudou', 12 );
//不動産閲覧履歴プラグイン の CSS を読み込まなくする (v1.5.0 ～)
remove_action( 'wp_enqueue_scripts', 'add_header_history_css_fudou', 12 );
//不動産トップスライダープラグインの CSS を読み込まなくする (v1.5.0 ～)
remove_action( 'wp_enqueue_scripts', 'add_header_topslider_css_fudou', 12 );
//シェアボタンは他ではいらない
if( isset( $fudou_share_buttons ) ){
    remove_filter( 'the_content', array( $fudou_share_buttons, 'fudou_share_the_content' ), 99 );
}




//---------------------------------------------------------------------------
//　パスワード保護ページの有効期限
//　60 * MINUTE_IN_SECONDS　＝60分
//　HOUR_IN_SECONDS　＝1時間
//　7 * DAY_IN_SECONDS　＝7日
//---------------------------------------------------------------------------
function custom_postpass_time() {
    require_once ABSPATH . 'wp-includes/class-phpass.php';
    $hasher = new PasswordHash( 8, true );
    setcookie( 'wp-postpass_' . COOKIEHASH, $hasher->HashPassword( wp_unslash( $_POST['post_password'] ) ), time()+(DAY_IN_SECONDS), COOKIEPATH );
    wp_safe_redirect( wp_get_referer() );
    exit();
}
add_action( 'login_form_postpass', 'custom_postpass_time' );

//---------------------------------------------------------------------------
//　保護ページの「保護中」の文字を削除
//---------------------------------------------------------------------------
add_filter('protected_title_format', 'remove_protected');
function remove_protected($title) {
    return '%s';
}


//---------------------------------------------------------------------------
//　サムネイルを使えるようにする
//---------------------------------------------------------------------------
add_theme_support('post-thumbnails');

/* サムネイルのサイズを指定（追加）する */
add_image_size( 'archive_thumbnail', 250, 200, false );

add_theme_support( 'post-thumbnails', array( 'post','page','fudo','room') );


//---------------------------------------------------------------------------
//　固定ページのパーマリンクの最後に.htmlを付ける
//---------------------------------------------------------------------------
/*
 add_action( 'init', 'edit_page_url' );
 if ( ! function_exists( 'edit_page_url' ) ) {
 function edit_page_url() {
 global $wp_rewrite;
 $wp_rewrite->use_trailing_slashes = false;
 $wp_rewrite->page_structure = $wp_rewrite->root . '%pagename%.html';
 // flush_rewrite_rules( false );
 }
 }
 */

//---------------------------------------------------------------------------
//　dns_prefetchの無効化
//---------------------------------------------------------------------------
add_filter( 'wp_resource_hints', 'remove_dns_prefetch', 10, 2 );
function remove_dns_prefetch( $hints, $relation_type ) {
    if ( 'dns-prefetch' === $relation_type ) {
        return array_diff( wp_dependencies_unique_hosts(), $hints );
    }
    return $hints;
}


//---------------------------------------------------------------------------
//　カスタムメニューの設定
//---------------------------------------------------------------------------
add_theme_support( 'menus' );
register_nav_menu( 'gnavi', 'Header navigation menu' );
register_nav_menu( 'footer_navi', 'Footer navigation menu' );

//　カスタムメニューのクラスをスリム化
add_filter( 'nav_menu_css_class', 'my_css_attributes_filter', 100, 1 );
add_filter( 'nav_menu_item_id', 'my_css_attributes_filter', 100, 1 );
add_filter( 'page_css_class', 'my_css_attributes_filter', 100, 1 );
function my_css_attributes_filter( $var ) {
    return is_array($var) ? array_intersect($var, array( 'current-menu-item','l-headerNav_functionSearch','l-headerNav_functionList','l-headerNav_functionSave','l-headerNav_functionHistory' )) : '';
}


/**

* Add a pingback url auto-discovery header for singularly identifiable articles.

*/

function twentyseventeen_pingback_header() {

    if ( is_singular() && pings_open() ) {

        printf( '<link rel="pingback" href="%s">' . "\n", get_bloginfo( 'pingback_url' ) );

    }

}
add_action( 'wp_head', 'twentyseventeen_pingback_header' );


function twentyseventeen_header_image_tag( $html, $header, $attr ) {

    if ( isset( $attr['sizes'] ) ) {

        $html = str_replace( $attr['sizes'], '100vw', $html );

    }

    return $html;

}

add_filter( 'get_header_image_tag', 'twentyseventeen_header_image_tag', 10, 3 );

/**

* Implement the Custom Header feature.

*/

require get_parent_theme_file_path( '/inc/custom-header.php' );





//---------------------------------------------------------------------------
//　外部ファイルの読み込み
// 例えば、カスタム投稿タイプを作成する関数を記述したcustom-post-type.php
//---------------------------------------------------------------------------
//include_once(TEMPLATEPATH ."/custom-post-type.php");


/*
 *管理画面 「投稿」を「新着情報」文字に。
 */
function change_post_menu_label() {
    global $menu;
    global $submenu;
    $menu[5][0] = '新着情報';
    $submenu['edit.php'][5][0] = '新着情報一覧';
    $submenu['edit.php'][10][0] = '新しい新着情報';
    $submenu['edit.php'][16][0] = 'タグ';
    //echo ";
}
function change_post_object_label() {
    global $wp_post_types;
    $labels = &$wp_post_types['post']->labels;
    $labels->name = '新着情報';
    $labels->singular_name = '新着情報';
    $labels->add_new = _x('追加', '新着情報');
    $labels->add_new_item = '新着情報の新規追加';
    $labels->edit_item = '新着情報の編集';
    $labels->new_item = '新規新着情報';
    $labels->view_item = '新着情報を表示';
    $labels->search_items = '新着情報を検索';
    $labels->not_found = '記事が見つかりませんでした';
    $labels->not_found_in_trash = 'ゴミ箱に記事は見つかりませんでした';
}
add_action( 'init', 'change_post_object_label' );
add_action( 'admin_menu', 'change_post_menu_label' );


/*
 * 投稿にアーカイブ(投稿一覧)を持たせるようにします。
 */
function post_has_archive( $args, $post_type ) {
    if ( 'post' == $post_type ||'fudo' == $post_type ) {
        $args['rewrite'] = true;
        $args['has_archive'] = 'post-all'; // ページ名
    }
    return $args;
}
add_filter( 'register_post_type_args', 'post_has_archive', 10, 2 );



//-- 最初の画像をアイキャッチにする
function catch_that_image() {
    global $post, $posts;
    $first_img = '';
    ob_start();
    ob_end_clean();
    $output = preg_match_all('/<img.+src=[\'"]([^\'"]+)[\'"].*>/i', $post->post_content, $matches);
    $first_img = $matches [1] [0];

    if(empty($first_img)){
        // 記事内で画像がなかったときのためのデフォルト画像を指定
        $first_img = "/wp/wp-content/plugins/fudou/img/nowprinting.jpg";
    }
    return $first_img;
}

//-- 最初の画像をアイキャッチにする
function catch_that_image2($str) {
    $first_img = '';
    ob_start();
    ob_end_clean();
    $output = preg_match_all('/<img.+src=[\'"]([^\'"]+)[\'"].*>/i', $str, $matches);
    $first_img = $matches [1] [0];
    if(empty($first_img)){
        // 記事内で画像がなかったときのためのデフォルト画像を指定
        $first_img = "/wp/wp-content/plugins/fudou/img/nowprinting.jpg";
    }
    return $first_img;
}



//（抜粋）の文字数調整
function new_excerpt_length($length) {
    return 10;
}
add_filter('excerpt_mblength', 'new_excerpt_length');




//--　管理画面ビュー切り替え時にspanなどタグが消えるのを防止する
function my_tiny_mce_before_init( $init_array ) {
    $init_array['valid_elements']          = '*[*]';
    $init_array['extended_valid_elements'] = '*[*]';
    return $init_array;
}
add_filter( 'tiny_mce_before_init' , 'my_tiny_mce_before_init' );


//--　全体の自動整形を解除・無効にする場合
add_action('init', function() {
    remove_filter('the_excerpt', 'wpautop');
    remove_filter('the_content', 'wpautop');
});

    add_filter('tiny_mce_before_init', function($init) {
        $init['wpautop'] = false;
        $init['apply_source_formatting'] = false;
        return $init;
    });

/**
 * ビジュアルエディタに切り替えで、空の span タグや i タグが消されるのを防止
 */
if ( ! function_exists('tinymce_init') ) {
    function tinymce_init( $init ) {
        $init['verify_html'] = false; // 空タグや属性なしのタグを消させない
        $initArray['valid_children'] = '+body[style], +div[div|span|a], +span[span]'; // 指定の子要素を消させない
        return $init;
    }
    // add_filter( 'tiny_mce_before_init', 'tinymce_init', 100 );
}

        //-- 固定ページにカテゴリをつける
        add_action('init','add_categories_for_pages');
        function add_categories_for_pages(){
            register_taxonomy_for_object_type('category', 'page');
        }
        add_action( 'pre_get_posts', 'nobita_merge_page_categories_at_category_archive' );
        function nobita_merge_page_categories_at_category_archive( $query ) {
            if ( $query->is_category== true && $query->is_main_query() ) {
                $query->set('post_type', array( 'post', 'page', 'nav_menu_item'));
            }
        }





        //Custom CSSFile Widget

        add_action('admin_menu', 'custom_css2_hooks');
        add_action('save_post', 'save_custom_css2');
        add_action('wp_head','insert_custom_css2');
        function custom_css2_hooks() {
            add_meta_box( 'my_custom_shikibesu', '物件番号', 'my_custom_shikibesu_in', 'room', 'advanced' );
            add_meta_box( 'my_custom_bukkenshubetsu_area', '物件種別', 'my_custom_bukkenshubetsu_in', 'room', 'advanced' );
            // add_meta_box( 'my_custom_setsubi_area', '設備・条件', 'my_custom_setsubi_in', 'room', 'advanced' );
            add_meta_box('custom_css2', '個別CSSファイル', 'custom_css2_input', 'post', 'normal', 'high');
            add_meta_box('custom_css2', '個別CSSファイル', 'custom_css2_input', 'page', 'normal', 'high');
        }
        function custom_css2_input() {
            global $post;
            echo '<input type="hidden" name="custom_css2_noncename" id="custom_css2_noncename" value="'.wp_create_nonce('custom-css2').'" />';
            echo '<textarea name="custom_css2" id="custom_css2" rows="5" cols="30" style="width:100%;">'.get_post_meta($post->ID,'_custom_css2',true).'</textarea>';
        }
        function save_custom_css2($post_id) {
            if (!wp_verify_nonce($_POST['custom_css2_noncename'], 'custom-css2')) return $post_id;
            if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) return $post_id;
            $custom_css2 = $_POST['custom_css2'];
            update_post_meta($post_id, '_custom_css2', $custom_css2);
        }
        function insert_custom_css2() {
            if (is_page() || is_single()) {
                if (have_posts()) : while (have_posts()) : the_post();

                if (get_post_meta(get_the_ID(), '_custom_css2', true) !='') {

                    $string=get_post_meta(get_the_ID(), '_custom_css2', true);
                    if(strstr($string, PHP_EOL)!==true){//--改行がある場合は複数ファイルに対応
                        $st_array = explode(PHP_EOL,$string);
                        foreach($st_array as $st){
                            echo "<link rel=\"stylesheet\" href=\"";
                            echo bloginfo('template_url').str_replace(array("\r\n","\r","\n"),'',$st)."\" type=\"text/css\" media=\"all\" />\n";
                        }
                    }else{
                        echo "<link rel=\"stylesheet\" href=\"";
                        echo bloginfo('template_url').$string."\" type=\"text/css\" media=\"all\" />\n";
                    }
                }
                endwhile; endif;
                rewind_posts();
            }
        }

        //Custom CSS Widget
        /*
         add_action('admin_menu', 'custom_css_hooks');
         add_action('save_post', 'save_custom_css');
         add_action('wp_head','insert_custom_css');
         function custom_css_hooks() {
         add_meta_box('custom_css', '個別CSS', 'custom_css_input', 'post', 'normal', 'high');
         add_meta_box('custom_css', '個別CSS', 'custom_css_input', 'page', 'normal', 'high');
         }
         function custom_css_input() {
         global $post;
         echo '<input type="hidden" name="custom_css_noncename" id="custom_css_noncename" value="'.wp_create_nonce('custom-css').'" />';
         echo '<textarea name="custom_css" id="custom_css" rows="5" cols="30" style="width:100%;">'.get_post_meta($post->ID,'_custom_css',true).'</textarea>';
         }
         function save_custom_css($post_id) {
         if (!wp_verify_nonce($_POST['custom_css_noncename'], 'custom-css')) return $post_id;
         if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) return $post_id;
         $custom_css = $_POST['custom_css'];
         update_post_meta($post_id, '_custom_css', $custom_css);
         }
         function insert_custom_css() {
         if (is_page() || is_single()) {
         if (have_posts()) : while (have_posts()) : the_post();
         if (get_post_meta(get_the_ID(), '_custom_css', true) !='') {
         echo "<style type=\"text/css\" media=\"all\">\n".get_post_meta(get_the_ID(), '_custom_css', true)."\n</style>\n";
         }
         endwhile; endif;
         rewind_posts();
         }
         }
         */



        //部屋投稿（カスタム）の追加と、カテゴリー、タグ機能の追加
        function my_custom_init() {
            //部屋投稿（カスタム）投稿「room」の設定
            register_post_type( 'room', array(
                'labels' =>  array(
                    'name' =>__('部屋編集'),
                    'all_items' => '部屋一覧',
                    'add_new' => '部屋を追加',
                    'add_new_item' => '部屋を追加',
                    'edit_item' => '部屋を編集',
                    'view_item' => '部屋を表示',
                    'singular_name'=> __('room')),
                'public' => true,
                'supports' => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'custom-fields' ,'comments' ),
                'menu_position' => 7,
                'has_archive' => true
            ));

            //      register_taxonomy_for_object_type('bukken', 'room');  //--カテ共有
            //      register_taxonomy_for_object_type('bukken_tag', 'room');　//--タグ共有

        }
        add_action( 'init', 'my_custom_init' );

        //--管理画面に関連物件を表示オプション
        function manage_room_th($columns) {
            $columns['kanrenn'] = "種別/価格/関連物件";
            return $columns;
        }
        add_filter( 'manage_edit-room_columns', 'manage_room_th' );

        function manage_room_td($column_name, $post_id) {
            global $post, $posts;

            if( $column_name == 'kanrenn' ) {
                $args = array(
                    'connected_type' => 'room_relation',
                    'connected_items' => $post_id
                );
                $connected_parent = get_posts($args);
                echo xls_custom_bukkenshubetsu_print($post_id)."　";
                $str=get_post_meta($post_id,'chinryo',true);
                $num = preg_replace('/[^0-9]/', '',$str);
                $tani=preg_replace('/'.$num.'/','',$str);
                echo number_unit($num).$tani."<br>";
                echo '<a href="/wp-admin/post.php?post='.$connected_parent[0]->ID.'&action=edit">'.$connected_parent[0]->post_title.'</a>';
            }
        }

        function xls_custom_bukkenshubetsu_print($post_id) {
            global $work_bukkenshubetsu;
            $bukkenshubetsu_data = get_post_meta($post_id,'bukkenshubetsu',true);
            $st="";
            foreach($work_bukkenshubetsu as $meta_box){
                if($bukkenshubetsu_data == $meta_box['id'] )
                    $st.=$meta_box['name'];
            }
            return $st;
        }
        add_action( 'manage_room_posts_custom_column', 'manage_room_td', 10, 2 );


        //--管理画面に表示順を表示（物件と部屋）
        function manage_room_th2($columns) {
            $columns['display_order'] = "表示順<br>(大数字優先)";
            return $columns;
        }
        add_filter( 'manage_edit-room_columns', 'manage_room_th2' );
        add_filter( 'manage_edit-fudo_columns', 'manage_room_th2' );

        function manage_room_td2($column_name, $post_id) {
            global $post, $posts;
            if( $column_name == 'display_order'){
                echo get_post_meta($post_id,'display_order',true);
            }
        }
        add_action( 'manage_room_posts_custom_column', 'manage_room_td2', 10, 2 );
        add_action( 'manage_fudo_posts_custom_column', 'manage_room_td2', 10, 2 );




        //--物件管理画面に関連物件
        function manage_room_th3($columns) {
            $columns['kanrenn'] = "関連物件";
            return $columns;
        }
        add_filter( 'manage_edit-fudo_columns', 'manage_room_th3' );

        function manage_fudo_td3($column_name, $post_id) {
            global $post, $posts;
            if( $column_name == 'kanrenn'){
                $args = array(
                    'connected_type' => 'room_relation',
                    'connected_items' => $post_id
                );
                $connected_parent = get_posts($args);
                if(count($connected_parent)>0){
                    foreach($connected_parent as $k=>$obj){
                        echo '<a href="/wp-admin/post.php?post='.$obj->ID.'&action=edit">'.$obj->post_title.'</a><br>';
                    }
                }
            }
        }
        add_action( 'manage_fudo_posts_custom_column', 'manage_fudo_td3', 10, 2 );




        // アイキャッチ画像表示用
        function get_thumb_img($size = 'full',$id) {
            $thumb_id = $id;                         // アイキャッチ画像のIDを取得
            $thumb_img = wp_get_attachment_image_src($thumb_id, $size);  // $sizeサイズの画像内容を取得
            $thumb_src = $thumb_img[0];    // 画像のurlだけ取得
            return '<img class="box1image" src="'.$thumb_src.'" alt=""/>';
        }


        function add_post_category_archive( $wp_query ) {
            if ($wp_query->is_main_query() && $wp_query->is_category()) {
                $wp_query->set( 'post_type', array('fudo','room'));
            }
        }
        add_action( 'pre_get_posts', 'add_post_category_archive' , 10 , 1);

        function add_post_tag_archive( $wp_query ) {
            if ($wp_query->is_main_query() && $wp_query->is_tag()) {
                $wp_query->set( 'post_type', array('fudo','room'));
            }
        }
        add_action( 'pre_get_posts', 'add_post_tag_archive' , 10 , 1);



        // カスタム投稿タイプの紐付け　p2pプラグイン実行
        function custom_post_relation() {
            p2p_register_connection_type( array(
                'name' => 'room_relation', // 紐付けグループの名称
                'from' => 'room', // 出力されるアイテム
                'to' => 'fudo', // 出力する投稿タイプ
            ) );
        }
        add_action( 'p2p_init', 'custom_post_relation' );


        //--管理画面にクラス追加「my-admin-class-○○」○○＝post_type
        add_filter( 'admin_body_class', 'add_admin_body_class' );
        function add_admin_body_class( $classes ) {
            $screen = get_current_screen();
            if ( $screen->base == 'edit' || $screen->base == 'post' ) {
                $classes .= 'my-admin-class-' . $screen->post_type;
            }
            return $classes;
        }
        //--管理画面のスタイルシート
        function my_admin_style(){
            wp_enqueue_style( 'my_admin_style', get_template_directory_uri().'/assets/css/my_admin_style.css' );
            wp_enqueue_script('custom_admin_script',"https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js", array('jquery'));
            wp_enqueue_script( 'my_admin_script', get_template_directory_uri().'/assets/js/my_admin_script.js');
        }
        add_action( 'admin_enqueue_scripts', 'my_admin_style' );




        //★ルーム記事公開前にアラート
        function confirm_publish(){
            global $post,$wpdb;

            if(get_post_type( get_the_ID() )=="room"){
                $lastid = $wpdb->get_var($wpdb->prepare("SELECT auto_increment FROM information_schema.tables WHERE table_name = 'wp_posts'",array()));
                $re=get_post_meta(get_the_ID(),'shikibesu',true)=="";
                $srt="";
                $gmt = time()+9*3600;
                $reiwa=(gmdate('y', $gmt)-18);


                $args = array(
                    'connected_type' => 'room_relation',
                    'connected_items' => get_the_ID()
                );
                $connected_parent = get_posts($args);
                $p2p_id=$connected_parent[0]->p2p_id;
                $p2p_id=$p2p_id==NULL?0:$p2p_id;


                if($re){
                    $srt="var URL=$(\".p2p-col-title\").find(\"a\").attr(\"title\");\n";
                    $srt.="var str = URL.split(\"/\");\n";
                    $srt.="var su=str.length-1;\n";
                    $srt.="var ch=(\"0000\" + str[su]).slice( -4 );\n";
                    $srt.="var ret = \"".$reiwa."\"+ch+\"".sprintf('%04d',get_the_ID())."\";\n";
                    $srt.='$("#shikibesu").val(ret);';
                }else{

                    $srt="var choice=$('input[name^=\"p2p_connections\"]').val();\n";
                    $srt.="if(choice!==".$p2p_id."){\n";
                    $srt.="var URL=$(\".p2p-col-title\").find(\"a\").attr(\"title\");\n";
                    $srt.="var str = URL.split(\"/\");\n";
                    $srt.="var su=str.length-1;\n";
                    $srt.="var ch=(\"0000\" + str[su]).slice( -4 );\n";
                    $srt.="var ret = \"".$reiwa."\"+ch+\"".sprintf('%04d',get_the_ID())."\";\n";
                    $srt.='$("#shikibesu").val(ret);';
                    $srt.="}\n";

                }
                echo '<script type="text/javascript"><!--
    $("#publish").on("click", function(){
        var cnt = $(\'input[name^="p2p_connections"]\').length;
        if(cnt==1){
            '.$srt.'
        return true;
        }else if(cnt>=2){
            alert("「物件の関連づけ」は１箇所のみです");
            return false;
        }else{
            alert("「物件への関連づけ」を行ってください");
            return false;
        }
    });
    $(function() {
        $("#normal-sortables").prepend($("#advanced-sortables").children("#my_custom_setsubi_area"));
        $("#my_custom_setsubi_area").prependTo($("#postexcerpt"));
    });
    // --></script>';
            }
            if(get_post_type( get_the_ID() )=="fudo"){
                $args = array(
                    'connected_type' => 'room_relation',
                    'connected_items' => $post->ID
                );
                $connected_parent = get_posts($args);
                $co=count($connected_parent);
                if($co>0){
                    echo '<script type="text/javascript"><!--
            $("#publish").on("click", function(){
                if($("#visibility-radio-private").prop("checked")){
                return confirm("関連された部屋も全て「非公開処理」が行われますが、\n再度公開させる場合は各部屋ごとに手動で行ってください。");
                }
            });
            $("#delete-action>a").on("click", function(){
                var cn=1;
                return confirm("関連された部屋情報はゴミ箱処理が行われず残ります。\n各関連部屋の情報は手動で削除してください。");
            });
        // --></script>';
                }
            }
        }
        add_action('admin_footer', 'confirm_publish');




        //★親物件の挙動に子をあわせる
        add_action( 'transition_post_status', 'transition_function', 10, 3 );
        function transition_function( $new_status, $old_status, $post ) {
            global $post;

            switch(true){
                case $post->post_type=="fudo"&&$new_status=="publish"&&$old_status="private":
                case $post->post_type=="fudo"&&$new_status=="private"&&$old_status="publish":
                case $post->post_type=="fudo"&&$new_status=="trash"&&($old_status="publish"||$old_status="private"):

                    //--roomに関連の親情報を集める
                    $args = array(
                    'connected_type' => 'room_relation',
                    'connected_items' => $post->ID
                    );
                    $connected_parent = get_posts($args);

                    if(count($connected_parent)>0){
                        foreach($connected_parent as $ck=>$cv){
                            remove_action('transition_post_status', 'transition_function');
                            $my_post  = array();
                            $my_post['ID']  = $cv->ID;
                            $my_post['post_status'] = $new_status; // 下書き
                            wp_update_post( $my_post );
                            add_action('transition_post_status', 'transition_function');
                        }
                    }

                    break;
                case $post->post_type=="fudo"&&$new_status=="publish"&&$old_status="publish":
                case $post->post_type=="room"&&$new_status=="publish"://--子を更新した時に親のMETAをUPする
                    $args = array(
                    'connected_type' => 'room_relation',
                    'connected_items' => $post->ID
                    );
                    $connected_parent = get_posts($args);
                    if(count($connected_parent)>0){
                        foreach($connected_parent as $ck=>$cv){
                            remove_action('transition_post_status', 'transition_function');
                            $re=get_post_meta($cv->ID);
                            foreach($re as $mk=>$mv){
                                update_post_meta($post->ID,$mk,$mv[0]);
                            }
                            add_action('transition_post_status', 'transition_function');
                        }
                    }
                    break;
            }

        }


        function my_connection_types() {
            // Posts 2 Posts プラグインが有効化されてるかチェック
            if ( !function_exists( 'p2p_register_connection_type' ) )
                return;

                // 登録する
                p2p_register_connection_type(
                    array(
                        'name' => 'posts_to_pages',
                        'from' => 'post',
                        'to' => 'page'
                    )
                    );
        }
        add_action( 'wp_loaded', 'my_connection_types' );

        //--部屋用のパーマリンクはIDに変更
        register_post_type( 'room',
            array(
                'public' => true,
                'has_archive' => true,
                'rewrite' => array(
                    "with_front" => true
                ),
                'cptp_permalink_structure' => '%post_id%'
            )
            );


        //--円用単位関数
        function number_unit($int=0){
            if(!empty($int)){
                return number_format($int);
            }
            // return ($int);
            /*
             $unit = array('万','億','兆','京');
             krsort($unit);
             if(is_numeric($int)){
             $tmp = '';
             $count = strlen($int);
             foreach($unit as $k => $v){
             if($count > (4 * ($k + 1))){
             if($int!==0) $tmp .= number_format(floor( $int /pow(10000,$k+1))).$v;
             $int = $int % pow(10000,$k+1);
             }
             }
             if($int!==0) $tmp .= number_format($int % pow(10000,$k+1));
             return $tmp;
             }else{
             return false;
             }
             */
        }


        function wpcf7_form_tag_filter_fudou2( $tag ){



            global $post;

            if ( isset( $post->ID ) ){

                $post_id = $post->ID;

            }else{

                $post_id = isset( $_GET['p'] ) ? myIsNum_f( $_GET['p'] ) : '';

            }

            if ( ! is_array( $tag ) ) return $tag;



            if( $post_id != "" ){



                $name = $tag['name'];

                if($name == 'your-subject'){

                    //物件番号

                    $tag_val  = get_post_meta( $post_id,'shikibesu', true );

                    //--roomに関連の親情報を集める
                    $args = array(
                        'connected_type' => 'room_relation',
                        'connected_items' => $post_id
                    );
                    $connected_parent = get_posts($args);

                    //物件タイトル
                    $tag_val .= '：'.$connected_parent[0]->post_title.' '.get_the_title( $post_id );



                    //物件価格

                    if( get_post_meta($post_id, 'seiyakubi', true) != "" ){

                        $tag_val .= 'ご成約済';

                    }else{



                        //非公開の場合

                        if( get_post_meta($post_id,'kakakukoukai',true) == "0" ){

                            $kakakujoutai_data = get_post_meta ($post_id,'kakakujoutai',true );

                            if( $kakakujoutai_data == "1" )    $tag_val .= '相談';

                            if( $kakakujoutai_data == "2" )    $tag_val .= '確定';

                            if( $kakakujoutai_data == "3" )    $tag_val .= '入札';



                            //価格状態 v1.9.0

                            $tag_val .= apply_filters( 'fudou_add_kakakujoutai_filter', '', $kakakujoutai_data, $post_id );



                        }else{

                            $kakaku_data = get_post_meta( $post_id,'kakaku',true );

                            if( is_numeric( $kakaku_data ) ){

                                if ( function_exists( 'fudou_money_format_ja' ) ) {

                                    // Money Format 億万円 表示

                                    $tag_val .= " ". apply_filters( 'fudou_money_format_ja', $kakaku_data );

                                }else{

                                    $tag_val .= " ". floatval( $kakaku_data )/10000;

                                    $tag_val .= ""."万円";

                                }

                            }

                        }

                    }

                    $tag['values'] = (array)$tag_val;

                }



                if( is_user_logged_in() ){



                    //global $current_user;

                    //get_currentuserinfo();

                    $current_user = wp_get_current_user( );



                    if( isset( $current_user->user_email ) ){

                        $pos = strpos(  $current_user->user_email , 'pseudo.twitter.com' );

                        if ($pos === false) {

                            if($name == 'your-email') $tag['values'] = (array)$current_user->user_email;

                        }

                    }

                    if( isset( $current_user->user_lastname ) && isset( $current_user->user_firstname ) ){

                        if($name == 'your-name')  $tag['values'] = (array)($current_user->user_lastname .' '. $current_user->user_firstname );

                    }

                }

            }

            return $tag;

        }

        add_filter( 'wpcf7_form_tag', 'wpcf7_form_tag_filter_fudou2', 11 );



        /*----------------------------------------------------------

        以下本体から削除して書き換えるメソッド

        ------------------------------------------------------------*/

        remove_filter('posts_where', 'cat_posts_where_fudou');
        function cat_posts_where_fudou_stw($where) {
            if( is_admin() ) {
                global $wpdb;
                $cat_id = isset($_GET['cat_id']) ? $_GET['cat_id'] : '';
                if( $cat_id != "" ) {
                    $sql = "SELECT DISTINCT P.ID";
                    $sql .=  " FROM ($wpdb->postmeta AS PM";
                    $sql .=  " INNER JOIN (($wpdb->terms AS T";
                    $sql .=  " INNER JOIN $wpdb->term_taxonomy AS TT ON T.term_id = TT.term_id) ";
                    $sql .=  " INNER JOIN $wpdb->term_relationships AS TR ON TT.term_taxonomy_id = TR.term_taxonomy_id) ON PM.post_id = TR.object_id)";
                    $sql .=  " INNER JOIN $wpdb->posts AS P ON PM.post_id = P.ID";
                    $sql .=  " WHERE T.term_id=".$cat_id."";
                    $sql .=  " AND  (P.post_type ='fudo' or P.post_type ='room')";//--roomを追加
                    $where .= " AND ID IN ( ".$sql." )";
                }
            }
            return $where;
        }
        add_filter('posts_where', 'cat_posts_where_fudou_stw');


        remove_action('restrict_manage_posts', 'cat_restrict_manage_posts_fudou');
        function cat_restrict_manage_posts_fudou_stw() {
            global $post_type;
            global $wpdb;
            if( $post_type == 'fudo') {
                $cat_id = isset($_GET['cat_id']) ? $_GET['cat_id'] : '';
                echo '<select name="cat_id" class="postform">';
                echo '<option value="">カテゴリ・タグすべて</option>';
                $sql = "SELECT DISTINCT T.term_id , T.name ";
                $sql .=  " FROM ($wpdb->postmeta AS PM";
                $sql .=  " INNER JOIN (($wpdb->terms AS T";
                $sql .=  " INNER JOIN $wpdb->term_taxonomy AS TT ON T.term_id = TT.term_id) ";
                $sql .=  " INNER JOIN $wpdb->term_relationships AS TR ON TT.term_taxonomy_id = TR.term_taxonomy_id) ON PM.post_id = TR.object_id)";
                $sql .=  " INNER JOIN $wpdb->posts AS P ON PM.post_id = P.ID";
                $sql .=  " WHERE  (P.post_type ='fudo' or P.post_type ='room')";//--room追加
                $sql .=  " AND TT.taxonomy ='bukken'";
                //    $sql = $wpdb->prepare($sql,'');
                $metas = $wpdb->get_results( $sql, ARRAY_A );
                if(!empty($metas)) {
                    echo '<optgroup label="物件カテゴリ"> ';
                    foreach ( $metas as $meta ) {
                        echo '<option value="'.$meta['term_id'].'"';
                        if( $cat_id == $meta['term_id'] ) echo ' selected="selected"';
                        echo '>　'.$meta['name'].'</option>';
                    }
                    echo '</optgroup>';
                }
                //echo '<option value="">物件タグ</option>';
                $sql = "SELECT DISTINCT T.term_id , T.name ";
                $sql .=  " FROM ($wpdb->postmeta AS PM";
                $sql .=  " INNER JOIN (($wpdb->terms AS T";
                $sql .=  " INNER JOIN $wpdb->term_taxonomy AS TT ON T.term_id = TT.term_id) ";
                $sql .=  " INNER JOIN $wpdb->term_relationships AS TR ON TT.term_taxonomy_id = TR.term_taxonomy_id) ON PM.post_id = TR.object_id)";
                $sql .=  " INNER JOIN $wpdb->posts AS P ON PM.post_id = P.ID";
                $sql .=  " WHERE  (P.post_type ='fudo' or P.post_type ='room')";//--room追加
                $sql .=  " AND TT.taxonomy ='bukken_tag'";
                //    $sql = $wpdb->prepare($sql,'');
                $metas = $wpdb->get_results( $sql, ARRAY_A );
                if(!empty($metas)) {
                    echo '<optgroup label="物件タグ"> ';
                    foreach ( $metas as $meta ) {
                        echo '<option value="'.$meta['term_id'].'"';
                        if( $cat_id == $meta['term_id'] ) echo ' selected="selected"';
                        echo '>　'.$meta['name'].'</option>';
                    }
                    echo '</optgroup>';
                }
                echo '</select>';
            }
        }
        add_action('restrict_manage_posts', 'cat_restrict_manage_posts_fudou_stw');


        /*----------------------------------------------------------
         Contact Form 7でメールアドレスの再入力チェック（STW森追加）
         ------------------------------------------------------------*/
        function wpcf7_main_validation_filter( $result, $tag ) {
            $type = $tag['type'];
            $name = $tag['name'];
            $_POST[$name] = trim( strtr( (string) $_POST[$name], "\n", " " ) );
            if ( 'email' == $type || 'email*' == $type ) {
                if (preg_match('/(.*)_confirm$/', $name, $matches)){
                    $target_name = $matches[1];
                    if ($_POST[$name] != $_POST[$target_name]) {
                        if (method_exists($result, 'invalidate')) {
                            $result->invalidate( $tag,"確認用のメールアドレスが一致していません");
                        } else {
                            $result['valid'] = false;
                            $result['reason'][$name] = '確認用のメールアドレスが一致していません';
                        }
                    }
                }
            }
            return $result;
        }

        add_filter( 'wpcf7_validate_email', 'wpcf7_main_validation_filter', 11, 2 );
        add_filter( 'wpcf7_validate_email*', 'wpcf7_main_validation_filter', 11, 2 );

        //コメントが許可されているかの関数
        function is_comment_open(){
            global $post;
            if ( isset($post->comment_status) ) {
                return $post->comment_status == 'open';
            }
            return false;
        }



        //----------------------------
        //　AFCフィールドを検索対象に
        //----------------------------

        function ACF_custom_searchadd( $where, $wp_query ) {

            // ここにACFのカスタムフィードのフィールド名をひとつずつ配列に入力します
            $list_searc_acf_add = array('kyoyusetsubi','bukken-blog','bukken-text','bukken-remarks','madori','nyukyo','important_point','biko');

            global $wpdb;

            if ( empty( $where )){
                return $where;
            }

            $terms = $wp_query->query_vars[ 's' ];

            $exploded_terms = explode( ' ', $terms );
            if( $exploded_terms === FALSE || count( $exploded_terms ) == 0 ){
                $exploded_terms = array( 0 => $terms );
            }

            $where = '';

            foreach( $exploded_terms as $tag ) :
            $where .= "
          AND (
            (".$wpdb->prefix."posts.post_title LIKE '%$tag%')
            OR (".$wpdb->prefix."posts.post_content LIKE '%$tag%')
            OR (".$wpdb->prefix."posts.ID LIKE '%$tag%')
            OR EXISTS (
              SELECT * FROM ".$wpdb->prefix."postmeta
                  WHERE post_id = ".$wpdb->prefix."posts.ID
                    AND (";

            foreach ($list_searc_acf_add as $searcheable_acf) :
            if ($searcheable_acf == $list_searc_acf_add[0]):
            $where .= " (meta_key LIKE '%" . $searcheable_acf . "%' AND meta_value LIKE '%$tag%') ";
            else :
            $where .= " OR (meta_key LIKE '%" . $searcheable_acf . "%' AND meta_value LIKE '%$tag%') ";
            endif;
            endforeach;

            $where .= ")
            )
            OR EXISTS (
              SELECT * FROM ".$wpdb->prefix."comments
              WHERE comment_post_ID = ".$wpdb->prefix."posts.ID
                AND comment_content LIKE '%$tag%'
            )
            OR EXISTS (
              SELECT * FROM ".$wpdb->prefix."terms
              INNER JOIN ".$wpdb->prefix."term_taxonomy
                ON ".$wpdb->prefix."term_taxonomy.term_id = ".$wpdb->prefix."terms.term_id
              INNER JOIN ".$wpdb->prefix."term_relationships
                ON ".$wpdb->prefix."term_relationships.term_taxonomy_id = ".$wpdb->prefix."term_taxonomy.term_taxonomy_id
              WHERE (
                  taxonomy = 'post_tag'
                    OR taxonomy = 'category'
                    OR taxonomy = 'myCustomTax'
                  )
                  AND object_id = ".$wpdb->prefix."posts.ID
                  AND ".$wpdb->prefix."terms.name LIKE '%$tag%'
            )
        )";
            endforeach;
            return $where;
        }

        add_filter( 'posts_search', 'ACF_custom_searchadd', 500, 2 );


        //----------------------------
        //　何でもページャー
        //----------------------------
        function pager($idname,$countRe,$view_pagecount,$zengo=2){//（id名,全件数,1ページ数）
            // 現在のページ情報を取得
            $id =get_query_var($idname);
            // ページ情報以外のパラメータを補完し、$other_paramにセット
            $other_param="";
            if(is_array($_GET)){
                foreach($_GET as $key => $value){
                    if ($key != $idname){
                        $other_param .= "&amp;".$key."=".urlencode($value);
                    }
                }
            }

            if($id==""){$id=1;}

            $maxPage=ceil($countRe / $view_pagecount);//1ページの表示数

            if( ($maxPage == 1) or ($maxPage < $id) ){return false;}

            $VIEW_PAGE_MENU_WIDTH=$zengo;//前と後ろの表示数

            $startMore="";
            $endMore="";
            $startPage = 1;
            if($id > $VIEW_PAGE_MENU_WIDTH + 1){
                $startPage = $id - $VIEW_PAGE_MENU_WIDTH;
                $startMore = "<a href=\"?".$idname."=".($startPage - 1).$other_param."\" class=\"pgbt all\">&lt;</a>";
            }
            $endPage=1;
            if($id + $VIEW_PAGE_MENU_WIDTH < $maxPage){
                $endPage = $id + $VIEW_PAGE_MENU_WIDTH;
                $endMore = "<a href=\"?".$idname."=".($endPage - 1).$other_param."\" class=\"pgbt all\">&gt;</a>";
            } else {
                $endPage = $maxPage;
            }
            $page_footer="";

            for($i = $startPage ; $i <= $endPage ; $i++){
                //$page_footer.= " ".(($id==$i)?"<span style='font-Size:120%'>$i</span>":"<div onclick=\"apos($i);\" class=\"pgbt\">$i</div>");
                $page_footer.= " ".(($id==$i)?'<span class="currentpage">' . $i . '</span>':"<a href=\"?".$idname."=".($i).$other_param."\" class=\"pgbt\">".$i."</a>");
            }

            $zeroMore=intval($id)==1?"":'<a href="?'.$idname.'=1'.$other_param.'" class="pgbt all">&lt;&lt;</a>';
            $maxMore=intval($id)!==intval($maxPage)?'<a href="?'.$idname.'='.$maxPage.$other_param.'" class="pgbt all">&gt;&gt;</a>':"";
            $page_footer = "\n" . '<div class="search-reslult-pager">'.$zeroMore.$startMore.$page_footer.$endMore.$maxMore."</div>\n";
            return $page_footer;
        }


        /**
         *サイトオプション
         */
        if(function_exists('acf_add_options_page')){
            acf_add_options_page( array(
                'page_title' => 'サイトオプション',
                'menu_title' => 'サイトオプション',
                'menu_slug'  => 'siteoption-settings',
                'capability' => 'manage_options',
            ) );
        }

        /**
         *TOPオプション
         */
        if(function_exists('acf_add_options_page')){
            acf_add_options_page( array(
                'page_title' => 'TOPオプション',
                'menu_title' => 'TOPオプション',
                'menu_slug'  => 'topoption-settings',
                'capability' => 'manage_options',
            ) );
        }

        /**
         *FAQ
         */
        if(function_exists('acf_add_options_page')){
            acf_add_options_page( array(
                'page_title' => 'FAQ',
                'menu_title' => 'FAQ',
                'menu_slug'  => 'faq-settings',
                'capability' => 'manage_options',
            ) );
        }

        /**
         *非表示対象画像
         */
        if(function_exists('acf_add_options_page')){
            acf_add_options_page( array(
                'page_title' => '非表示画像',
                'menu_title' => '非表示画像',
                'menu_slug'  => 'ban-settings',
                'capability' => 'manage_options',
            ) );
        }


/**
 * 子供のpost_idから親のpost_idを求める
 *  */
        function get_parent_ID($post_id){
            //--roomに関連の親情報を集める
            $args = array(
                'connected_type' => 'room_relation',
                'connected_items' => $post_id
            );
            $connected_parent = get_posts($args);
            $oya_title=$connected_parent[0]->post_title;
            $parent_post_ID = $connected_parent[0]->ID;

            return $parent_post_ID;
        }

/**
 * 路線名取得
 */
function get_rosen_name1($post_id) {
    global $wpdb;
    $rosen_name = '';

    $koutsurosen_data = get_post_meta($post_id, 'koutsurosen1', true);

    if($koutsurosen_data !=""){
        $sql = "SELECT rosen_name FROM " . $wpdb->prefix . DB_ROSEN_TABLE . " WHERE rosen_id =".$koutsurosen_data."";
        //    $sql = $wpdb->prepare($sql,'');
        $metas = $wpdb->get_row( $sql );
        if(!empty($metas)){
            $rosen_name = $metas->rosen_name;
        }
    }

    return $rosen_name;
}

function get_rosen_name2($post_id) {
    global $wpdb;
    $rosen_name = '';

    $koutsurosen_data = get_post_meta($post_id, 'koutsurosen2', true);

    if($koutsurosen_data !=""){
        $sql = "SELECT rosen_name FROM " . $wpdb->prefix . DB_ROSEN_TABLE . " WHERE rosen_id =".$koutsurosen_data."";
        //    $sql = $wpdb->prepare($sql,'');
        $metas = $wpdb->get_row( $sql );
        if(!empty($metas)){
            $rosen_name = $metas->rosen_name;
        }
    }

    return $rosen_name;
}

function get_rosen_name3($post_id) {
    global $wpdb;
    $rosen_name = '';

    $koutsurosen_data = get_post_meta($post_id, 'koutsurosen3', true);

    if($koutsurosen_data !=""){
        $sql = "SELECT rosen_name FROM " . $wpdb->prefix . DB_ROSEN_TABLE . " WHERE rosen_id =".$koutsurosen_data."";
        //    $sql = $wpdb->prepare($sql,'');
        $metas = $wpdb->get_row( $sql );
        if(!empty($metas)){
            $rosen_name = $metas->rosen_name;
        }
    }

    return $rosen_name;
}


        /**
         * 路線 駅 バス 徒歩
         *
         * @since Fudousan Plugin 1.6.0
         *
         * @param int $post_id Post ID.
         */
        function my_custom_koutsu1_print($post_id) {
            global $wpdb;
            $rosen_name = '';

            $koutsurosen_data = get_post_meta($post_id, 'koutsurosen1', true);
            $koutsueki_data = get_post_meta($post_id, 'koutsueki1', true);

            $shozaichiken_data = get_post_meta($post_id,'shozaichicode',true);
            $shozaichiken_data = myLeft($shozaichiken_data,2);

            if($koutsurosen_data !=""){
                $sql = "SELECT rosen_name FROM " . $wpdb->prefix . DB_ROSEN_TABLE . " WHERE rosen_id =".$koutsurosen_data."";
                //    $sql = $wpdb->prepare($sql,'');
                $metas = $wpdb->get_row( $sql );
                if(!empty($metas)){
                    $rosen_name = $metas->rosen_name;
                    echo '<span class="o-roomStation_name o-text __bold">'.$rosen_name."</span>";
                }
            }

            if($koutsurosen_data !="" && $koutsueki_data !=""){
                $sql = "SELECT DTS.station_name";
                $sql .=  " FROM " . $wpdb->prefix . DB_ROSEN_TABLE . " AS DTR";
                $sql .=  " INNER JOIN " . $wpdb->prefix . DB_EKI_TABLE . " AS DTS ON DTR.rosen_id = DTS.rosen_id";
                $sql .=  " WHERE DTS.station_id=".$koutsueki_data." AND DTS.rosen_id=".$koutsurosen_data."";
                //    $sql = $wpdb->prepare($sql,'');
                $metas = $wpdb->get_row( $sql );
                if(!empty($metas)){
                    if($metas->station_name != '＊＊＊＊'){
                        if( $metas->station_name != '' ){
                            echo $metas->station_name.'駅';
                        }
                    }
                }
            }

            // if(get_post_meta($post_id, 'koutsutoho1', true) !="")
            //     echo ' 徒歩'.get_post_meta($post_id, 'koutsutoho1', true).'m';

                if(get_post_meta($post_id, 'koutsutoho1f', true) !="")
                    echo ' 徒歩'.get_post_meta($post_id, 'koutsutoho1f', true).'分';


                    //バス路線
                    /*
                     * バス路線名、バス停名表示
                     *
                     * @since Fudousan Bus Plugin 1.0.0
                     * For inc-single-fudo.php and inc-archive-fudo.php
                     * admin_fudou.php fudo-widget.php fudo-widget3.php
                     * apply_filters( 'fudoubus_buscorse_busstop_single', '', $post_id, 1 );
                     *
                     * @param int $post_id Post ID.
                     * @param int $type 1 or 2.
                     */
                    $koutsubusstei = apply_filters( 'fudoubus_buscorse_busstop_single', '', $post_id, 1 );

                    if( !$koutsubusstei ){
                        $koutsubusstei = get_post_meta($post_id, 'koutsubusstei1', true);
                    }
                    $koutsubussfun = get_post_meta($post_id, 'koutsubussfun1', true);
                    $koutsutohob1f = get_post_meta($post_id, 'koutsutohob1f', true);

                    if( $koutsubusstei || $koutsubussfun ){

                        if($rosen_name == 'バス'){
                            echo '(' . $koutsubusstei;
                            if( !empty( $koutsubussfun ) ) echo ' 乗'.$koutsubussfun.'分';
                        }else{
                            echo '<br />';
                            echo ' バス(' . $koutsubusstei;
                            if( !empty( $koutsubussfun ) ) echo ' 乗'.$koutsubussfun.'分';
                        }

                        if($koutsutohob1f !="" )
                            echo ' 停歩'.$koutsutohob1f.'分';
                            echo ')';
                    }
        }

        function my_custom_koutsu2_print($post_id) {
            global $wpdb;

            $rosen_name = '';
            $koutsurosen_data = get_post_meta($post_id, 'koutsurosen2', true);
            $koutsueki_data = get_post_meta($post_id, 'koutsueki2', true);

            $shozaichiken_data = get_post_meta($post_id,'shozaichicode',true);
            $shozaichiken_data = myLeft($shozaichiken_data,2);


            if($koutsurosen_data !=""){
                $sql = "SELECT rosen_name FROM " . $wpdb->prefix . DB_ROSEN_TABLE . " WHERE rosen_id =".$koutsurosen_data."";
                //    $sql = $wpdb->prepare($sql,'');
                $metas = $wpdb->get_row( $sql );
                if(!empty($metas)){
                    $rosen_name = $metas->rosen_name;
                    echo '<span class="o-roomStation_name o-text __bold">'.$rosen_name."</span>";
                }
            }

            if($koutsurosen_data !="" && $koutsueki_data !=""){
                $sql = "SELECT DTS.station_name";
                $sql .=  " FROM " . $wpdb->prefix . DB_ROSEN_TABLE . " AS DTR";
                $sql .=  " INNER JOIN " . $wpdb->prefix . DB_EKI_TABLE . " AS DTS ON DTR.rosen_id = DTS.rosen_id";
                $sql .=  " WHERE DTS.station_id=".$koutsueki_data." AND DTS.rosen_id=".$koutsurosen_data."";
                //    $sql = $wpdb->prepare($sql,'');
                $metas = $wpdb->get_row( $sql );
                if(!empty($metas)){
                    if($metas->station_name != '＊＊＊＊'){
                        if( $metas->station_name != '' ){
                            echo $metas->station_name.'駅';
                        }
                    }
                }
            }

            // if(get_post_meta($post_id, 'koutsutoho2', true) !="")
            //     echo ' 徒歩'.get_post_meta($post_id, 'koutsutoho2', true).'m';

                if(get_post_meta($post_id, 'koutsutoho2f', true) !="")
                    echo ' 徒歩'.get_post_meta($post_id, 'koutsutoho2f', true).'分';


                    /*
                     * バス路線名、バス停名表示
                     *
                     * @since Fudousan Bus Plugin 1.0.0
                     * For inc-single-fudo.php and inc-archive-fudo.php
                     * admin_fudou.php fudo-widget.php fudo-widget3.php
                     * apply_filters( 'fudoubus_buscorse_busstop_single', '', $post_id, 1 );
                     *
                     * @param int $post_id Post ID.
                     * @param int $type 1 or 2.
                     */
                    $koutsubusstei = apply_filters( 'fudoubus_buscorse_busstop_single', '', $post_id, 2 );

                    if( !$koutsubusstei ){
                        $koutsubusstei = get_post_meta($post_id, 'koutsubusstei2', true);
                    }
                    $koutsubussfun = get_post_meta($post_id, 'koutsubussfun2', true);
                    $koutsutohob2f = get_post_meta($post_id, 'koutsutohob2f', true);

                    if( $koutsubusstei || $koutsubussfun ){

                        if($rosen_name == 'バス'){
                            echo '(' . $koutsubusstei;
                            if( !empty( $koutsubussfun ) ) echo ' 乗'.$koutsubussfun.'分';
                        }else{
                            echo '<br />';
                            echo ' バス(' . $koutsubusstei;
                            if( !empty( $koutsubussfun ) ) echo ' 乗'.$koutsubussfun.'分';
                        }

                        if($koutsutohob2f !="" )
                            echo ' 停歩'.$koutsutohob2f.'分';
                            echo ')';
                    }
        }

        function my_custom_koutsu3_print($post_id) {
            global $wpdb;

            $rosen_name = '';
            $koutsurosen_data = get_post_meta($post_id, 'koutsurosen3', true);
            $koutsueki_data = get_post_meta($post_id, 'koutsueki3', true);

            $shozaichiken_data = get_post_meta($post_id,'shozaichicode',true);
            $shozaichiken_data = myLeft($shozaichiken_data,2);


            if($koutsurosen_data !=""){
                $sql = "SELECT rosen_name FROM " . $wpdb->prefix . DB_ROSEN_TABLE . " WHERE rosen_id =".$koutsurosen_data."";
                //    $sql = $wpdb->prepare($sql,'');
                $metas = $wpdb->get_row( $sql );
                if(!empty($metas)){
                    $rosen_name = $metas->rosen_name;
                    echo '<span class="o-roomStation_name o-text __bold">'.$rosen_name."</span>";
                }
            }

            if($koutsurosen_data !="" && $koutsueki_data !=""){
                $sql = "SELECT DTS.station_name";
                $sql .=  " FROM " . $wpdb->prefix . DB_ROSEN_TABLE . " AS DTR";
                $sql .=  " INNER JOIN " . $wpdb->prefix . DB_EKI_TABLE . " AS DTS ON DTR.rosen_id = DTS.rosen_id";
                $sql .=  " WHERE DTS.station_id=".$koutsueki_data." AND DTS.rosen_id=".$koutsurosen_data."";
                //    $sql = $wpdb->prepare($sql,'');
                $metas = $wpdb->get_row( $sql );
                if(!empty($metas)){
                    if($metas->station_name != '＊＊＊＊'){
                        if( $metas->station_name != '' ){
                            echo $metas->station_name.'駅';
                        }
                    }
                }
            }

            // if(get_post_meta($post_id, 'koutsutoho3', true) !="")
            //     echo ' 徒歩'.get_post_meta($post_id, 'koutsutoho3', true).'m';

                if(get_post_meta($post_id, 'koutsutoho3f', true) !="")
                    echo ' 徒歩'.get_post_meta($post_id, 'koutsutoho3f', true).'分';


                    /*
                     * バス路線名、バス停名表示
                     *
                     * @since Fudousan Bus Plugin 1.0.0
                     * For inc-single-fudo.php and inc-archive-fudo.php
                     * admin_fudou.php fudo-widget.php fudo-widget3.php
                     * apply_filters( 'fudoubus_buscorse_busstop_single', '', $post_id, 1 );
                     *
                     * @param int $post_id Post ID.
                     * @param int $type 1 or 2.
                     */
                    $koutsubusstei = apply_filters( 'fudoubus_buscorse_busstop_single', '', $post_id, 2 );

                    if( !$koutsubusstei ){
                        $koutsubusstei = get_post_meta($post_id, 'koutsubusstei3', true);
                    }
                    $koutsubussfun = get_post_meta($post_id, 'koutsubussfun3', true);
                    $koutsutohob3f = get_post_meta($post_id, 'koutsutohob3f', true);

                    if( $koutsubusstei || $koutsubussfun ){

                        if($rosen_name == 'バス'){
                            echo '(' . $koutsubusstei;
                            if( !empty( $koutsubussfun ) ) echo ' 乗'.$koutsubussfun.'分';
                        }else{
                            echo '<br />';
                            echo ' バス(' . $koutsubusstei;
                            if( !empty( $koutsubussfun ) ) echo ' 乗'.$koutsubussfun.'分';
                        }

                        if($koutsutohob3f !="" )
                            echo ' 停歩'.$koutsutohob3f.'分';
                            echo ')';
                    }
        }


    function my_custom_koutsu_all($post_id) {
            global $wpdb;
            $rosen_name = '';

            $koutsurosen_data = get_post_meta($post_id, 'koutsurosen1', true);
            $koutsueki_data = get_post_meta($post_id, 'koutsueki1', true);

            $shozaichiken_data = get_post_meta($post_id,'shozaichicode',true);
            $shozaichiken_data = myLeft($shozaichiken_data,2);

            if($koutsurosen_data !=""){
                $sql = "SELECT rosen_name FROM " . $wpdb->prefix . DB_ROSEN_TABLE . " WHERE rosen_id =".$koutsurosen_data."";
                //    $sql = $wpdb->prepare($sql,'');
                $metas = $wpdb->get_row( $sql );
                if(!empty($metas)){
                    $rosen_name = $metas->rosen_name;
                    echo ''.$rosen_name." ";
                }
            }

            if($koutsurosen_data !="" && $koutsueki_data !=""){
                $sql = "SELECT DTS.station_name";
                $sql .=  " FROM " . $wpdb->prefix . DB_ROSEN_TABLE . " AS DTR";
                $sql .=  " INNER JOIN " . $wpdb->prefix . DB_EKI_TABLE . " AS DTS ON DTR.rosen_id = DTS.rosen_id";
                $sql .=  " WHERE DTS.station_id=".$koutsueki_data." AND DTS.rosen_id=".$koutsurosen_data."";
                //    $sql = $wpdb->prepare($sql,'');
                $metas = $wpdb->get_row( $sql );
                if(!empty($metas)){
                    if($metas->station_name != '＊＊＊＊'){
                        if( $metas->station_name != '' ){
                            echo $metas->station_name.'駅 ';
                        }
                    }
                }
            }

            // if(get_post_meta($post_id, 'koutsutoho1', true) !="")
            //     echo ' 徒歩'.get_post_meta($post_id, 'koutsutoho1', true).'m';

                if(get_post_meta($post_id, 'koutsutoho1f', true) !="")
                    echo ' 徒歩'.get_post_meta($post_id, 'koutsutoho1f', true).'分<br>';


                    //バス路線
                    /*
                     * バス路線名、バス停名表示
                     *
                     * @since Fudousan Bus Plugin 1.0.0
                     * For inc-single-fudo.php and inc-archive-fudo.php
                     * admin_fudou.php fudo-widget.php fudo-widget3.php
                     * apply_filters( 'fudoubus_buscorse_busstop_single', '', $post_id, 1 );
                     *
                     * @param int $post_id Post ID.
                     * @param int $type 1 or 2.
                     */
                    $koutsubusstei = apply_filters( 'fudoubus_buscorse_busstop_single', '', $post_id, 1 );

                    if( !$koutsubusstei ){
                        $koutsubusstei = get_post_meta($post_id, 'koutsubusstei1', true);
                    }
                    $koutsubussfun = get_post_meta($post_id, 'koutsubussfun1', true);
                    $koutsutohob1f = get_post_meta($post_id, 'koutsutohob1f', true);

                    if( $koutsubusstei || $koutsubussfun ){

                        if($rosen_name == 'バス'){
                            echo '(' . $koutsubusstei;
                            if( !empty( $koutsubussfun ) ) echo ' 乗'.$koutsubussfun.'分';
                        }else{
                            echo '<br />';
                            echo ' バス(' . $koutsubusstei;
                            if( !empty( $koutsubussfun ) ) echo ' 乗'.$koutsubussfun.'分';
                        }

                        if($koutsutohob1f !="" )
                            echo ' 停歩'.$koutsutohob1f.'分';
                            echo ')';
                    }


            $rosen_name = '';
            $koutsurosen_data = get_post_meta($post_id, 'koutsurosen2', true);
            $koutsueki_data = get_post_meta($post_id, 'koutsueki2', true);

            $shozaichiken_data = get_post_meta($post_id,'shozaichicode',true);
            $shozaichiken_data = myLeft($shozaichiken_data,2);


            if($koutsurosen_data !=""){
                $sql = "SELECT rosen_name FROM " . $wpdb->prefix . DB_ROSEN_TABLE . " WHERE rosen_id =".$koutsurosen_data."";
                //    $sql = $wpdb->prepare($sql,'');
                $metas = $wpdb->get_row( $sql );
                if(!empty($metas)){
                    $rosen_name = $metas->rosen_name;
                    echo $rosen_name." ";
                }
            }

            if($koutsurosen_data !="" && $koutsueki_data !=""){
                $sql = "SELECT DTS.station_name";
                $sql .=  " FROM " . $wpdb->prefix . DB_ROSEN_TABLE . " AS DTR";
                $sql .=  " INNER JOIN " . $wpdb->prefix . DB_EKI_TABLE . " AS DTS ON DTR.rosen_id = DTS.rosen_id";
                $sql .=  " WHERE DTS.station_id=".$koutsueki_data." AND DTS.rosen_id=".$koutsurosen_data."";
                //    $sql = $wpdb->prepare($sql,'');
                $metas = $wpdb->get_row( $sql );
                if(!empty($metas)){
                    if($metas->station_name != '＊＊＊＊'){
                        if( $metas->station_name != '' ){
                            echo $metas->station_name.'駅 ';
                        }
                    }
                }
            }

            // if(get_post_meta($post_id, 'koutsutoho2', true) !="")
            //     echo ' 徒歩'.get_post_meta($post_id, 'koutsutoho2', true).'m';

                if(get_post_meta($post_id, 'koutsutoho2f', true) !="")
                    echo ' 徒歩'.get_post_meta($post_id, 'koutsutoho2f', true).'分<br>';


                    /*
                     * バス路線名、バス停名表示
                     *
                     * @since Fudousan Bus Plugin 1.0.0
                     * For inc-single-fudo.php and inc-archive-fudo.php
                     * admin_fudou.php fudo-widget.php fudo-widget3.php
                     * apply_filters( 'fudoubus_buscorse_busstop_single', '', $post_id, 1 );
                     *
                     * @param int $post_id Post ID.
                     * @param int $type 1 or 2.
                     */
                    $koutsubusstei = apply_filters( 'fudoubus_buscorse_busstop_single', '', $post_id, 2 );

                    if( !$koutsubusstei ){
                        $koutsubusstei = get_post_meta($post_id, 'koutsubusstei2', true);
                    }
                    $koutsubussfun = get_post_meta($post_id, 'koutsubussfun2', true);
                    $koutsutohob2f = get_post_meta($post_id, 'koutsutohob2f', true);

                    if( $koutsubusstei || $koutsubussfun ){

                        if($rosen_name == 'バス'){
                            echo '(' . $koutsubusstei;
                            if( !empty( $koutsubussfun ) ) echo ' 乗'.$koutsubussfun.'分';
                        }else{
                            echo '<br />';
                            echo ' バス(' . $koutsubusstei;
                            if( !empty( $koutsubussfun ) ) echo ' 乗'.$koutsubussfun.'分';
                        }

                        if($koutsutohob2f !="" )
                            echo ' 停歩'.$koutsutohob2f.'分';
                            echo ')';
                    }


            $rosen_name = '';
            $koutsurosen_data = get_post_meta($post_id, 'koutsurosen3', true);
            $koutsueki_data = get_post_meta($post_id, 'koutsueki3', true);

            $shozaichiken_data = get_post_meta($post_id,'shozaichicode',true);
            $shozaichiken_data = myLeft($shozaichiken_data,2);


            if($koutsurosen_data !=""){
                $sql = "SELECT rosen_name FROM " . $wpdb->prefix . DB_ROSEN_TABLE . " WHERE rosen_id =".$koutsurosen_data."";
                //    $sql = $wpdb->prepare($sql,'');
                $metas = $wpdb->get_row( $sql );
                if(!empty($metas)){
                    $rosen_name = $metas->rosen_name;
                    echo $rosen_name." ";
                    // echo '<span class="o-roomStation_name o-text __bold">'.$rosen_name."</span>";
                }
            }

            if($koutsurosen_data !="" && $koutsueki_data !=""){
                $sql = "SELECT DTS.station_name";
                $sql .=  " FROM " . $wpdb->prefix . DB_ROSEN_TABLE . " AS DTR";
                $sql .=  " INNER JOIN " . $wpdb->prefix . DB_EKI_TABLE . " AS DTS ON DTR.rosen_id = DTS.rosen_id";
                $sql .=  " WHERE DTS.station_id=".$koutsueki_data." AND DTS.rosen_id=".$koutsurosen_data."";
                //    $sql = $wpdb->prepare($sql,'');
                $metas = $wpdb->get_row( $sql );
                if(!empty($metas)){
                    if($metas->station_name != '＊＊＊＊'){
                        if( $metas->station_name != '' ){
                            echo $metas->station_name.'駅 ';
                        }
                    }
                }
            }

            // if(get_post_meta($post_id, 'koutsutoho3', true) !="")
            //     echo ' 徒歩'.get_post_meta($post_id, 'koutsutoho3', true).'m';

                if(get_post_meta($post_id, 'koutsutoho3f', true) !="")
                    echo ' 徒歩'.get_post_meta($post_id, 'koutsutoho3f', true).'分';


                    /*
                     * バス路線名、バス停名表示
                     *
                     * @since Fudousan Bus Plugin 1.0.0
                     * For inc-single-fudo.php and inc-archive-fudo.php
                     * admin_fudou.php fudo-widget.php fudo-widget3.php
                     * apply_filters( 'fudoubus_buscorse_busstop_single', '', $post_id, 1 );
                     *
                     * @param int $post_id Post ID.
                     * @param int $type 1 or 2.
                     */
                    $koutsubusstei = apply_filters( 'fudoubus_buscorse_busstop_single', '', $post_id, 2 );

                    if( !$koutsubusstei ){
                        $koutsubusstei = get_post_meta($post_id, 'koutsubusstei3', true);
                    }
                    $koutsubussfun = get_post_meta($post_id, 'koutsubussfun3', true);
                    $koutsutohob3f = get_post_meta($post_id, 'koutsutohob3f', true);

                    if( $koutsubusstei || $koutsubussfun ){

                        if($rosen_name == 'バス'){
                            echo '(' . $koutsubusstei;
                            if( !empty( $koutsubussfun ) ) echo ' 乗'.$koutsubussfun.'分';
                        }else{
                            echo '<br />';
                            echo ' バス(' . $koutsubusstei;
                            if( !empty( $koutsubussfun ) ) echo ' 乗'.$koutsubussfun.'分';
                        }

                        if($koutsutohob3f !="" )
                            echo ' 停歩'.$koutsutohob3f.'分';
                            echo ')';
                    }
        }

        /**
         * 建物構造
         *
         * @since Fudousan Plugin 1.0.0 *
         * @param int $post_id Post ID.
         */
        function my_custom_tatemonokozo_print($post_id) {
            $tatemonokozo_data = get_post_meta($post_id,'tatemonokozo',true);

            if($tatemonokozo_data=="1")     echo '木造';
            if($tatemonokozo_data=="2")     echo 'ブロック';
            if($tatemonokozo_data=="3")     echo '鉄骨造';
            if($tatemonokozo_data=="4")     echo 'RC（鉄筋コンクリート）';
            if($tatemonokozo_data=="5")     echo 'SRC（鉄骨鉄筋コンクリート）';
            if($tatemonokozo_data=="6")     echo 'PC（プレキャストコンクリート）';
            if($tatemonokozo_data=="7")     echo 'HPC（鉄骨プレキャストコンクリート）';
            if($tatemonokozo_data=="9")     echo 'その他';
            if($tatemonokozo_data=="10")     echo '軽量鉄骨';
            if($tatemonokozo_data=="11")     echo 'ALC（軽量気泡コンクリート）';
            if($tatemonokozo_data=="12")     echo '鉄筋ブロック';
            if($tatemonokozo_data=="13")     echo 'CFT(コンクリート充填鋼管)';

            //text
            if( $tatemonokozo_data !='' && !is_numeric($tatemonokozo_data) ) echo $tatemonokozo_data;

        }

        function my_custom_tatemonokozo_print_br($post_id) {
            $tatemonokozo_data = get_post_meta($post_id,'tatemonokozo',true);
            if($tatemonokozo_data=="1")     echo '木造';
            if($tatemonokozo_data=="2")     echo 'ブロック';
            if($tatemonokozo_data=="3")     echo '鉄骨造';
            if($tatemonokozo_data=="4")     echo 'RC<br>（鉄筋コンクリート）';
            if($tatemonokozo_data=="5")     echo 'SRC<br>（鉄骨鉄筋コンクリート）';
            if($tatemonokozo_data=="6")     echo 'PC<br>（プレキャストコンクリート）';
            if($tatemonokozo_data=="7")     echo 'HPC<br>（鉄骨プレキャストコンクリート）';
            if($tatemonokozo_data=="9")     echo 'その他';
            if($tatemonokozo_data=="10")     echo '軽量鉄骨';
            if($tatemonokozo_data=="11")     echo 'ALC<br>（軽量気泡コンクリート）';
            if($tatemonokozo_data=="12")     echo '鉄筋ブロック';
            if($tatemonokozo_data=="13")     echo 'CFT<br>(コンクリート充填鋼管)';

            //text
            if( $tatemonokozo_data !='' && !is_numeric($tatemonokozo_data) ) echo $tatemonokozo_data;

        }

        /**
         * 所在地
         *
         * @since Fudousan Plugin 1.5.0 *
         * @param int $post_id Post ID.
         */
        function my_custom_shozaichi_print($post_id) {
            global $wpdb;

            $shozaichiken_data = get_post_meta($post_id,'shozaichicode',true);
            $shozaichiken_data = myLeft($shozaichiken_data,2);

            if($shozaichiken_data=="")
                $shozaichiken_data = get_post_meta($post_id,'shozaichiken',true);

                if($shozaichiken_data != ""){
                    /*
                     $sql = "SELECT middle_area_name FROM " . $wpdb->prefix . DB_KEN_TABLE . " WHERE middle_area_id=".$shozaichiken_data."";
                     //    $sql = $wpdb->prepare($sql,'');
                     $metas = $wpdb->get_row( $sql );
                     if( !empty($metas) ) echo $metas->middle_area_name;
                     */
                    // echo fudo_ken_name( $shozaichiken_data );
                }

                $shozaichicode_data = get_post_meta($post_id,'shozaichicode',true);
                $shozaichicode_data = myLeft($shozaichicode_data,5);
                $shozaichicode_data = myRight($shozaichicode_data,3);

                if($shozaichiken_data !="" && $shozaichicode_data !=""){
                    $sql = "SELECT narrow_area_name FROM " . $wpdb->prefix . DB_SHIKU_TABLE . " WHERE middle_area_id=".$shozaichiken_data." and narrow_area_id =".$shozaichicode_data."";
                    //    $sql = $wpdb->prepare($sql,'');
                    $metas = $wpdb->get_row( $sql );
                    if( !empty($metas) ) echo $metas->narrow_area_name;
                }
        }


        function get_my_custom_shozaichi_print($post_id) {
            global $wpdb;

            $shozaichiken_data = get_post_meta($post_id,'shozaichicode',true);
            $shozaichiken_data = myLeft($shozaichiken_data,2);

            if($shozaichiken_data=="")
                $shozaichiken_data = get_post_meta($post_id,'shozaichiken',true);

                if($shozaichiken_data != ""){
                    /*
                     $sql = "SELECT middle_area_name FROM " . $wpdb->prefix . DB_KEN_TABLE . " WHERE middle_area_id=".$shozaichiken_data."";
                     //    $sql = $wpdb->prepare($sql,'');
                     $metas = $wpdb->get_row( $sql );
                     if( !empty($metas) ) echo $metas->middle_area_name;
                     */
                    $ken = fudo_ken_name( $shozaichiken_data );
                }

                $shozaichicode_data = get_post_meta($post_id,'shozaichicode',true);
                $shozaichicode_data = myLeft($shozaichicode_data,5);
                $shozaichicode_data = myRight($shozaichicode_data,3);

                if($shozaichiken_data !="" && $shozaichicode_data !=""){
                    $sql = "SELECT narrow_area_name FROM " . $wpdb->prefix . DB_SHIKU_TABLE . " WHERE middle_area_id=".$shozaichiken_data." and narrow_area_id =".$shozaichicode_data."";
                    //    $sql = $wpdb->prepare($sql,'');
                    $metas = $wpdb->get_row( $sql );
                    if( !empty($metas) ) $ken .= $metas->narrow_area_name;
                }

                return $ken;
        }


//物件に紐づく部屋を洗い出す関数
//--roomに関連の親情報を集める
function get_rooms_count($post_id){

    $args = array(
        'connected_type' => 'room_relation',
        'connected_items' => $post_id,
        'posts_per_page' => 20,
        'meta_query' => array(
            'relation' => 'AND',
            'meta_nyukyogenkyo' => array(
                    'relation' => 'OR',
                    array(
                        'relation' => 'OR',
                        array(
                        'key' => 'nyukyogenkyo',
                        'type' => 'numeric',
                        'value' => 3),//即入居可
                        array(
                        'key' => 'nyukyogenkyo',
                        'type' => 'numeric',
                        'value' => 2),//居住中
                        array(
                            'key' => 'nyukyogenkyo',
                            'type' => 'numeric',
                            'value' => 11),//空室
                        array(
                            'key' =>  'nyukyogenkyo',
                            'value' => '*'
                        )
                    ),
                ),
                'meta_kakaku' => array(
                    'key'=>'kakaku',
                    'type' => 'numeric',
                    'compare' => 'EXISTS',
                    )
        ),
          'orderby' => array( 'meta_nyukyogenkyo' => 'DESC','meta_kakaku' => 'ASC'),
          'order' => 'DESC'
    );

    $connected_parent = get_posts($args);
    $bukken_mansitu_fg = get_field('bukken_status');
    // print_r($connected_parent);
    if(count($connected_parent)>0){
        return count($connected_parent);
    }else{
        return 0;
    }
}

        /**
         *ナビゲーション追加
         */

        //functions.php
        function register_my_menus() {
            register_nav_menus( array( //複数のナビゲーションメニューを登録する関数
                //'「メニューの位置」の識別子' => 'メニューの説明の文字列',
                'header_function_menu' => 'header_function_menu',
                'header_page_menu' => 'header_page_menu',
                'footer_service'  => 'footer_service',
                'footer_contact'  => 'footer_contact',
                'footer_company'  => 'footer_company',
            ) );
        }
        add_action( 'after_setup_theme', 'register_my_menus' );

/**
 * single-roomにて仕様
 * 駐車場区分
 */
function the_custom_chushajokubun_print($post_id){
        $chushajokubun_data = get_post_meta($post_id,'chushajokubun',true);
        $chushajobiko_data = get_post_meta($post_id,'chushajobiko',true);

        if($chushajokubun_data=="1")    echo 'あり（空き要確認）';

        if($chushajokubun_data=="2")    echo 'なし';

        // if($chushajokubun_data=="3")    echo '近隣';

        if($chushajokubun_data=="4")    echo '要確認';

        //text

        if( $chushajokubun_data !='' && !is_numeric($chushajokubun_data) ) echo $chushajokubun_data;
        if( $chushajobiko_data !='' ) echo '<br>' . nl2br($chushajobiko_data);
}

/**
 * 学区
 * single-room.php
 */

function the_custom_gakku($post_id){

    $gakku = array();
    $gakku[] = get_post_meta($post_id,'shuuhenshougaku',true);
    $gakku[] = get_post_meta($post_id,'shuuhenchuugaku',true);
    $gakku[] = esc_textarea(get_post_meta($post_id,'shuuhensonota',true));

    $disp_gakku = implode('<br>', $gakku);

    echo $disp_gakku;
}

function get_custom_gakku($post_id){

    $gakku = array();
    $gakku[] = get_post_meta($post_id,'shuuhenshougaku',true);
    $gakku[] = get_post_meta($post_id,'shuuhenchuugaku',true);
    $gakku[] = esc_textarea(get_post_meta($post_id,'shuuhensonota',true));

    // $disp_gakku = implode('<br>', $gakku);
    if($gakku[0] !== ''){
        return true;
    }else{
        return false;
    }
}

/**
 * single-room.php
 * single-fudo.php
 * コメントのフォーマット
 *
 */
function fudo_theme_comment($comment, $args, $depth) {
    $GLOBALS['comment'] = $comment;

    ?>

    <div class="o-rsingleComment_item" id="comment-<?php comment_ID();?>">
        <p class="o-rsingleComment_name">
          <?php //printf(__('%s'), get_comment_author_link()) ?>
       <?php if ($comment->comment_approved == '0') : ?>
          <em><?php _e('Your comment is awaiting moderation.') ?></em>
       <?php endif; ?>

       <span class="o-rsingleComment_date"><?php printf(__('%1$s at %2$s'), get_comment_date(),  get_comment_time()) ?></span>
       <?php edit_comment_link(__('(Edit)'),'  ','') ?>
        </p>

       <p class="o-rsingleComment_text">
       <?php echo get_comment_text(); ?>
       </p>
          <?php comment_reply_link(array_merge( $args, array('depth' => $depth, 'max_depth' => $args['max_depth']))) ?>
    </div>

 <?php
         }

/**
 * ○部屋の画像　　物権用
 */
function get_bukken_image_count($post_id){
    $i = 0;

    $bukken_madori_images = get_field('bukken_madori_images', $post_id);
    if(is_array($bukken_madori_images)) $i += count($bukken_madori_images);

    $bukken_gaikan_kyouyou = get_field('bukken_gaikan_kyouyou', $post_id);
    if(is_array($bukken_gaikan_kyouyou)) $i += count($bukken_gaikan_kyouyou);

    $bukken_situnai_senyu = get_field('bukken_situnai_senyu', $post_id);
    if(is_array($bukken_situnai_senyu)) $i += count($bukken_situnai_senyu);

    // $bukken_madori_images = get_field('bukken_madori_images', $post_id);
    // if(is_array($bukken_madori_images)) $i += count($bukken_madori_images);

    // $bukken_living_images = get_field('bukken_living_images', $post_id);
    // if(is_array($bukken_living_images)) $i += count($bukken_living_images);

    // $bukken_bedroom_images = get_field('bukken_bedroom_images', $post_id);
    // if(is_array($bukken_bedroom_images)) $i += count($bukken_bedroom_images);

    // $bukken_bathroom_images = get_field('bukken_bathroom_images', $post_id);
    // if(is_array($bukken_bathroom_images)) $i += count($bukken_bathroom_images);

    // $bukken_room_images = get_field('bukken_room_images', $post_id);
    // if(is_array($bukken_room_images)) $i += count($bukken_room_images);

    // $bukken_gaikan_images = get_field('bukken_gaikan_images', $post_id);
    // if(is_array($bukken_gaikan_images)) $i += count($bukken_gaikan_images);

    // $bukken_other_images = get_field('bukken_other_images', $post_id);
    // if(is_array($bukken_other_images)) $i += count($bukken_other_images);

    return $i;
}



/**
 * モーダルで表示される物件画像生成
 */
function get_bukken_images($post_id){

//棟の外観
$bukken_madori_images = get_field('bukken_madori_images', $post_id);
if($bukken_madori_images){
    echo '<h4 class="o-title o-modalTitle_sub">間取り</h4>';
    foreach($bukken_madori_images as $v){
        $image_src = $v['sizes']['large'];
        echo '<img src="' . $image_src . '" alt="" class="o-modalRoom_img" loading="lazy">' . PHP_EOL;
    }
}

//棟その他
$bukken_gaikan_kyouyou = get_field('bukken_gaikan_kyouyou', $post_id);
if($bukken_gaikan_kyouyou){
    echo '<h4 class="o-title o-modalTitle_sub">外観・共用部</h4>';
    foreach($bukken_gaikan_kyouyou as $v){
        $image_src = $v['sizes']['large'];
        echo '<img src="' . $image_src . '" alt="" class="o-modalRoom_img" loading="lazy">' . PHP_EOL;
    }
}

//リビング画像
$bukken_situnai_senyu = get_field('bukken_situnai_senyu', $post_id);
if($bukken_situnai_senyu){
    echo '<h4 class="o-title o-modalTitle_sub">室内・専有部</h4>';
    foreach($bukken_situnai_senyu as $v){
        $image_src = $v['sizes']['large'];
        echo '<img src="' . $image_src . '" alt="" class="o-modalRoom_img" loading="lazy">' . PHP_EOL;
    }
}
// スクレイピングした画像
$h_images = stw_get_singlefudo_images($post_id);
if ( 0 < count($h_images) ){
    echo '<h4 class="o-title o-modalTitle_sub">すべての画像</h4>';


    $i = 1;
    foreach($h_images as $oh){
        if(!in_array(basename($oh->image_path), get_ban_image_file_name())){
            $image_src = stw_generate_imgurl($oh->image_path);
            // 管理者ログインしていれば削除対象画像URLが出る
            if(is_user_logged_in()){
                echo '<input id="copyTarget' . $i . '" type="text" value="' . basename($oh->image_path) . '" style="width:600px;border:1px solid #444;" readonly><button style="background-color:#444;" onclick="copyToClipboard(' . $i . ')">Copy</button>';
            }
            echo '<img src="' . $image_src . '" alt="" class="o-modalRoom_img" loading="lazy">' . PHP_EOL;
        }
        $i++;
    }
}

    // //棟の外観
    // $bukken_gaikan_images = get_field('bukken_gaikan_images', $post_id);
    // if($bukken_gaikan_images){
    //     echo '<h4 class="o-title o-modalTitle_sub">棟の外観</h4>';
    //     foreach($bukken_gaikan_images as $v){
    //         $image_src = $v['sizes']['large'];
    //         echo '<img src="' . $image_src . '" alt="" class="o-modalRoom_img" loading="lazy">' . PHP_EOL;
    //     }
    // }

    // //棟その他
    // $bukken_other_images = get_field('bukken_other_images', $post_id);
    // if($bukken_other_images){
    //     echo '<h4 class="o-title o-modalTitle_sub">棟その他</h4>';
    //     foreach($bukken_other_images as $v){
    //         $image_src = $v['sizes']['large'];
    //         echo '<img src="' . $image_src . '" alt="" class="o-modalRoom_img" loading="lazy">' . PHP_EOL;
    //     }
    // }

    // //リビング画像
    // $bukken_living_images = get_field('bukken_living_images', $post_id);
    // if($bukken_living_images){
    //     echo '<h4 class="o-title o-modalTitle_sub">リビング</h4>';
    //     foreach($bukken_living_images as $v){
    //         $image_src = $v['sizes']['large'];
    //         echo '<img src="' . $image_src . '" alt="" class="o-modalRoom_img" loading="lazy">' . PHP_EOL;
    //     }
    // }

    // //寝室
    // $bukken_bedroom_images = get_field('bukken_bedroom_images', $post_id);
    // if($bukken_bedroom_images){
    //     echo '<h4 class="o-title o-modalTitle_sub">寝室</h4>';
    //     foreach($bukken_bedroom_images as $v){
    //         $image_src = $v['sizes']['large'];
    //         echo '<img src="' . $image_src . '" alt="" class="o-modalRoom_img" loading="lazy">' . PHP_EOL;
    //     }
    // }

    // //浴室
    // $bukken_bathroom_images = get_field('bukken_bathroom_images', $post_id);
    // if($bukken_bathroom_images){
    //     echo '<h4 class="o-title o-modalTitle_sub">浴室</h4>';
    //     foreach($bukken_bathroom_images as $v){
    //         $image_src = $v['sizes']['large'];
    //         echo '<img src="' . $image_src . '" alt="" class="o-modalRoom_img" loading="lazy">' . PHP_EOL;
    //     }
    // }

    // //室内その他
    // $bukken_room_images = get_field('bukken_room_images', $post_id);
    // if($bukken_room_images){
    //     echo '<h4 class="o-title o-modalTitle_sub">室内その他</h4>';
    //     foreach($bukken_room_images as $v){
    //         $image_src = $v['sizes']['large'];
    //         echo '<img src="' . $image_src . '" alt="" class="o-modalRoom_img" loading="lazy">' . PHP_EOL;
    //     }
    // }

    // //物件間取り画像
    // $bukken_madori_images = get_field('bukken_madori_images', $post_id);
    // if($bukken_madori_images){
    //     echo '<h4 class="o-title o-modalTitle_sub">物件間取り</h4>';
    //     foreach($bukken_madori_images as $v){
    //         $image_src = $v['sizes']['large'];
    //         echo '<img src="' . $image_src . '" alt="" class="o-modalRoom_img" loading="lazy">' . PHP_EOL;
    //     }
    // }




}

// コピペ用スクリプト
function my_load_widget_scripts() {
    if(is_user_logged_in()){
    wp_enqueue_script('copypaste', get_theme_file_uri('/assets/js/copypaste.js'), array());
    }
}

// wp_footerに処理を登録
add_action('wp_footer', 'my_load_widget_scripts');



/**
 * ○部屋の画像　部屋用
 */
function get_room_image_count($post_id){
    $i = 0;

    $room_madori_images = get_field('room_madori_images', $post_id);
    if(is_array($room_madori_images)) $i += count($room_madori_images);

    $room_situnai_senyu = get_field('room_situnai_senyu', $post_id);
    if(is_array($room_situnai_senyu)) $i += count($room_situnai_senyu);

    $room_gaikan_kyouyou = get_field('room_gaikan_kyouyou', $post_id);
    if(is_array($room_gaikan_kyouyou)) $i += count($room_gaikan_kyouyou);

    //棟の写真
    $parent_post_ID = get_parent_ID($post_id);
    $bukken_madori_images = get_field('bukken_madori_images', $parent_post_ID);
    if(is_array($bukken_madori_images)) $i += count($bukken_madori_images);

    $bukken_gaikan_kyouyou = get_field('bukken_gaikan_kyouyou', $parent_post_ID);
    if(is_array($bukken_gaikan_kyouyou)) $i += count($bukken_gaikan_kyouyou);

    $bukken_situnai_senyu = get_field('bukken_situnai_senyu', $parent_post_ID);
    if(is_array($bukken_situnai_senyu)) $i += count($bukken_situnai_senyu);

    return $i;
}


/**
 * モーダルで表示される部屋画像生成
 */
function get_room_images($post_id){

    //間取り
    $room_madori_images = get_field('room_madori_images', $post_id);
    if($room_madori_images){
        echo '<h4 class="o-title o-modalTitle_sub">間取り</h4>';
        foreach($room_madori_images as $v){
            $image_src = $v['sizes']['large'];
            echo '<img src="' . $image_src . '" alt="" class="o-modalRoom_img" loading="lazy">' . PHP_EOL;
        }
    }

    //室内・専有部
    $room_situnai_senyu = get_field('room_situnai_senyu', $post_id);
    if($room_situnai_senyu){
        echo '<h4 class="o-title o-modalTitle_sub">室内・専有部</h4>';
        foreach($room_situnai_senyu as $v){
            $image_src = $v['sizes']['large'];
            echo '<img src="' . $image_src . '" alt="" class="o-modalRoom_img" loading="lazy">' . PHP_EOL;
        }
    }

    //外観・共用部
    $room_gaikan_kyouyou = get_field('room_gaikan_kyouyou', $post_id);
    if($room_gaikan_kyouyou){
        echo '<h4 class="o-title o-modalTitle_sub">外観・共用部</h4>';
        foreach($room_gaikan_kyouyou as $v){
            $image_src = $v['sizes']['large'];
            echo '<img src="' . $image_src . '" alt="" class="o-modalRoom_img" loading="lazy">' . PHP_EOL;
        }
    }

    //棟の写真群
    $parent_post_ID = get_parent_ID($post_id);
    //棟の外観
    $bukken_madori_images = get_field('bukken_madori_images', $parent_post_ID);
    if($bukken_madori_images){
        echo '<h4 class="o-title o-modalTitle_sub">間取り</h4>';
        foreach($bukken_madori_images as $v){
            $image_src = $v['sizes']['large'];
            echo '<img src="' . $image_src . '" alt="" class="o-modalRoom_img" loading="lazy">' . PHP_EOL;
        }
    }

    //棟その他
    $bukken_gaikan_kyouyou = get_field('bukken_gaikan_kyouyou', $parent_post_ID);
    if($bukken_gaikan_kyouyou){
        echo '<h4 class="o-title o-modalTitle_sub">外観・共用部</h4>';
        foreach($bukken_gaikan_kyouyou as $v){
            $image_src = $v['sizes']['large'];
            echo '<img src="' . $image_src . '" alt="" class="o-modalRoom_img" loading="lazy">' . PHP_EOL;
        }
    }

    //リビング画像
    $bukken_situnai_senyu = get_field('bukken_situnai_senyu', $parent_post_ID);
    if($bukken_situnai_senyu){
        echo '<h4 class="o-title o-modalTitle_sub">室内・専有部</h4>';
        foreach($bukken_situnai_senyu as $v){
            $image_src = $v['sizes']['large'];
            echo '<img src="' . $image_src . '" alt="" class="o-modalRoom_img" loading="lazy">' . PHP_EOL;
        }
    }
    // スクレイピングした画像
    $h_images = stw_get_singlefudo_images($post_id);
    if ( 0 < count($h_images) ){
        echo '<h4 class="o-title o-modalTitle_sub">すべての画像</h4>';
        foreach($h_images as $oh){
            if(!in_array(basename($oh->image_path), get_ban_image_file_name())){
                $image_src = stw_generate_imgurl($oh->image_path);
                if(is_user_logged_in()){
                    echo '<input id="copyTarget' . $i . '" type="text" value="' . basename($oh->image_path) . '" style="width:600px;border:1px solid #444;" readonly><button style="background-color:#444;" onclick="copyToClipboard(' . $i . ')">Copy</button>';
                }
                echo '<img src="' . $image_src . '" alt="" class="o-modalRoom_img" loading="lazy">' . PHP_EOL;
            }
        }
    }

}


/**
 *　検討リストカウント　ハートマーク
 */

function get_mylist(){
    $user = isset($_REQUEST['user']) ? $_REQUEST['user'] : "";
    $count = 0;
    global $favorite_post_ids;
    if ( !empty($user) ) {
        if ( wpfp_is_user_favlist_public($user) )
            $favorite_post_ids = wpfp_get_users_favorites($user);
        } else {
            $favorite_post_ids = wpfp_get_users_favorites();
            $count = (is_array($favorite_post_ids))?count($favorite_post_ids) : 0;
        }
    return $count;
}


/**
 * 各問い合わせに、対象post_idを渡す
 * CF7のフック
 */
function kaiza_form_tag_filter($tag){
    if ( ! is_array( $tag ) )
    return $tag;

    if(isset($_GET['post_id'])){ //投稿ID
        $postids = htmlspecialchars($_GET['post_id']);

        //
        $bukken_room_name = "問い合わせ対象物件\n\n";

        //カンマ区切りのgetデータを分割し配列に。
        $postid = explode(",", $postids);
        foreach($postid as $v){
            $bukken_room_name .= get_the_title($v) . "\n";
        }

        //CF7の名称変数を修得
        $name = $tag['name'];

    //こちらにCF7上で値をいれたいfield名を指定
    if($name == 'your-message'){
    $tag['values'] = (array)$bukken_room_name;
    }

    }
    return $tag;
    }
add_filter('wpcf7_form_tag', 'kaiza_form_tag_filter', 11);


/**
 * メタ情報整形
 */

function the_meta_information(){
    global $post;
    $post_id = $post->ID;
    $disp_keywords = '';
    $disp_rosen_names = '';
    $description = '';
    $keywords = array();
    $rosen_names = array();

    if($post->post_type == 'room'){
        $parent_post_ID = get_parent_ID($post_id);
        $title = get_the_title($parent_post_ID) . '/' . get_the_title($post_id) .
        '（' . xls_custom_madorisu_print($post_id) . '）の部屋・間取り一覧と周辺情報｜高級賃貸東京';

        $disp_keywords = get_the_title($parent_post_ID) . ' ' . get_field('room_floor') . '階';

        $rosen_names[0] = get_rosen_name1($parent_post_ID);
        $rosen_names[1] = get_rosen_name2($parent_post_ID);
        $rosen_names[2] = get_rosen_name3($parent_post_ID);

        $disp_rosen_names = implode(' ', $rosen_names);
        $description = get_the_title($parent_post_ID) . 'は「' . $disp_rosen_names . '」を利用できる高級賃貸マンションです。ご内見は来店不要、現地お待ち合わせにてご案内いたします。';

    }elseif($post->post_type == 'fudo'){
        $title = get_the_title($post_id) . 'の物件・間取り一覧と周辺情報｜高級賃貸東京';
        $keywords[0] = get_the_title($post_id);
        $keywords[1] = get_the_title($post_id) . ' 部屋';
        $keywords[2] = get_the_title($post_id) . ' 間取り';
        $keywords[3] = get_the_title($post_id) . ' 賃貸';

        $disp_keywords = implode(',', $keywords);

        $rosen_names[0] = get_rosen_name1($post_id);
        $rosen_names[1] = get_rosen_name2($post_id);
        $rosen_names[2] = get_rosen_name3($post_id);

        $disp_rosen_names = implode(' ', $rosen_names);
        $description = get_the_title($post_id) . 'は「' . $disp_rosen_names . '」を利用できる高級賃貸マンションです。ご内見は来店不要、現地お待ち合わせにてご案内いたします。';

    }else{
        //その他の固定ページ群など

    }

    //独自のものがあったらそれで上書き
    if(get_field('meta_title')!=''){$title = get_field('meta_title');}
    if(get_field('meta_keyword')!='') {$disp_keywords = get_field('meta_keyword');}
    if(get_field('meta_description')!='') {$description = get_field('meta_description');}

    //TOP
    if(is_home()){
        $title = get_field('meta_title', 'option');
        $disp_keywords = get_field('meta_keyword', 'option');
        $description = get_field('meta_description', 'option');
    }

    if($_GET['bukken'] == 'jsearch'){
        $title = '検索';
        // $title = get_station_name(9213);
        require_once get_template_directory().'/inc/inc-search-title.php';
        $title = $search_result_title;
    }

    if(get_post_type()=='post'){
        $title = get_the_title();
        $disp_keyword = get_field('meta_keyword');
        $description = get_field('meta_description');
    }

    echo '<title>' . $title . '</title>'  . PHP_EOL;
    echo '<meta name="keywords" content="' . $disp_keywords . '" />' . PHP_EOL;
    echo '<meta name="description" content="' . $description . '" />' . PHP_EOL;
}

/**
 * 物件に表示する特にオススメした特徴
 */

function the_recommend_speciality($post_id){
    $specialities = get_field('recommend_speciality', $post_id);

    if( have_rows('recommend_speciality', $post_id) ):

        ?>

        <div class="o-rsingleTag flex-w">
        <?php
        foreach( $specialities as $v ){//print_r($v);
            ?>
            <div class="o-rsingleTag_item"><i class="o-rsingleTag_icon o-icon __min flaticon-tag"></i>
                <a href="<?php echo $v->recommend_speciality_link;?>" class="o-rsingleTag_link o-link __nocolor"><?php echo $v->post_title;?></a></div>
        <?php
        }
        ?>
        </div>

        <?php
    endif;
}

//functions.phpなどに
/**
 * SQL確認したいとき
 */
function sql_dump($query)
{
    var_dump($query);
    return $query;
}
// add_filter('query', 'sql_dump');


/**
 * SQLを整形して表示
 */
function the_sql($sql){
    $pre_disp_sql = explode('AND', $sql);
    $disp_sql =  implode('<br>AND', $pre_disp_sql);
    $disp_sql = str_replace('WHERE', '<br>WHERE', $disp_sql);
    $disp_sql = str_replace('INNER', '<br>INNER', $disp_sql);
    $disp_sql = str_replace('LEFT', '<br>LEFT', $disp_sql);
    $disp_sql = str_replace('ORDER', '<br>ORDER', $disp_sql);

    echo $disp_sql;
}


add_filter('template_include','stw_custom_search_template',12);
/**
 * search.phpのテンプレート切り替え
 * @param string $template
 * @return string
 */
function stw_custom_search_template($template){
    if ( is_search() ){
        $post_types = get_query_var('post_type');
        foreach ( (array) $post_types as $post_type )
            if ( 'post' == $post_type ) $templates[] = "staffblog-search.php";
            $templates[] = 'search.php';
            $template = get_query_template('search',$templates);
    }
    return $template;
}

/**
 * 検索対象をbukken,roomに
 */
function search_pre_get_posts( $query ) {
    if ( $query->is_search && !is_admin() ){
    $query->set( 'post_type', array("fudo") );
    // $query->set( 'post_type', array("fudo", "room") );
    }
    return $query;
    }
    add_action( 'pre_get_posts', 'search_pre_get_posts' );


/**
 * 閲覧履歴・最近見た物件の数を返す関数
 * fudouhistoryプラグインでカウントしている数を返す。
 * cookie:fudou_singleで閲覧した物件idをシリアライズ化したものを保持している。
 * cookieが無い場合は0を返す。
 * 参考ソース：plugins/fudouhistory/json/dat_history_bukken.php L65-L74
 * @return number
 */
function stw_count_his() {
    // 戻り値
    $hiscon = 0;
    // cookie値を取得
    $historys = isset($_COOKIE['fudou_single']) ? $_COOKIE['fudou_single'] : '';
    if ( !empty($historys) ){
        // cookieの値をunserializeして配列にする。
        $historys = str_replace( "\\" ,"" , $historys);
        $historys = maybe_unserialize( $historys );
        // 配列の要素数=閲覧した物件idの数
        $hiscon = count($historys);
    }
    return $hiscon;
}


/**
 * スクレイピングした画像情報を取得する関数
 * 引数で渡されたpost_idよりuniq_idを取得。
 * housing_imagesから、該当uniq_idの全件レコードを取得し返す。
 * uniq_idが存在しないか、housing_imagesに該当レコードが無い場合は、空配列を返す。
 * @param int $post_id
 * @param int $type 取得する画像タイプ（1:間取り画像2:外観）なしの場合は間取り以外の画像全て
 * @return array|object|NULL
 */
function stw_get_singlefudo_images( $post_id, $type=null ){
    global $wpdb;
    $rtn_arr = array();
    // postmetaよりuniq_idを取得
    $uniq_res = $wpdb->get_row($wpdb->prepare("select meta_value from ".$wpdb->prefix . "postmeta where post_id = %s and meta_key = 'uniq_id'", $post_id ));
    if ( $uniq_res->meta_value ){
        $uniq_id = $uniq_res->meta_value;
        // スクレイピングした画像情報を取得
        if ( 1 == $type ){ // 間取り画像
            $images_res = $wpdb->get_results($wpdb->prepare("select * from " . $wpdb->prefix ."housing_images where uniq_id = %s and is_madori = 1", $uniq_id));

        } elseif ( 2 == $type ) { // 外観
            $images_res = $wpdb->get_results($wpdb->prepare("select * from " . $wpdb->prefix ."housing_images where uniq_id = %s and description like '外観%' ", $uniq_id));
            //echo '<pre>';var_dump($uniq_id);echo '</pre>';

        } else {
            $images_res = $wpdb->get_results($wpdb->prepare("select * from " . $wpdb->prefix ."housing_images where uniq_id = %s and is_madori = 0", $uniq_id));
        }
        if ( 0 < $wpdb->num_rows ){
            $rtn_arr = $images_res;
        }
    }

    return $rtn_arr;
}


/**
 * スクレイピングした間取り画像URLを取得する関数
 * 引数で渡されたroom_post_idよりuniq_idを取得。
 * housing_imagesから、該当uniq_idでis_madoriが1のレコードを1件取得し返す。
 * uniq_idが存在しないか、housing_imagesに該当レコードが無い場合は、空文字を返す
 * @param int $room_post_id
 * @return string
 */
function stw_get_singlefudo_madori_image( $room_post_id ){
    global $wpdb;
    $rtn_path = '';
    // postmetaよりuniq_idを取得
    $uniq_res = $wpdb->get_row($wpdb->prepare("select meta_value from ".$wpdb->prefix . "postmeta where post_id = %s and meta_key = 'uniq_id'", $room_post_id ));
    if ( $uniq_res->meta_value ){
        $uniq_id = $uniq_res->meta_value;
        // スクレイピングした画像情報を取得
        $images_res = $wpdb->get_results($wpdb->prepare("select * from " . $wpdb->prefix ."housing_images where uniq_id = %s and is_madori = 1", $uniq_id));
        if ( 0 < $wpdb->num_rows ){
            $rtn_path = stw_generate_imgurl($images_res[0]->image_path);
        }
    }

    return $rtn_path;
}


function stw_generate_imgurl( $path ){
    return home_url() . '/fudoimages/' . $path;
}

/**
 * 非表示画像オプション値取得
 */
function get_ban_image_file_name(){
    $ban_image_file_name = array();
    $ban_image_file_name = explode("\n", get_field('ban_image_file_name', 'option'));
    $ban_image_file_name = array_map('trim', $ban_image_file_name);
    $ban_image_file_name = array_filter($ban_image_file_name, 'strlen');
    $ban_image_file_name = array_values($ban_image_file_name);

    return $ban_image_file_name;

}


/**
 * 部屋IDから取得元のURLを表示
 * 管理者ログイン時のみ。
 */
function get_source_url($room_id){
    global $wpdb;

    
    $ret = $wpdb->get_row($wpdb->prepare("select A.ID, A.post_type, B.url as `url`, source from kctwp_posts A, kctwp_housing_transformed B where A.post_name = B.uniq_id and ID = " . $room_id . ";"));

    return '<p style="margin:20px 0;color:red;">取得元：<a href="' . $ret->url . '" target="_blank">' . $ret->source . "</a></p>";

}
?>
