<?php get_header();?>



<div id="staffblog" class="l-cont __main content-area underlayer">
  <main id="main" class="site-main" role="main">


      <div class="o-fv __page mb-3">
          <img src="https://koukyu-chintai.com/wp-content/uploads/cropped-cropped-main-1-2000x1200-1-1024x614.jpg" alt="" class="o-fvImg" loading="lazy">
          <div class="o-fvCatch">
              <h2 class="o-fvCatch_title o-title __large __white __nobold">役立つ高級賃貸の知識一覧</h2>
              <span class="o-fvCatch_sub o-title __mid __white"><?php single_cat_title(); ?>
              </span>
          </div>
      </div>

      <div class="flex-nw-no-xl jc-between l-wrap __maxw02 __type02 l-row">
        <div class="p-postMain l-rowLeft post">
           <div class=" flex-w">
            <?php
            $paged=urlencode($_GET["page"]);
            $list = new WP_Query("category_name=staffblog&paged=$paged");

            $posts_per_page=get_option('posts_per_page');
            $max_num_pages = $list->max_num_pages;
            query_posts('category_name=staffblog&paged='.$paged.'&showposts='.$posts_per_page); ?>
            <?php while (have_posts()) : the_post(); ?>
               <article class="o-post __2">
                   <div class="o-postWrap">
                <div class="o-postThumb"><?php the_post_thumbnail('full'); ?></div>
                <div class="o-postHeader">
                    <ul class="o-postMeta flex-nw jc-between ai-center">
                        <li class="o-postCat"><?php the_category(); ?></li>
                        <li class="o-postDate"><?php the_time('Y.m.d')?></li>
                    </ul>
                    <h3 class="o-postTitle"><a href="<?php the_permalink() ?>" class="text-link"><?php the_title(); ?></a></h3>
                </div>
                       <a href="<?php the_permalink() ?>" class="o-postLink"></a>
                   </div>
            </article>
            <?php endwhile;?>
            </div>

          <div class="tablenav">
              <?php
              global $wp_rewrite;
              $paginate_base = esc_url(get_pagenum_link(1));
              $paginate_format = '';
              if (strpos($paginate_base, '?') || !$wp_rewrite->using_permalinks()){
                  $paginate_base = add_query_arg('page', '%#%');
              } else {
                  if (substr($paginate_base, -1 ,1) != '/'){
                      $paginate_format = '/';
                  }
                  $paginate_format .= user_trailingslashit('?page=%#%');
                  $paginate_base .= '%_%';

              }
              echo paginate_links(array(
                  'base' => $paginate_base,
                  'format' => $paginate_format,
                  'total' =>$max_num_pages,
                  'mid_size' => 5,
                  'current' => ($paged ? $paged : 1),
                  'prev_next' => false
              ));

              ?>
        </div>
          </div>

          <div class="p-postSide l-rowRight post">
              <?php get_sidebar(); ?>
          </div>
      </div>

  </main><!-- #main -->
</div><!-- #about -->

<?php get_footer('single'); ?>
