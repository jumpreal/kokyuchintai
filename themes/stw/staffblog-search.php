<?php
/**
 * The template for displaying search results pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>

<div id="staffblog" class="l-cont __main content-area underlayer">



    <main id="main" class="site-main" role="main">
    <?php
            global $wp_query;
            $total_results = $wp_query->found_posts;
            $search_query = get_search_query();
            ?>
            <div class="o-fv __page mb-3">
                <img src="https://koukyu-chintai.com/wp-content/uploads/cropped-cropped-main-1-2000x1200-1-1024x614.jpg" alt="" class="o-fvImg" loading="lazy">
                <div class="o-fvCatch">
                    <h2 class="o-fvCatch_title o-title __large __white __nobold">「
                        <?php echo $search_query; ?>」の検索結果
                    </h2>
                    <span class="o-fvCatch_sub o-title __mid __white">
                        <?php echo $total_results; ?>件ヒットしました。
                    </span>
                </div>
            </div>

            <div class="flex-nw-no-xl jc-between l-wrap __maxw02 __type02 l-row">
                <div class="p-postMain l-rowLeft post">
                    <div class=" flex-w">
                        <?php
                        if( $total_results >0 ):
                        if(have_posts()):
                        while(have_posts()): the_post();
                        ?>
                        <article class="o-post __2">
                            <div class="o-postWrap">
                                <div class="o-postThumb">
                                    <?php the_post_thumbnail('full'); ?>
                                </div>
                                <div class="o-postHeader">
                                    <ul class="o-postMeta flex-nw jc-between ai-center">
                                        <li class="o-postCat">
                                            <?php the_category(); ?>
                                        </li>
                                        <li class="o-postDate">
                                            <?php the_time('Y.m.d')?>
                                        </li>
                                    </ul>
                                    <h3 class="o-postTitle"><a href="<?php the_permalink() ?>" class="text-link">
                                            <?php the_title(); ?></a></h3>
                                </div>
                                <a href="<?php the_permalink() ?>" class="o-postLink"></a>
                            </div>
                        </article>
                        <?php endwhile; endif; else: ?>
                        <h3 class="o-title __title">「<?php echo $search_query; ?>」 に一致する記事は見つかりませんでした。</h3>

                        <?php endif; ?>
                    </div>
                </div>



                <div class="p-postSide l-rowRight post">
                    <?php get_sidebar(); ?>
                </div>
            </div>

    </main><!-- #main -->
</div><!-- #about -->

<?php get_footer('single'); ?>
        
