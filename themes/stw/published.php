<?php
/*
Template Name: published
*/
?>
<?php get_header();?>



<div id="published" class="content-area underlayer">
  <main id="main" class="site-main" role="main">

    <nav id="breadcrumbs">
      <ul class="cf">
        <li><a href="/"><span>ホーム</span></a></li>
        <li>掲載依頼について</li>
      </ul>
    </nav>

    <article class="ul-flex">
      <section class="ul-contents">
        <h2 class="ul-title mincho">当サイトへの掲載依頼について</h2>

        <section class="published01">
          <div class="publi-box">
            <h3 class="ul-title01 mincho">管理会社様・不動産オーナー様へ</h3>
            <p>高級賃貸東京では、厳選した物件のみHPに掲載しておりますので、ご依頼いただいた物件につきましては、弊社がお役立ちできるかどうかの審査をさせていただきます。<br>
              その結果、掲載を見送らせていただく場合があることを、あらかじめご了承ください。</p>
            <h4>・掲載費用について</h4>
            <p>掲載させて頂いた物件が成約となった場合のみ、広告料または仲介手数料を請求させて頂きます。<br>
            成約に至るまでは一切費用は不要ですので、お気軽にご相談ください。</p>
          </div>

          <div class="con-box">
            <p>掲載依頼につきましては、下記よりお願い致します。</p>
            <div class="con-btn"><a href="/contact/">お問い合わせフォームへ</a></div>
          </div>

        </section><!-- .published01 -->
      </section><!-- .ul-contents -->

      <?php get_sidebar(); ?>
    </article><!-- .ul-flex -->

  </main><!-- #main -->
</div><!-- #published -->

<?php get_footer(); ?>
