<?php

/*

 * 不動産プラグインウィジェット

 * @package WordPress 5.0

 * @subpackage Fudousan Plugin

 * Version: 5.0.0

*/





//トップ投稿表示(最近の記事)

function fudo_toukouInit_top_stw() {

    register_widget('fudo_toukou_top_stw');

}

add_action('widgets_init', 'fudo_toukouInit_top_stw');



//トップ物件表示

function fudo_widgetInit_top_r_stw() {

    register_widget('fudo_widget_top_r_stw');

}

add_action('widgets_init', 'fudo_widgetInit_top_r_stw');



// トップ投稿表示(最近の記事)

class fudo_toukou_top_stw extends WP_Widget {



    /**

     * Register widget with WordPress 4.3.

     */

    function __construct() {

        parent::__construct(

            'fudo_toukou_top_stw',             // Base ID

            '最近の投稿表示' ,        // Name

            array( 'description' => '最近の投稿記事(抜粋優先・アイキャッチ画像)を表示します。', )    // Args

        );

    }



    /** @see WP_Widget::form */

    function form($instance) {

        $title = isset($instance['title']) ? esc_attr($instance['title']) : '';

        $t_cat = isset($instance['t_cat']) ? esc_attr($instance['t_cat']) : '';

        $item  = isset($instance['item']) ? esc_attr($instance['item']) : '';



        if($item=='') $item = 5;



        ?>

        <p><label for="<?php echo $this->get_field_id('title'); ?>">

        <?php _e('title'); ?> <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo $title; ?>" /></label></p>



        <p><label for="<?php echo $this->get_field_id('t_cat'); ?>">

        投稿カテゴリ<br /><?php wp_dropdown_categories(array('show_option_all' => '全てのカテゴリ', 'hide_empty' => 0, 'hierarchical' => 1, 'show_count' => 0, 'name' => $this->get_field_name('t_cat'), 'orderby' => 'name', 'selected' => $t_cat  ));?></label></p>



        <p><label for="<?php echo $this->get_field_id('item'); ?>">

        表示数 <input class="widefat" id="<?php echo $this->get_field_id('item'); ?>" name="<?php echo $this->get_field_name('item'); ?>" type="text" value="<?php echo $item; ?>" /></label></p>



        <?php

    }



    /** @see WP_Widget::update */

    function update($new_instance, $old_instance) {

        return $new_instance;

    }



    /** @see WP_Widget::widget */

    function widget($args, $instance) {

        global $wpdb;

        global $post;



        // outputs the content of the widget



            extract( $args );



        $title = isset($instance['title']) ? apply_filters('widget_title', $instance['title']) : '';

        $t_cat = isset($instance['t_cat']) ? $instance['t_cat'] : '';

        $item  = isset($instance['item']) ?  $instance['item'] : '';



        $category_link = '';

        echo $before_widget;





            if($t_cat > 0 ){

                $category_link = get_category_link( $t_cat );

            }

            //カテゴリ指定の場合はカテゴリリンク

            if ( $title !='' ){

                if($category_link){

                    echo $before_title . '<a href="' . $category_link . '">' . $title . '</a>' .  $after_title;

                }else{

                    echo $before_title . $title . $after_title;

                }

            }


            $sql  =  "SELECT DISTINCT P.ID , P.post_title , P.post_date , P.post_content , P.post_excerpt";

            $sql .=  " FROM $wpdb->posts AS P ";

            $sql .=  " INNER JOIN ($wpdb->term_taxonomy AS TT ";

            $sql .=  " INNER JOIN $wpdb->term_relationships AS TR ON TT.term_taxonomy_id = TR.term_taxonomy_id) ON P.ID = TR.object_id";

            $sql .=  " WHERE P.post_password='' AND P.post_status='publish'";

            $sql .=  " AND P.post_type='post'";



            if( $t_cat > 0 )

            $sql .=  " AND TT.term_id =" . $t_cat .  "";

            $sql .=  " ORDER BY P.post_date DESC";

            $sql .=  " LIMIT 0,".$item."";



        //    $sql = $wpdb->prepare($sql,'');

            $metas = $wpdb->get_results( $sql, ARRAY_A );

            if(!empty($metas)) {



                echo "\n";

                echo "\n";

                echo '<ul id="toukou_top">';

                foreach ( $metas as $meta ) {

                    $meta_id = $meta['ID'];

                    $meta_post_title = $meta['post_title'];

                    $meta_post_date  = $meta['post_date'];



                    $meta_content_data = $meta['post_content'];

                    $meta_content_data = str_replace(array("\r\n","\r","\n","\t"), '',  mb_substr( strip_tags( $meta_content_data ) , 0, 130) );



                    $meta_excerpt_data  =  $meta['post_excerpt'];

                    $meta_excerpt_data = str_replace(array("\r\n","\r","\n","\t"), '',  mb_substr( strip_tags( $meta_excerpt_data ) , 0, 130) );



                    $meta_post_date = myLeft($meta_post_date,10);

                    echo "\n";



                    echo '<li><a href="' . get_permalink($meta_id) . '"><span class="toukou_top_post_title">'. $meta_post_title . '</span></a><br />';

                    echo '<ul class="toukou_top_post_excerpt">';

                    echo '<li><span class="toukou_top_post_thumbnail">' . get_the_post_thumbnail( $meta_id , 'thumbnail' ) . '</span>';



                    if($meta_excerpt_data){

                        echo '' . strip_shortcodes( $meta_excerpt_data ) . '';

                    }else{

                        echo '' . strip_shortcodes( $meta_content_data ) . '';

                    }



                    echo '<span style="float:right;">['.$meta_post_date.']　<a href="' . get_permalink($meta_id) . '">more・・</a></span>';


                    echo '</li></ul>';

                    echo '</li>';

                    echo "\n";

                }

                echo '</ul>';

                echo "\n";

                echo "\n";

            }



        echo $after_widget;



    }

} // Class fudo_toukou_top

















/**

 * トップ物件表示

 *

 * Version: 1.9.2

 */

class fudo_widget_top_r_stw extends WP_Widget {



    /**

     * Register widget with WordPress 4.3.

     */

    function __construct() {

        parent::__construct(

            'fudo_top_r',             // Base ID

            'トップ物件表示' ,        // Name

            array( 'description' => 'トップページに物件を表示します。', )    // Args

        );

    }



    /** @see WP_Widget::form */

    function form($instance) {



        global $is_fudoukaiin,$is_fudoubus;



        $title      = isset($instance['title']) ? esc_attr($instance['title']) : '';

        $item       = isset($instance['item']) ? esc_attr($instance['item']) : '';

        $shubetsu   = isset($instance['shubetsu']) ? esc_attr($instance['shubetsu']) : '';

        $bukken_cat = isset($instance['bukken_cat']) ? esc_attr($instance['bukken_cat']) : '';

        $sort       = isset($instance['sort']) ? esc_attr($instance['sort']) : '';



        $view1 = isset($instance['view1']) ? esc_attr($instance['view1']) : '';

        $view2 = isset($instance['view2']) ? esc_attr($instance['view2']) : '';

        $view3 = isset($instance['view3']) ? esc_attr($instance['view3']) : '';

        $view4 = isset($instance['view4']) ? esc_attr($instance['view4']) : '';

        $view5 = isset($instance['view5']) ? esc_attr($instance['view5']) : '';

        $view6 = isset($instance['view6']) ? esc_attr($instance['view6']) : '';    //バス路線



        $kaiinview = isset($instance['kaiinview']) ? esc_attr($instance['kaiinview']) : '';



        if($item=='') $item = 4;



        ?>

        <p><label for="<?php echo $this->get_field_id('title'); ?>">

        タイトル <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo $title; ?>" /></label></p>





        <p><label for="<?php echo $this->get_field_id('item'); ?>">

        表示数 <input class="widefat" id="<?php echo $this->get_field_id('item'); ?>" name="<?php echo $this->get_field_name('item'); ?>" type="text" value="<?php echo $item; ?>" /></label></p>





        <p><label for="<?php echo $this->get_field_id('shubetsu'); ?>">

        種別 <select class="widefat" id="<?php echo $this->get_field_id('shubetsu'); ?>" name="<?php echo $this->get_field_name('shubetsu'); ?>">

            <option value="1"<?php if($shubetsu == 1){echo ' selected="selected"';} ?>>物件すべて</option>

            <option value="2"<?php if($shubetsu == 2){echo ' selected="selected"';} ?>>売買すべて</option>

            <option value="3"<?php if($shubetsu == 3){echo ' selected="selected"';} ?>>売買土地</option>

            <option value="4"<?php if($shubetsu == 4){echo ' selected="selected"';} ?>>売買戸建</option>

            <option value="5"<?php if($shubetsu == 5){echo ' selected="selected"';} ?>>売買マンション</option>

            <option value="6"<?php if($shubetsu == 6){echo ' selected="selected"';} ?>>売買住宅以外の建物全部</option>

            <option value="7"<?php if($shubetsu == 7){echo ' selected="selected"';} ?>>売買住宅以外の建物一部</option>

            <option value="10"<?php if($shubetsu == 10){echo ' selected="selected"';} ?>>賃貸すべて</option>

            <option value="11"<?php if($shubetsu == 11){echo ' selected="selected"';} ?>>賃貸居住用</option>

            <option value="12"<?php if($shubetsu == 12){echo ' selected="selected"';} ?>>賃貸事業用</option>

        </select></label></p>





        <p><label for="<?php echo $this->get_field_id('bukken_cat'); ?>">

        物件カテゴリ <select class="widefat" id="<?php echo $this->get_field_id('bukken_cat'); ?>" name="<?php echo $this->get_field_name('bukken_cat'); ?>">

            <option value="0"<?php if($bukken_cat == 0){echo ' selected="selected"';} ?>>すべて</option>

<?php



        //物件カテゴリ

        $terms = get_terms('bukken', 'hide_empty=0');



        if ( !empty( $terms ) ){

            foreach ( $terms as $term ) {

                echo  '<option value="'.$term->term_id.'"';

                if( $bukken_cat == $term->term_id )    echo ' selected="selected"';

                echo '>'.$term->name.'</option>';

            }

        }

?>

        </select></label></p>

<p>ピックアップ</p>


        <p><label for="<?php echo $this->get_field_id('sort'); ?>">

        並び順 <select class="widefat" id="<?php echo $this->get_field_id('sort'); ?>" name="<?php echo $this->get_field_name('sort'); ?>">

            <option value="2"<?php if($sort == 2){echo ' selected="selected"';} ?>>新しい順</option>

            <option value="3"<?php if($sort == 3){echo ' selected="selected"';} ?>>古い順</option>

            <option value="4"<?php if($sort == 4){echo ' selected="selected"';} ?>>高い順</option>

            <option value="5"<?php if($sort == 5){echo ' selected="selected"';} ?>>安い順</option>

            <option value="6"<?php if($sort == 6){echo ' selected="selected"';} ?>>広い順(面積)</option>

            <option value="7"<?php if($sort == 7){echo ' selected="selected"';} ?>>狭い順(面積)</option>

            <option value="1"<?php if($sort == 1){echo ' selected="selected"';} ?>>ランダム</option>

        </select></label></p>



        表示項目<br />

        <p><label for="<?php echo $this->get_field_id('view1'); ?>">

        タイトル <select class="widefat" id="<?php echo $this->get_field_id('view1'); ?>" name="<?php echo $this->get_field_name('view1'); ?>">

            <option value="1"<?php if($view1 == 1){echo ' selected="selected"';} ?>>表示する</option>

            <option value="2"<?php if($view1 == 2){echo ' selected="selected"';} ?>>表示しない</option>

        </select></label></p>



        <p><label for="<?php echo $this->get_field_id('view2'); ?>">

        価格 <select class="widefat" id="<?php echo $this->get_field_id('view2'); ?>" name="<?php echo $this->get_field_name('view2'); ?>">

            <option value="1"<?php if($view2 == 1){echo ' selected="selected"';} ?>>表示する</option>

            <option value="2"<?php if($view2 == 2){echo ' selected="selected"';} ?>>表示しない</option>

        </select></label></p>



        <p><label for="<?php echo $this->get_field_id('view3'); ?>">

        間取り・土地面積 <select class="widefat" id="<?php echo $this->get_field_id('view3'); ?>" name="<?php echo $this->get_field_name('view3'); ?>">

            <option value="1"<?php if($view3 == 1){echo ' selected="selected"';} ?>>表示する</option>

            <option value="2"<?php if($view3 == 2){echo ' selected="selected"';} ?>>表示しない</option>

        </select></label></p>



        <p><label for="<?php echo $this->get_field_id('view4'); ?>">

        地域 <select class="widefat" id="<?php echo $this->get_field_id('view4'); ?>" name="<?php echo $this->get_field_name('view4'); ?>">

            <option value="1"<?php if($view4 == 1){echo ' selected="selected"';} ?>>表示する</option>

            <option value="2"<?php if($view4 == 2){echo ' selected="selected"';} ?>>表示しない</option>

        </select></label></p>



        <p><label for="<?php echo $this->get_field_id('view5'); ?>">

        路線・駅 <select class="widefat" id="<?php echo $this->get_field_id('view5'); ?>" name="<?php echo $this->get_field_name('view5'); ?>">

            <option value="1"<?php if($view5 == 1){echo ' selected="selected"';} ?>>表示する</option>

            <option value="2"<?php if($view5 == 2){echo ' selected="selected"';} ?>>表示しない</option>

        </select></label></p>



    <?php if( $is_fudoubus ) { ?>

        <p><label for="<?php echo $this->get_field_id('view6'); ?>">

        バス路線 <select class="widefat" id="<?php echo $this->get_field_id('view6'); ?>" name="<?php echo $this->get_field_name('view6'); ?>">

            <option value="2"<?php if($view6 == 2){echo ' selected="selected"';} ?>>表示しない</option>

            <option value="1"<?php if($view6 == 1){echo ' selected="selected"';} ?>>表示する</option>

        </select></label></p>

    <?php } ?>





    <?php if($is_fudoukaiin && get_option('kaiin_users_can_register') == '1' ){ ?>

        <p><label for="<?php echo $this->get_field_id('kaiinview'); ?>">

        会員物件 <select class="widefat" id="<?php echo $this->get_field_id('kaiinview'); ?>" name="<?php echo $this->get_field_name('kaiinview'); ?>">

            <option value="1"<?php if($kaiinview == 1){echo ' selected="selected"';} ?>>会員・一般物件を表示する</option>

            <option value="2"<?php if($kaiinview == 2){echo ' selected="selected"';} ?>>会員物件を表示しない</option>

            <option value="3"<?php if($kaiinview == 3){echo ' selected="selected"';} ?>>会員物件だけ表示</option>

        </select></label></p>

    <?php } ?>


        <?php

    }





    /** @see WP_Widget::update */

    function update($new_instance, $old_instance) {

        return $new_instance;

    }



    /** @see WP_Widget::widget */

    function widget($args, $instance) {



        global $is_fudoukaiin,$is_fudoubus;

        global $wpdb;



        $img_path = get_option('upload_path');

        if ( empty( $img_path ) ){

            $img_path = 'wp-content/uploads';

        }

        //WordPressのアドレス(URL)

        $img_folder_url = site_url( '/' ) . $img_path;





        // outputs the content of the widget



            extract( $args );



        $title = isset($instance['title']) ? apply_filters('widget_title', $instance['title']) : '';

        $item =       isset($instance['item']) ? $instance['item']: 4;

        $shubetsu =   isset($instance['shubetsu']) ? $instance['shubetsu']: '';

        $bukken_cat = isset($instance['bukken_cat']) ? $instance['bukken_cat']: '';



        $sort =  isset($instance['sort'])  ? $instance['sort'] : '';

        $view1 = isset($instance['view1']) ? $instance['view1']: '';

        $view2 = isset($instance['view2']) ? $instance['view2']: '';

        $view3 = isset($instance['view3']) ? $instance['view3']: '';

        $view4 = isset($instance['view4']) ? $instance['view4']: '';

        $view5 = isset($instance['view5']) ? $instance['view5']: '';

        $view6 = isset($instance['view6']) ? $instance['view6']: '';    //バス路線



        $kaiinview = isset($instance['kaiinview']) ? $instance['kaiinview'] : 1 ;



        if( !$is_fudoukaiin || get_option('kaiin_users_can_register') != '1' ){

            $kaiinview = 1;

        }



        //表示数

        if($item =="")     $item=4;



        //NEW/UP

        $newup_mark = get_option('newup_mark');

        if($newup_mark == '') $newup_mark=14;



        $where_data = "";

        //種別
        switch ($shubetsu) {

            case 1 :    //物件すべて

                break;

            case 2 :    //売買すべて

                $where_data = " AND CAST( PM_1.meta_value AS SIGNED )<3000";

                break;

            case 3 :    //売買土地

                $where_data = " AND Left(PM_1.meta_value,2) ='11'";

                break;

            case 4 :    //売買戸建

                $where_data = " AND Left(PM_1.meta_value,2) ='12'";

                break;

            case 5 :    //売買マンション

                $where_data = " AND Left(PM_1.meta_value,2) ='13'";

                break;

            case 6 :    //売住宅以外の建物全部

                $where_data = " AND Left(PM_1.meta_value,2) ='14'";

                break;

            case 7 :    //売住宅以外の建物一部

                $where_data = " AND Left(PM_1.meta_value,2) ='15'";

                break;



            case 10 :    //賃貸すべて

                $where_data = " AND  CAST( PM_1.meta_value AS SIGNED )>3000";

                break;

            case 11 :    //賃貸居住用

                $where_data = " AND Left(PM_1.meta_value,2) ='31'";

                break;

            case 12 :    //賃貸事業用

                $where_data = " AND Left(PM_1.meta_value,2) ='32'";

                break;



            default:

                $order_data = "";

                break;

        }





        //sort

        switch ($sort) {

            case 1 :    //ランダム

                $order_data = " rand()";

                break;

            case 2 :    //新しい順 (登録日)

                $order_data = " P.post_date DESC";

                break;

            case 3 :    //古い順 (登録日)

                $order_data = " P.post_date ASC";

                break;

            case 4 :    //高い順

                $order_data = " CAST( PM_2.meta_value AS SIGNED ) DESC";

                break;

            case 5 :    //安い順

                $order_data = " CAST( PM_2.meta_value AS SIGNED ) ASC";

                break;

            case 6 :    //広い順

                $order_data = " CAST( PM_3.meta_value AS SIGNED ) DESC";

                break;

            case 7 :    //狭い順

                $order_data = " CAST( PM_3.meta_value AS SIGNED ) ASC";

                break;

            case 8 :    //新しい順 (更新日)

                $order_data = " P.post_modified DESC";

                break;

            case 9 :    //古い順 (更新日)

                $order_data = " P.post_modified ASC";

                break;



            default:

                $order_data = " rand()";

                break;

        }






        //SQL

        $sql = "";

        $sql = $sql . "SELECT DISTINCT P.ID,P.post_title,P.post_modified,P.post_date";

        $sql = $sql . " FROM ((((((($wpdb->posts AS P";

        $sql = $sql . " INNER JOIN $wpdb->postmeta AS PM   ON P.ID = PM.post_id) ";

        //種別

        if( $shubetsu != 1 ){

            $sql = $sql . " INNER JOIN $wpdb->postmeta AS PM_1 ON P.ID = PM_1.post_id) ";

        }else{

            $sql = $sql . " )";

        }



        //価格(sort)

        if( $sort == 4 || $sort == 5 ){

            $sql = $sql . " INNER JOIN $wpdb->postmeta AS PM_2 ON P.ID = PM_2.post_id) ";

        }else{

            $sql = $sql . " )";

        }



        //面積(sort)

        if( $sort == 6 || $sort == 7 ){

            $sql = $sql . " INNER JOIN $wpdb->postmeta AS PM_3 ON P.ID = PM_3.post_id) ";

        }else{

            $sql = $sql . " )";

        }



        //会員

        if( $kaiinview != 1 ){

            $sql = $sql . " INNER JOIN $wpdb->postmeta AS PM_4 ON P.ID = PM_4.post_id) ";

        }else{

            $sql = $sql . " )";

        }


        /**
         * SQLに部屋の満室状況判断を加える
         * bukken_status
         * 1:空室あり
         * 2:満室
         */

        $sql = $sql . " INNER JOIN $wpdb->postmeta AS PM_999 ON P.ID = PM_999.post_id ";



        //カテゴリ

        if( $bukken_cat > 0){

            $sql = $sql . " INNER JOIN $wpdb->term_relationships AS TR ON P.ID = TR.object_id) ";

            $sql = $sql . " INNER JOIN $wpdb->term_taxonomy AS TT ON TR.term_taxonomy_id = TT.term_taxonomy_id) ";

        }else{

            $sql = $sql . " ))";

        }





        /*

         * トップ物件表示 org追加SQL条件 INNER JOIN

         *

         * Version: 1.7.4

         */

        $sql =  apply_filters( 'fudo_top_widget_inner_join_data', $sql, $kaiinview );





            $sql = $sql . " WHERE";

            $sql = $sql . " P.post_status='publish' AND P.post_password = '' AND (P.post_type ='fudo')";
            // $sql = $sql . " P.post_status='publish' AND P.post_password = '' AND (P.post_type ='fudo' OR P.post_type ='room')";


        //種別

        if( $shubetsu != 1 )

            $sql = $sql . " AND PM_1.meta_key='bukkenshubetsu'";



        //価格(sort)

        if( $sort == 4 || $sort == 5 )

            $sql = $sql . " AND PM_2.meta_key='kakaku'";



        //面積(sort)

        if( $sort == 6 || $sort == 7 ){

            $sql = $sql . " AND (PM_3.meta_key='tatemonomenseki' or PM_3.meta_key='tochikukaku' )";

            $sql = $sql . " AND PM_3.meta_value !='' ";

        }



        //会員物件を表示しない

        if( $kaiinview == 2 ){

            $sql = $sql . " AND PM_4.meta_key='kaiin' AND PM_4.meta_value < 1 ";

        }



        //会員物件だけ表示

        if( $kaiinview == 3 ){

            $sql = $sql . " AND PM_4.meta_key='kaiin' AND PM_4.meta_value > 0 ";

        }



        //画像 2020.12.26 画像を条件にするとスクレイピングしたデータが出ないので、条件削除
        // $sql = $sql . " AND (PM.meta_key='fudoimg1' Or PM.meta_key='fudoimg2') ";

        // $sql = $sql . " AND PM.meta_value !='' ";


        //満室フラグ

        $sql = $sql . " AND (PM_999.meta_key='bukken_status' AND PM_999.meta_value='1') ";



        //カテゴリ

        if( $bukken_cat > 0)

            $sql = $sql . " AND TT.term_id = ".$bukken_cat."";



        /*

         * トップ物件表示 org追加SQL条件 WHERE

         *

         * Version: 1.7.4

         */

        $sql =  apply_filters( 'fudo_top_widget_where_data', $sql, $kaiinview );



//--　ルームを追加する（ここではタイトルだけ）misawa
// $where_data=$where_data." OR (P.post_type ='room' AND P.post_status='publish')";


        $sql = $sql . $where_data;

        $sql = $sql . " ORDER BY ".$order_data." limit ".$item."";

// print_r($sql);
    //    $sql = $wpdb->prepare($sql,'');
        $metas = $wpdb->get_results( $sql, ARRAY_A );





        //ユーザー別会員物件リスト

        $kaiin_users_rains_register = get_option('kaiin_users_rains_register');



        //物件詳細リンク

        if ( defined('FUDOU_BUKKEN_TARGET') && FUDOU_BUKKEN_TARGET== 1 ){

            $target_link = ' target="_blank"  rel="noopener noreferrer"';

        }else{

            $target_link = '';

        }




        if( !empty( $metas ) ){



            // echo $before_widget;



            if ( $title ){

                // echo $before_title . $title . $after_title;

            }


            // echo '<div id="box'.$args['widget_id'].'">';

            // echo '<ul id="'.$args['widget_id'].'_1" class="grid-content">';

            echo '<div class="o-room __list flex-w">';


            //grid_count css class

            $grid_count = 1;

            $fudoimg_url = "";

            foreach ( $metas as $meta ) {



                $rosen_bus = false;    //路線を「バス」に設定している?



                $post_id    = $meta['ID'];


                $post_title    = $meta['post_title'];

                $post_url    = str_replace('&p=','&amp;p=',get_permalink($post_id));



                //newup_mark

                $post_modified =  $meta['post_modified'];

                $post_date =   $meta['post_date'];

                $post_modified_date =  vsprintf("%d-%02d-%02d", sscanf($post_modified, "%d-%d-%d"));

                $post_date =  vsprintf("%d-%02d-%02d", sscanf($post_date, "%d-%d-%d"));



                $newup_mark_img=  '';

                if( $newup_mark != 0 && is_numeric($newup_mark) ){

                    if( ( abs(strtotime($post_modified_date) - strtotime(date("Y/m/d"))) / (60 * 60 * 24) ) < $newup_mark ){

                        if($post_modified_date == $post_date ){

                            $newup_mark_img = '<div class="new_mark">new</div>';

                        }else{

                            $newup_mark_img = '<div class="new_mark">up</div>';

                        }

                    }

                }

//----------room用の情報取得--------------------------------------------

//--room用に全情報を集める
$rooms_get_post=get_post($post_id);

$get_post_meta=get_post_meta($post_id);
//echo "<pre>".var_dump($get_post_meta)."</pre>";

//--roomに関連の親情報を集める
$args = array(
    'connected_type' => 'room_relation',
    'connected_items' => $post_id
);
$connected_parent = get_posts($args);

//---------------------------------------------------------------------



                //会員2

                $kaiin = 0;

                if( !is_user_logged_in() && get_post_meta($post_id, 'kaiin', true) > 0 ) $kaiin = 1;

                //ユーザー別会員物件リスト

                $kaiin2 = users_kaiin_bukkenlist( $post_id, $kaiin_users_rains_register, get_post_meta( $post_id, 'kaiin', true ) );





                global $fvpids;
                $fvflg = ( in_array($post_id,$fvpids) ) ? true : false;
                $sumiflg = ( $fvflg ) ? 'sumi' : 'mi';
                $fvicon = ( $fvflg ) ? 'flaticon-heart __checked' : 'flaticon-heart';
                // echo '<li class="'.@$args['widget_id'].' box1 grid_count'. $grid_count . '">';
                echo '<section class="o-roomItem">';

                echo '<a href="' . home_url() . "/fudo/" . $post_id . '/?wpfpaction=add&postid=' . $post_id . '" class="o-roomSave" data-fvflg="' . $sumiflg .'" data-pid="' . $post_id .'"><i class="o-icon ' . $fvicon . '"></i></a>';


                //grid_count css class

                $grid_count++;

                if( $grid_count > 4 ){

                    $grid_count = 1;

                }



            // echo $newup_mark_img;//--「up」表示


                /*

                 * トップ物件表示 追加項目

                 *

                 * Version: 1.7.12

                 */

                do_action( 'fodou_top_bukken0', $post_id, $kaiin, $kaiin2 );


// echo '<div class="right_box">';
                //タイトル

            $oya_title="";
            if($rooms_get_post->post_type=="room"){

            //--roomに関連の親情報を集める
            $args = array(
                'connected_type' => 'room_relation',
                'connected_items' => $post_id
            );
            $connected_parent = get_posts($args);
            $oya_title=$connected_parent[0]->post_title."／";
            }





                do_action( 'fodou_top_bukken1', $post_id, $kaiin, $kaiin2 );



                //会員2

                if ( !my_custom_kaiin_view( 'kaiin_kakaku',$kaiin,$kaiin2 )

                    && !my_custom_kaiin_view( 'kaiin_madori', $kaiin, $kaiin2 )

                    && !my_custom_kaiin_view( 'kaiin_menseki', $kaiin, $kaiin2 )

                    && !my_custom_kaiin_view( 'kaiin_shozaichi', $kaiin, $kaiin2 )

                    && !my_custom_kaiin_view( 'kaiin_kotsu', $kaiin, $kaiin2) ){

                    echo '<span class="top_kaiin">※この物件は 会員様限定で公開している物件です</span>';

                }
                do_action( 'fodou_top_bukken2', $post_id, $kaiin, $kaiin2 );



                //価格 v1.7.12

//                 if ( my_custom_kaiin_view('kaiin_kakaku',$kaiin,$kaiin2) ){

// // echo '<p class="bukkenshubetsu">'.xls_custom_bukkenshubetsu_print($post_id).'</p>';

//                         if($view2=="1"){

//                             echo '<p><span class="top_price">';


// if($rooms_get_post->post_type=="room"){
//     $str=$get_post_meta["chinryo"][0];
//     $num = preg_replace('/[^0-9]/', '',$str);
//     $tani=preg_replace('/'.$num.'/','',$str);

//                             echo number_unit($num).$tani;
// }else{

//                             if( get_post_meta($post_id, 'seiyakubi', true) != "" ){

//                                 echo 'ご成約済';

//                             }else{

//                                 //非公開の場合

//                                 if(get_post_meta($post_id,'kakakukoukai',true) == "0"){

//                                     $kakakujoutai_data = get_post_meta($post_id,'kakakujoutai',true);

//                                     if($kakakujoutai_data=="1")    echo '相談';

//                                     if($kakakujoutai_data=="2")    echo '確定';

//                                     if($kakakujoutai_data=="3")    echo '入札';



//                                     //価格状態 v1.9.0

//                                     do_action( 'fudou_add_kakakujoutai', $kakakujoutai_data, $post_id );

//                                     echo '　';

//                                 }else{

//                                     $kakaku_data = get_post_meta($post_id,'kakaku',true);
//                                     $str=$kakaku_data;
//                                     $num = preg_replace('/[^0-9]/', '',$str);
//                                     $tani=preg_replace('/'.$num.'/','',$str);
//                                     echo number_unit($num).$tani;

//                                     /*
//                                     if( is_numeric( $kakaku_data ) ){
//                                         if ( function_exists( 'fudou_money_format_ja' ) ) {
//                                             // Money Format 億円 表示
//                                             echo apply_filters( 'fudou_money_format_ja', $kakaku_data );
//                                         }else{
//                                             echo number_format($kakaku_data);
//                                             echo "円";
//                                         }
//                                     }
//                                     */
//                                 }

//                             }
// }

//                         }

//                 }
//                 echo "</span>";




                do_action( 'fodou_top_bukken3', $post_id, $kaiin, $kaiin2 );





                //間取り・土地面積

                if($view3=="1"){

                    //物件に紐づく部屋を洗い出す関数
                    // echo "<p>" . get_rooms_count($post_id) . "部屋募集中</p>";
                    echo '<p class="o-roomStatus">' . get_rooms_count($post_id) . '部屋募集中</p>';






                //会員項目表示判定

                if ( !my_custom_kaiin_view('kaiin_gazo',$kaiin,$kaiin2) ){


                    echo '<img class="box1image" src="'.WP_PLUGIN_URL.'/fudou/img/kaiin.jpg" alt="" />';

                }else{



        if($rooms_get_post->post_type=="room"){
            // echo '<a href="' . $post_url . '" ' . $target_link . '>';
            // //--room最初の画像をゲット
            //  if ($get_post_meta["_thumbnail_id"][0]!==NULL){
            // echo get_thumb_img('large',$get_post_meta["_thumbnail_id"][0]);
            //  }else{
            //     $room_img=get_field('room_img',$post_id);
            //     echo '<img class="box1image" src="'.$room_img[0]["sizes"]["large"].'" alt=""/>';
            //  }
            // echo "</a>";
        }else{

                    //サムネイル画像

                    // $fudoimg1_data = get_post_meta($post_id, 'fudoimg1', true);

                    // if($fudoimg1_data != '')    $fudoimg_data=$fudoimg1_data;

                    // $fudoimg2_data = get_post_meta($post_id, 'fudoimg2', true);

                    // if($fudoimg2_data != '')    $fudoimg_data=$fudoimg2_data;

                    // $fudoimg_alt = str_replace("　"," ",$post_title);


                    $bukken_gaikan_kyouyou = get_field('bukken_gaikan_kyouyou', $post_id);
                    // print_r($bukken_gaikan_kyouyou[0]['sizes']['large']);
                    if($bukken_gaikan_kyouyou[0]['sizes']['large']){
                        $fudoimg_url = $bukken_gaikan_kyouyou[0]['sizes']['large'];
                    }else{
                        $res_arr = stw_get_singlefudo_images($post_id,2);
                        if ( 0 < count($res_arr) ){
                            $fudoimg_url = stw_generate_imgurl($res_arr[0]->image_path);
                        }else {
                            $fudoimg_url = WP_PLUGIN_URL .'/fudou/img/nowprinting.jpg" alt="" />';
                        }
                    }




                    if( $fudoimg_url !="" ){

                                    echo '<div class="o-roomCont o-roomThumb">';
                                    echo '<img class="o-roomThumb_img" src="' . $fudoimg_url .'" alt="' . $fudoimg_alt . '" title="' . $fudoimg_alt . '" />';

                                                echo '<div class="o-roomSummary">';
                                                echo '<h3 class="o-roomTitle o-title __white">';
                                                echo str_replace("　"," ",$oya_title.$post_title).'';
                                                echo '</h3>';
                                                    echo '<p class="o-roomPrice o-title __en">';
                                                    //賃料
                                                    if(get_field('chinryos', $post_id)){
                                                        the_field('chinryos', $post_id);
                                                    }else{
                                                        echo get_kakaku_list_for_fudo($post_id);
                                                    }
                                                    // echo  '<span class="o-roomPrice_unit">円</span></p>';
                                                    echo '<div class="o-roomSpace flex-nw jc-start ai-center">';
                                                        echo  '<div class="o-roomSpace_meter"><i class="o-roomTag_icon o-icon __min __white flaticon-buildings"></i>';
                                                        //面積
                                                        echo get_senyumenseki_list_for_fudo($post_id);
                                                        echo  '</div>';
                                                        echo '<div class="o-roomSpace_room"><i class="flaticon-blueprint o-icon __min __white"></i>';
                                                        if(get_field('bukken_madori_list', $post_id)){
                                                            the_field('bukken_madori_list', $post_id);
                                                        }else{
                                                            echo get_madori_list_for_fudo($post_id);
                                                        }
                                                        echo '</div>';
                                                    echo '</div>';
                                                echo '</div>';


                                    echo '</div>';






                    }else{
                        // //--room用にサムネイル取得
                        // if($rooms_get_post->post_type!=="room"){

                        // echo '<img class="box1image" src="'.WP_PLUGIN_URL.'/fudou/img/nowprinting.jpg" alt="" />';

                        // }else{

                        // //--room最初の画像をゲット
                        //  if ($get_post_meta["_thumbnail_id"][0]!==NULL){
                        //     echo get_thumb_img('large',$get_post_meta["_thumbnail_id"][0]);
                        //  }else{
                        //     $room_img=get_field('room_img',$post_id);
                        //     //$str=$rooms_get_post->post_content;
                        //     //$room_first_img=catch_that_image2($str);
                        //     echo '<img class="box1image" src="'.$room_img[0]["sizes"]["large"].'" alt=""/>';
                        //  }

                        // }


                    }


        }

                }//アイキャッチブロック



                echo '<div class="o-roomCont o-roomDesc flex-nw jc-between">';
                echo '<div class="o-roomInfo">';
                echo '<p class="o-roomAddress flex-nw jc-between"><span>';
                my_custom_shozaichi_print($post_id);
                echo get_post_meta($post_id, 'shozaichimeisho', true);
                // echo get_my_custom_shozaichi_print($post_id) . get_post_meta($post_id, 'shozaichimeisho', true);
                echo '</span><a href="';
                $map_link = "https://www.google.co.jp/maps/place/" . urlencode(get_my_custom_shozaichi_print($post_id) . get_post_meta($post_id, 'shozaichimeisho', true));
                echo $map_link;
                echo '" class="o-roomMap o-link __tiny">MAPで表示</a> </p>';
                echo '<ul class="o-roomStation">';
                    echo '<li class="o-roomStation_list">';
                    my_custom_koutsu1_print($post_id);
                    echo '</li>';
                    echo '<li class="o-roomStation_list">';
                    my_custom_koutsu2_print($post_id);
                    echo '</li>';
                    echo '<li class="o-roomStation_list">';
                    my_custom_koutsu3_print($post_id);
                    echo '</li>';
                echo '</ul>';
                echo '<div class="o-roomTag flex-w" style="display:none;">';
                echo '<i class="o-roomTag_icon o-icon __min flaticon-tag"></i>';
                    echo '<a href="" class="o-roomTag_link o-link __nocolor">ペット相談可</a>';
                echo '</div>';
            echo '</div>';
            echo '<div class="o-roomBuilding">';
                echo '<ul>';
                    echo '<li class="o-roomBuilding_item">';
                    //築年月
                    $tatemonochikunenn = get_post_meta($post_id, 'tatemonochikunenn', true);
                    if(!empty($tatemonochikunenn)){

                        if(strpos($tatemonochikunenn, '月')){
                            $tatemonochikunenn_data = $tatemonochikunenn . "01";
                            $tatemonochikunenn_data = str_replace(array("年","月"), "/", $tatemonochikunenn_data);
                        }else{
                            $tatemonochikunenn_data = $tatemonochikunenn . "/01";
                        }
                    }

                    // echo "<p>" . $tatemonochikunenn . "</p>";

                    // echo get_sintiku_mark($tatemonochikunenn_data);
                    //築年数
                    echo get_tikunensu($tatemonochikunenn_data);
                    echo '</li>';
                    echo '<li class="o-roomBuilding_item">';
                    //建物構造
                    my_custom_tatemonokozo_print_br($post_id);
                    echo '</li>';

                    //建物階数
                    echo '<li class="o-roomBuilding_item">';
                    if(get_post_meta($post_id, 'tatemonokaisu1', true)!="") echo '地上'.get_post_meta($post_id, 'tatemonokaisu1', true)."階";
                    if(get_post_meta($post_id, 'tatemonokaisu2', true)!="") echo '<br>地下'.get_post_meta($post_id, 'tatemonokaisu2', true)."階";
                    echo '</li>';

                    echo '</ul>';
                            //お問い合わせ
                            echo "<p style='background:#000;'>";
                            echo "<a class='o-roomContact o-btn __basic01' href='" . home_url() . "/" . get_field('kuusitu_inquiry_link', 'option') . "?post_id=" . $post_id . "' title='" .  $target_link . "'>詳細</a></p>";

            echo '</div>';//class="o-roomCont o-roomDesc flex-nw jc-between

                    // echo "<p style='background:#000;'><a href='" . home_url() . "/fudo/" . $post_id . "/?wpfpaction=add&postid=" . $post_id . "'>お気に入りに追加</a></p>";

                    // echo "<p style='background:#000;'><a href='" . $map_link . "' target='_blank'>MAPで表示</a></p>";
                    //間取り

                    if ( my_custom_kaiin_view('kaiin_madori',$kaiin,$kaiin2) ){

                            // echo '<span class="top_madori">';
                            // if($rooms_get_post->post_type=="room"){
                            // //echo $get_post_meta["room_information_0_room_details"][0];
                            // }

                            // $madorisyurui_data = get_post_meta($post_id,'madorisyurui',true);

                            // echo get_post_meta($post_id,'madorisu',true);

                            // if($madorisyurui_data=="10")    echo 'R ';

                            // if($madorisyurui_data=="20")    echo 'K ';

                            // if($madorisyurui_data=="25")    echo 'SK ';

                            // if($madorisyurui_data=="30")    echo 'DK ';

                            // if($madorisyurui_data=="35")    echo 'SDK ';

                            // if($madorisyurui_data=="40")    echo 'LK ';

                            // if($madorisyurui_data=="45")    echo 'SLK ';

                            // if($madorisyurui_data=="50")    echo 'LDK ';

                            // if($madorisyurui_data=="55")    echo 'SLDK ';

                            // echo '</span>';

                            // if(get_field('bukken_madori_list', $post_id)){
                            //     the_field('bukken_madori_list', $post_id);
                            // }else{
                            //     echo get_madori_list_for_fudo($post_id);
                            // }

                    }







                    //建物階数
                    // if(get_post_meta($post_id, 'tatemonokaisu1', true)!="") echo '地上'.get_post_meta($post_id, 'tatemonokaisu1', true)."階";
                    // if(get_post_meta($post_id, 'tatemonokaisu2', true)!="") echo '地下'.get_post_meta($post_id, 'tatemonokaisu2', true)."階";


                    //土地面積

                    // if ( my_custom_kaiin_view('kaiin_menseki',$kaiin,$kaiin2) ){

                    //         echo '<span class="top_menseki">';

                    //         if ( get_post_meta($post_id,'bukkenshubetsu',true) < 1200 ) {

                    //             if( get_post_meta($post_id, 'tochikukaku', true) !="" )

                    //                 echo ' '.get_post_meta($post_id, 'tochikukaku', true).'m&sup2;';

                    //         }
                    //         echo '</span>';

                    // }

                }



echo '</p>';

                do_action( 'fodou_top_bukken4', $post_id, $kaiin, $kaiin2 );




echo '<p>';




                do_action( 'fodou_top_bukken5', $post_id, $kaiin, $kaiin2 );






                do_action( 'fodou_top_bukken6', $post_id, $kaiin, $kaiin2 );





                //交通バス路線

                if ( my_custom_kaiin_view( 'kaiin_kotsu', $kaiin,$kaiin2 ) ){

                        if( $view6=="1" &&  $is_fudoubus ){

/*
                            if( $rosen_bus ){

                                echo ' ';

                            }else{

                                echo '<br />';

                            }
*/
                            echo '<span class="top_kotsubus">';
                            echo apply_filters( 'fudoubus_buscorse_busstop_single', '', $post_id, 1 );
                            echo '</span>';

                        }

                }

// echo '</p>';

//--ルーム用概要（他の物件も含む）
// echo '<p class="gaiyo">'.$rooms_get_post->post_excerpt."</p>";//--概要2

            // echo '</div>';



                do_action( 'fodou_top_bukken7', $post_id, $kaiin, $kaiin2 );



                /*

                echo '<br />更新日:';

                echo $post_modified_date;

                */




                // echo '<div class="under_box">';

                // //会員ロゴ

                // if( get_post_meta( $post_id, 'kaiin', true ) == 1 ) {

                //     $kaiin_logo = '<span class="fudo_kaiin_type_logo"><img src="' . WP_PLUGIN_URL . '/fudou/img/kaiin_s.jpg" alt="会員物件" /></span>';

                //     echo apply_filters( 'fudou_kaiin_logo_view', $kaiin_logo );

                // }
                // do_action( 'fudo_kaiin_type_logo', $post_id );    //会員ロゴ
                // do_action( 'fodou_top_bukken8', $post_id, $kaiin, $kaiin2 );

                // // echo '<span style="float:right;" class="box1low"><a href="' . $post_url . '" ' . $target_link . '>物件詳細</a></span>';



                // echo '</div>';





                do_action( 'fodou_top_bukken9', $post_id, $kaiin, $kaiin2 );


                echo '<a href="' . get_permalink($post_id) . '" class="o-link __over"></a>';
                echo '</section>';
                // echo '</li>';



            }    //loop



            // echo '</ul>';

            // echo '</div>';


            // echo '</section>';

            echo '</div>';

            // echo $after_widget;



            $top_widget_id = str_replace( '-' , '_' ,@$args['widget_id']);



            //jquery.matchHeight.js

            ?>

            <?php if ( $top_widget_id ):?>

            <script type="text/javascript">



                <?php if( apply_filters( 'fudou_imagesloaded_use', true ) ){  ?>

                    imagesLoaded( '#box<?php echo $args['widget_id']; ?>', function(){

                        setTimeout('topbukken<?php echo $top_widget_id; ?>()', 200);

                    });

                <?php }else{ ?>

                    setTimeout('topbukken<?php echo $top_widget_id; ?>()', 1000);

                <?php } ?>



                function topbukken<?php echo $top_widget_id; ?>() {

                    jQuery.noConflict();

                    var jtop$ = jQuery;

                    jtop$(function() {

                        jtop$('#<?php echo $args['widget_id']; ?>_1 > li').matchHeight();



                    });

                }

            </script>
            <?php endif;?>

            <?php



        }





    }

} // Class fudo_widget_top_r
