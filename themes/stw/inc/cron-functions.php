<?php
if ( ! wp_next_scheduled( 'room_manshitu_task' ) ) {
    wp_schedule_event( time(), 'daily', 'room_manshitu_task' );
}
add_action( 'room_manshitu_task', 'stw_cron_room_manshitu' );

/**
 * 満室状態の物件に属する部屋が、満室状態ではなかった場合、これを満室状態にする処理
 */
function stw_cron_room_manshitu() {
    global $wpdb;

    // ログフィアル関連準備
    $logfile = get_template_directory() . '/inc/cron_room_manshitu.log';
    error_log(date_i18n('Y/m/d H:i:s') . ' [info] START' . "\n",3,$logfile);

    // 変数宣言
    $room_ids = array();
    $room_ids_str = '';
    $meta_ids = array();
    $meta_ids_str = '';

    // 満室状態になっている物件に属する部屋のidを取得するSQL
    $target_room_ids_query = <<<EOT
select
    p2p_from
from
    {$wpdb->prefix}p2p
where
    p2p_to IN(
        select
            ID
        from
            {$wpdb->prefix}posts
        where
            post_type = 'fudo'
        and ID IN(
                select
                    post_id
                from
                    {$wpdb->prefix}postmeta
                where
                    meta_key = 'bukken_status'
                and meta_value = '2'
            )
    )
EOT;

    // 満室状態になっている物件に属する部屋のうち、部屋が満室状態になっていないもののmeta_idを返すSQL
    $target_room_meta_ids_query = <<<EOF
select
    meta_id
from
    {$wpdb->prefix}postmeta
where
    post_id IN(%s)
and meta_key = 'bukken_status'
and meta_value = '1'
EOF;

    // 指定meta_idのmeta_valueを2（満室状態）にするSQL
    $update_query = "update ".$wpdb->prefix."postmeta set meta_value = '2' where meta_id IN (%s)";


    // 満室状態になっている物件に属する部屋のidを取得する
    $room_ids_res = $wpdb->get_results($target_room_ids_query);
    //ログ出力
    error_log(date_i18n('Y/m/d H:i:s') . '[info] 物件が満室になっている部屋の数：' . $wpdb->num_rows . '件' . "\n",3,$logfile);

    if ( 0 < $wpdb->num_rows ){

        // 次のSQLパラメータにする為に、取得した部屋idを一次配列に格納し直す。
        foreach ( $room_ids_res as $one_row ){
            $room_ids[] = $one_row->p2p_from;
        }
        // SQLのIN句で使える文字列に変換
        $room_ids_str = implode(",", $room_ids);

        // 満室状態になっている物件に属する部屋のうち、部屋が満室状態になっていないもののmeta_idを取得する
        $target_room_meta_ids_query = str_replace('%s', $room_ids_str, $target_room_meta_ids_query);
        $meta_ids_res = $wpdb->get_results( $target_room_meta_ids_query );
        //ログ出力
        error_log(date_i18n('Y/m/d H:i:s') . '[info] 満室状態になっていない部屋の数：' . $wpdb->num_rows . '件' . "\n",3,$logfile);

        if ( 0 < $wpdb->num_rows ){

            foreach ( $meta_ids_res as $one_row ){
                $meta_ids[] = $one_row->meta_id;
            }
            // SQLのIN句で使える文字列に変換
            $meta_ids_str = implode(',', $meta_ids);
            //ログ出力
            error_log(date_i18n('Y/m/d H:i:s') . '[info] 処理対象meta_id：' . $meta_ids_str . "\n",3,$logfile);

            // 指定meta_idのmeta_valueを2（満室状態）にする
            $update_query = str_replace('%s', $meta_ids_str, $update_query);
            $wpdb->query( $update_query );

        }

    }
    // ログ出力
    error_log(date_i18n('Y/m/d H:i:s') . ' [info] END' . "\n",3,$logfile);

}
