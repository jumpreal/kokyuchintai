<?php
global $wpdb;

$search_result_title = '';

//エリア取得

//複数市区タイトル $ksik_id = $_GET['ksik']; //県市区

$ksik_name = '';
$ksik_id = $_GET['ksik'];
$ksiks = '';
$ksiks_name = array();

if (is_array($ksik_id)) {

    if(count($_GET['ksik'])>1){
        $ksiks = implode(', ', $_GET['ksik']);
        $where = " WHERE narrow_area_id in (" . $ksiks . ") and middle_area_id = 13";
    }else{
        $ksiks = $_GET['ksik'][0];
        $where = " WHERE  middle_area_id = 13 and narrow_area_id=" . $ksiks;
    }

    $sql  = "SELECT narrow_area_name FROM " . $wpdb->prefix . DB_SHIKU_TABLE . " ";

    $sql .= $where;

        //$sql = $wpdb->prepare($sql,'');
        // echo $sql;
        $metas = $wpdb->get_results( $sql, ARRAY_A );

        if(!empty($metas)) {

            foreach ( $metas as $meta ) {

                // $ksik_name .= '' . $meta['narrow_area_name'] .' ';
                $ksiks_name[] = $meta['narrow_area_name'];
            }

            if(count($ksiks_name) < 4){
                $ksik_name = implode('・', $ksiks_name);
            }else{
                $ksik_name = $ksiks_name[0] . "・" . $ksiks_name[1] . "・" . $ksiks_name[2] . " 他";
            }
        }

    $search_result_title = $ksik_name;
}



//駅名取得
$eki_name = '';
$ekis = '';
$eki = array();

if(!empty($_GET['eki'])){
    if(count($_GET['eki'])>1){
        $ekis = implode(', ', $_GET['eki']);
        $where = " WHERE station_id in (" . $ekis . ")";
    }else{
        $ekis = $_GET['eki'][0];
        $where = " WHERE station_id=" . $ekis;
    }

    $sql = "SELECT DISTINCT station_name";

    $sql .=  " FROM " . $wpdb->prefix . DB_EKI_TABLE;

    $sql .=  $where;
    
    $metas = $wpdb->get_results( $sql, ARRAY_A );

    if(!empty($metas)) {

        foreach ( $metas as $meta ) {

            // $eki_name .= '' . $meta['station_name'] .'駅 ';
            $eki[] = $meta['station_name'] .'駅';

        }

        if(count($eki) < 4){
            $eki_name = implode('・', $eki);
        }else{
            $eki_name = $eki[0] . "・" . $eki[1] . "・" . $eki[2] . " 他";
        }

    }

    $search_result_title .= $eki_name;

}


    //間取り

    $madori_id = isset($_GET['mad']) ? myIsNum_f($_GET['mad']) : '';

    $madori = array();
    
    if(!empty($madori_id)) {
        
        $i=0;
        
        foreach($madori_id as $meta_box){
            
            $madori_name = '';
            $madorisu_data = $madori_id[$i];



            //間取りタイプ

            $madorisyurui_data = myRight($madorisu_data,2);



            //2桁対策

            $madorisu_count = mb_strlen( $madorisu_data, 'utf-8');

            //部屋数
            $madori_name .= myLeft( $madorisu_data, $madorisu_count-2 );



            if($madorisyurui_data=="10")    $madori_name .= 'R';

            if($madorisyurui_data=="20")    $madori_name .= 'K';

            if($madorisyurui_data=="25")    $madori_name .= 'SK';

            if($madorisyurui_data=="30")    $madori_name .= 'DK';

            if($madorisyurui_data=="35")    $madori_name .= 'SDK';

            if($madorisyurui_data=="40")    $madori_name .= 'LK';

            if($madorisyurui_data=="45")    $madori_name .= 'SLK';

            if($madorisyurui_data=="50")    $madori_name .= 'LDK';

            if($madorisyurui_data=="55")    $madori_name .= 'SLDK';

            $i++;

            $madori[] = $madori_name;

        }

        if(count($madori) < 4){
            $madori_name = implode('・', $madori);
        }else{
            $madori_name = $madori[0] . "・" . $madori[1] . "・" . $madori[2] . " 他";
        }

        if(!empty($search_result_title)){
            $search_result_title .= "｜" . $madori_name;
        }else{
            $search_result_title .= $madori_name;
        }

    }


    //価格の安い順（so が null　かつ ord がnull）
    if( (!isset($_GET['so']) && !isset($_GET['ord'])) || ($_GET['so'] == 'pop' &&  $_GET['ord'] == 'd')){
        $search_result_title .= " の高級賃貸一覧";
    }elseif(  $_GET['so'] == 'kak' &&  $_GET['ord'] == 'a'){
        $search_result_title .= " の安い高級賃貸一覧";
    }elseif( $_GET['so'] == 'tam' &&  $_GET['ord'] == 'd'){
        $search_result_title .= " の広い高級賃貸一覧";
    }else{
        $search_result_title .= " の高級賃貸一覧";
    }

?>