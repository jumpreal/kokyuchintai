<?php

/**
 * ajax送信先URLを、jsのグローバル変数として保持しておく。
 */
function add_stw_support_ajaxurl() {
    ?>
    <script>
        var ajaxurl = '<?php echo admin_url( 'admin-ajax.php'); ?>';
    </script>
<?php
}
add_action( 'wp_head', 'add_stw_support_ajaxurl', 1 );

/**
 * 検索で市区町村データを取得するajax
 */
function stw_sikutyouson_json() {
    global $wpdb;
    // リクエスト値取得
    $shu_data          = $_POST['shu'];
    $shozaichiken_data = $_POST['ken'];
    if($shu_data == '2') $shu_data = '> 3000' ;
    //市区
    $sql  =  " SELECT DISTINCT NA.narrow_area_name, CAST( RIGHT(LEFT(PM.meta_value,5),3) AS SIGNED ) AS narrow_area_id";
    $sql .=  " FROM ((($wpdb->posts AS P";
    $sql .=  " INNER JOIN $wpdb->postmeta AS PM   ON P.ID = PM.post_id) ";
    $sql .=  " INNER JOIN $wpdb->postmeta AS PM_1 ON P.ID = PM_1.post_id) ";
    $sql .=  " INNER JOIN " . $wpdb->prefix . DB_SHIKU_TABLE . " AS NA ON CAST( RIGHT(LEFT(PM.meta_value,5),3) AS SIGNED ) = NA.narrow_area_id)";

    //検索 SQL 表示制限 INNER JOIN
    $sql .=  apply_filters( 'inc_archive_kensaku_sql_inner_join', '' );

    $sql .=  " WHERE PM.meta_key='shozaichicode' ";
    $sql .=  " AND P.post_status='publish' AND P.post_password = '' AND P.post_type ='fudo'";
    $sql .=  " AND PM_1.meta_key='bukkenshubetsu'";
    $sql .=  " AND CAST( PM_1.meta_value AS SIGNED ) ".$shu_data."";
    $sql .=  " AND CAST( LEFT(PM.meta_value,2) AS SIGNED ) = ". $shozaichiken_data;
    $sql .=  " AND NA.middle_area_id = ". $shozaichiken_data;

    //検索 SQL 表示制限 WHERE
    $sql .=  apply_filters( 'inc_archive_kensaku_sql_where', '' );

    $sql .=  " ORDER BY CAST( PM.meta_value AS SIGNED )";
    //$sql = $wpdb->prepare($sql,'');
    $metas = $wpdb->get_results( $sql,  ARRAY_A );

    $rstCount = 0;
    $GetDat = '';
    $result_arr = array();
    if(!empty($metas)) {
        foreach ( $metas as $meta ) {
            $result_arr[] = array('id'=>$meta['narrow_area_id'], 'name'=>$meta['narrow_area_name']);
        }
    }

    echo json_encode($result_arr,JSON_UNESCAPED_UNICODE);

    die();
}
add_action( 'wp_ajax_stw_sikutyouson_json', 'stw_sikutyouson_json' );
add_action( 'wp_ajax_nopriv_stw_sikutyouson_json', 'stw_sikutyouson_json' );

/**
 * 検索で路線・駅情報を取得するajax
 */
function stw_roseneki_json() {
    global $wpdb;
    $res_arr = array();
    $sql = <<<EOT
select
    ros.rosen_id,
    ros.rosen_name,
    sta.station_id,
    sta.station_name
from
    (
        SELECT distinct
            PM_ros.meta_value as rosen_id,
            PM_eki.meta_value as station_id
        FROM
            kctwp_posts P
            LEFT JOIN
                kctwp_postmeta PM_roomfloor
            on  P.ID = PM_roomfloor.post_id
            LEFT JOIN
                kctwp_postmeta PM_roomno
            on  P.ID = PM_roomno.post_id
            LEFT JOIN
                kctwp_postmeta PM_senyu
            on  P.ID = PM_senyu.post_id
            LEFT JOIN
                kctwp_postmeta PM_heyamuki
            on  P.ID = PM_heyamuki.post_id
            LEFT JOIN
                kctwp_postmeta PM_madorisu
            on  P.ID = PM_madorisu.post_id
            LEFT JOIN
                kctwp_postmeta PM_madorisyurui
            on  P.ID = PM_madorisyurui.post_id
            LEFT JOIN
                kctwp_postmeta PM_eki
            on  P.ID = PM_eki.post_id
            LEFT JOIN
                kctwp_postmeta PM_ros
            on  P.ID = PM_ros.post_id
        WHERE
            P.post_type = 'room'
        AND P.post_status = 'publish'
        AND PM_roomfloor.meta_key = 'room_floor'
        AND PM_roomno.meta_key = 'room_no'
        AND PM_senyu.meta_key = 'senyumenseki'
        AND PM_heyamuki.meta_key = 'heyamuki'
        AND PM_madorisu.meta_key = 'madorisu'
        AND PM_madorisyurui.meta_key = 'madorisyurui'
        AND PM_eki.meta_key like 'koutsueki%'
        AND PM_eki.meta_value is not null
        AND PM_ros.meta_key like 'koutsurosen%'
        AND PM_ros.meta_value is not null
    ) pmeta
    inner join
        kctwp_train_station sta
    on  pmeta.rosen_id = sta.rosen_id
    and pmeta.station_id = sta.station_id
    inner join
        kctwp_train_rosen ros
    on  sta.rosen_id = ros.rosen_id
order by
    ros.rosen_id,
    sta.station_ranking
EOT;

    $res = $wpdb->get_results($sql,ARRAY_A);
    $ros_id = 0;
    $kari_arr = array();
    $eki_arr = array();
    foreach ( $res as $i => $one ){
        // 前周回の路線idと違ったら、$kari_arrに路線情報を入れる。
        if ( $ros_id != $one['rosen_id'] ){
            // このループ初回のみ
            if ( 0 == count($kari_arr) && 0 == count($eki_arr) ){
                $kari_arr = array(
                    'rosid' => $one['rosen_id'],
                    'rosname' => $one['rosen_name'],
                );
            // 初回以降
            } else {
                // $kari_arrも$eki_arrも存在する場合は、$kari_arrに$eki_arrを追加したものを、$res_arrに追加する。
                $kari_arr['ekis'] = $eki_arr;
                $res_arr[] = $kari_arr;
                // 追加しおわったら、今の周回分のデータを$kari_arrで上書きし、$eki_arrを初期化
                $kari_arr = array(
                'rosid' => $one['rosen_id'],
                'rosname' => $one['rosen_name'],
                );
                $eki_arr = array();
            }
        }
        $eki_arr[] = array('ekiid'=> $one['station_id'], 'ekiname'=> $one['station_name']);
        $ros_id = $one['rosen_id'];
    }

    /*
    if ( 0 !== count($res) ){
        $ekiarr = array();
        foreach ( $ekis as $ekione ){
            $ekiarr[] = array('ekiid'=> $ekione['station_id'], 'ekiname'=> $ekione['station_name']);
        }
        $res_arr[] = array(
            'rosid' => $rosone['rosen_id'],
            'rosname' => $rosone['rosen_name'],
            'ekis' => $ekiarr
        );

    }*/

    echo json_encode($res_arr,JSON_UNESCAPED_UNICODE);

    die();
}
add_action( 'wp_ajax_stw_roseneki_json', 'stw_roseneki_json' );
add_action( 'wp_ajax_nopriv_stw_roseneki_json', 'stw_roseneki_json' );

/**
 * 検索条件：入居可能日の一覧取得
 * 年月と上中下旬のデータを現在から1年後まで生成して配列で返す。
 * post_meta key:nyukyosyun には、1,2,3という数値が入る。それぞれ上旬中旬下旬のデータ。
 * フォームで送る際に年月の後にカンマ区切りでこの値も渡す想定。
 * @return array 'val'=>Y/m,1, 'txt'=>Y年m月 上旬 形式の2要素の配列を、1年分格納した配列
 */
function str_get_nyukyokanoubi() {
    // 現在年月
    $nyukyokanou_arr = array();
    $nyukyokanou_arr[] = array( 'val'=>date('Y/m,1',strtotime('now')), 'txt'=>date('Y年m月 上旬',strtotime('now')) );
    $nyukyokanou_arr[] = array( 'val'=>date('Y/m,2',strtotime('now')), 'txt'=>date('Y年m月 中旬',strtotime('now')) );
    $nyukyokanou_arr[] = array( 'val'=>date('Y/m,3',strtotime('now')), 'txt'=>date('Y年m月 下旬',strtotime('now')) );
    for ( $i=1; $i<=12; $i++){
        $nyukyokanou_arr[] = array( 'val'=>date('Y/m,1',strtotime(date('Y-m-01').'+'.$i.'month')), 'txt'=>date('Y年m月 上旬',strtotime(date('Y-m-01').'+'.$i.'month')) );
        $nyukyokanou_arr[] = array( 'val'=>date('Y/m,2',strtotime(date('Y-m-01').'+'.$i.'month')), 'txt'=>date('Y年m月 中旬',strtotime(date('Y-m-01').'+'.$i.'month')) );
        $nyukyokanou_arr[] = array( 'val'=>date('Y/m,3',strtotime(date('Y-m-01').'+'.$i.'month')), 'txt'=>date('Y年m月 下旬',strtotime(date('Y-m-01').'+'.$i.'month')) );
    }
    return $nyukyokanou_arr;
}

/**
 * エリアidからエリア名を取得する関数
 * 検索後のGETパラメータから、検索したエリア名を取得する際に使用。
 * @param string|array $area_id
 * @return array エリア名が入った一次配列
 */
function str_getareaname( $area_id ){
    global $wpdb;

    $sql_val = '';
    if ( is_array($area_id) ){
        $sql_val = implode(',', $area_id);
    } else {
        $sql_val = $area_id;
    }
    $sql = "select narrow_area_name from " . $wpdb->prefix . "area_narrow_area where narrow_area_id in (" . $sql_val . ") and middle_area_id = 13";
    $area_names = $wpdb->get_results($sql, ARRAY_A);
    return $area_names;
}


/**
 * 駅idから路線名駅名を取得する。
 * 検索後のGETパラメータから、検索した駅名を取得する際に使用。
 * @param string|array $eki_id
 * @return array 駅名が入った一次配列
 */
function stw_getekiname( $eki_id ){
    global $wpdb;

    $sql_val = '';
    if ( is_array($eki_id) ){
        $sql_val = implode(',', $eki_id);
    } else {
        $sql_val = $eki_id;
    }
    $sql = "select CONCAT(r.rosen_name,s.station_name) as roseneki from " . $wpdb->prefix . "train_station s inner join " . $wpdb->prefix . "train_rosen r on s.rosen_id = r.rosen_id where s.middle_area_id = 13 and s.station_id in (" . $sql_val .")";
    $roseneki_names = $wpdb->get_results($sql, ARRAY_A);
    return $roseneki_names;
}