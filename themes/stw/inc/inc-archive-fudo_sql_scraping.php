<?php

/**

* The Template for displaying fudou archive posts.

*

* @package WordPress4.9

* @subpackage Fudousan Plugin

* Version: 1.9.2

*/
global $wpdb;
/**
 * 検索のGETパラメータ取得
 */
// 賃料下限
$kalc = ( isset($_GET['kalc']) ) ? $_GET['kalc'] : null;
$kalc = ( !is_null($kalc) && 'NaN' == $kalc ) ? '0' : $kalc; // NaNの時0にする
$kalc = ( !is_null($kalc) && is_numeric($kalc) ) ? $kalc : null; // 文字列が入ってきたらnullにする

//賃料上限
$kahc = ( isset($_GET['kahc']) ) ? $_GET['kahc'] : null;
$kahc = ( !is_null($kahc) && 'NaN' == $kahc ) ? '100' : $kahc; // NaNの時100にする
$kahc = ( !is_null($kahc) && is_numeric($kahc) ) ? $kahc : null; // 文字列が入ってきたらnullにする

//駅徒歩
$hof = ( isset($_GET['hof']) ) ? $_GET['hof'] : null;
$hof = ( !is_null($hof) && is_numeric($hof) ) ? $hof : null; // 文字列が入ってきたらnullにする
$hof = ( '0' == $hof ) ? null: $hof; // 初期値0の場合は条件を設定していないものとする

//面積 最小 最大
$mel = ( isset($_GET['mel']) ) ? $_GET['mel'] : null; // 最小
$mel = ( !is_null($mel) && is_numeric($mel) ) ? $mel : null; // 文字列が入ってきたらnullにする
$meh = ( isset($_GET['meh']) ) ? $_GET['meh'] : null; // 最大
$meh = ( !is_null($meh) && is_numeric($meh) ) ? $meh : null; // 文字列が入ってきたらnullにする
// 面積のパラメータバリデーション
// 両方とも初期値なら条件指定していないとみなす。
if ( '0' == $mel && '0' == $meh ){ $mel = null; $meh = null; }
// 最小値のみ指定、最大値デフォルトだったら、最大値に1000を入れる。
if ( !is_null($mel) && '0' == $meh ) $meh = '1000';
// 最大が最小より小さければ無効とみなす。
if ( $meh < $mel ) { $mel = null; $meh = null; }

// エリア
$ksik = ( isset($_GET['ksik']) && is_array($_GET['ksik']) ) ? $_GET['ksik'] : null;
$ksik_data = array(); // 実際にSQLのwhere句パラメータとして使う文字列
if ( !is_null($ksik) ){
    foreach ($ksik as $ok ){
        if ( myIsNum_f($ok) ){
            // stwは東京都のみなので、post_meta の key:shozaichicode 形式に合わせて、最初に県コード13（東京都）を追加する
            $ksik_data[] = "'13". $ok . "000000'";
        } else {
            // 整数以外が入っていたら、条件とはしない。
            $ksik = null;
            break;
        }
    }
}
if ( !is_null($ksik) && 0 < count($ksik_data) ){
    // where句のINで使う文字列に変換
    $ksik_data = implode( ',', $ksik_data );
}

// 駅
$eki = ( isset($_GET['eki']) && is_array($_GET['eki']) ) ? $_GET['eki'] : null;
$eki_data = array(); // 実際にSQLのwhere句パラメータとして使う文字列
if ( !is_null($eki) ){
    foreach ($eki as $oe ){
        if ( myIsNum_f($oe) ){
            $eki_data[] = "'$oe'";
        } else {
            // 整数以外が入っていたら、条件とはしない。
            $eki = null;
            break;
        }
    }
}
if ( !is_null($eki) && 0 < count($eki_data) ){
    // where句のINで使う文字列に変換
    $eki_data = implode( ',', $eki_data );
}

// 入居可能日
$nyukyo = ( isset($_GET['nyukyo']) && '2' == $_GET['nyukyo'] ) ? $_GET['nyukyo'] : null; // 2の場合のみ即入居可で検索されることになる。

// 設備
$set_all_arr = array();
$set_arr = array(); // setで受け取った値を一時格納する配列
$set2_arr = array(); // set2で受け取った値を一時格納する配列
$koda_arr = array(); // kodaで受け取った値を一時格納する配列
$set = ( isset($_GET['set']) && is_array($_GET['set']) ) ? $_GET['set'] : null;
$set2 = ( isset($_GET['set2']) && is_array($_GET['set2']) ) ? $_GET['set2'] : null;
$koda = ( isset($_GET['koda']) && is_array($_GET['koda']) ) ? $_GET['koda'] : null;
if ( !is_null($set) ){
    foreach ( $set as $os ){
        if ( is_numeric($os) ) {
            $set_arr[] = "'%$os%'"; // LIKEで部分一致できるようにする。
        } else {
            $set_arr = array(); // GETパラメータに1つでも数値以外のものが入っていたら、条件として扱わない。
            break;
        }
    }
}
if ( !is_null($set2) ){
    foreach ( $set2 as $os ){
        if ( is_numeric($os) ) {
            $set2_arr[] = "'%$os%'"; // LIKEで部分一致できるようにする。
        } else {
            $set2_arr = array(); // GETパラメータに1つでも数値以外のものが入っていたら、条件として扱わない。
            break;
        }
    }
}
if ( !is_null($koda) ){
    foreach ( $koda as $ok ){
        if ( is_numeric($ok) ) {
            $koda_arr[] = "'%$ok%'"; // LIKEで部分一致できるようにする。
        } else {
            $koda_arr = array(); // GETパラメータに1つでも数値以外のものが入っていたら、条件として扱わない。
            break;
        }
    }
}
$set_all_arr = array_merge($set_arr,$set2_arr,$koda_arr); // 値格納した3配列を結合して1つに。

// 間取り
$mad_arr = array(); // madorisuとmadorisyuruiを分けて格納するためのもの。
$mad = ( isset($_GET['mad']) && is_array($_GET['mad']) ) ? $_GET['mad'] : null;
if ( !is_null($mad) ) {
    foreach ( $mad as $om ) {
        if ( is_numeric($om) ) {
            // $omをふたつに割って、madorisuとmadorisyuruiに分けて配列に格納していく。
            $mc = mb_strlen( $om, 'utf-8');
            $madorisu = myLeft( $om, $mc-2 );
            $madorisyurui = myRight( $om, 2);
            // where句で使えるようにSQLで文字列として判別されるように加工しておく。
            $mad_arr[] = array( 'su'=>"'$madorisu'", 'syu'=>"'$madorisyurui'" );
        } else {
            $mad_arr = array(); // 数値以外が入っていたら条件として扱わない。
            break;
        }
    }
}

// ソート種別
$so = ( isset($_GET['so']) && !empty($_GET['so']) ) ? $_GET['so'] : null; // ソート種別
if ( !is_null($so) ){
    // soの値がkak（価格）tam（面積）tac（築年数）以外の場合は条件として扱わない。
    switch ( $so ){
        case 'pop': break;
        case 'kak': break;
        case 'tam': break;
        case 'tac': break;
        default: $so = null;
    }
}

// ソート順
$ord = ( isset($_GET['ord']) && !empty($_GET['ord']) ) ? $_GET['ord'] : null; // ソート順
if ( !is_null($ord) ){
    // ordの値は、a（ASC）かd（DESC）以外は条件として扱わない。
    switch ( $ord ){
        case 'a': $ord = ' ASC ';break;
        case 'd': $ord = ' DESC ';break;
        default: $ord = null;
    }
}


/**
 * SELECT , LEFT JOINのベースを追加。
 */
$sql = "SELECT count(DISTINCT P2P.p2p_to) AS co FROM {$wpdb->prefix}posts P LEFT JOIN {$wpdb->prefix}p2p P2P on P.ID = P2P.p2p_from ";
$sql .= " LEFT JOIN {$wpdb->prefix}postmeta PM on P.ID = PM.post_id "; // 賃料
// $sql .= " LEFT JOIN {$wpdb->prefix}postmeta PM_roomfloor on P.ID = PM_roomfloor.post_id "; // 階数
// $sql .= " LEFT JOIN {$wpdb->prefix}postmeta PM_roomno on P.ID = PM_roomno.post_id "; // 号室
$sql .= " LEFT JOIN {$wpdb->prefix}postmeta PM_senyu on P.ID = PM_senyu.post_id "; // 専有面積
// $sql .= " LEFT JOIN {$wpdb->prefix}postmeta PM_heyamuki on P.ID = PM_heyamuki.post_id "; // 部屋向き
$sql .= " LEFT JOIN {$wpdb->prefix}postmeta PM_madorisu on P.ID = PM_madorisu.post_id "; // 間取り数
$sql .= " LEFT JOIN {$wpdb->prefix}postmeta PM_madorisyurui on P.ID = PM_madorisyurui.post_id "; // 間取り種類



/**
 * 取得したパラメータに応じて、LEFT JOINを行うセクション
 */
// 駅徒歩
if ( !is_null($hof) ){
    $sql .= " LEFT JOIN {$wpdb->prefix}postmeta PM_hof on P.ID = PM_hof.post_id "; // 駅徒歩
}
// エリア
if ( !is_array($ksik_data) ){ // 条件として使えるならば、implodeで配列から文字列になっているはず。
    $sql .= " LEFT JOIN {$wpdb->prefix}postmeta PM_area on P.ID = PM_area.post_id "; // エリア
}
// 駅
if ( !is_array($eki_data) ){ // 条件として使えるならば、implodeで配列から文字列になっているはず。
    $sql .= " LEFT JOIN {$wpdb->prefix}postmeta PM_eki on P.ID = PM_eki.post_id "; // 駅
}
// 入居可能日
if ( !is_null($nyukyo) ){
    $sql .= " LEFT JOIN {$wpdb->prefix}postmeta PM_nyukyo on P.ID = PM_nyukyo.post_id "; // 入居可能日
}
// 設備
if ( 0 < count($set_all_arr) ){
    $sql .= " LEFT JOIN setsubi S on P.ID = S.ID "; // 設備ビュー
}
// ソート種別（築年数）
if ( !is_null($so) && !is_null($ord) && 'tac' == $so ){
    $sql .= " LEFT JOIN {$wpdb->prefix}postmeta PM_tac on P.ID = PM_tac.post_id ";
}
if ( !is_null($so) && !is_null($ord) && 'pop' == $so ){
// 人気順
    $sql .= " LEFT JOIN {$wpdb->prefix}popularpostsdata PM_pop on P.ID = PM_pop.postid ";
}

/**
 * where句のベースを追加。
 */
$sql .= " WHERE P.post_type = 'room' AND P.post_status = 'publish' ";
$sql .= " AND PM.meta_key = 'kakaku' "; // 賃料のmeta_key指定

// $sql .= " AND PM_roomfloor.meta_key = 'room_floor' "; // 階数のmeta_key指定
// $sql .= " AND PM_roomno.meta_key = 'room_no' "; // 号室のmeta_key指定
$sql .= " AND PM_senyu.meta_key = 'senyumenseki' "; // 専有面積のmeta_key指定
// $sql .= " AND PM_heyamuki.meta_key = 'heyamuki' "; // 部屋向きのmeta_key指定
$sql .= " AND PM_madorisu.meta_key = 'madorisu' "; // 間取り数のmeta_key指定
$sql .= " AND PM_madorisyurui.meta_key = 'madorisyurui' "; // 間取り種類のmeta_key指定


/**
 * 取得したパラメータに応じて、WHERE句の追加を行うセクション
 */
// 価格
if ( !is_null($kalc) && !is_null($kahc) ){
    $kalc = intval( $kalc ) * 10000;
    $kahc = intval( $kahc ) * 10000;
    $sql .= " AND PM.meta_value BETWEEN  $kalc AND $kahc ";
}
// 駅徒歩
if ( !is_null($hof) ) {
    $sql .= " AND PM_hof.meta_key like 'koutsutoho%' "; //koutsutoho1f,2f,3fがあるのでそれら全てを指定
    $sql .= " AND PM_hof.meta_value <= $hof ";
}
// 面積
if ( !is_null($mel) && !is_null($meh) ){
    $sql .= " AND PM_senyu.meta_value BETWEEN  $mel AND $meh ";
}
// エリア
if ( !is_array($ksik_data) ){
    $sql .= " AND PM_area.meta_key = 'shozaichicode' ";
    $sql .= " AND PM_area.meta_value IN ($ksik_data) ";
}
// 駅
if ( !is_array($eki_data) ){
    $sql .= " AND PM_eki.meta_key like 'koutsueki%' "; // koutsueki1,2,3があるのでそれら全てを指定
    $sql .= " AND PM_eki.meta_value IN ($eki_data) ";
}
// 入居可能日
if ( !is_null($nyukyo) ){
    $sql .= " AND PM_nyukyo.meta_key = 'nyukyogenkyo' ";
    $sql .= " AND PM_nyukyo.meta_value = '3' "; // 部屋の編集画面、「引渡/入居」「現況」のselectboxの値が該当。内訳:) 11:空室 1:満室 2:居住中 3:即入居可
}
// 設備
if ( 0 < count($set_all_arr) ){
    foreach ( $set_all_arr as $val ){
        $sql .= " AND S.meta_value like $val ";
    }
}
// 間取り数
if ( $maxcon = count($mad_arr) ){
    $sql .= " AND ( ";
    // カウンターを使って制御するのでforにした。
    for ( $i = 0; $i <= ($maxcon-1); $i++ ){
        // 初回以外は最初にORをつける
        if ( 0 != $i ){
            $sql .= " OR ";
        }
        $sql .= " (PM_madorisu.meta_value = {$mad_arr[$i]['su']} AND PM_madorisyurui.meta_value = {$mad_arr[$i]['syu']}) ";
    }
    $sql .= " ) ";
}
// ソート種別（築年数）
if ( !is_null($so) && !is_null($ord) && 'tac' == $so ){
    $sql .= " AND PM_tac.meta_key = 'tatemonochikunenn' ";
}



/**
 * ORDER BY句の追加を行うセクション
 */
if ( !is_null($so) && !is_null($ord) ){
    switch ( $so ){
        case 'pop':
            $sql .= " ORDER BY CAST(PM_pop.pageviews as signed) " . $ord;
            break;        
        case 'kak':
            $sql .= " ORDER BY CAST(PM.meta_value as signed) " . $ord;
            break;
        case 'tam':
            $sql .= " ORDER BY CAST(PM_senyu.meta_value as signed) " . $ord;
            break;
        case 'tac':
            $sql .= " ORDER BY CAST(PM_tac.meta_value as signed) " . $ord;
            break;
    }
} else {
    $sql .= " ORDER BY CAST(PM.meta_value as signed) ASC ";
}

/*
$sql = "SELECT count(DISTINCT P2P.p2p_to) AS co

from kctwp_posts P

left join kctwp_postmeta PM on P.ID = PM.post_id
left join kctwp_postmeta PM2 on P.ID = PM2.post_id
left join kctwp_postmeta PM_madorisu on P.ID = PM_madorisu.post_id
left join kctwp_postmeta PM_madorisyurui on P.ID = PM_madorisyurui.post_id
left join kctwp_postmeta PM_kai on P.ID = PM_kai.post_id
left join kctwp_postmeta PM_source on P.ID = PM_source.post_id
left join setsubi S on P.ID = S.ID
left join kctwp_p2p P2P on P.ID = P2P.p2p_from

where
PM.meta_key = \"kakaku\" and
PM2.meta_key = \"senyumenseki\" and
PM_madorisu.meta_key = \"madorisu\" and
PM_madorisyurui.meta_key = \"madorisyurui\" and
PM_kai.meta_key = \"kai\" and
PM_source.meta_key = \"source\" and
P.post_type = \"room\" and post_status = \"publish\"

order by cast(PM.meta_value as signed) desc";
*/

//上記検索結果の親IDをobject_idとして取得するように置換
    $sql2 = str_replace("SELECT count(DISTINCT P2P.p2p_to) AS co","SELECT DISTINCT CAST(P2P.p2p_to as signed) as object_id",$sql);
    $sql2 .=  " LIMIT ".$limit_from.",".$limit_to."";

if(is_user_logged_in()){
    echo $sql2;
}