<?php
function stw_favorite_ajax() {
    $pid = ( isset($_POST['pid']) && !empty($_POST['pid']) ) ? $_POST['pid'] : null;

    if ( !is_null($pid) ){
        wpfp_do_add_to_list($pid);
    }
    return 'comp';
}
add_action( 'wp_ajax_stw_favorite_ajax', 'stw_favorite_ajax' );
add_action( 'wp_ajax_nopriv_stw_favorite_ajax', 'stw_favorite_ajax' );

function stw_favorite_remove_ajax() {
    $pid = ( isset($_POST['pid']) && !empty($_POST['pid']) ) ? $_POST['pid'] : null;

    if ( !is_null($pid) ){
        wpfp_do_remove_favorite($pid);
    }
    return 'comp';
}
add_action( 'wp_ajax_stw_favorite_remove_ajax', 'stw_favorite_remove_ajax' );
add_action( 'wp_ajax_nopriv_stw_favorite_remove_ajax', 'stw_favorite_remove_ajax' );