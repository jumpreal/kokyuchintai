<?php
/*
Template Name:固定ページ
Template Post Type: post,page
*/
?>
<?php



    /**** 検索 SQL ****/

    //require_once WP_PLUGIN_DIR . '/fudou/inc/inc-archive-fudo.php';

    require_once get_template_directory().'/inc/inc-archive-fudo.php';

  echo "<p>" . __FILE__ . "の" . __LINE__ . "行目</p>";

$kari_sql = <<<EOT

SELECT DISTINCT
    CAST(P2.ID as signed) as object_id
FROM
    ((((
                    (
                        ((((((
                                                (
                                                    wp_posts AS P
                                                    INNER JOIN
                                                        wp_postmeta AS PM
                                                    ON  P.ID = PM.post_id
                                                )
                                                INNER JOIN
                                                    wp_postmeta AS PM_1
                                                ON  P.ID = PM_1.post_id
                                            ))))))
                        INNER JOIN
                            wp_postmeta AS PM_6
                        ON  P.ID = PM_6.post_id
                    )
                    INNER JOIN
                        wp_postmeta AS PM_7
                    ON  P.ID = PM_7.post_id
                ))))
    LEFT JOIN
        wp_postmeta AS PM_99
    on  PM_1.meta_value = PM_99.meta_value
    LEFT JOIN
        wp_postmeta AS PM_999
    on  PM_99.post_id = PM_999.post_id
    LEFT JOIN
        wp_posts AS P2
    on  PM_999.post_id = P2.ID
WHERE
    (
        P.post_status = 'publish'
    AND P.post_password = ''
    AND (
            P.post_type = 'fudo'
        OR  P.post_type = 'room'
        )
    AND PM.meta_key = 'bukkenshubetsu'
    AND CAST(PM.meta_value AS SIGNED) > 3000
    AND PM_1.meta_key = 'shikibesu'
    AND PM_999.meta_key = 'kakaku'
    AND (
            case
                when P.post_type = 'fudo' then P.ID = PM_99.post_id
                else P.ID <> PM_99.post_id
            end
        )
    and P2.post_type = 'fudo'
    AND ((
                PM_6.meta_key = 'madorisu'
            AND PM_6.meta_value = '1'
            AND PM_7.meta_key = 'madorisyurui'
            AND PM_7.meta_value = '50'
            ))
    )
ORDER BY
    CAST(PM_999.meta_value AS SIGNED) ASC
LIMIT 0, 5

EOT;
get_header();
?>
<div id="<?php echo $post->post_name;?>">

    <div class="o-fv __archive" style="background-image:url(
                                       <?php
                                       // アイキャッチ画像が設定されているかチェック
                                       if(has_post_thumbnail()){
                                           // アイキャッチ画像を表示する
                                           the_post_thumbnail_url( get_the_ID(),'full' );
                                       }else{
                                           // 代替画像を表示する
                                           the_field('map-mvimg', 'option');
                                       }
                                       ?>
                                       )">
        <?php the_custom_header_markup(); ?>
        <div class="o-fvCatch">
            <h2 class="o-fvCatch_title o-title __large mb-1">「000」地図検索結果一覧</h2>
            <div class="o-fvCatch_result"><span class="o-fvCatch_num o-text __bold __en">3,329</span>件 見つかりました。</div>
        </div>
        <div class="o-fvBtn flex-nw jc-end ai-center">
            <a href="" class="o-fvBtn_item __save o-btn __basic02"><i class="o-icon __mid flaticon-download"></i>この条件を保存する</a>
            <a id="js-saveModal" class="o-fvBtn_item __term o-btn __basic10"><i class="o-icon __mid __white flaticon-check-mark"></i>保存した条件から探す</a>
        </div>
    </div>

    <div id="primary" class="content-area">
        <main id="main2" class="site-main" role="main">

            <div class="l-main">
                <div class="l-contRoomlist">
                    <div class="l-wrap flex-nw jc-between">
                        <div class="o-search side l-sideSearch l-rowLeft search">
                            <div class="l-sideSearch_wrap">
                            <?php
                            $sc = ( isset($_COOKIE['searchSaveList']) && !empty($_COOKIE['searchSaveList']) ) ? json_decode(stripcslashes($_COOKIE['searchSaveList'])) : null;
                            $sc_con = ( is_null($sc) ) ? 0 : count($sc);
                            ?>
                                <a href="<?php echo get_page_link(910);?>" class="o-searchSave">⭐️<i class="o-icon __min flaticon-check-mark"></i>保存した条件から探す(<span class="searchCount"><?php echo $sc_con;?></span>)</a>
                                <div class="o-searchCont l-wrap">
                                    <div class="o-searchStep area">
                                        <p class="o-searchTitle o-title __mid __withsub">エリア・沿線を選択</p>
                                    </div>
                                    <div class="o-searchStep conditions">
                                        <p class="o-searchTitle o-title __mid __withsub">探したい賃貸の条件を指定</p>
                                    </div>
                                    <div class="o-searchStep popular">
                                        <p class="o-searchTitle o-title __mid __withsub">人気の検索条件から選ぶ</p>
                                    </div>

                                    <?php if ( is_active_sidebar( 'top_widgets' ) ) : ?>
                                    <?php dynamic_sidebar('top_widgets'); ?>
                                    <?php endif; ?>

                                    <div class="o-searchKeyword o-searchStep">
                                        <p class="o-searchTitle o-title __mid __white __withsub">キーワードで検索する</p>
                                        <?php if(is_active_sidebar('widget-1')) : ?>
                                        <ul class="">
                                            <?php dynamic_sidebar('widget-1'); ?>
                                        </ul>
                                        <?php endif; ?>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <div class="l-rowRight searchmain">
                            <section class="l-cont roomlist maplist">

                                <div class="l-contRoomlist_term mb-3">

                                    <div class="l-contRommlist_termWrap __term flex-nw">
                                        <div class="l-contRoomlist_termTitle">こだわり条件</div>
                                        <div class="l-contRoomlist_termDesc">
                                            <div class="l-contRoomlist_termDesc_wrap flex-w">
                                                <span class="l-contRoomlist_termDesc_item"> エリアで指定した内容が並びます。</span>
                                                <span class="l-contRoomlist_termDesc_item"> エリアで指定した内容が並びます。</span>
                                                <span class="l-contRoomlist_termDesc_item"> エリアで指定した内容が並びます。</span>
                                                <span class="l-contRoomlist_termDesc_item"> エリアで指定した内容が並びます。</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <!-- MAP -->

                                <section class="l-contMap">



                                    <?php
                                //loop SQL


                                if($sql !=''){
                                    // echo $sql2;
                                    //$sql2 = $wpdb->prepare($sql2,'');
                                    $marker_data = array();
                                    $metas = $wpdb->get_results( $kari_sql, ARRAY_A );

                                    if(!empty($metas)) {

                                        foreach ( $metas as $meta ) {

                                            $object_id = $meta['object_id'];//post_id
                                            $bukken_mei = get_the_title($object_id);
                                            $bosyu_juuko = get_field('bukken_madori_images', $object_id);
                                            $madori_img = $bosyu_juuko[0]['sizes']['thumbnail'];
                                            if( empty($madori_img) ){
                                                $madori_img = 'http://kokyuchintai-tokyo.com/wp/wp-content/themes/stw/assets/images/common/logo1.png';
                                            }
                                            $syozaiti = get_my_custom_shozaichi_print($object_id) . get_post_meta($object_id, 'shozaichimeisho', true);
                                            $marker_data[] = array('syozaiti' => $syozaiti, 'bukken_mei' => $bukken_mei, 'madori_img' => $madori_img);
                                        }

                                        $marker_data_json = json_encode($marker_data);

                                    }else{

                                        echo "物件がありませんでした。<br /><br />";

                                    }

                                }else{

                                    echo "条件があいませんでした。<br /><br />";

                                }

                                //loop SQL END

                                ?>

                                    <div id="mymap" class="l-contMap_map"></div>

                                </section>
                                <!-- MAP -->




                        </div>
                    </div>
                </div>
            </div>










        </main><!-- #main -->

        <?php

/**
 * 参考URL
 * https://www.webdesignleaves.com/pr/plugins/googlemap_01.html
 * https://qiita.com/SignHack/items/d6df5a5abf72e6feffe9
 * https://www.tam-tam.co.jp/tipsnote/javascript/post7755.html
 */

 ?>
        <script type="text/javascript">
            var geocoder;
            var map;
            var marker = [];
            var infoWindow = [];

            function initMap() {
                geocoder = new google.maps.Geocoder();
                var latlng = new google.maps.LatLng(-34.397, 150.644);
                var mapOptions = {
                    zoom: 8,
                    center: latlng
                }
                map = new google.maps.Map(document.getElementById('mymap'), mapOptions);
                geocodeAddress();
            }

            function geocodeAddress() {
                //ここをDBから取得する。
                var bukken_mei = "";
                var chinryo = "";
                /*
                var markerData = [
                  {syozaiti: '茨城県古河市東三丁目11-29',
                   bukken_mei: 'レーベン古河',
                   chinryo: '150,000'},
                  {syozaiti: '東京都千代田区内幸町１丁目３−１',
                   bukken_mei: '幸ビルヂング',
                   chinryo: '300,000'}
                  ];
                  */
                var markerData = <?php echo $marker_data_json;?>;
                for (var i = 0; i < markerData.length; i++) {
                    // 名前空間でiを保持。geocodeの通信後処理でも、iが使える。
                    (function(i) {
                        geocoder.geocode({
                                'address': markerData[i]['syozaiti']
                            },
                            function(results, status) {
                                if (status == 'OK') {
                                    // マーカー毎の処理
                                    marker[i] = new google.maps.Marker({
                                        map: map,
                                        position: results[0].geometry.location
                                    });

                                    infoWindow[i] = new google.maps.InfoWindow({ // 吹き出しの追加
                                        content: '<div class="sample">' +
                                            '<img src="' + markerData[i]['madori_img'] + '"><br>' +
                                            markerData[i]['bukken_mei'] + '<br>' + // 必ず配列から参照する。forの中で変数代入すると最後の値で固定される。
                                            //markerData[i]['chinryo'] + // 同様に必ず配列から参照する。
                                            '</div>' // 吹き出しに表示する内容
                                    });
                                    // マーカーのクリックイベントの関数登録
                                    google.maps.event.addListener(marker[i], 'click', function(event) {
                                        // 情報ウィンドウの表示
                                        infoWindow[i].open(map, marker[i]);
                                        // 別ウインドウの表示
                                        // showCmd();
                                    });

                                    map.setCenter(results[0].geometry.location);

                                } else {
                                    alert('Geocode was not successful for the following reason: ' + status);
                                }
                            }); // end. geocoder.geocode

                    })(i);

                } //for
            }

            // マーカーにクリックイベントを追加
            function markerEvent(i) {
                marker[i].addListener('click', function() { // マーカーをクリックしたとき
                    infoWindow[i].open(map, marker[i]); // 吹き出しの表示
                });
            }
        </script>

    </div><!-- #primary -->

    <?php
/**
 * 検索フォームを読み込む
 */
//get_template_part('template/searchform-for-map');


?>


</div><!-- .wrap -->

<?php
function add_inc_single_fudo_js2() {
  //GoogleMaps API KEY
  $googlemaps_api_key = get_option('googlemaps_api_key');
  //GoogleMaps API KEY local use map1.7.5
  $googlemaps_api_key_local = get_option('googlemaps_api_key_local');
  $is_local = strpos( home_url() , '/localhost/' );

  wp_deregister_script( 'googlemapapi' );
  if( $googlemaps_api_key && $is_local === false ){
    wp_enqueue_script( 'googlemapapi', 'https://maps.googleapis.com/maps/api/js?key=' . $googlemaps_api_key . '&callback=initMap', array(), '', true );
  }else{
    //GoogleMaps API KEY local use map1.7.5
    if( $googlemaps_api_key && $is_local !== false && $googlemaps_api_key_local ){
      wp_enqueue_script( 'googlemapapi', 'https://maps.googleapis.com/maps/api/js?key=' . $googlemaps_api_key . '&callback=initMap', array(), '', true );
    }else{
      wp_enqueue_script( 'googlemapapi', 'https://maps.googleapis.com/maps/api/js', array(), '', true );
    }
  }
  //For HTTP ang HTTPS
  //$js_url = WP_PLUGIN_URL . '/fudou/js/single-fudoumap.min.js';
  //$js_url = str_replace( 'http:', '', $js_url );
  // $js_url = plugins_url( '/fudou/js/single-fudoumap.min.js' );
  // wp_enqueue_script( 'single-fudo-fudoumap', $js_url, array( 'jquery' ), '1.8.2', true );
}
add_action( 'wp_footer', 'add_inc_single_fudo_js2' );
?>
<?php get_footer('top');?>

<?php include('template/fixednav1.php');?>
