<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress4.0
 * @subpackage Fudousan Plugin
 */

get_header(); ?>


<!--<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_directory'); ?>/assets/css/home.css">-->

<div class="o-fv __top __bigtitle" style="background-image:url(<?php the_field('top-mvimg', 'option'); ?>);">
    <?php the_custom_header_markup(); ?>
    <div class="o-fvCatch">
        <h2 class="o-fvCatch_title o-title __huge __white __nobold mb-2">
            <?php the_field('top-mvtitle', 'option'); ?>
        </h2>
        <div class="o-fvForm">
            <?php include("searchform.php"); ?>
        </div>
    </div>
</div>


<div class="l-main">
    <div class="l-contNewestate __section">
        <div class="l-wrap flex-nw-no-xl jc-between">
            <div class="o-search side l-sideSearch l-rowLeft search">
                <div class="l-sideSearch_wrap">
                <?php
                $sc = ( isset($_COOKIE['searchSaveList']) && !empty($_COOKIE['searchSaveList']) ) ? json_decode(stripcslashes($_COOKIE['searchSaveList'])) : null;
                $sc_con = ( is_null($sc) ) ? 0 : count($sc);
                ?>
                    <a href="<?php echo get_page_link(910);?>" class="o-searchSave">⭐️<i class="o-icon __min flaticon-check-mark"></i>保存した条件から探す(<span class="searchCount"><?php echo $sc_con;?></span>)</a>

                    <!--1124px までのpcで表示-->
                    <div class="l-sideSearch_cont __pc o-searchCont l-wrap">
                        <div class="o-searchStep area">
                            <!--  <p class="o-searchTitle o-title __mid __withsub">--><p class="o-searchTitles o-title __mid __withsub" style="position:relative;text-align:center;margin-bottom: 1.6rem;">エリア・沿線を選択</p>
                            <div class="o-searchArea">
                                <div class="flex-nw">
                                    <a id="js-areaModal_side" class="o-searchArea_item"><i class="o-icon o-searchArea_itemIcon flaticon-location"></i>エリア</a>
                                    <a id="js-stationModal_side" class="o-searchArea_item"><i class="o-icon o-searchArea_itemIcon flaticon-train"></i>沿線・駅</a>
                                    <!--<a href="" class="o-searchArea_item"><i class="o-icon o-searchArea_itemIcon flaticon-map"></i>地図</a>-->
                                </div>
                                <div id="selectedSearch" class="o-searchInfo_selected __area">

                                    <!--  <li class="o-searchArea_selectedItem">都営浅草線 高輪台駅</li>-->
                                </div>
                            </div>
                        </div>
                        <div class="o-searchStep conditions">
                            <!-- <p class="o-searchTitle o-title __mid __withsub"> --><p class="o-searchTitles o-title __mid __withsub" style="position:relative;text-align:center;margin-bottom: 1.6rem;">探したい賃貸の条件を指定。</p>
                            <div class="o-searchMoney o-searchBox">
                                <h5 class="o-searchSubtitle">賃料<span class="o-searchSubtitle_unit">(万円)</span></h5>
                                <input type="hidden" class="o-searchMoney_slider range-slider" id="homeChinryoSlider" value="0,200" style="display:none;" />
                                <?php if( have_rows('サーチフォーム上設備', 'option') ):$i = 1;?>
                                <span class="wpcf7-form-control-wrap chintai">
                                    <span class="wpcf7-form-control wpcf7-checkbox __2">
                                    <?php while( have_rows('サーチフォーム上設備', 'option') ): the_row();?>
                                        <?php
                                        //name属性を設定
                                        if(get_sub_field('共用or部屋', 'option') == '共用') {
                                            $setubi_name = 'set2';
                                        } else {
                                            $setubi_name = 'set3';
                                        }

                                        //値を取得
                                        if(get_sub_field('共用or部屋', 'option') == '共用') {
                                            if(get_sub_field('共用設備', 'option') !== '選択してください'){
                                                $field = get_sub_field_object('共用設備', 'option');
                                                // print_r($field);
                                                $setubi_value = $field['value'];
                                                $setubi_label = $field['choices'][$setubi_value];
                                            }else{

                                            }
                                        }else{
                                            if(get_sub_field('共用設備', 'option') !== '選択してください'){
                                                $field = get_sub_field_object('部屋設備', 'option');
                                                $setubi_value = $field['value'];
                                                $setubi_label = $field['choices'][$setubi_value];
                                            }else{

                                            }
                                        }
                                        ?>
                                        <span class="wpcf7-list-item <?php echo ($i == 0) ? 'first':'';?>">
                                            <label><input type="checkbox" name="<?php echo $setubi_name;?>" value="<?php echo $setubi_value;?>"><span class="wpcf7-list-item-label"><?php echo $setubi_label;?></span></label>
                                        </span>
                                     <?php $i++; endwhile;?>
                                     </span>
                                 </span>
                                 <?php endif;?>
                            </div>
                            <div class="o-searchBox">
                                <div class="o-searchBox_cont o-formSize">
                                    <h5 class="o-searchSubtitle">面積</h5>
                                    <div class="flex-nw ai-center">
                                        <div class="o-formSelect_wrap u-w100">
                                            <span class="wpcf7-form-control-wrap menseki-kagen">
                                                <select name="mel" class="wpcf7-form-control wpcf7-select o-formSelect" id="homeMensekiMin" aria-invalid="false">
                                                    <option value="0">下限なし</option>
                                                    <option value="10">10m²</option>
                                                    <option value="15">15m²</option>
                                                    <option value="20">20m²</option>
                                                    <option value="25">25m²</option>
                                                    <option value="30">30m²</option>
                                                    <option value="35">35m²</option>
                                                    <option value="40">40m²</option>
                                                    <option value="50">50m²</option>
                                                    <option value="60">60m²</option>
                                                    <option value="70">70m²</option>
                                                    <option value="80">80m²</option>
                                                    <option value="90">90m²</option>
                                                    <option value="100">100m²</option>
                                                    <option value="200">200m²</option>
                                                    <option value="300">300m²</option>
                                                    <option value="400">400m²</option>
                                                    <option value="500">500m²</option>
                                                    <option value="600">600m²</option>
                                                    <option value="700">700m²</option>
                                                    <option value="800">800m²</option>
                                                    <option value="900">900m²</option>
                                                    <option value="1000">1000m²</option>
                                                </select></span></div>
                                        <div>〜</div>
                                        <div class="o-formSelect_wrap u-w100">
                                            <span class="wpcf7-form-control-wrap menseki-jogen">
                                                <select name="meh" class="wpcf7-form-control wpcf7-select o-formSelect" id="homeMensekiMax" aria-invalid="false">
                                                    <option value="10">10m²</option>
                                                    <option value="15">15m²</option>
                                                    <option value="20">20m²</option>
                                                    <option value="25">25m²</option>
                                                    <option value="30">30m²</option>
                                                    <option value="35">35m²</option>
                                                    <option value="40">40m²</option>
                                                    <option value="50">50m²</option>
                                                    <option value="60">60m²</option>
                                                    <option value="70">70m²</option>
                                                    <option value="80">80m²</option>
                                                    <option value="90">90m²</option>
                                                    <option value="100">100m²</option>
                                                    <option value="200">200m²</option>
                                                    <option value="300">300m²</option>
                                                    <option value="400">400m²</option>
                                                    <option value="500">500m²</option>
                                                    <option value="600">600m²</option>
                                                    <option value="700">700m²</option>
                                                    <option value="800">800m²</option>
                                                    <option value="900">900m²</option>
                                                    <option value="1000">1000m²</option>
                                                    <option value="0" selected="selected">上限なし</option>
                                                </select></span></div>

                                    </div>

                                </div>
                                <div class="o-searchBox_cont">
                                    <div class="flex-nw l-row">
                                        <div class="o-formSize l-rowLeft rentsearch2">
                                            <h5 class="o-searchSubtitle">駅徒歩</h5>
                                            <div class="o-formSelect_wrap u-w100">
                                                <span class="wpcf7-form-control-wrap toho">
                                                    <select name="eki" class="wpcf7-form-control wpcf7-select o-formSelect" id="homeHof" aria-invalid="false">
                                                        <option value="0">指定なし</option>
                                                        <option value="1">1分以内</option>
                                                        <option value="3">3分以内</option>
                                                        <option value="5">5分以内</option>
                                                        <option value="10">10分以内</option>
                                                        <option value="15">15分以内</option>
                                                    </select></span></div>

                                        </div>
                                        <!-- 2020/12/20 入居可能日を即入居可のチェックボックスに変更。 -->
                                        <div class="l-rowRight rentsearch2">
                                            <h5 class="o-searchSubtitle">入居可能日</h5>
                                            <div class="o-formCheckbtn u-w100">
                                                <span class="wpcf7-form-control-wrap wpcf7-checkbox  nyukyobi">
                                                    <span class="u-w100">
                                                        <label>
                                                            <!-- 2020/12/20 スタイルが当たらないので、style属性でチェック欄が出るように仮修正 -->
                                                            <input type="checkbox"  style="appearance:auto;" id="homeNyukyo" value="2">
                                                            <span class="wpcf7-list-item-label">
                                                                即入居可
                                                            </span>
                                                        </label>
                                                    </span>
                                                </span>
                                            </div>
                                        </div>
                                        <!--  2020/12/20 入居可能日修正前、selectboxで出していたもの。
                                        <div class="l-rowRight rentsearch2">
                                            <h5 class="o-searchSubtitle">入居可能日</h5>
                                            <div class="o-formSelect_wrap u-w100">
                                                <span class="wpcf7-form-control-wrap nyukyobi">
                                                    <select name="nyukyobi" class="wpcf7-form-control wpcf7-select o-formSelect nyukyo" id="homeNyukyo" aria-invalid="false">
                                                        <option value="0">指定なし</option>
                                                    <?php //$nyukyokanou_arr = str_get_nyukyokanoubi();?>
                                                    <?php //foreach ($nyukyokanou_arr as $od ):?>
                                                        <option value="<?php //echo $od['val'];?>"><?php //echo $od['txt'];?></option>
                                                    <?php //endforeach;?>
                                                    </select>
                                                </span>
                                            </div>
                                        </div>
                                        -->
                                    </div>
                                </div>
                            </div>
                            <div class="o-searchBox">
                                <div class="o-searchBox_cont o-searchRoom">
                                    <h5 class="o-searchSubtitle">間取り</h5>
                                    <p>
                                        <span class="wpcf7-form-control-wrap madori">
                                            <span id="homeMadori" class="wpcf7-form-control wpcf7-checkbox __4">
                                                <span class="wpcf7-list-item first"><label><input type="checkbox" name="madori" value="110"><span class="wpcf7-list-item-label">1R</span></label></span>
                                                <span class="wpcf7-list-item first"><label><input type="checkbox" name="madori" value="120"><span class="wpcf7-list-item-label">1K</span></label></span>
                                                <span class="wpcf7-list-item first"><label><input type="checkbox" name="madori" value="130"><span class="wpcf7-list-item-label">1DK</span></label></span>
                                                <span class="wpcf7-list-item first"><label><input type="checkbox" name="madori" value="150"><span class="wpcf7-list-item-label">1LDK</span></label></span>
                                                <span class="wpcf7-list-item first"><label><input type="checkbox" name="madori" value="155"><span class="wpcf7-list-item-label">1SLDK</span></label></span>
                                                <span class="wpcf7-list-item first"><label><input type="checkbox" name="madori" value="230"><span class="wpcf7-list-item-label">2DK</span></label></span>
                                                <span class="wpcf7-list-item first"><label><input type="checkbox" name="madori" value="250"><span class="wpcf7-list-item-label">2LDK</span></label></span>
                                                <span class="wpcf7-list-item first"><label><input type="checkbox" name="madori" value="255"><span class="wpcf7-list-item-label">2SLDK</span></label></span>
                                                <span class="wpcf7-list-item first"><label><input type="checkbox" name="madori" value="350"><span class="wpcf7-list-item-label">3LDK</span></label></span>
                                                <span class="wpcf7-list-item first"><label><input type="checkbox" name="madori" value="355"><span class="wpcf7-list-item-label">3SLDK</span></label></span>
                                                <span class="wpcf7-list-item first"><label><input type="checkbox" name="madori" value="450"><span class="wpcf7-list-item-label">4LDK</span></label></span>
                                                <span class="wpcf7-list-item first"><label><input type="checkbox" name="madori" value="455"><span class="wpcf7-list-item-label">4SLDK</span></label></span>
                                                <span class="wpcf7-list-item first"><label><input type="checkbox" name="madori" value="550"><span class="wpcf7-list-item-label">5LDK</span></label></span>
                                                <span class="wpcf7-list-item first"><label><input type="checkbox" name="madori" value="555"><span class="wpcf7-list-item-label">5SLDK</span></label></span>
                                            </span>
                                        </span>
                                    </p>
                                </div>
                                <div class="o-searchBox_cont">
                                    <h5 class="o-searchSubtitle">設備</h5>
                                    <p> <a id="js-setsubiModal_side" class="o-searchBtn o-btn __basic11 __sizem __min mb-3">設備を選ぶ</a></p>
                                    <h5 class="o-searchSubtitle">こだわり条件</h5>
                                    <p> <a id="js-kodawariModal_side" class="o-searchBtn o-btn __basic11  __sizem __min mb-3">こだわり条件を選ぶ</a>
                                    </p>
                                </div>
                            </div>

                        <div class="o-searchStep o-searchBtn">
                            <div class="o-searchBtn_wrap jc-center">
                                <a href="" class="o-searchBtn_search o-btn __basic02 __mid search mb-1 searchRun" data-searchform="home"><i class="o-icon flaticon-search"></i>この条件で検索する</a>
                                <div class="o-searchBtn_function flex-w jc-center ai-center">
                                    <a href="" class="o-searchBtn_save o-btn __tiny __basic11 __sizes modalSearchSave" data-searchform="home"><i class="o-icon flaticon-download"></i>検索条件を保存</a>
                                    <a href="" class="o-searchBtn_reset o-btn __tiny __basic04 __sizem">条件をリセット</a>
                                </div>
                            </div>
                        </div>

                            <?php if ( have_rows('popular_search', 'option') ):?>
                            <div class="o-searchStep popular">
                                <!-- <p class="o-searchTitle o-title __mid __withsub"> 人気の検索条件から選ぶ-->
                                <p class="o-searchTitles o-title __mid __withsub" style="position:relative;text-align:center;margin-bottom: 1.6rem;">他にもこのような検索条件から選ばれてます</p>
                                <div class="o-searchTerms">
                                    <form id="homePopularSearch" class="o-searchTerms_select o-formChecklist" action="">
                                    <ul>
                                    <?php $popcon=0; while ( have_rows('popular_search', 'option') ): the_row();?>
                                    <!--
                                        <input class="o-formChecklist_input" name="popular" id="homepopser<?php echo $popcon;?>" type="radio" value="<?php echo get_sub_field('display_popular_search_link');?>">
                                        <label class="o-formChecklist_label" for="homepopser<?php echo $popcon;?>"><?php the_sub_field('display_popular_search_name'); ?></label>
                                    -->
                                        <li><a href="<?php echo get_sub_field('display_popular_search_link');?>" target="_blank"><?php the_sub_field('display_popular_search_name'); ?></a></li>

                                    <?php $popcon++; endwhile;?>
                                    </ul>
                                    </form>
                                </div>
                            </div>
                            <?php endif;?>


                        </div>

                    </div>

                    <!-- 1124px以下で表示 -->
                    <div class="l-sideSearch_cont __sp">
                        <div class="l-sideSearch_wrap l-wrap flex-nw-no-md">
                            <div class="l-sideSearch_function __area flex-nw js-modal1">
                                <p class="l-sideSearch_functionTitle"><i class="o-icon __large __nobold flaticon-location"></i>検索エリア</p>
                                <p class="l-sideSearch_functionDesc">指定なし</p>
                            </div>

                            <div class="l-sideSearch_function __terms flex-nw js-modal2">
                                <p class="l-sideSearch_functionTitle"><i class="o-icon __title __nobold flaticon-construction"></i>賃貸の条件</p>
                                <p class="l-sideSearch_functionDesc">指定なし指定なし指定なし</p>
                            </div>

                            <div class="l-sideSearch_btn">
                                <div class="l-sideSearch_btnWrap flex-w jc-center">
                                    <a href="" id="searchRun" class="l-sideSearch_btn_search o-btn __basic02 __p search mb-1" data-searchform="home"><i class="o-icon flaticon-search"></i>この条件で検索する</a>
                                    <div class="l-sideSearch_btnFunction flex-w jc-center ai-center">
                                        <a href="" class="l-sideSearch_btnSave o-btn __min __basic07 __sizem modalSearchSave" data-searchform="home"><i class="o-icon flaticon-download"></i>検索条件を保存</a>
                                        <a href="" class="l-sideSearch_btnReset o-btn __min __basic04 __sizem">条件をリセット</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="l-rowRight searchmain">
                <section class="l-cont newestate">
                    <h2 class="o-title __large mb-3">新着物件</h2>

                    <?php
                if ( is_active_sidebar( 'top_widgets_new_fudo' ) ) :
                dynamic_sidebar('top_widgets_new_fudo');
                endif;

                //高い順
                //&shu=&ros=&eki=&ken=0&sik=&kalc=&kahc=&kalb=&kahb=&hof=&tik=0&mel=0&meh=0&so=kak&ord=d
                ?>
                    <a href="<?php echo home_url();?>/?bukken=jsearch" class="l-contNewestate_link o-link __mid __text type01">新着一覧を見る<span class="o-linkItem"></span></a>

                </section>
                <div class="l-contOuter __xl">
                    <?php
           //オススメ物件
                 get_template_part('template/bukken', 'osusume');
           ?>
                </div>



            </div>
        </div>


    </div>





    </main><!-- #main -->
</div><!-- #top -->



<?php get_footer('top'); ?>
