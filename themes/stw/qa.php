<?php
/*
Template Name: qa
*/
?>
<?php get_header();?>



<div id="qa" class="content-area underlayer">
  <main id="main" class="site-main" role="main">

    <nav id="breadcrumbs">
      <ul class="cf">
        <li><a href="/"><span>ホーム</span></a></li>
        <li>高級賃貸東京のQ&amp;A</li>
      </ul>
    </nav>

    <article class="ul-flex">
      <section class="ul-contents">
        <h2 class="ul-title mincho">高級賃貸東京のQ&amp;A</h2>

        <section class="qa01">
          <div class="qa-box">
            <h3 class="ul-title02 mincho"><span>Q</span>HPに載ってない物件も紹介できる？</h3>
            <p>もちろん、他社サイトで掲載されている物件でもご紹介できます。<br>
              お問い合わせフォームから詳細ページのURLをお送り頂ければ、お調べして紹介させて頂きます。</p>
          </div>

          <div class="qa-box">
            <h3 class="ul-title02 mincho"><span>Q</span>1日にどれぐらいの物件を内覧できる？</h3>
            <p>1日の内覧件数は原則3部屋までとさせて頂き、同じ物件なら30分程度、別の物件ですと移動が必要なので、1時間〜1時間半程のお時間頂戴します。<br>
              人気な物件ですと予約が取りにくいこともあり、ご希望の日時にご案内できないこともありますので、どうかご了承ください。</p>
          </div>

          <div class="qa-box">
            <h3 class="ul-title02 mincho"><span>Q</span>内覧のとき移動は車？</h3>
            <p>社用車も用意しておりますが、予約状況により使用できない場合も御座います。<br>
              その際は、電車移動となること予めご了承くださいませ。</p>
          </div>

          <div class="qa-box">
            <h3 class="ul-title02 mincho"><span>Q</span>仲介手数料はいくら？</h3>
            <p>仲介手数料は「賃料の1ヶ月分＋消費税」とさせて頂きます。<br>
              ご契約時には物件により異なりますが、以下の金銭項目が必要となります。</p>
            <div class="note">
              <ul class="list-s">
                <li>・敷金（保証金）</li>
                <li>・礼金</li>
                <li>・保証委託料</li>
                <li>・ご入居月の賃料と翌月の賃料</li>
                <li>・火災保険料</li>
                <li>・鍵交換費用</li>
              </ul>
            </div>
          </div>

          <div class="qa-box">
            <h3 class="ul-title02 mincho"><span>Q</span>空室になってるのに内覧できないの？</h3>
            <p>募集状況につきましては、常に最新の状況をHPに掲載するように心掛けておりますが、貸主様が定める募集条件に変更が生じたり、別のお客様から申し込みが入ったことで、ご案内できないこともありますので、予めご了承ください。</p>
          </div>
        </section><!-- .qa01 -->
      </section><!-- .ul-contents -->

      <?php get_sidebar(); ?>
    </article><!-- .ul-flex -->

  </main><!-- #main -->
</div><!-- #about -->

<?php get_footer(); ?>
