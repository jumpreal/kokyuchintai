<?php
/*
Template Name:マイリスト一覧用の固定ページ
Template Post Type: post,page
*/
$sc = ( isset($_COOKIE['searchSaveList']) && !empty($_COOKIE['searchSaveList']) ) ? json_decode(stripcslashes($_COOKIE['searchSaveList'])) : null;
$sc_con = ( is_null($sc) ) ? 0 : count($sc);

?>
<?php get_header(); ?>


<!--<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_directory'); ?>/assets/css/page.css">-->


<div id="<?php echo $post->post_name;?>" class="p-mypage">
    <main id="main" class="site-main" role="main">

        <div class="o-fv __page mb-0">
            <img src="https://koukyu-chintai.com/wp-content/uploads/cropped-cropped-main-1-2000x1200-1-1024x614.jpg" alt="" class="o-fvImg" loading="lazy">
            <div class="o-fvCatch">
                <h2 class="o-fvCatch_title o-title __large __white __nobold">
                    <?php the_title(); ?>
                </h2>
            </div>
        </div>

        <section class="l-wrap __maxwsmall">

<?php
    $user = isset($_REQUEST['user']) ? $_REQUEST['user'] : "";
    $count = 0;
    global $favorite_post_ids;
    if ( !empty($user) ) {
        if ( wpfp_is_user_favlist_public($user) )
            $favorite_post_ids = wpfp_get_users_favorites($user);
    } else {
        $favorite_post_ids = wpfp_get_users_favorites();
        $count = count($favorite_post_ids);
        $post_ids = implode(",", $favorite_post_ids);
    }
        // print_r($favorite_post_ids);

?>
            <div class="p-mypageTab o-tab __basic01 mb-6">
                <a href="<?php bloginfo('url'); ?>/myhistory" class="p-mypageTab_item history o-tabItem" ><i class="o-icon flaticon-history"></i>最近見た物件<span class="p-mypageTab_notification o-partsNotification stock __sizem"><?php echo stw_count_his();?></span></a>
                <a class="p-mypageTab_item save __active o-tabItem" class=""><i class="o-icon flaticon-love"></i>検討リスト<span class="p-mypageTab_notification o-partsNotification stock __sizem"><?php echo @get_mylist();?></span></a>
                <a href="<?php bloginfo('url'); ?>/myterms" class="p-mypageTab_item terms o-tabItem" ><i class="o-icon flaticon-download"></i>保存した検索条件<span class="p-mypageTab_notification o-partsNotification stock __sizem searchCount"><?php echo $sc_con;?></span></a>
            </div>

            <section class="p-postCont">
                <p class="u-text-ac"><?php the_content(); ?></p>
            </section>

            <section class="p-mypageCont __save o-partsTerms __area l-cont __section">

                <div class="p-mypageRoom o-room __list flex-w">
                <?php

                // print_r($favorite_post_ids);
                    foreach($favorite_post_ids as $bukken_id){
                        include('template/bukken-card.php');
                    }
                ?>
                </div>
                <div class="p-mypageContact u-text-ac">
                        <a href="<?php echo home_url() . "/" . get_field('kengaku_nairan_link', 'option');?>?post_id=<?php echo $post_ids;?>" class="p-mypageContact_btn o-btn __sizel __basic02 __mid __maxwidth01">まとめて内覧予約</a>
                </div>
            </section>



        </section>



    </main><!-- #main -->
</div><!-- #about -->

<?php get_footer('top'); ?>
