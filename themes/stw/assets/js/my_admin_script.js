// JavaScript Document
var jqOther = jQuery.noConflict(true);	
jqOther(function() {
	var gets=GetQueryString();
	if (jqOther('body.post-type-page').length&&gets.post==329) {
		jqOther('body.post-type-page #postdivrich').hide();
	}
});

function GetQueryString(){
    var result = {};
    if( 1 < window.location.search.length ){
        var query = window.location.search.substring( 1 );
        var parameters = query.split( '&' );

        for( var i = 0; i < parameters.length; i++ ){
            var element = parameters[ i ].split( '=' );

            var paramName = decodeURIComponent( element[ 0 ] );
            var paramValue = decodeURIComponent( element[ 1 ] );
           result[ paramName ] = paramValue;
        }
    }
    return result;
}