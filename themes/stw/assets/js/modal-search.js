//https://koukyu-chintai.com/?bukken=jsearch&shu=2&kalc=20&kahc=40&hof=0&mel=0&meh=0&ken=13&nyukyo=0
//https://koukyu-chintai.com/?bukken=jsearch&shu=2&kalc=20&kahc=40&hof=0&mel=0&meh=0&ksik[]=0&ros=&eki=&setu=undefined&koda=&nyukyo=0
//https://koukyu-chintai.com/?bukken=jsearch&shu=2&kalc=20&kahc=40&hof=0&mel=0&meh=0&ksik[]=&ros=&eki=&setu=&koda=&nyukyo=0
(function ($) {

    $(function () {

        var amoc = 0; // areamodalをopenした回数
        var smoc = 0; // stationmodalをopenした回数

        // searchmodal制御部
        $('.l-headerNav_functionSearch , .js-modal , .js-modal1, .js-modal2').on('click', function () {
            $('#searchModal').fadeIn();
            return false;
        });
        $('.js-modal-close').on('click', function () {
            $('#searchModal').fadeOut();
            return false;
        });

        // areamodal制御分
        $('#js-areaModal , #js-areaModal_side').on('click', function () {
            $('#areaModal').fadeIn();
            if ( 0 == amoc ){ // aremodal表示初回だけ、中身のデータを取得
                $.ajax({
                    type: 'POST',
                    url: ajaxurl,
                    data: {
                      'action' : 'stw_sikutyouson_json',
                      'shu' : 2,
                      'ken' : 13
                    }
                  }).done(function(data){
                    var areaJson = JSON.parse(data);
                    $.each(areaJson, function(i,val){
                      var achkd = '';
                      $.each(getKsik, function(y,ogk){
                         if ( val['id'] == ogk ){
                             achkd = 'checked';
                         }
                      });
                      var ainp = '<input class="o-formCheckbtn_input" id="area'+val['id']+'" type="checkbox" name="area" value="'+ val['id']+'" '+ achkd + '>';
                      var alab = '<label class="o-modalChild_label o-formCheckbtn_label flex-item" for="area'+val['id']+'">'+val['name']+'</label>';
                      $('#areaForm').append(ainp+alab);
                    });
                  }).fail(function(data){
                    console.log('area ajax faild.');
                  });
            }
            amoc++;

            if ($('#areaModal').hasClass("js-modalActive")) { // クリックされた要素がjs-modalActiveクラスだったら
                $('#areaModal').removeClass("js-modalActive");
            } else {
                $('#areaModal').addClass("js-modalActive");
            }
            return false;

        });
        $('.js-areamodal-close').on('click', function () {
            $('#areaModal').fadeOut();
            if ($('#areaModal').hasClass("js-modalActive")) { // クリックされた要素がjs-modalActiveクラスだったら
                $('#areaModal').removeClass("js-modalActive");
            } else {
                $('#areaModal').addClass("js-modalActive");
            }
            return false;
        });

        // stationmodal制御分
        $('#js-stationModal , #js-stationModal_side').on('click', function () {
            $('#stationModal').fadeIn();
            if ( 0 == smoc ){ // stationmodal表示初回だけ、中身のデータを取得
                $.ajax({
                    type: 'POST',
                    url: ajaxurl,
                    data: {
                      'action' : 'stw_roseneki_json'
                    }
                  }).done(function(sdata){
                    var stationJson = JSON.parse(sdata);
                    $.each(stationJson, function(i,rosens){
                        var rosid = rosens['rosid'];
                        var rosname = rosens['rosname'];
                        var ekis = rosens['ekis'];
                        var vRosName = '<div class="o-modalCont"><h4 class="o-modalTitle">'+rosname+'</h4><div class="flex-column3-md-2 flex-w">';
                        var ekidoms = '';
                        $.each(ekis, function(i,eki){
                           var echkd = '';
                           $.each(getEki, function(s,oge){
                              if ( eki['ekiid'] == oge ){
                                  echkd = 'checked';
                              }
                           });
                           var ekiid = eki['ekiid'];
                           var ekiname = eki['ekiname'];
                           var ekiinp = '<input class="o-formCheckbtn_input" name="eki" type="checkbox" value="'+ekiid+'" id="eki'+rosid+ekiid+'" data-rosenid="'+rosid+'" data-rosname="'+rosname+'" '+echkd+'>';
                           var ekilab = '<label class="o-modalChild_label o-formCheckbtn_label flex-item" for="eki'+rosid+ekiid+'">'+ekiname+'駅</label>';
                           ekidoms += ekiinp+ekilab;
                        });
                        var ekiEnddiv = '</div></div>';
                        $('#rosEkis').append(vRosName+ekidoms);
                    });
                  }).fail(function(sdata){
                    console.log('area ajax faild.');
                  });
            }
            smoc++;

            if ($('#stationModal').hasClass("js-modalActive")) { // クリックされた要素がjs-modalActiveクラスだったら
                $('#stationModal').removeClass("js-modalActive");
            } else {
                $('#stationModal').addClass("js-modalActive");
            }
            return false;

        });
        $('.js-stationmodal-close').on('click', function () {
            $('#stationModal').fadeOut();
            if ($('#stationModal').hasClass("js-modalActive")) { // クリックされた要素がjs-modalActiveクラスだったら
                $('#stationModal').removeClass("js-modalActive");
            } else {
                $('#stationModal').addClass("js-modalActive");
            }
            return false;
        });


        // setubimodal制御分
        $('#js-setsubiModal,#js-setsubiModal_side').on('click', function () {
            $('#setsubiModal').fadeIn();
            if ($('#setsubiModal').hasClass("js-modalActive")) { // クリックされた要素がjs-modalActiveクラスだったら
                $('#setsubiModal').removeClass("js-modalActive");
            } else {
                $('#setsubiModal').addClass("js-modalActive");
            }
            return false;

        });
        $('.js-setsubimodal-close').on('click', function () {
            $('#setsubiModal').fadeOut();
            if ($('#setsubiModal').hasClass("js-modalActive")) { // クリックされた要素がjs-modalActiveクラスだったら
                $('#setsubiModal').removeClass("js-modalActive");
            } else {
                $('#setsubiModal').addClass("js-modalActive");
            }
            return false;
        });

        // kodawarimodal制御分
        $('#js-kodawariModal,#js-kodawariModal_side').on('click', function () {
            $('#kodawariModal').fadeIn();
            if ($('#kodawariModal').hasClass("js-modalActive")) { // クリックされた要素がjs-modalActiveクラスだったら
                $('#kodawariModal').removeClass("js-modalActive");
            } else {
                $('#kodawariModal').addClass("js-modalActive");
            }
            return false;

        });
        $('.js-kodawarimodal-close').on('click', function () {
            $('#kodawariModal').fadeOut();
            if ($('#kodawariModal').hasClass("js-modalActive")) { // クリックされた要素がjs-modalActiveクラスだったら
                $('#kodawariModal').removeClass("js-modalActive");
            } else {
                $('#kodawariModal').addClass("js-modalActive");
            }
            return false;
        });

        // savemodal制御分
        $('#js-saveModal').on('click', function () {
            $('#saveModal').fadeIn();
            if ($('#saveModal').hasClass("js-modalActive")) { // クリックされた要素がjs-modalActiveクラスだったら
                $('#saveModal').removeClass("js-modalActive");
            } else {
                $('#saveModal').addClass("js-modalActive");
            }
            return false;

        });
        $('.js-savemodal-close').on('click', function () {
            $('#saveModal').fadeOut();
            if ($('#saveModal').hasClass("js-modalActive")) { // クリックされた要素がjs-modalActiveクラスだったら
                $('#saveModal').removeClass("js-modalActive");
            } else {
                $('#saveModal').addClass("js-modalActive");
            }
            return false;
        });

        // 検索条件を保存modal制御分
        $('.modalSearchSave').on('click', function (e) {
            e.preventDefault();
            $('#searchSaveModal').fadeIn();
            if ($('#searchSaveModal').hasClass("js-modalActive")) { // クリックされた要素がjs-modalActiveクラスだったら
                $('#searchSaveModal').removeClass("js-modalActive");
            } else {
                $('#searchSaveModal').addClass("js-modalActive");
            }
            return false;

        });
        $('.js-searchsavemodal-close').on('click', function () {
            $('#searchSaveModal').fadeOut();
            if ($('#searchSaveModal').hasClass("js-modalActive")) { // クリックされた要素がjs-modalActiveクラスだったら
                $('#searchSaveModal').removeClass("js-modalActive");
            } else {
                $('#searchSaveModal').addClass("js-modalActive");
            }
            return false;
        });

        // estimatemodal制御分
        $('#js-estimateModal ,#js-estimateModal2').on('click', function () {
            $('#estimateModal').fadeIn();
            if ($('#estimateModal').hasClass("js-modalActive")) { // クリックされた要素がjs-modalActiveクラスだったら
                $('#estimateModal').removeClass("js-modalActive");
            } else {
                $('#estimateModal').addClass("js-modalActive");
            }
            return false;

        });
        $('.js-estimatemodal-close').on('click', function () {
            $('#estimateModal').fadeOut();
            if ($('#estimateModal').hasClass("js-modalActive")) { // クリックされた要素がjs-modalActiveクラスだったら
                $('#estimateModal').removeClass("js-modalActive");
            } else {
                $('#estimateModal').addClass("js-modalActive");
            }
            return false;
        });


        // 部屋の画像モーダル
        $('#js-roomimgModal,#js-roomimgModal2').on('click', function () {
            $('#roomimgModal').fadeIn();
            if ($('#roomimgModal').hasClass("js-modalActive")) { // クリックされた要素がjs-modalActiveクラスだったら
                $('#roomimgModal').removeClass("js-modalActive");
            } else {
                $('#roomimgModal').addClass("js-modalActive");
            }
            return false;

        });
        $('.js-roomimgmodal-close').on('click', function () {
            $('#roomimgModal').fadeOut();
            if ($('#roomimgModal').hasClass("js-modalActive")) { // クリックされた要素がjs-modalActiveクラスだったら
                $('#roomimgModal').removeClass("js-modalActive");
            } else {
                $('#roomimgModal').addClass("js-modalActive");
            }
            return false;
        });

        // ビル物件の画像モーダル
        $('#js-fudoimgModal,#js-fudoimgModal2').on('click', function () {
            $('#fudoimgModal').fadeIn();
            if ($('#fudoimgModal').hasClass("js-modalActive")) { // クリックされた要素がjs-modalActiveクラスだったら
                $('#fudoimgModal').removeClass("js-modalActive");
            } else {
                $('#fudoimgModal').addClass("js-modalActive");
            }
            return false;

        });
        $('.js-roomimgmodal-close').on('click', function () {
            $('#fudoimgModal').fadeOut();
            if ($('#fudoimgModal').hasClass("js-modalActive")) { // クリックされた要素がjs-modalActiveクラスだったら
                $('#fudoimgModal').removeClass("js-modalActive");
            } else {
                $('#fudoimgModal').addClass("js-modalActive");
            }
            return false;
        });



        // 家賃のスライダーバー制御部
        $('.range-slider').jRange({
            from: 0,
            to: 200,
            step: 1,
            scale: [0, 10, 20, 30, 40, 50, 60, 70, 80, 90, 100],
            format: '%s',
            width: 667,
            showLabels: true,
            isRange: true
        });


        // areaModalの条件反映
        $('#areaRun').on('click', function () {
            var area = [];
            var areaText = [];
            $('input:checkbox[name="area"]:checked').each(function () {
                area.push(this.value);
                areaText.push($('label[for="area' + this.value + '"').text());
            });
            $('.d-area').remove();
            $('.hd-area').remove();
            if (0 < area.length) {
                $.each(area, function(i, areaid){
                    $('#msSik').append('<input class="hd-area" type="hidden" name="ksik[]" value="'+areaid+'">');
                    $('.o-searchInfo_selected.__area').append('<span class="o-searchInfo_selectedItem d-area">'+areaText[i]+'</span>');
                });
            }
            $('#areaModal').fadeOut();
            $('#areaModal').removeClass("js-modalActive");
        });

        // stationModalの条件反映
        $('#stationRun').on('click', function (e) {
            e.preventDefault();
            var eki = [];
            var ekiText = [];
            $('input:checkbox[name="eki"]:checked').each(function () {
                eki.push(this.value);
                var r = $(this).attr('data-rosname');
                var rosid = $(this).attr('data-rosenid');
                ekiText.push( r + $('label[for="eki' + rosid + this.value + '"').text());
            });
            $('.d-tation').remove();
            $('.hd-eki').remove();
            if (0 < eki.length) {
                $.each(eki, function(i, ekiid){
                    $('#msEki').append('<input class="hd-eki" type="hidden" name="eki[]" value="'+ekiid+'">');
                    $('.o-searchInfo_selected.__area').append('<span class="o-searchInfo_selectedItem d-tation">'+ekiText[i]+'</span>');
                });
            }
            $('#stationModal').fadeOut();
            $('#stationModal').removeClass("js-modalActive");
        });

        // setubiModalの条件反映
        $('.setubi_run').on('click', function () {
            reflectSetubi();
            $('#setsubiModal').fadeOut();
            if ($('#setsubiModal').hasClass("js-modalActive")) { // クリックされた要素がjs-modalActiveクラスだったら
                $('#setsubiModal').removeClass("js-modalActive");
            } else {
                $('#setsubiModal').addClass("js-modalActive");
            }
        });

        // kodawariModalの条件反映
        $('.kodawari_run').on('click', function () {
            var koda = [];
            var kodaText = [];
            $('input:checkbox[name="kodawari"]:checked').each(function () {
                koda.push(this.value);
                kodaText.push($('label[for="' + this.value + '"').text());
            });
            $('.hd-kodawari').remove();
            if( 0 < koda.length ){
                $.each(koda, function(i, kodaid){
                   $('#msKoda').append('<input class="hd-kodawari" type="hidden" name="koda[]" value="'+kodaid+'">');
                });
            }
            $('#kodawariModal').fadeOut();
            if ($('#kodawariModal').hasClass("js-modalActive")) { // クリックされた要素がjs-modalActiveクラスだったら
                $('#kodawariModal').removeClass("js-modalActive");
            } else {
                $('#kodawariModal').addClass("js-modalActive");
            }

        });

        // searchModalの条件反映
        $('.searchRun').on('click', function (e) {
            e.preventDefault();
            var searchFormFlg = $(this).attr('data-searchform');
            var popsel = '#'+searchFormFlg+'PopularSearch input[name="popular"]:checked';
            var popularSearch = $(popsel).val();

            reflectSearchConditions(searchFormFlg);
            reflectSetubi();
            // フォーム送信
            $('#modalSearchForm').submit();
        });

        // 検索条件の保存モーダル表示時の処理
        $('.modalSearchSave').on('click', function(e){
            e.preventDefault();
            var baseURL = '';
            var searchFormFlg = $(this).attr('data-searchform');
            if ('mdl' == searchFormFlg) {
                reflectSearchConditions('mdl');
            } else if ('side' == searchFormFlg) {
                reflectSearchConditions('side');
            } else if ('home' == searchFormFlg) {
                reflectSearchConditions('home');
            }

            if ('direct' == searchFormFlg) {
                // directは検索結果画面の条件保存ボタンからくる場合。アクセスしたページURLをそのままURLとして出す。
                baseURL = $(this).attr('data-url');
            } else {
                // direct以外は、フォーム値からURLを生成する。
                // 隠れたフォームからフォーム値取得
                baseURL = $('#modalSearchForm').attr('action');
                var kalc = $('#msKalc').val();
                var kahc = $('#msKahc').val();
                var hof = $('#msHof').val();
                var mel = $('#msMel').val();
                var meh = $('#msMeh').val();
                var ksik = '&ksik[]=';
                var sikAr = []
                $('input[name="ksik[]"]').each(function(){
                   sikAr.push(this.value);
                });
                if (1 < sikAr.length){
                    ksik += sikAr.join('&ksik[]=');
                } else if ( 1 == sikAr.length) {
                    ksik = '&ksik[]='+sikAr[0];
                }
                var eki = '&eki[]=';
                var ekiAr = [];
                $('#msEki input[name="eki[]"]').each(function(){
                    ekiAr.push(this.value);
                 });
                 if (1 < ekiAr.length){
                     eki += ekiAr.join('&eki[]=');
                 } else if ( 1 == ekiAr.length) {
                     eki = '&eki[]='+ekiAr[0];
                 }
                var setu = $('#msSetu').val();
                var koda = $('#msKoda').val();
                var nyukyo = $('#msNyukyo').val();

                // URL構築
                baseURL += '?bukken=jsearch&shu=2';
                baseURL += '&kalc='+kalc+'&kahc='+kahc+'&hof='+hof+'&mel='+mel+'&meh='+meh+ksik;
                baseURL += '&ken=13'+eki+'&setu='+setu+'&koda='+koda+'&nyukyo='+nyukyo;
            }

            $('#searchSaveURL').text(baseURL);
        });

        $('#searchSaveRun').on('click', function(e){
           e.preventDefault();
           $.cookie.json = true;
           var motoData = $.cookie('searchSaveList');
           if ( typeof motoData === "undefined"){
               motoData = [];
           }
           var name = $('#searchSaveName').val();
           var url = $('#searchSaveURL').text();
           var nd = new Date();
           var y = nd.getFullYear();
           var m = (nd.getMonth()+1);
           var d = nd.getDate();
           var now = y+'年'+m+'月'+d+'日';
           var cdata = { "name": name, "url": url, "date": now }
           console.log('現在日時');
           console.log(nd);
           console.log('今の月');
           console.log(m);

           motoData.push(cdata);
           $.cookie('searchSaveList',motoData,{expires:30, path:'/'});
           var con = motoData.length;
           $('.searchCount').text(con);
           $('#searchSaveName').val('');

           $('#searchSaveModal').fadeOut();
           if ($('#searchSaveModal').hasClass("js-modalActive")) { // クリックされた要素がjs-modalActiveクラスだったら
               $('#searchSaveModal').removeClass("js-modalActive");
           } else {
               $('#searchSaveModal').addClass("js-modalActive");
           }
           return false;
        });

        // 保存した検索条件を削除する
        $('.o-partsTerms_delete').on('click', function(e){
            e.preventDefault();
            var ci = $(this).attr('data-ci');
            $.cookie.json = true;
            var motoData = $.cookie('searchSaveList');
            motoData.splice(ci,1);
            $.cookie('searchSaveList',motoData,{expires:30, path:'/'});
            location.reload();
            return false;
        });


    });

    function reflectSearchConditions( searchFormFlg ){
        // 賃料スライダー値取得反映
        var rangeStr = '';
        if ( 'mdl' == searchFormFlg ){
            rangeStr = $('#mdlChinryoSlider').val();
        } else if ( 'side' == searchFormFlg ){
            rangeStr = $('#sideChinryoSlider').val();
            // 共通セレクタでとるならこれ
            //rangeStr = $('.o-searchMoney_slider').val();
        } else if ( 'home' == searchFormFlg ){
            rangeStr = $('#homeChinryoSlider').val();
        }
        var chin = rangeStr.split(',');
        $('#msKalc').val(chin[0]);
        $('#msKahc').val(chin[1]);

        // 面積値取得
        var mel = '';
        if ( 'mdl' == searchFormFlg ){
            mel = $('#mdlMensekiMin').val();
        } else if ( 'side' == searchFormFlg ){
            mel = $('#sideMensekiMin').val();
        } else if ( 'home' == searchFormFlg ){
            mel = $('#homeMensekiMin').val();
        }
        var meh = '';
        if ( 'mdl' == searchFormFlg ){
            meh = $('#mdlMensekiMax').val();
        } else if ( 'side' == searchFormFlg ){
            meh = $('#sideMensekiMax').val();
        } else if ( 'home' == searchFormFlg ){
            meh = $('#homeMensekiMax').val();
        }
        $('#msMel').val(mel);
        $('#msMeh').val(meh);

        // 駅徒歩取得
        var hof = '';
        if ( 'mdl' == searchFormFlg ){
            hof = $('#mdlHof').val();
        } else if ( 'side' == searchFormFlg ){
            hof = $('#sideHof').val();
        } else if ( 'home' == searchFormFlg ){
            hof = $('#homeHof').val();
        }
        $('#msHof').val(hof);

        // 入居可能日
        var nyukyo = '';
        if ( 'mdl' == searchFormFlg ){
            nyukyo = $('#mdlNyukyo:checked').val();
        } else if ( 'side' == searchFormFlg ){
            nyukyo = $('#sideNyukyo:checked').val();
        } else if ( 'home' == searchFormFlg ){
            nyukyo = $('#homeNyukyo:checked').val();
        }
        $('#msNyukyo').val(nyukyo);

        // 間取り
        var madori = [];
        if ( 'mdl' == searchFormFlg ){
          $('#mdlMadori input:checkbox[name="madori"]:checked').each(function () {
              madori.push(this.value);
          });
        } else if ( 'side' == searchFormFlg ){
          $('#sideMadori input:checkbox[name="madori"]:checked').each(function () {
              madori.push(this.value);
          });
        } else if ( 'home' == searchFormFlg ){
          $('#homeMadori input:checkbox[name="madori"]:checked').each(function () {
              madori.push(this.value);
          });
        }
        $('.hd-madori').remove();
        if( 0 < madori.length ){
            $.each(madori, function(i, madid){
               $('#msMad').append('<input class="hd-madori" type="hidden" name="mad[]" value="'+madid+'">');
            });
        }
    }

    /*
     * 設備modalやサーチフォーム上設備などの条件反映
     */
    function reflectSetubi() {
        var setu = [];
        var setuText = [];
        var setu2 = [];
        var setu2Text = [];
        var upSetu = [];
        $('input:checkbox[name="setubi"]:checked').each(function () {
            setu.push(this.value);
            setuText.push($('label[for="' + this.value + '"').text());
        });
        $('input:checkbox[name="setubi2"]:checked').each(function () {
            setu2.push(this.value);
            setu2Text.push($('label[for="' + this.value + '"').text());
        });
        $('input:checkbox[name="set2"]:checked').each(function () {
            upSetu.push(this.value);
        });
        $('input:checkbox[name="set3"]:checked').each(function () {
            upSetu.push(this.value);
        });

        $('.hd-set').remove();
        if ( 0 < setu.length ){
            $.each(setu, function(i, setuid){
                $('#msSet').append('<input class="hd-set" type="hidden" name="set[]" value="'+setuid+'">');
            });
        }
        // サーチフォーム上設備はsetとして反映する。
        if ( 0 < upSetu.length ){
            $.each(upSetu, function(i, setuid){
                $('#msSet').append('<input class="hd-set" type="hidden" name="set[]" value="'+setuid+'">');
            });
        }
        $('.hd-set2').remove();
        if ( 0 < setu2.length ){
            $.each(setu2, function(i, setu2id){
                $('#msSet2').append('<input class="hd-set2" type="hidden" name="set2[]" value="'+setu2id+'">');
            });
        }
    }

})(jQuery);
