// JavaScript Document


(function($) {
    //ページ内リンク
    $(function(){
        $('a[href^="#"]').click(function(){
            var adjust = 0;
            var speed = 400;
            var href= $(this).attr("href");
            var target = $(href == "#" || href == "" ? 'html' : href);
            var position = target.offset().top + adjust;
            $('body,html').animate({scrollTop:position}, speed, 'swing');
            return false;
        });
    });

    //アコーディオン
    $(function(){
        $('.o-rsingleFaq_question').on('click', function(){
            $(this).toggleClass('active').next().slideToggle(300);
        });
    });

    //タブ

    $(function(){
    $('.js-tab').click(function(){
        $('.js-active').removeClass('js-active');
        $(this).addClass('js-active');
        $('.js-shown').removeClass('js-shown');
        // クリックしたタブからインデックス番号を取得
        const index = $(this).index();
        // クリックしたタブと同じインデックス番号をもつコンテンツを表示
        $('.js-tabItem').eq(index).addClass('js-shown');
    });
    });

//    //追尾
//    $(function(){
//        if ( $('.js-fixedStart').length ){
//            var scrollStart = $('.js-fixedStart').offset().top; //ページ上部からの距離を取得
//            var scrollEnd = $('.js-fixedEnd').offset().top; //ページ上部からの距離を取得
//            var distance = 0;
//
//            $(document).scroll(function(){
//                distance = $(this).scrollTop(); //スクロールした距離を取得
//
//                if (scrollStart <= distance) { //スクロール距離が『.sikaku_box』の位置を超えたら
//                    $('.js-fixedStart').addClass('js-fixed'); //class『fixed』を追加
//                } else if (scrollStart >= distance) { //スクロールがページ上部まで戻ったら
//                    $('.js-fixedStart').removeClass('js-fixed'); //class『fixed』を削除
//                }
//
//                if (scrollEnd <= distance) { //スクロール距離が『.end_box』の位置を超えたら
//                    $('.js-fixedStart').addClass('js-fixedNone'); //class『none』を追加
//                } else{
//                    $('.js-fixedStart').removeClass('js-fixedNone'); //class『none』を削除
//                }
//            });
//        }
//    });
//
//
//
    //目次ナビゲーション

    $(function () {
        $(window).on('load',function () {
            if ( $("#o-rsingleSummary_list").length ){
                $("#o-rsingleSummary_list").navSync();
            }
        });
    });

    $(function(){
        // $('#o-rsingleSummary_list a').each(function(){
        //   var nav_w = $(this).outerWidth();
        //   console.log(nav_w);

        // });
        var array = [];
        for(var i = 0; i < $("#o-rsingleSummary_list.__sp a").length; i++){
            array.push($("#o-rsingleSummary_list.__sp a").eq(i).outerWidth());
        }
        //console.log(array);

        var nav_1 = ($('#o-rsingleSummary_list.__sp .item1').outerWidth(true));
        var nav_2 = $('#o-rsingleSummary_list.__sp .item2').outerWidth(true);
        var nav_3 = $('#o-rsingleSummary_list.__sp .item3').outerWidth(true);
        var nav_4 = $('#o-rsingleSummary_list.__sp .item4').outerWidth(true);
        var nav_5 = $('#o-rsingleSummary_list.__sp .item5').outerWidth(true);
        var nav_6 = $('#o-rsingleSummary_list.__sp .item6').outerWidth(true);
        var nav_7 = $('#o-rsingleSummary_list.__sp .item7').outerWidth(true);
        var nav_8 = $('#o-rsingleSummary_list.__sp .item8').outerWidth(true);
        var nav_9 = $('#o-rsingleSummary_list.__sp .item9').outerWidth(true);
        var nav_10 = $('#o-rsingleSummary_list.__sp .item10').outerWidth(true);
        var nav_11 = $('#o-rsingleSummary_list.__sp .item11').outerWidth(true);
        var nav_12 = $('#o-rsingleSummary_list.__sp .item12').outerWidth(true);
//
//        var sc_nav3 = nav_1 + nav_2;
//        var sc_nav4 = nav_1 + nav_2 + nav_3;
//        var sc_nav5 = nav_1 + nav_2 + nav_3 + nav_4;
//        var sc_nav6 = nav_1 + nav_2 + nav_3 + nav_4 + nav_5;
//        var sc_nav7 = nav_1 + nav_2 + nav_3 + nav_4 + nav_5 + nav_6;
//        var sc_nav8 = nav_1 + nav_2 + nav_3 + nav_4 + nav_5 + nav_6 + nav_7;
//        var sc_nav9 = nav_1 + nav_2 + nav_3 + nav_4 + nav_5 + nav_6 + nav_7 + nav_8;
//        var sc_nav10 = nav_1 + nav_2 + nav_3 + nav_4 + nav_5 + nav_6 + nav_7 + nav_8 + nav_9;
//        var sc_nav11 = nav_1 + nav_2 + nav_3 + nav_4 + nav_5 + nav_6 + nav_7 + nav_8 + nav_9 + nav_10;
//        var sc_nav12 = nav_1 + nav_2 + nav_3 + nav_4 + nav_5 + nav_6 + nav_7 + nav_8 + nav_9 + nav_10 + nav_11;


        //console.log(nav_1 + nav_2 + 'px');
        var startPos = 0;
        var winScrollTop = 0;

        var nav_item = $('#o-rsingleSummary_list.__sp .item1');

        $(window).on('scroll',function(){
            winScrollTop = $(this).scrollTop();
            if (winScrollTop >= startPos) {
                //console.log('下');
                if( $('#o-rsingleSummary_list.__sp .item2 a').hasClass('navsync-menu-highlight') ){
                    nav_item.css('margin-left', '-' + nav_1 + 'px');
                }
                if( $('#o-rsingleSummary_list.__sp .item3 a').hasClass('navsync-menu-highlight') ){
                    nav_item.css('margin-left', '-' + sc_nav3 + 'px');
                }
                if( $('#o-rsingleSummary_list.__sp .item4 a').hasClass('navsync-menu-highlight') ){
                    nav_item.css('margin-left', '-' + sc_nav4 + 'px');
                }
                if( $('#o-rsingleSummary_list.__sp .item5 a').hasClass('navsync-menu-highlight') ){
                    nav_item.css('margin-left', '-' + sc_nav5 + 'px');
                }
                if( $('#o-rsingleSummary_list.__sp .item6 a').hasClass('navsync-menu-highlight') ){
                    nav_item.css('margin-left', '-' + sc_nav6 + 'px');
                }
                if( $('#o-rsingleSummary_list.__sp .item7 a').hasClass('navsync-menu-highlight') ){
                    nav_item.css('margin-left', '-' + sc_nav7 + 'px');
                }
                if( $('#o-rsingleSummary_list.__sp .item8 a').hasClass('navsync-menu-highlight') ){
                    nav_item.css('margin-left', '-' + sc_nav8 + 'px');
                }
                if( $('#o-rsingleSummary_list.__sp .item9 a').hasClass('navsync-menu-highlight') ){
                    nav_item.css('margin-left', '-' + sc_nav9 + 'px');
                }
                if( $('#o-rsingleSummary_list.__sp .item10 a').hasClass('navsync-menu-highlight') ){
                    nav_item.css('margin-left', '-' + sc_nav10 + 'px');
                }
                if( $('#o-rsingleSummary_list.__sp .item11 a').hasClass('navsync-menu-highlight') ){
                    nav_item.css('margin-left', '-' + sc_nav11 + 'px');
                }
                if( $('#o-rsingleSummary_list.__sp .item12 a').hasClass('navsync-menu-highlight') ){
                    nav_item.css('margin-left', '-' + sc_nav12 + 'px');
                }
            } else {
                //console.log('上');
                if( $('#o-rsingleSummary_list.__sp .item1 a').hasClass('navsync-menu-highlight') ){
                    nav_item.css('margin-left', '0');
                }
                if( $('#o-rsingleSummary_list.__sp .item2 a').hasClass('navsync-menu-highlight') ){
                    nav_item.css('margin-left', '-' + nav_1 + 'px');
                }
                if( $('#o-rsingleSummary_list.__sp .item3 a').hasClass('navsync-menu-highlight') ){
                    nav_item.css('margin-left', '-' + sc_nav3 + 'px');
                }
                if( $('#o-rsingleSummary_list.__sp .item4 a').hasClass('navsync-menu-highlight') ){
                    nav_item.css('margin-left', '-' + sc_nav4 + 'px');
                }
                if( $('#o-rsingleSummary_list.__sp .item5 a').hasClass('navsync-menu-highlight') ){
                    nav_item.css('margin-left', '-' + sc_nav5 + 'px');
                }
                if( $('#o-rsingleSummary_list.__sp .item6 a').hasClass('navsync-menu-highlight') ){
                    nav_item.css('margin-left', '-' + sc_nav6 + 'px');
                }
                if( $('#o-rsingleSummary_list.__sp .item7 a').hasClass('navsync-menu-highlight') ){
                    nav_item.css('margin-left', '-' + sc_nav7 + 'px');
                }
                if( $('#o-rsingleSummary_list.__sp .item8 a').hasClass('navsync-menu-highlight') ){
                    nav_item.css('margin-left', '-' + sc_nav8 + 'px');
                }
                if( $('#o-rsingleSummary_list.__sp .item9 a').hasClass('navsync-menu-highlight') ){
                    nav_item.css('margin-left', '-' + sc_nav9 + 'px');
                }
                if( $('#o-rsingleSummary_list.__sp .item10 a').hasClass('navsync-menu-highlight') ){
                    nav_item.css('margin-left', '-' + sc_nav10 + 'px');
                }
                if( $('#o-rsingleSummary_list.__sp .item11 a').hasClass('navsync-menu-highlight') ){
                    nav_item.css('margin-left', '-' + sc_nav11 + 'px');
                }
                if( $('#o-rsingleSummary_list.__sp .item12 a').hasClass('navsync-menu-highlight') ){
                    nav_item.css('margin-left', '-' + sc_nav12 + 'px');
                }
            }
            startPos = winScrollTop;
        });
    });

    //ヘッダー追尾
    $(function() {
        var $win = $(window),
            $header = $('header'),
            headerHeight = $header.outerHeight(),
            startPos = 0;

        $win.on('load scroll', function() {
            var value = $(this).scrollTop();
            if ( value > startPos && value > headerHeight ) {
                $header.css('top', '-' + headerHeight + 'px');
            } else {
                $header.css('top', '0');
            }
            startPos = value;
        });
    });

    //アニメーション
    $(window).on('scroll', function () {

        var elem = $('.o-animateMove');
        var isAnimate = 'is-shown';

        elem.each(function () {

            var elemOffset = $(this).offset().top;
            var scrollPos = $(window).scrollTop();
            var wh = $(window).height();

            if (scrollPos > elemOffset - wh + (wh / 4)) {
                $(this).addClass(isAnimate);
            }
        });

    });

    $(window).on('load', function() {
        // メッセージ欄隠す
        $('#fbMsg').hide();
        // 検討リスト処理
        $('.o-roomSave,.o-rsingleSave,.o-rsingleAction_functionItem.__save').on('click', function (e) {
           e.preventDefault();
           var pid = $(this).attr('data-pid');
           var fvflg = $(this).attr('data-fvflg');
           var act = 'stw_favorite_ajax';
           var childi = $(this).children('i');
           var me = $(this);
           if ( 'sumi' == fvflg ){
               act = 'stw_favorite_remove_ajax';
           }
           var detailPageFlg = false; // 物件ページなどの詳細ページか判別するフラグ
           // 【判別1】押されたボタンが「検討リストに追加するボタン」かどうかで判別。
           if ( $(this).hasClass('o-rsingleAction_functionItem') ){
               detailPageFlg = true;
           }
           // 【判別2】押されたボタンが詳細ページ上部「〜枚の画像」右側のボタンかどうかで判別。
           if ( $(this).hasClass('o-rsingleSave') ){
               detailPageFlg = true;
           }


           // ajax通信処理
           $.ajax({
               type: 'POST',
               url: ajaxurl,
               data: {
                   'action' : act,
                   'pid' : pid,
                   'postid' : pid // wpfp_do_remove_favoriteでcookie削除に必要
               }
           }).done(function(res){
               var cn = $('.l-headerNav_functionList a span').text(); // グロナビ検討リストの表示件数
               var pn = null; // タブのあるページの検討リスト表示件数
               if ( $('.p-mypageTab .save span').length ){
                   pn = $('.p-mypageTab .save span').text();
               }

               // アイコンと検討リスト格納フラグの切り替え
               if ( 'mi' == fvflg ){
                   // 詳細ページ上の3つのボタンを押したときは、他のボタンも切り替える
                   if ( detailPageFlg ){
                       // 登録済み状態を表すボタンに変更
                       $('.o-rsingleSave,.o-rsingleAction_functionItem.__save').children('i').removeClass('flaticon-heart');
                       $('.o-rsingleSave,.o-rsingleAction_functionItem.__save').children('i').addClass('flaticon-heart __checked');
                       // テキスト表示の2ボタンはテキストを変更
                       $('.o-rsingleAction_functionItem.__save .o-rsingleAction_functionText').text('検討リストから削除する');
                       // data-fvflgの変更
                       $('.o-rsingleSave,.o-rsingleAction_functionItem.__save').attr('data-fvflg','sumi');

                   } else {
                   // 詳細ページ上の3つのボタン以外の場合は、押された自身のボタンのみ切り替える
                       if ( $(childi).hasClass('flaticon-heart') ){
                           $(childi).removeClass('flaticon-heart');
                           $(childi).addClass('flaticon-heart __checked');
                       }
                       $(me).attr('data-fvflg','sumi');
                   }
                   $('#fbMsg').text('検討リストに追加しました。');
                   cn++;
                   if ( pn != null) {
                       pn++;
                   }

               }
               if ( 'sumi' == fvflg ){
                   // 詳細ページ上の3つのボタンを押したときは、他のボタンも切り替える
                   if ( detailPageFlg ){
                       // 未登録状態を表すボタンに変更
                       $('.o-rsingleSave,.o-rsingleAction_functionItem.__save').children('i').removeClass('flaticon-heart __checked');
                       $('.o-rsingleSave,.o-rsingleAction_functionItem.__save').children('i').addClass('flaticon-heart');
                       // テキスト表示の2ボタンはテキストを変更
                       $('.o-rsingleAction_functionItem.__save .o-rsingleAction_functionText').text('検討リストに追加する');
                       // data-fvflgの変更
                       $('.o-rsingleSave,.o-rsingleAction_functionItem.__save').attr('data-fvflg','mi');

                   } else {
                    // 詳細ページ上の3つのボタン以外の場合は、押された自身のボタンのみ切り替える
                       if ( $(childi).hasClass('flaticon-heart __checked') ){
                           $(childi).removeClass('flaticon-heart __checked');
                           $(childi).addClass('flaticon-heart');
                       }
                       $(me).attr('data-fvflg','mi');
                   }
                   $('#fbMsg').text('検討リストから削除しました。');
                   cn--;
                   if ( pn != null) {
                       pn--;
                   }
               }
               $('#fbMsg').fadeIn('slow',function(){
                   $(this).delay(3000).fadeOut('slow');
               });
               $('.l-headerNav_functionList a span').text(cn); // グロナビ検討リストの表示件数に処理済みの値
               if ( pn != null){
                   $('.p-mypageTab .save span').text(pn); // タブのあるページの検討リスト表示件数に処理済みの値
               }

           }).fail(function(er){
              console.log('fav ajax faild.');
              console.log(er);
           });
        });
    });

})(jQuery);




$(function() {
var jqOther = jQuery.noConflict(true);

if (jqOther('#mylist')&&jqOther('#mylist').length>0) {

    jqOther("a.permalink").each(function(i, o){
    var content=o.textContent;
    var a_href = jqOther(this).attr('href');
    jqOther('#mylist textarea.wpcf7-textarea').append(a_href+"\n"+content+"\n\n");

    });

    jqOther('a.wpfp-link.remove-parent').click(function() {
        setTimeout(function(){
        location.reload();
        },2000);
    });

}
if (jqOther('table.rooms a.wpfp-link')&&jqOther('table.rooms a.wpfp-link').length>0) {
    jqOther('table.rooms a.wpfp-link').click(function() {
        setTimeout(function(){
        location.reload();
        },2000);
    });
}

});


