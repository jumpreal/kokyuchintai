<?php
global $fvpids;

$fvpids = wpfp_get_users_favorites();
?>
<!doctype html>
<html lang="ja">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <?php the_meta_information(); ?>
    <?php wp_head(); ?>

    <link rel="stylesheet" type="text/css" href="<?php bloginfo('template_directory'); ?>/style.css">
    <link rel="stylesheet" type="text/css" href="<?php bloginfo('template_directory'); ?>/assets/css/common.css">
    <link rel="stylesheet" type="text/css" href="<?php bloginfo('template_directory'); ?>/assets/css/style2017.css">
    <link rel="stylesheet" type="text/css" href="<?php bloginfo('template_directory'); ?>/assets/css/corners2017.css">
    <link rel="stylesheet" type="text/css" href="<?php bloginfo('template_directory'); ?>/assets/css/body.css">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri();?>/assets/css/jquery.range.css">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri();?>/assets/css/modal-search.css">
    <link rel="stylesheet" type="text/css" href="<?php bloginfo('template_directory'); ?>/assets/font/flaticon.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/v4-shims.css">
    <link rel="stylesheet" type="text/css" href="<?php bloginfo('template_directory'); ?>/assets/css/style-child.css">
    <link rel="stylesheet" type="text/css" href="<?php bloginfo('template_directory'); ?>/assets/css/swiper.min.css">

    <link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/highlight.js/8.7/styles/default.min.css">

    <!--
<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_directory'); ?>/assets/css/style-stw.css">
<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_directory'); ?>/assets/css/style-stwsp.css">
-->




    <link rel="shortcut icon" href="<?php bloginfo('template_directory'); ?>/assets/images/common/favicon.ico">
    <link rel="apple-touch-icon" href="<?php bloginfo('template_directory'); ?>/assets/images/common/apple-touch-icon.ico">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="<?php bloginfo('template_directory'); ?>/assets/js/common.js"></script>
    <script src="<?php bloginfo('template_directory'); ?>/assets/js/favajax.js"></script>
    <script defer src="https://use.fontawesome.com/releases/v5.8.1/js/all.js" integrity="sha384-g5uSoOSBd7KkhAMlnQILrecXvzst9TdC09/VM+pjDTCM+1il8RHz5fKANTFFb+gQ" crossorigin="anonymous"></script>
    <?php
    $ksik = ( isset($_GET['ksik']) && !empty($_GET['ksik']) ) ? $_GET['ksik'] : '0';
    if ( is_array($ksik) ){
        $ksik = json_encode($ksik);
    } else {
        $ksik = '["' . $ksik . '"]';
    }
    $eki = ( isset($_GET['eki']) && !empty($_GET['eki']) ) ? $_GET['eki'] : '0';
    if ( is_array($eki) ){
        $eki = json_encode($eki);
    } else {
        $eki = '["' . $eki . '"]';
    }
    ?>
    <script>
    var getKsik = <?php echo $ksik;?>;
    var getEki = <?php echo $eki;?>;
    </script>

<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-NBGGPWZ');</script>
<!-- End Google Tag Manager -->

</head>

    <body <?php body_class(''); ?>>
<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-NBGGPWZ');</script>
<!-- End Google Tag Manager -->    
    <div id="fb-root"></div>
    <script async defer crossorigin="anonymous" src="https://connect.facebook.net/ja_JP/sdk.js#xfbml=1&version=v7.0" nonce="F0KOgt9i"></script>

    <nav class="l-headerHnav">
        <div class="l-headerHnav_wrap">

            <div class="l-headerBtn l-headerHnav_close"><span class="l-headerBtn_bar"><strong>MENU</strong></span></div>
            <div class="l-headerHnav_cont">
            <div class="l-headerHnav_box __search">
                <h3 class="l-headerHnav_title">SEARCH</h3>
                <a href=""  class="l-headerHnav_searchbtn js-modal  o-btn __sizexl __basic09 __p mb-1"><i class="o-icon flaticon-search"></i>物件を検索する</a>
                <div class="l-headerHnav_tab o-tab __basic03">
                    <a href="<?php bloginfo('url'); ?>/myhistory" class="l-headerHnav_tabItem history o-tabItem" ><i class="o-icon flaticon-history"></i>最近見た物件<span class="l-headerHnav_tabItem_notification o-partsNotification stock __sizes"><?php echo stw_count_his();?></span></a>
                    <a href="<?php bloginfo('url'); ?>/mylist" class="l-headerHnav_tabItem save o-tabItem" class=""><i class="o-icon flaticon-love"></i>検討リスト<span class="l-headerHnav_tabItem_notification o-partsNotification stock __sizes"><?php echo @get_mylist();?></span></a>
                    <?php $sc = ( isset($_COOKIE['searchSaveList']) && !empty($_COOKIE['searchSaveList']) ) ? json_decode(stripcslashes($_COOKIE['searchSaveList'])) : null; $sc_con = ( is_null($sc) ) ? 0 : count($sc);?>
                    <a href="<?php bloginfo('url'); ?>/myterms" class="l-headerHnav_tabItem terms o-tabItem" ><i class="o-icon flaticon-download"></i>保存した検索条件<span class="l-headerHnav_tabItem_notification o-partsNotification stock __sizes searchCount"><?php echo $sc_con;?></span></a>
                </div>
            </div>

            <div class="l-headerHnav_box __search">
                <h3 class="l-headerHnav_title">BLOG</h3>
                <div class="l-contBlog_cont flex-w">

                    <?php
                    // posts_per_pageで取得件数の指定、orderbyでソート順を新着順にしています。
                    $args = array('post_type'=>'post', 'posts_per_page' => 2, 'orderby' => 'date');
                    $query = new WP_Query($args);
                    ?>
                    <?php if( $query->have_posts() ) : ?>

                    <?php while ($query->have_posts()) : $query->the_post(); ?>
                    <article class="l-contBlog_post o-post __2 __white">
                        <div class="o-postWrap">
                            <div class="o-postThumb">
                                <?php if (has_post_thumbnail()) :
                                // アイキャッチ画像のIDを取得
                                $thumbnail_id = get_post_thumbnail_id();

                                // mediumサイズの画像内容を取得（引数にmediumをセット）
                                $eye_image_array = wp_get_attachment_image_src( $thumbnail_id , 'medium' );
                                $eye_image = $eye_image_array[0];
                                else :
                                $eye_image = "https://koukyu-chintai.com/wp-content/uploads/cropped-cropped-main-1-2000x1200-1-1024x614.jpg";
                                endif ; ?>
                                <img class="l-contCompany_img" src="<?php echo $eye_image; ?>" alt="">
                            </div>
                            <div class="o-postHeader">
                                <ul class="o-postData flex-nw jc-between">
                                    <?php
                                    $category = get_the_category();
                                    $cat_id   = $category[0]->cat_ID;
                                    $cat_name = $category[0]->cat_name;
                                    ?>
                                    <li class="o-postCat"><a href=""><?php echo $cat_name; ?></a></li>
                                    <li class="o-postDate"><?php the_time('Y/m/d'); ?></li>
                                </ul>
                                <h3 class="o-postTitle"><?php the_title(); ?></h3>
                            </div>

                            <a href="<?php the_permalink();?>" class="o-link __over"></a>
                        </div>
                    </article>

                    <?php endwhile; ?>

                    <?php endif; ?>
                    <?php wp_reset_postdata(); ?>

                </div>
                <a href="<?php bloginfo('url'); ?>/category/staffblog/" class="l-contNewestate_link o-link __mid __white __text type01">新着一覧を見る<span class="o-linkItem"></span></a>
                </section>
            </div>

            <div class="l-headerHnav_box flex-w jc-between">
                <div class="flex-nw-no-md jc-between">
                    <nav class="l-headerHnav_listCont service">
                        <h4 class="l-headerHnav_listTitle o-title __mid __white __en">SERVICE</h4>
                        <?php
                        wp_nav_menu( array(
                            'theme_location' => 'footer_service',
                            'container' => 'false',  //コンテナのデフォルト <div> を <nav> に変更
                            'menu_class' => 'l-footerNav_list o-list __mid __gray01 type01',  //デフォルトの menu-{メニュー名} を変更
                        ) );
                        ?>
                    </nav>
                    <nav class="l-headerHnav_listCont contact">
                        <h4 class="l-headerHnav_listTitle o-title __mid __white __en">CONTACT</h4>
                        <?php
                        wp_nav_menu( array(
                            'theme_location' => 'footer_contact',
                            'container' => 'false',  //コンテナのデフォルト <div> を <nav> に変更
                            'menu_class' => 'l-footerNav_list o-list __mid __gray01 type01',  //デフォルトの menu-{メニュー名} を変更
                        ) );
                        ?>
                    </nav>
                    <nav class="l-headerHnav_listCont company">
                        <h4 class="l-headerHnav_listTitle o-title __mid __white __en">COMPANY</h4>
                        <?php
                        wp_nav_menu( array(
                            'theme_location' => 'footer_company',
                            'container' => 'false',  //コンテナのデフォルト <div> を <nav> に変更
                            'menu_class' => 'l-footerNav_list o-list __mid __gray01 type01',  //デフォルトの menu-{メニュー名} を変更
                        ) );
                        ?>
                    </nav>
                </div>





            </div>
            <div class="l-headerHnav_contact">
                <div class="l-headerHnav_contactTel o-text __title __bold __white __en"><?php the_field('free_dial', 'option'); ?><span class="l-headerHnav_contactHour"><?php the_field('business_holiday', 'option'); ?></span></div>
                <div class="l-headerHnav_contactSns flex-nw">
                    <a class="l-headerHnav_contactSns_item fb" href="<?php the_field('sns_fb', 'option'); ?>" target="_blank" rel="nofollow"><i class="fab fa-facebook-square"></i>
                    </a>
                    <a class="l-headerHnav_contactSns_item tw" href="<?php the_field('sns_tw', 'option'); ?>" target="_blank" rel="nofollow"><i class="fab fa-twitter"></i></a>
                </div>
            </div>
            </div>
        </div><!--gnav-wrap-->
    </nav>


    <header class="l-header">
        <div class="l-wrap header flex-w jc-between">
            <div class="flex-w ai-center">
                <div class="l-headerBtn"><span class="l-headerBtn_bar"><strong>MENU</strong></span></div>
                <h1 class="l-headerLogo l-headerCont flex-w ai-center">
                    <a href="<?php bloginfo('url'); ?>" class="l-headerLogo_link"><img src="<?php bloginfo('template_directory'); ?>/assets/images/common/logo.png" alt="高級賃貸東京" class="l-headerLogo_img"></a>
                    <?php
        $fudo_pages = wp_count_posts('fudo');
        $fudo_count = $fudo_pages->publish;

        $room_pages = wp_count_posts('room');
        $room_count = $room_pages->publish;

        // global $wpdb;
        // $query = "SELECT count(DISTINCT P2.ID) AS co FROM (((((((((((kctwp_posts AS P INNER JOIN kctwp_postmeta AS PM ON P.ID = PM.post_id) INNER JOIN kctwp_postmeta AS PM_1 ON P.ID = PM_1.post_id) ) ) )) ))) ) LEFT JOIN kctwp_postmeta AS PM_99 ON PM_1.meta_value = PM_99.meta_value LEFT JOIN kctwp_postmeta AS PM_999 ON PM_99.post_id = PM_999.post_id LEFT JOIN kctwp_posts AS P2 ON PM_999.post_id = P2.ID ) LEFT JOIN kctwp_postmeta AS PM_888 ON P.ID = PM_888.post_id WHERE ( P.post_status='publish' AND P.post_password = '' AND (P.post_type ='fudo' OR P.post_type ='room') AND PM.meta_key='bukkenshubetsu' AND CAST(PM.meta_value AS SIGNED) > 0 AND PM_1.meta_key = 'shikibesu' AND PM_888.meta_key='kakaku' AND (CASE WHEN P.post_type = 'fudo' THEN P.ID = PM_99.post_id ELSE P.ID <> PM_99.post_id END) AND P2.post_type = 'fudo' AND P2.post_status = 'publish' )";
        // $select_result = $wpdb->get_row($query);
        // $fudo_count = $select_result->co;
        if (DEBUGFLG) echo "<p>" . __FILE__ . "の" . __LINE__ . "行目</p>";
        ?>
                    <p class="l-headerNumber">高級物件数<span class="l-headerNumber_ttl"><strong class="l-headerNumber_num">
                                <?php echo $fudo_count;?></strong>件</span></p>
                </h1>
            </div>
            <nav class="l-headerNav flex-w">
                <?php
        wp_nav_menu( array(
            'theme_location' => 'header_page_menu',
            'container' => 'false',  //コンテナのデフォルト <div> を <nav> に変更
            'menu_class' => 'l-headerNav_page flex-w ai-center',  //デフォルトの menu-{メニュー名} を変更
        ) );
        ?>

                <?php
    $user = isset($_REQUEST['user']) ? $_REQUEST['user'] : "";
    $count = 0;
    global $favorite_post_ids;
    if ( !empty($user) ) {
        if ( wpfp_is_user_favlist_public($user) )
            $favorite_post_ids = wpfp_get_users_favorites($user);
    } else {
        $favorite_post_ids = wpfp_get_users_favorites();
        $count = count($favorite_post_ids);
        $post_ids = implode(",", $favorite_post_ids);
    }
        // print_r($favorite_post_ids);
    // 検索保存件数
    $sc = ( isset($_COOKIE['searchSaveList']) && !empty($_COOKIE['searchSaveList']) ) ? json_decode(stripcslashes($_COOKIE['searchSaveList'])) : null;
    $sc_con = ( is_null($sc) ) ? 0 : count($sc);
?>

                <ul class="l-headerNav_function flex-w ai-center" style="user-select: auto;">
                    <li class="l-headerNav_functionSearch"><a title="l-headerNav_functionSearch" href="<?php echo home_url();?>/">物件検索</a></li>
                    <li class="l-headerNav_functionList current-menu-item">
                        <a href="<?php echo home_url();?>/mylist/" aria-current="page">検討リスト

                            <span class="p-mypageTab_notification o-partsNotification stock __sizes __top">
                                <?php echo $count;?></span>
                        </a>
                    </li>
                    <li class="l-headerNav_functionSave">
                        <a href="<?php echo home_url();?>/myterms">検索保存
                            <span class="p-mypageTab_notification o-partsNotification stock __sizes __top searchCount">
                                <?php echo $sc_con;?></span>
                        </a>


                    </li>
                    <li class="l-headerNav_functionHistory">
                        <a href="<?php echo home_url();?>/myhistory">閲覧履歴
                            <span class="p-mypageTab_notification o-partsNotification stock __sizes __top">
                                <?php echo stw_count_his();?></span>
                        </a>
                    </li>
                </ul>
            </nav>
        </div>

    </header>

    <p id="fbMsg" class="o-partsNotice"></p>



    <div id="page" class="site">


        <div class="l-wrap main">
