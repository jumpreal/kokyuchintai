<?php
/*
Template Name:
*/
?>
<?php get_header();?>



<div id="<?php echo $post->post_name;?>" class="p-mypage">
    <main id="main" class="site-main" role="main">

        <div class="o-fv __page mb-3">
            <img src="https://koukyu-chintai.com/wp-content/uploads/cropped-cropped-main-1-2000x1200-1-1024x614.jpg" alt="" class="o-fvImg" loading="lazy">
            <div class="o-fvCatch">
                <h2 class="o-fvCatch_title o-title __large __white __nobold">
                    <?php the_title(); ?>
                </h2>
            </div>
        </div>


        <section class="l-wrap __maxwsmall">
        <section class="p-postCont">
            <p class="u-text-ac">10日間までに見た物件が保存されます。</p>
        </section>
        </section>

        <article class="ul-flex">
            <section class="ul-contents">
                <h2 class="ul-title mincho">高級賃貸東京について</h2>
                <section class="about01">
                    <div class="about-box">
                        <h3 class="ul-title01 mincho">高級賃貸東京とは</h3>

                        <p>
                            <?php echo nl2br(get_field("about-text01"));?>
                        </p>
                        <p class="img"><img src="<?php bloginfo('template_directory'); ?>/assets/images/about/about01.png" alt=""></p>
                    </div>

                    <div class="about-box">
                        <h3 class="ul-title01 mincho">高級賃貸東京の店舗について</h3>
                        <p>
                            <?php echo nl2br(get_field("about-text02"));?>
                        </p>
                        <p class="img"><img src="<?php bloginfo('template_directory'); ?>/assets/images/about/about02.png" alt=""></p>
                    </div>
                </section><!-- .about01 -->
            </section><!-- .ul-contents -->

            <?php get_sidebar(); ?>
        </article><!-- .ul-flex -->

    </main><!-- #main -->
</div><!-- #about -->

<?php get_footer(); ?>
