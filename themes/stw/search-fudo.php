<form role="search" method="get" class="o-form search flex-nw ai-center search-form" action="<?php echo esc_url( home_url( '/' ) ); ?>">
    <input type="text" value="" name="s" class="o-formInput text __reset" placeholder="例) 銀座 駅近" />
    <button type="submit" class="o-btn search"><i class="o-icon flaticon-search"></i></button>
</form>
