<?php

/**

 * The Template for displaying fudou archive posts.

 *

 * Template: archive-fudo.php

 *

 * @package WordPress4.9

 * @subpackage Fudousan Plugin

 * @subpackage Twenty Ten - Thirteen

 * Version: 1.9.6

 */





    /**

     * Extends the default WordPress body class 'sidebar' to archive-fudo:

     * @since Twenty Thirteen 1.0

     * @return array Filtered class values.

     */

    function twentythirteen_body_class_add_sidebar( $classes ) {

        $classes[] = 'sidebar';

        return $classes;

    }

    if( get_option('template') == 'twentythirteen' ){

        add_filter( 'body_class', 'twentythirteen_body_class_add_sidebar' );

    }





    /**** 検索 SQL ****/

    //require_once WP_PLUGIN_DIR . '/fudou/inc/inc-archive-fudo.php';

    require_once get_template_directory().'/inc/inc-archive-fudo.php';






    //検索結果の物件カウント

        $metas_co = 0;

        if($sql !=''){

            //$sql = $wpdb->prepare($sql,'');
            if(DEBUGFLG) {
            echo '<br/><br/><br/><br/><br/><br/><br/><br/>';
            the_sql($sql);
            }
            /**
             * 2020-07-10 09:36:33
             * 検索結果の数が合わなかったのを修正
             *
             */
            //$sql = str_replace("count(DISTINCT P.ID)", "count(DISTINCT P2.ID)", $sql);

            /**
             * シンプルに公開済み物件のみで考える形式
             */
            /*
            $sql = "select distinct(A.p2p_to) as `co` from kctwp_p2p A left join kctwp_posts B on B.ID = A.p2p_to
            left join kctwp_postmeta PM on B.ID = PM.post_id
            where (B.post_type=\"fudo\" or B.post_type=\"room\") and B.post_status = \"publish\" and PM.meta_key = \"kakaku\" order by cast(PM.meta_value as signed) ASC";
            */

            $metas = $wpdb->get_row( $sql );

            if( !empty( $metas ) ){

                $metas_co = $metas->co;

            }

        }else{

            $metas_co = 0;

        }







    //ソート・ページナビ ver1.9.6

        $page_navigation = '';



        //条件検索URL

        if($bukken_slug_data=="jsearch"){



            //url生成



            //間取り

            $madori_url = '';

            if(!empty($madori_id)) {

                $i=0;

                foreach($madori_id as $meta_box){

                    $madori_url .= '&amp;mad%5B%5D='.$madori_id[$i];

                    $i++;

                }

            }


            //共用条件

            $setsubi_url = '';

            if(!empty($set_id2)) {

                $i=0;

                foreach($set_id2 as $meta_box){

                    $setsubi_url .= '&amp;set2%5B%5D='.$set_id2[$i];

                    $i++;

                }

            }

            //設備条件

            $setsubi_url = '';

            if(!empty($set_id)) {

                $i=0;

                foreach($set_id as $meta_box){

                    $setsubi_url .= '&amp;set%5B%5D='.$set_id[$i];

                    $i++;

                }

            }



            $add_url  = '';



            //複数種別

            if( $shub !='' ) $add_url  .= '&amp;shub='.$shub;



            if (is_array($bukken_shubetsu)) {

                $i=0;

                foreach($bukken_shubetsu as $meta_set){

                    $add_url  .= '&amp;shu%5B%5D='.$bukken_shubetsu[$i];

                    $i++;

                }



            } else {

                $add_url  .= '&amp;shu='.$bukken_shubetsu;

            }





            $add_url .= '&amp;ros='. $ros_id;

            $add_url .= '&amp;eki='. $eki_id;

            $add_url .= apply_filters( 'fudoubus_add_url_archive', '' );



            if($ken_id == '00') $ken_id = '0';

            $add_url .= '&amp;ken='. $ken_id;

            $add_url .= '&amp;sik='. $sik_id;

            $add_url .= '&amp;kalc='.$kalc_data;

            $add_url .= '&amp;kahc='.$kahc_data;

            $add_url .= '&amp;kalb='.$kalb_data;

            $add_url .= '&amp;kahb='.$kahb_data;

            $add_url .= '&amp;hof='. $hof_data;

            $add_url .= $madori_url;

            $add_url .= '&amp;tik='. $tik_data;

            $add_url .= '&amp;mel='. $mel_data;

            $add_url .= '&amp;meh='. $meh_data;

            $add_url .= $setsubi_url;



            $joken_url  = $site .'?bukken=jsearch';





            //複数市区

            if (is_array($ksik_id)) {

                $i=0;

                foreach($ksik_id as $meta_set){

                    $add_url .= '&amp;ksik%5B%5D='.$ksik_id[$i];

                    $i++;

                }

            }



            //複数駅

            if(is_array( $rosen_eki )  ){

                $i=0;

                foreach($rosen_eki as $meta_set){

                    $add_url .= '&amp;re%5B%5D='.$rosen_eki[$i];

                    $i++;

                }

            }



            //オリジナルフィルター $joken_url ver1.9.1

            $joken_url = apply_filters( 'fudou_org_joken_url_archive', $joken_url );



            //オリジナルフィルター $add_url

            $add_url = apply_filters( 'fudou_org_add_url_archive', $add_url );



            $joken_url .= $add_url;

            if( $s != '' ){

                $s_tag = '&amp;s=' . $s;

            }else{

                $s_tag = '';

            }



        }else{

            //物件カテゴリ・物件タグ

            if( $taxonomy_name == 'bukken_tag' ){

                $joken_url = $site.'?bukken_tag='.$slug_data.'';

            }else{

                $joken_url = $site.'?bukken='.$slug_data.'';

            }



            //キーワード

            if( $s != '' ){

                $joken_url  = $site .'?s='.$s.'&bukken=search';



                if($searchtype == 'id')

                    $joken_url  .= '&amp;st=id';



                if($searchtype == 'chou')

                    $joken_url  .= '&amp;st=chou';

            }





            $bukken = isset( $_GET['bukken'] ) ? $_GET['bukken'] : '';

            $bukken_slug_data = esc_attr( stripslashes( $bukken ));

            $add_url  = '&amp;bk='.$bukken;



            $add_url .= '&amp;shu='.$bukken_shubetsu;



            if($mid_id=='99999')    $mid_id = "";

            $add_url .= '&amp;mid='.$mid_id;



            if($nor_id=='99999')    $nor_id = "";

            $add_url .= '&amp;nor='.$nor_id;

            $add_url .= apply_filters( 'fudoubus_add_url_archive', '' );



            //オリジナルフィルター $joken_url ver1.9.1

            $joken_url = apply_filters( 'fudou_org_joken_url_archive', $joken_url );



            //オリジナルフィルター $add_url

            $add_url = apply_filters( 'fudou_org_add_url_archive', $add_url );



            if ($taxonomy_name == '') $joken_url .= $add_url;



            $s_tag = '';



        }



        //ソートORDER 画像・ページナビ用 ver1.9.6

        global $page_navi_type;

        $page_navi_type = '1.9.6';



        /*

         * bukken_order2 Fix

         *

         * inc-archive-fudo.php

         * add_filter( 'bukken_order_reversal', 'bukken_order_reversal' )

         * ver1.9.6

         */

        $bukken_order2 = apply_filters( 'bukken_order_reversal', $bukken_order );



        //物件がある場合

        if( $metas_co != 0 ){



            //ソート用画像



            //価格画像

            $kak_img = '<img src="'.$plugin_url.'img/sortbtms_.png" border="0" align="absmiddle">';

            if($bukken_sort == 'kak' && $bukken_order2 =='')

                $kak_img = '<img src="'.$plugin_url.'img/sortbtms_asc.png" border="0" align="absmiddle">';

            if($bukken_sort=='kak' && $bukken_order2 =='d')

                $kak_img = '<img src="'.$plugin_url.'img/sortbtms_desc.png" border="0" align="absmiddle">';



            if($bukken_sort_data2 == "post_modified" && $bukken_sort == '')

                $kak_img = '<img src="'.$plugin_url.'img/sortbtms_.png" border="0" align="absmiddle">';



            //面積

            $tam_img = '<img src="'.$plugin_url.'img/sortbtms_.png" border="0" align="absmiddle">';

            if($bukken_sort=='tam' && $bukken_order2 =='')

                $tam_img = '<img src="'.$plugin_url.'img/sortbtms_asc.png" border="0" align="absmiddle">';



            if($bukken_sort=='tam' && $bukken_order2 =='d')

                $tam_img = '<img src="'.$plugin_url.'img/sortbtms_desc.png" border="0" align="absmiddle">';



            //間取り

            $mad_img = '<img src="'.$plugin_url.'img/sortbtms_.png" border="0" align="absmiddle">';

            if($bukken_sort=='mad' && $bukken_order2 =='')

                $mad_img = '<img src="'.$plugin_url.'img/sortbtms_asc.png" border="0" align="absmiddle">';

            if($bukken_sort=='mad' && $bukken_order2 =='d')

                $mad_img = '<img src="'.$plugin_url.'img/sortbtms_desc.png" border="0" align="absmiddle">';





            //住所

            $sho_img = '<img src="'.$plugin_url.'img/sortbtms_.png" border="0" align="absmiddle">';

            if($bukken_sort=='sho' && $bukken_order2 =='')

                $sho_img = '<img src="'.$plugin_url.'img/sortbtms_asc.png" border="0" align="absmiddle">';

            if($bukken_sort=='sho' && $bukken_order2 =='d')

                $sho_img = '<img src="'.$plugin_url.'img/sortbtms_desc.png" border="0" align="absmiddle">';





            //築年月

            $tac_img = '<img src="'.$plugin_url.'img/sortbtms_.png" border="0" align="absmiddle">';

            if($bukken_sort=='tac' && $bukken_order2 =='')

                $tac_img = '<img src="'.$plugin_url.'img/sortbtms_asc.png" border="0" align="absmiddle">';

            if($bukken_sort=='tac' && $bukken_order2 =='d')

                $tac_img = '<img src="'.$plugin_url.'img/sortbtms_desc.png" border="0" align="absmiddle">';





            $page_navigation = '';

            // $page_navigation .= '<div class="nav-previous">';





            //価格

            // if($bukken_sort=='kak'){

            //     $page_navigation .= '<b><a class="kak" href="'.$joken_url.'&amp;so=kak&amp;ord='.$bukken_order.$s_tag.'">'.$kak_img.'価格</a></b> ';
            // }else{

            //     $page_navigation .= '<a class="kak" href="'.$joken_url.'&amp;so=kak&amp;ord='. $s_tag.'">'.$kak_img.'価格</a> ';

            // }


            //並べ替えボックス用の<option>値生成
            $selected_1 ="";
            $selected_2 ="";
            $selected_3 ="";
            $selected_4 ="";
            $selected_5 ="";
            $selected_6 ="";

            if($bukken_sort == 'kak' && $bukken_order2 =='d')//安い順
                $selected_1 = 'selected="selected"';
            if($bukken_sort=='kak' && $bukken_order2 =='')
                $selected_2 = 'selected="selected"';
            if($bukken_sort=='tam' && $bukken_order2 =='d')
                $selected_3 = 'selected="selected"';
            if($bukken_sort=='tam' && $bukken_order2 =='')
                $selected_4 = 'selected="selected"';
            if($bukken_sort=='tac' && $bukken_order2 =='d')
                $selected_5 = 'selected="selected"';
            if($bukken_sort=='tac' && $bukken_order2 =='')
                $selected_6 = 'selected="selected"';



            // $joken_urlは検索ロジックによって上記で設定されたものは不整合がでる。単純にアクセスされたURLを出せば良い。
            if ( isset($_GET['so']) ){
                // ソート条件がURLにある場合は、URIから&soとそれ以降の文字を削除したものを出す。
                $joken_url = home_url() . strstr($_SERVER['REQUEST_URI'], '&so=', true);
            } else {
                // ソート条件がない場合は、そのままURIをつける。
                $joken_url = home_url() . $_SERVER['REQUEST_URI'];
            }

            //var_dump('test2');
            //var_dump(strstr($_SERVER['REQUEST_URI'], '&so=', true));

            $p_navi = '';
            $p_navi .=  '<option ' . $selected_2 . 'value="' . $joken_url.'&amp;so=kak&amp;ord=a'.$s_tag .'">価格の安い順</option>';
            $p_navi .=  '<option ' . $selected_1 . 'value="' . $joken_url.'&amp;so=kak&amp;ord=d'.$s_tag . '">価格の高い順</option>';
            $p_navi .=  '<option ' . $selected_4 . 'value="' . $joken_url.'&amp;so=tam&amp;ord=a'.$s_tag . '">面積の狭い順</option>';
            $p_navi .=  '<option ' . $selected_3 . 'value="' . $joken_url.'&amp;so=tam&amp;ord=d'.$s_tag . '">面積の広い順</option>';
            $p_navi .=  '<option ' . $selected_5 . 'value="' . $joken_url.'&amp;so=tac&amp;ord=a'.$s_tag . '">築年数の古い順</option>';
            $p_navi .=  '<option ' . $selected_6 . 'value="' . $joken_url.'&amp;so=tac&amp;ord=d'.$s_tag . '">築年数の浅い順</option>';


            //面積

            // if($bukken_sort=='tam'){

            //     $page_navigation .= '<b><a class="tam" href="'.$joken_url.'&amp;so=tam&amp;ord='.$bukken_order.$s_tag.'">'.$tam_img.'面積</a></b> ';

            // }else{

            //     $page_navigation .= '<a class="tam" href="'.$joken_url.'&amp;so=tam&amp;ord=' . $s_tag.'">'.$tam_img.'面積</a> ';

            // }



            // //間取り

            // if($bukken_sort=='mad'){

            //     $page_navigation .= '<b><a class="mad" href="'.$joken_url.'&amp;so=mad&amp;ord='.$bukken_order.$s_tag.'">'.$mad_img.'間取</a></b> ';

            // }else{

            //     $page_navigation .= '<a class="mad" href="'.$joken_url.'&amp;so=mad&amp;ord=' . $s_tag.'">'.$mad_img.'間取</a> ';

            // }



            // //住所

            // if($bukken_sort=='sho'){

            //     $page_navigation .= '<b><a class="sho" href="'.$joken_url.'&amp;so=sho&amp;ord='.$bukken_order.$s_tag.'">'.$sho_img.'住所</a></b> ';

            // }else{

            //     $page_navigation .= '<a class="sho" href="'.$joken_url.'&amp;so=sho&amp;ord=' . $s_tag.'">'.$sho_img.'住所</a> ';

            // }



            //築年月

            // if($bukken_sort=='tac'){

            //     $page_navigation .= '<b><a class="tac" href="'.$joken_url.'&amp;so=tac&amp;ord='.$bukken_order.$s_tag.'">'.$tac_img.'築年月</a></b> ';

            // }else{

            //     $page_navigation .= '<a class="tac" href="'.$joken_url.'&amp;so=tac&amp;ord=' . $s_tag.'">'.$tac_img.'築年月</a> ';

            // }



            /*

             * 追加 物件ソート用タグ

             *

             * @since Fudousan Plugin 1.7.8

             * For archive-fudoXXXX.php apply_filters( 'fudou_archive_navigation', $page_navigation, $bukken_sort, $joken_url, $bukken_page_data, $bukken_order, $s_tag );

            */

            // $page_navigation = apply_filters( 'fudou_archive_navigation', $page_navigation, $bukken_sort, $joken_url, $bukken_page_data, $bukken_order, $s_tag );





            // $page_navigation .= '</div>';

            $page_navigation .= '';





            //ページナビ

            $page_navigation .= f_page_navi($metas_co,$posts_per_page,$bukken_page_data,$bukken_sort,$bukken_order2,$s,$joken_url);



            $page_navigation .= '';

            $page_navigation .= '';



        }    //物件がある場合



    // .ソート・ページナビ





        //パーマリンクチェック

        $permalink_structure = get_option('permalink_structure');

        if ( $permalink_structure != '' ) {

            $add_url_point = mb_strlen( $add_url, "utf-8" ) ;

            if( $add_url_point > 5 ){

                $add_url_point = $add_url_point - 5;

                $add_url = '?' . myRight( $add_url, $add_url_point ) ;

            }else{

                $add_url = '';

            }

        }







    //物件一覧ページ

    get_header();

?>


<!--<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_directory'); ?>/assets/css/archive.css">-->

<div class="o-fv __archive" style="background-image:url(
                                   <?php
                                   // アイキャッチ画像が設定されているかチェック
                                   if(has_post_thumbnail()){
                                       // アイキャッチ画像を表示する
                                       the_post_thumbnail_url( get_the_ID(),'full' );
                                   }else{
                                       // 代替画像を表示する
                                       the_field('archive-mvimg', 'option');
                                   }
                                   ?>
                                   )">
    <?php the_custom_header_markup(); ?>
    <div class="o-fvCatch">
        <h2 class="o-fvCatch_title o-title __huge mb-1">検索結果一覧</h2>
        <div class="o-fvCatch_result"><span class="o-fvCatch_num o-text __bold __en"><?php echo $metas_co;?></span>件 見つかりました。</div>
    </div>
    <div class="o-fvBtn flex-nw jc-end ai-center">
        <a href="" class="o-fvBtn_item __save o-btn __basic02 modalSearchSave" data-searchform="direct" data-url="<?php echo home_url() . $_SERVER['REQUEST_URI'];?>"><i class="o-icon __mid flaticon-download"></i>この条件を保存する</a>
        <a id="js-saveModal" class="o-fvBtn_item __term o-btn __basic10"><i class="o-icon __mid __white flaticon-check-mark"></i>保存した条件から探す</a>
    </div>
</div>

<div id="primary" class="content-area">
    <main id="main" class="site-main" role="main">

        <div class="l-main">
            <div class="l-contRoomlist">
                <div class="l-wrap flex-nw jc-between">
                    <div class="o-search side l-sideSearch l-rowLeft search">
                        <?php include('template/side-search.php');?>

                    </div>
                    <div class="l-rowRight searchmain __result">
                        <section class="l-cont roomlist">


                            <div class="l-contRoomlist_term mb-3">
                                <div class="l-contRoomlist_termWrap __area flex-nw">
                                    <div class="l-contRoomlist_termTitle">選択した条件</div>
                                    <div class="l-contRoomlist_termDesc">
                                        <div class="l-contRoomlist_termDesc_wrap flex-w">
                                            <span class="l-contRoomlist_termDesc_item">
                                                <?php
                                                $area_res = ( isset($_GET['ksik']) && !empty($_GET['ksik']) ) ? $_GET['ksik'] : null;
                                                $koda_res = ( isset($_GET['koda']) && !empty($_GET['koda']) ) ? $_GET['koda'] : null;
                                                ?>

                                                <?php
                                                //サイドバーの検索条件を文字列にて表示
                                                include get_template_directory().'/inc/inc-search-title.php';
                                                echo $search_result_title;
                                                ?>
                                            </span>

                                        </div>
                                    </div>
                                </div>
                            </div>





                            <div class="l-contRoomlist_function flex-nw  mb-3">
                                <div class="l-contRoomlist_functionSelect o-formSelect_wrap">
                                    <select class="l-contRoomlist_functionSelect_form o-formSelect __white __sizem __bold __mid" name="example2" size="1"  onChange="location.href=value;">
                                        <?php echo $p_navi;?>
                                    </select>
                                </div>
                                <div class="o-pagenationSelect l-contRoomlist_functionPagenation flex-nw">



                                    <?php //--並べ替えのナビ
                                    echo $page_navigation; ?>

                                </div>
                            </div>


                            <?php //do_action( 'archive-fudo1' ); ?>








                                <?php
                                //loop SQL


                                if($sql !=''){
                                    // SQL2は下記で作成
                                    /// themes/stw/inc/inc-archive-fudo_sql.php

                                    // $sql2 = "select ID as object_id from " . $wpdb->prefix . "posts where post_type=\"fudo\" AND post_status = \"publish\" limit 0,10";
                                    if (DEBUGFLG) echo $sql2;
                                    //$sql2 = $wpdb->prepare($sql2,'');
                                    $metas = $wpdb->get_results( $sql2, ARRAY_A );

                                    if(!empty($metas)) {

                                        foreach ( $metas as $meta ) {

                                            $meta_id = $meta['object_id'];    //post_id
                                            // echo $meta_id;


                                            //--これを独自loopに差し替える
                                            //    require 'archive-fudo-loop.php';
                                            if(DEBUGFLG) echo "<p>" . __FILE__ . "の" . __LINE__ . "行目</p>";
                                            require 'archive-fudo-loop-stw.php';

                                        }

                                    }else{

                                        echo "物件がありませんでした。<br /><br />";

                                    }

                                }else{

                                    echo "条件があいませんでした。<br /><br />";

                                }

                                //loop SQL END

                                ?>

                            <?php do_action( 'archive-fudo2' ); ?>

                            <div class="l-contRoomlist_function flex-nw  mb-3">
                                <div class="l-contRoomlist_functionSelect o-formSelect_wrap">
                                    <select class="l-contRoomlist_functionSelect_form o-formSelect __white __sizem __bold __mid" name="example2" size="1"  onChange="location.href=value;">
                                        <?php echo $p_navi;?>
                                    </select>
                                </div>
                                <div class="o-pagenationSelect l-contRoomlist_functionPagenation flex-nw">



                                    <?php //--並べ替えのナビ
                                    echo $page_navigation; ?>

                                </div>
                            </div>

                        </section>
                    </div>
                </div>
            </div>
        </div>



    </main><!-- #main -->
</div><!-- #primary -->

<?php get_footer('top'); ?>

<?php include('template/fixednav1.php');?>
