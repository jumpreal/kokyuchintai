<?php
/*
Template Name: 検索履歴
Template Post Type: post,page
*/
$sc = ( isset($_COOKIE['searchSaveList']) && !empty($_COOKIE['searchSaveList']) ) ? json_decode(stripcslashes($_COOKIE['searchSaveList'])) : null;
$sc_con = ( is_null($sc) ) ? 0 : count($sc);

?>
<?php get_header(); ?>


<!--<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_directory'); ?>/assets/css/page.css">-->


<div id="<?php echo $post->post_name;?>" class="p-mypage">
    <main id="main" class="site-main" role="main">

        <div class="o-fv __page mb-0">
            <img src="https://koukyu-chintai.com/wp-content/uploads/cropped-cropped-main-1-2000x1200-1-1024x614.jpg" alt="" class="o-fvImg" loading="lazy">
            <div class="o-fvCatch">
                <h2 class="o-fvCatch_title o-title __large __white __nobold">
                    <?php the_title(); ?>
                </h2>
            </div>
        </div>
        <section class="l-wrap __maxwsmall">
            <div class="p-mypageTab o-tab __basic01 mb-6">
                <a class="p-mypageTab_item history __active o-tabItem" ><i class="o-icon flaticon-history"></i>最近見た物件<span class="p-mypageTab_notification o-partsNotification stock __sizes">12</span></a>
                <a href="<?php bloginfo('url'); ?>/mylist" class="p-mypageTab_item save o-tabItem" class=""><i class="o-icon flaticon-love"></i>検討リスト<span class="p-mypageTab_notification o-partsNotification stock __sizes"><?php echo @get_mylist();?></span></a>
                <a href="<?php bloginfo('url'); ?>/myterms" class="p-mypageTab_item terms o-tabItem" ><i class="o-icon flaticon-download"></i>保存した検索条件<span class="p-mypageTab_notification o-partsNotification stock __sizes"><?php echo $sc_con;?></span></a>
            </div>

            <section class="p-postCont">
                <p class="u-text-ac"><?php the_content(); ?></p>
            </section>

            <section class="p-mypageCont __history o-partsTerms __area l-cont __section">

                <div class="o-room __compact __4 flex-w">
                <?php
                    if ( is_active_sidebar( 'kensakurireki_widgets' ) ) :
                        dynamic_sidebar('kensakurireki_widgets');
                    endif;
                ?>
                </div>

            </section>
        </section>



        <?php
// echo do_shortcode('[contact-form-7 id="18" title="コンタクトフォーム 1"]'); ?>


    </main><!-- #main -->
</div><!-- #about -->

<?php get_footer('top'); ?>
