<?php
/*
Template Name: お問い合わせ
Template Post Type: post,page
*/
?>

<?php get_header();?>



<div id="<?php echo $post->post_name;?>" class="p-mypage">
    <main id="main" class="site-main" role="main">

        <div class="o-fv __page mb-3"  style="background-image:url(
            <?php
            // アイキャッチ画像が設定されているかチェック
            if(has_post_thumbnail()){
                // アイキャッチ画像を表示する
                the_post_thumbnail_url( get_the_ID(),'full' );
            }else{
                // 代替画像を表示する
                the_field('search-mvimg', 'option');
            }
            ?>
                                              )">
            <div class="o-fvCatch">
                <h2 class="o-fvCatch_title o-title __large __white __nobold">
                    <?php the_title(); ?>
                </h2>
            </div>
        </div>

        <section class="l-wrap __maxwsmall mb-6">
            <section class="p-postCont p-otherCont">
<!--                <?php the_content(); ?>-->
                <div class="contactfoam-box" style="user-select: auto;">
                    <p style="user-select: auto;">弊社へのお問い合わせにつきましては、下記のフォームよりお願い致します。</p>






                                    <div role="form" class="wpcf7" id="wpcf7-f345-o1" lang="ja" dir="ltr">
<div class="screen-reader-response"></div>
<form action="/contact/#wpcf7-f345-o1" method="post" class="wpcf7-form" novalidate="novalidate">
<div style="display: none;">
<input type="hidden" name="_wpcf7" value="345" />
<input type="hidden" name="_wpcf7_version" value="5.1.1" />
<input type="hidden" name="_wpcf7_locale" value="ja" />
<input type="hidden" name="_wpcf7_unit_tag" value="wpcf7-f345-o1" />
<input type="hidden" name="_wpcf7_container_post" value="0" />
<input type="hidden" name="g-recaptcha-response" value="" />
</div>

    <div class="o-formSet">
        <p class="o-formSet_title">お名前<span class="o-formRequired">必須</span></p>
        [text* your-name]
    </div>

    <div class="o-formSet">
        <p class="o-formSet_title">お電話番号<span class="o-formRequired">必須</span></p>
        [text* your-name]
    </div>

    <div class="o-formSet">
        <p class="o-formSet_title">メールアドレス<span class="o-formRequired">必須</span></p>
        [text* your-name]
    </div>

    <div class="o-formSet">
        <p class="o-formSet_title">ご用件<span class="o-formRequired">必須</span></p>
        <select name="yoken" class="wpcf7-form-control wpcf7-select wpcf7-validates-as-required" aria-required="true" aria-invalid="false"><option value="掲載依頼">掲載依頼</option><option value="その他">その他</option></select>
    </div>

    <div class="o-formSet">
        <p class="o-formSet_title">内容<span class="o-formRequired">必須</span></p>
        <textarea name="naiyo" cols="40" rows="10" class="wpcf7-form-control wpcf7-textarea wpcf7-validates-as-required" aria-required="true" aria-invalid="false"></textarea>
    </div>









<table class="table-foam">
<tr>
<th>お名前<span class="required">(必須)</span></th>
<td>
<span class="wpcf7-form-control-wrap your-name"><input type="text" name="your-name" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false" /></span>
</td>
</tr>
<tr>
<th>電話番号<span class="required">(必須)</span></th>
<td>
<span class="wpcf7-form-control-wrap your-tel"><input type="tel" name="your-tel" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-tel wpcf7-validates-as-required wpcf7-validates-as-tel" aria-required="true" aria-invalid="false" /></span>
</td>
</tr>
<tr>
<th>メールアドレス<span class="required">(必須)</span></th>
<td>
<span class="wpcf7-form-control-wrap your-email"><input type="email" name="your-email" value="info@jumpshare.co" size="40" class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email" aria-required="true" aria-invalid="false" /></span>
</td>
</tr>
<tr>
<th>ご用件<span class="required">(必須)</span></th>
<td>
<span class="wpcf7-form-control-wrap yoken"><select name="yoken" class="wpcf7-form-control wpcf7-select wpcf7-validates-as-required" aria-required="true" aria-invalid="false"><option value="掲載依頼">掲載依頼</option><option value="その他">その他</option></select></span>
</td>
</tr>
<tr>
<th>問い合わせ内容<span class="required">(必須)</span></th>
<td>
<span class="wpcf7-form-control-wrap naiyo"><textarea name="naiyo" cols="40" rows="10" class="wpcf7-form-control wpcf7-textarea wpcf7-validates-as-required" aria-required="true" aria-invalid="false"></textarea></span>
</td>
</tr>
</table>
<input type="submit" value="送信" class="wpcf7-form-control wpcf7-submit" />
    <div class="o-formTel flex-w jc-center">
        <a href="" class="o-formTel_contact o-btn __mid __basic07 __sizem"><i class="o-icon flaticon-download"></i>検索条件を保存</a>
        <a href="" class="o-formTel_num">0120-916-546 <span class="o-formTel_holiday">祝休除く 10 ~ 16時</span></a>
    </div>
</div>
<div class="wpcf7-response-output wpcf7-display-none"></div></form></div>
                </div>
            </section>

        </section>

  </main><!-- #main -->
</div><!-- #about -->

<?php get_footer('top'); ?>
