<?php
/*
Template Name: about
*/
?>
<?php get_header();?>



<div id="about" class="content-area underlayer">
  <main id="main" class="site-main" role="main">

    <nav id="breadcrumbs">
      <ul class="cf">
        <li><a href="/"><span>ホーム</span></a></li>
        <li>高級賃貸東京について</li>
      </ul>
    </nav>

    <article class="ul-flex">
      <section class="ul-contents">
        <h2 class="ul-title mincho">高級賃貸東京について</h2>
        <section class="about01">
          <div class="about-box">
            <h3 class="ul-title01 mincho">高級賃貸東京とは</h3>

            <p><?php echo nl2br(get_field("about-text01"));?></p>
            <p class="img"><img src="<?php bloginfo('template_directory'); ?>/assets/images/about/about01.png" alt=""></p>
          </div>

          <div class="about-box">
            <h3 class="ul-title01 mincho">高級賃貸東京の店舗について</h3>
            <p><?php echo nl2br(get_field("about-text02"));?></p>
            <p class="img"><img src="<?php bloginfo('template_directory'); ?>/assets/images/about/about02.png" alt=""></p>
          </div>
        </section><!-- .about01 -->
      </section><!-- .ul-contents -->

      <?php get_sidebar(); ?>
    </article><!-- .ul-flex -->

  </main><!-- #main -->
</div><!-- #about -->

<?php get_footer(); ?>
