<?php
    // infinitescrollスクリプト用フラグ
    $introduce_archive = true;

    get_header();
    $dp_options = get_desing_plus_option();

    // 紹介アーカイブ
    if ($dp_options['introduce_archive_image']) {
      $image = wp_get_attachment_image_src($dp_options['introduce_archive_image'], 'full');
    }
    $catch = trim($dp_options['introduce_archive_catch']);
    $catch_bg = $dp_options['introduce_archive_image_catch_bg'];
    $catch_bg_opacity = $dp_options['introduce_archive_image_catch_bg_opacity'];
    $archive_text = array();
    if ($dp_options['introduce_archive_text'] && is_array($dp_options['introduce_archive_text'])) {
      foreach($dp_options['introduce_archive_text'] as $key => $value) {
        if (!empty($value['headline']) || !empty($value['desc'])) {
          $archive_text[] = $value;
        }
      }
    }

    // 紹介カテゴリーで紹介アーカイブ表示の場合
    // functions.php custom_post_type_template_include()の処理でこのファイルが読み込まれる
    if (is_tax()) {
      $term = get_queried_object();
      if (!empty($term->term_id)) {
        $term_meta = get_option('taxonomy_'.$term->term_id);
        if (!empty($term_meta['image'])) {
          $term_image = wp_get_attachment_image_src($term_meta['image'], 'full');
        } else {
          $term_image = null;
        }
        if (!empty($term_image[0]) || !empty($term_meta['catch']) || !empty($term_meta['archive_text'])) {
          if (!empty($term_image[0])) {
            $image = $term_image;
          }
          $catch = '';
          $archive_text = array();
          if (!empty($term_meta['catch'])) {
            $catch = trim($term_meta['catch']);
          }
          if (!empty($term_meta['catch_bg'])) {
            $catch_bg = $term_meta['catch_bg'];
          }
          if (!empty($term_meta['catch_bg_opacity'])) {
            $catch_bg_opacity = $term_meta['catch_bg_opacity'];
          }
          if (!empty($term_meta['archive_text']) && is_array($term_meta['archive_text'])) {
            foreach($term_meta['archive_text'] as $key => $value) {
              if (!empty($value['headline']) || !empty($value['desc'])) {
                $archive_text[] = $value;
              }
            }
          }
        }
      }
    }
?>

<?php if ($dp_options['show_breadcrumb_introduce_archive']) get_template_part('breadcrumb'); ?>

<?php if (!empty($image[0])) { ?>
<div id="header_image">
 <img src="<?php echo esc_attr($image[0]); ?>" alt="" />
<?php   if ($catch) { ?>
 <div class="caption_bar rich_font" style="background:rgba(<?php echo esc_attr(implode(',', hex2rgb($catch_bg))); ?>,<?php echo esc_attr($catch_bg_opacity); ?>);">
  <?php echo str_replace(array("\r\n", "\r", "\n"), ' ', esc_html($catch)); ?>
 </div>
<?php   } ?>
</div>
<?php } ?>

<div id="main_col">

<?php if (count($archive_text)) { ?>
 <div id="introduce_header" class="post_content">
<?php
        if (count($archive_text) == 1) {
          foreach($archive_text as $value) {
            if (!empty($value['headline'])) {
?>
  <h2 class="headline"><?php echo str_replace(array("\r\n", "\r", "\n"), '<br />', esc_html($value['headline'])); ?></h2>
<?php
            }
            if (!empty($value['desc'])) {
?>
  <p><?php echo str_replace(array("\r\n", "\r", "\n"), '<br />', esc_html($value['desc'])); ?></p>
<?php
            }
          }
        } else {
          if (count($archive_text) == 2 || count($archive_text) == 2) {
            $col_class = 'post_col post_col-2';
          } else {
            $col_class = 'post_col post_col-3';
          }
?>
  <div class="post_row">
<?php
          foreach($archive_text as $value) {
?>
   <div class="<?php echo esc_attr($col_class); ?>">
<?php
            if (!empty($value['headline'])) {
?>
   <h2 class="headline"><?php echo str_replace(array("\r\n", "\r", "\n"), '<br />', esc_html($value['headline'])); ?></h2>
<?php
            }
            if (!empty($value['desc'])) {
?>
   <p><?php echo str_replace(array("\r\n", "\r", "\n"), '<br />', esc_html($value['desc'])); ?></p>
<?php
            }
?>
   </div>
<?php
          }
?>
  </div>
<?php
        }
?>
 </div>
<?php } ?>

<?php if ( have_posts() ) : ?>
    <div class="flex-wrap">
<?php
// echo 'aa';
        $i = 0;
        $row = 0;
        while ( have_posts() ) :
            the_post();

            if ($i > 0 && $i % 3 == 0) {
                $row++;
?>
<?php
            }

            $col_class = '';
            if ($row % 2 == 0) {
                if ($i % 3 == 0) {
                    $col_class = ' show_info';
                }
            } else {
                if ($i % 3 == 2) {
                    $col_class = ' show_info';
                }
            }

            $bukken_id = get_field('related_fudo', $post->ID);
            $b_id = $bukken_id[0];
            // var_dump($b_id);

             /**
             * single-fudoから読み込み
             * single-roomからも活用
             * アイキャッチ読み込み
             */
            $fudo_gaikan_images = get_field('bukken_gaikan_kyouyou', $b_id);
            // print_r($fudo_gaikan_images[0]['sizes']);
            if($fudo_gaikan_images != ''){
                $fudoimg_data=$fudo_gaikan_images[0]['sizes']['large'];
                // echo $fudoimg_data;
            }else{
                $fudoimg_data="https://koukyu-chintai.com/wp-content/uploads/cropped-cropped-main-1-2000x1200-1-1024x614.jpg";
            }

            // $room_images = get_field('room_situnai_senyu', $b_id);
            // // print_r($room_images[0]['sizes']);
            // if($room_images != ''){
            //     $roomimg_data=$room_images[0]['sizes']['full'];
            //     // echo $roomimg_data;
            // }else{
            //     $roomimg_data="https://koukyu-chintai.com/wp-content/uploads/cropped-cropped-main-1-2000x1200-1-1024x614.jpg";
            // }

            $img1 = @$fudo_gaikan_images[0]['sizes']['large'];
            $img2 = @$fudo_gaikan_images[1]['sizes']['large'];
            $img3 = @$fudo_gaikan_images[2]['sizes']['large'];
            if ( is_null($img1) || is_null($img2) || is_null($img3) ){
                $gai_images = stw_get_singlefudo_images($b_id,2);
                $img1_path = '';
                if ( 0 < count($gai_images) ) {
                    $img1_path = $gai_images[0]->image_path;
                    $img1 = stw_generate_imgurl($img1_path);
                }
                $h_images = stw_get_singlefudo_images($b_id);
                $h_images_count_without_ban = count($h_images);

                if ( 0 < count($h_images) ){
                    foreach ($h_images as $oh ){
                        if ( is_null($img1) ){
                            if(!in_array(basename($oh->image_path), get_ban_image_file_name())){
                            $img1 = stw_generate_imgurl($oh->image_path);
                            continue;
                            }else{
                                $h_images_count_without_ban--;
                                continue;
                            }
                        }
                        if ( is_null($img2) && $oh->image_path != $img1_path ){
                            if(!in_array(basename($oh->image_path), get_ban_image_file_name())){
                            $img2 = stw_generate_imgurl($oh->image_path);
                            continue;
                            }else{
                                $h_images_count_without_ban--;
                                continue;
                            }
                        }
                        if ( is_null($img3) && $oh->image_path != $img1_path ){
                            if(!in_array(basename($oh->image_path), get_ban_image_file_name())){
                            $img3 = stw_generate_imgurl($oh->image_path);
                            continue;
                            }else{
                                $h_images_count_without_ban--;
                                continue;
                            }
                        }
                    }
                    /*
                    $img1 = ( isset($h_images[0]) ) ? stw_generate_imgurl($h_images[0]->image_path) : '';
                    $img2 = ( isset($h_images[1]) ) ? stw_generate_imgurl($h_images[1]->image_path) : '';
                    $img3 = ( isset($h_images[2]) ) ? stw_generate_imgurl($h_images[2]->image_path) : '';
                    */
                }
            }

?>
     <section class="o-post __3 vacant <?php echo esc_attr($col_class); ?>">
         <div class="o-postThumb">
             <?php

            //  if ( has_post_thumbnail() ) {
            //      the_post_thumbnail('size2');
            //  } else {
            //      if ( is_mobile() ) {
            //          echo '<img src="' . get_template_directory_uri() . '/img/common/no_image1.gif" title="" alt="" />';
            //      } else {
            //          echo '<img src="' . get_template_directory_uri() . '/img/common/no_image2.gif" title="" alt="" />';
            //      }
            //  }

             ?>

              <?php if(isset($img1)) {?>
                  <img src="<?php echo $img1; ?>">
              <?php  }elseif (has_post_thumbnail()) { the_post_thumbnail('size3'); } else { ?>
              <img src="<?php echo get_template_directory_uri(); ?>/img/common/no_image3.gif" title="" alt="" />
              <?php } ?>

         </div>
         <div class="o-postWrap">
             <a href="<?php echo get_the_permalink($post->ID);?>" class="o-postLink"></a>
             <header class="o-postHeader __nop">
                 <div class="o-postCat"><?php echo esc_html(trim(get_post_meta($post->ID, 'shoulder_copy', true))); ?></div>
                 <h3 class="o-postTitle"><?php trim_title(38); ?></h3>
             </header>

             <ul class="o-postDetail mb-1">
                                <li class="o-postDetail_item __price">
                                <span class="o-postDetail_name">賃料</span><?php echo get_kakaku_list_for_fudo($b_id);?>
                                </li>
                                <li class="o-postDetail_item __year"><span class="o-postDetail_name">築年月</span>
                                <?php
                                $tatemonochikunenn = get_post_meta($b_id, 'tatemonochikunenn', true);
                                // var_dump($tatemonochikunenn);
                            if(!empty($tatemonochikunenn)){

                                if(strpos($tatemonochikunenn, '月')){
                                    $tatemonochikunenn_data = $tatemonochikunenn . "01";
                                    $tatemonochikunenn_data = str_replace(array("年","月"), "/", $tatemonochikunenn_data);
                                }else{
                                    $tatemonochikunenn_data = $tatemonochikunenn . "/01";
                                }
                            }

                            echo $tatemonochikunenn;
                            echo "(" . get_tikunensu($tatemonochikunenn_data) . ")";
                            // echo get_sintiku_mark($tatemonochikunenn_data);
                            ?>
                                </li>
                                <li class="o-postDetail_item __location"><span class="o-postDetail_name">所在地</span>
                                <?php echo str_replace("東京都", "", my_custom_shozaichi_print($b_id));?>
                                <?php my_custom_shozaichi_print($b_id);?>
                                <?php echo get_post_meta($b_id, 'shozaichimeisho', true);?>
                                <?php echo get_post_meta($b_id, 'shozaichimeisho2', true);?>
                                </li>
                                <li class="o-postDetail_item __size">
                                <span class="o-postDetail_name">間取り</span><?php echo get_senyumenseki_list_for_fudo($b_id);?>/
                                <?php echo get_madori_list_for_fudo($b_id);?>
                                </li>
                                <li class="o-postDetail_item __size"><span class="o-postDetail_name">登録日：</span><?php echo get_the_date("Y-m-d"); ?></li>
                            </ul>

             <?php
             $metas = array();
             if ($post->post_type == 'post') {
                 if ($dp_options['show_categories']) {
                     foreach(explode('-', $dp_options['show_categories']) as $cat) {
                         if ($cat == 1) {
                             $terms = get_the_terms($post->ID, 'category');
                             if ($terms && !is_wp_error($terms)) {
                                 foreach ($terms as $term) {
                                     $metas['category'][] = '<li class="o-postTag_item"><a href="'.get_term_link($term).'" title="'.esc_attr($term->name).'">'.esc_html($term->name).'</a></li>';
                                 }
                                 $metas['category'] = ''.implode('', $metas['category']).'';
                             }
                         } elseif (!empty($dp_options['use_category'.$cat])) {
                             $terms = get_the_terms($post->ID, $dp_options['category'.$cat.'_slug']);
                             if ($terms && !is_wp_error($terms)) {
                                 foreach ($terms as $term) {
                                     $metas['category'.$cat][] = '<li class="o-postTag_item"><a href="'.get_term_link($term).'" title="'.esc_attr($term->name).'">'.esc_html($term->name).'</a></li>';
                                 }
                                 $metas['category'.$cat] = ''.implode('', $metas['category'.$cat]).'';
                             }
                         }
                     }
                 }

             } elseif ($post->post_type == $dp_options['introduce_slug']) {
                //  if ($dp_options['show_introduce_categories']) {
                //      foreach(explode('-', $dp_options['show_introduce_categories']) as $cat) {
                //          if (!empty($dp_options['use_introduce_category'.$cat])) {
                //              $terms = get_the_terms($post->ID, $dp_options['introduce_category'.$cat.'_slug']);
                //              if ($terms && !is_wp_error($terms)) {
                //                  foreach ($terms as $term) {
                //                      $metas['introduce_category'.$cat][] = '<li class="o-postTag_item"><a href="'.get_term_link($term).'" title="'.esc_attr($term->name).'">'.esc_html($term->name).'</a></li>';
                //                  }
                //                  $metas['introduce_category'.$cat] = ''.implode('', $metas['introduce_category'.$cat]).'';
                //              }
                //          }
                //      }
                //  }
             }

             if ($metas) {
                 echo '    <ul class="o-postTag flex-wrap">'.implode('', $metas).'</ul>'."\n";
             }
             ?>
             <a href="<?php the_permalink() ?>" title="<?php the_title_attribute(); ?>" class="o-link over"></a>
         </div>
     </section>

<?php $i++; endwhile; ?>

 </div><!-- END #introduce_list -->
<?php // the_pagination(); ?>
<?php if ((!$paged || $paged == 1) && show_posts_nav()) { ?>
 <div id="load_post" class="inview-fadein"><?php next_posts_link( __( 'read more', 'tcd-w' ) ); ?></div>
<?php } ?>

<?php else: ?>
 <p class="no_post"><?php _e('There is no registered post.', 'tcd-w'); ?></p>
<?php endif; ?>

</div><!-- END #main_col -->

<?php get_footer(); ?>
