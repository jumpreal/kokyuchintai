(function($) {
// searchmodal制御部
    $('.js-modalTest').on('click', function () {
        $('#searchModal').fadeIn();
        return false;
    });
    $('.js-modal-close').on('click', function () {
        $('#searchModal').fadeOut();
        return false;
    });
    $('.js-modal-closeBg').on('click', function () {
        $('#searchModal').fadeOut();
        return false;
    });
});
