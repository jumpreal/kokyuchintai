<?php
    // infinitescrollスクリプト用フラグ
    $introduce_archive = true;

    get_header();
    $dp_options = get_desing_plus_option();

    // 紹介アーカイブ
    if ($dp_options['introduce_archive_image']) {
      $image = wp_get_attachment_image_src($dp_options['introduce_archive_image'], 'full');
    }
    $catch = trim($dp_options['introduce_archive_catch']);
    $catch_bg = $dp_options['introduce_archive_image_catch_bg'];
    $catch_bg_opacity = $dp_options['introduce_archive_image_catch_bg_opacity'];
    $archive_text = array();
    if ($dp_options['introduce_archive_text'] && is_array($dp_options['introduce_archive_text'])) {
      foreach($dp_options['introduce_archive_text'] as $key => $value) {
        if (!empty($value['headline']) || !empty($value['desc'])) {
          $archive_text[] = $value;
        }
      }
    }

    // 紹介カテゴリーで紹介アーカイブ表示の場合
    // functions.php custom_post_type_template_include()の処理でこのファイルが読み込まれる
    if (is_tax()) {
      $term = get_queried_object();
      if (!empty($term->term_id)) {
        $term_meta = get_option('taxonomy_'.$term->term_id);
        if (!empty($term_meta['image'])) {
          $term_image = wp_get_attachment_image_src($term_meta['image'], 'full');
        } else {
          $term_image = null;
        }
        if (!empty($term_image[0]) || !empty($term_meta['catch']) || !empty($term_meta['archive_text'])) {
          if (!empty($term_image[0])) {
            $image = $term_image;
          }
          $catch = '';
          $archive_text = array();
          if (!empty($term_meta['catch'])) {
            $catch = trim($term_meta['catch']);
          }
          if (!empty($term_meta['catch_bg'])) {
            $catch_bg = $term_meta['catch_bg'];
          }
          if (!empty($term_meta['catch_bg_opacity'])) {
            $catch_bg_opacity = $term_meta['catch_bg_opacity'];
          }
          if (!empty($term_meta['archive_text']) && is_array($term_meta['archive_text'])) {
            foreach($term_meta['archive_text'] as $key => $value) {
              if (!empty($value['headline']) || !empty($value['desc'])) {
                $archive_text[] = $value;
              }
            }
          }
        }
      }
    }
?>

<?php if ($dp_options['show_breadcrumb_introduce_archive']) get_template_part('breadcrumb'); ?>

<?php if (!empty($image[0])) { ?>
<div id="header_image">
 <img src="<?php echo esc_attr($image[0]); ?>" alt="" />
<?php   if ($catch) { ?>
 <div class="caption_bar rich_font" style="background:rgba(<?php echo esc_attr(implode(',', hex2rgb($catch_bg))); ?>,<?php echo esc_attr($catch_bg_opacity); ?>);">
  <?php echo str_replace(array("\r\n", "\r", "\n"), ' ', esc_html($catch)); ?>
 </div>
<?php   } ?>
</div>
<?php } ?>

<div id="main_col">

<?php if (count($archive_text)) { ?>
 <div id="introduce_header" class="post_content">
<?php
        if (count($archive_text) == 1) {
          foreach($archive_text as $value) {
            if (!empty($value['headline'])) {
?>
  <h2 class="headline"><?php echo str_replace(array("\r\n", "\r", "\n"), '<br />', esc_html($value['headline'])); ?></h2>
<?php
            }
            if (!empty($value['desc'])) {
?>
  <p><?php echo str_replace(array("\r\n", "\r", "\n"), '<br />', esc_html($value['desc'])); ?></p>
<?php
            }
          }
        } else {
          if (count($archive_text) == 2 || count($archive_text) == 2) {
            $col_class = 'post_col post_col-2';
          } else {
            $col_class = 'post_col post_col-3';
          }
?>
  <div class="post_row">
<?php
          foreach($archive_text as $value) {
?>
   <div class="<?php echo esc_attr($col_class); ?>">
<?php
            if (!empty($value['headline'])) {
?>
   <h2 class="headline"><?php echo str_replace(array("\r\n", "\r", "\n"), '<br />', esc_html($value['headline'])); ?></h2>
<?php
            }
            if (!empty($value['desc'])) {
?>
   <p><?php echo str_replace(array("\r\n", "\r", "\n"), '<br />', esc_html($value['desc'])); ?></p>
<?php
            }
?>
   </div>
<?php
          }
?>
  </div>
<?php
        }
?>
 </div>
<?php } ?>

<?php if ( have_posts() ) : ?>
    <div class="flex-wrap">
<?php
        $i = 0;
        $row = 0;
        while ( have_posts() ) :
            the_post();

            if ($i > 0 && $i % 3 == 0) {
                $row++;
?>
<?php
            }

            $col_class = '';
            if ($row % 2 == 0) {
                if ($i % 3 == 0) {
                    $col_class = ' show_info';
                }
            } else {
                if ($i % 3 == 2) {
                    $col_class = ' show_info';
                }
            }
?>
     <section class="o-post __4 vacant <?php echo esc_attr($col_class); ?>">
         <div class="o-postThumb">
             <?php
             if ( has_post_thumbnail() ) {
                 the_post_thumbnail('size1');
             } else {
                 if ( is_mobile() ) {
                     echo '<img src="' . get_template_directory_uri() . '/img/common/no_image1.gif" title="" alt="" />';
                 } else {
                     echo '<img src="' . get_template_directory_uri() . '/img/common/no_image2.gif" title="" alt="" />';
                 }
             } ?>
         </div>
         <div class="o-postWrap">
             <header class="o-postHeader __nop">
                 <div class="o-postCat"><?php echo esc_html(trim(get_post_meta($post->ID, 'shoulder_copy', true))); ?></div>
                 <h3 class="o-postTitle o-title __p"><a href="<?php the_permalink(); ?>"><?php trim_title(38); ?></a></h3>
             </header>

             <?php
             $metas = array();
             if ($post->post_type == 'post') {
                 if ($dp_options['show_categories']) {
                     foreach(explode('-', $dp_options['show_categories']) as $cat) {
                         if ($cat == 1) {
                             $terms = get_the_terms($post->ID, 'category');
                             if ($terms && !is_wp_error($terms)) {
                                 foreach ($terms as $term) {
                                     $metas['category'][] = '<span data-href="'.get_term_link($term).'" title="'.esc_attr($term->name).'">'.esc_html($term->name).'</span>';
                                 }
                                 $metas['category'] = '<li class="o-postTag_item">'.implode('', $metas['category']).'</li>';
                             }
                         } elseif (!empty($dp_options['use_category'.$cat])) {
                             $terms = get_the_terms($post->ID, $dp_options['category'.$cat.'_slug']);
                             if ($terms && !is_wp_error($terms)) {
                                 foreach ($terms as $term) {
                                     $metas['category'.$cat][] = '<span  data-href="'.get_term_link($term).'" title="'.esc_attr($term->name).'">'.esc_html($term->name).'</span>';
                                 }
                                 $metas['category'.$cat] = '<li class="o-postTag_item">'.implode('', $metas['category'.$cat]).'</li>';
                             }
                         }
                     }
                 }

             } elseif ($post->post_type == $dp_options['introduce_slug']) {
                 if ($dp_options['show_introduce_categories']) {
                     foreach(explode('-', $dp_options['show_introduce_categories']) as $cat) {
                         if (!empty($dp_options['use_introduce_category'.$cat])) {
                             $terms = get_the_terms($post->ID, $dp_options['introduce_category'.$cat.'_slug']);
                             if ($terms && !is_wp_error($terms)) {
                                 foreach ($terms as $term) {
                                     $metas['introduce_category'.$cat][] = '<span data-href="'.get_term_link($term).'" title="'.esc_attr($term->name).'">'.esc_html($term->name).'</span>';
                                 }
                                 $metas['introduce_category'.$cat] = '<li class="o-postTag_item">'.implode('', $metas['introduce_category'.$cat]).'</li>';
                             }
                         }
                     }
                 }
             }

             if ($metas) {
                 echo '    <ul class="o-postTag flex-wrap">'.implode('', $metas).'</ul>'."\n";
             }
             ?>
             <a href="<?php the_permalink() ?>" title="<?php the_title_attribute(); ?>" class="o-link over"></a>
         </div>
     </section>

<?php $i++; endwhile; ?>

 </div><!-- END #introduce_list -->


 <?php get_template_part('navigation'); get_template_part('navigation2'); ?>

 
<?php if ((!$paged || $paged == 1) && show_posts_nav()) { ?>
 <div id="load_post" class="inview-fadein"><?php next_posts_link( __( 'read more', 'tcd-w' ) ); ?></div>
<?php } ?>

<?php else: ?>
 <p class="no_post"><?php _e('There is no registered post.', 'tcd-w'); ?></p>
<?php endif; ?>

</div><!-- END #main_col -->

<?php get_footer(); ?>
