
<?php
$dp_options = get_desing_plus_option();

global $header_slider;
$header_slider = array();

// 画像スライダー
if ($dp_options['header_content_type'] == 'type1') {
    for ($i = 1; $i <= 5; $i++) {
        if (!is_mobile()) {
            $image = wp_get_attachment_image_src($dp_options['slider_image'.$i], 'full');
        } else {
            $image = wp_get_attachment_image_src($dp_options['slider_image_mobile'.$i], 'full');
        }
        if (!empty($image[0])) {
            $header_slider['header_content_type'] = 'type1';
            $header_slider['slider'][$i]['image'] = $image;
        }
    }

    // 動画
} elseif ($dp_options['header_content_type'] == 'type2') {
    if ($dp_options['slider_video']) {
        $header_slider['header_content_type'] = 'type2';
        $header_slider['slider_video'] = wp_get_attachment_url($dp_options['slider_video']);
        $image = wp_get_attachment_image_src($dp_options['slider_video_image'], 'full');
        if (!empty($image[0])) {
            $header_slider['slider_video_image'] = $image;
        }
    }

    // youtube
} elseif ($dp_options['header_content_type'] == 'type3') {
    if ($dp_options['slider_youtube_url']) {
        $header_slider['header_content_type'] = 'type3';
        $header_slider['slider_youtube_url'] = $dp_options['slider_youtube_url'];
        $image = wp_get_attachment_image_src($dp_options['slider_youtube_image'], 'full');
        if (!empty($image[0])) {
            $header_slider['slider_youtube_image'] = $image;
        }
    }
}

get_header();
?>

<?php
// 画像スライダー
if (!empty($header_slider['slider'])) :
?>
<div id="header_slider">
    <?php
    $is_first_slide = true;
    foreach ($header_slider['slider'] as $i => $slider) :
    if ($dp_options['slider_url'.$i] && ($dp_options['use_slider_caption'.$i] == 0 || ($dp_options['use_slider_caption'.$i] == 1 && $dp_options['show_slider_button'.$i] == 0))) {
        $wrap_anchor = true;
    } else {
        $wrap_anchor = false;
    }
    ?>
    <div class="item item<?php echo esc_attr($i); ?>">
        <?php   if ($wrap_anchor) { ?>
        <a href="<?php echo esc_attr($dp_options['slider_url'.$i]); ?>"<?php if ($dp_options['slider_target'.$i]) { echo ' target="_blank"'; } ?>>
            <?php   } ?>
            <?php   if ($dp_options['use_slider_caption'.$i] == 1) { ?>
            <div class="caption">
                <?php
    if ($dp_options['slider_headline'.$i]) {
        echo '<p class="headline rich_font">'.str_replace(array("\r\n", "\r", "\n"), '<br />', esc_html($dp_options['slider_headline'.$i])).'</p>';
    }
    if ($dp_options['slider_caption'.$i]) {
        echo '<p class="catchphrase rich_font">'.str_replace(array("\r\n", "\r", "\n"), '<br />', esc_html($dp_options['slider_caption'.$i])).'</p>';
    }
    if ($dp_options['show_slider_button'.$i] == 1 && $dp_options['slider_button'.$i] && $dp_options['slider_url'.$i]) {
        echo '<a class="button" href="'.esc_attr($dp_options['slider_url'.$i]).'"'.($dp_options['slider_target'.$i] ? ' target="_blank"' : '').'>'.esc_html($dp_options['slider_button'.$i]).'</a>';
    } elseif ($dp_options['show_slider_button'.$i] == 1 && $dp_options['slider_button'.$i]) {
        echo '<div class="button">'.esc_html($dp_options['slider_button'.$i]).'</div>';
    }
                ?>
            </div><!-- END .caption -->
            <?php   } ?>
            <?php   if ($is_first_slide) { $is_first_slide = false; ?>
            <img src="<?php echo esc_attr($slider['image'][0]); ?>" alt="" />
            <?php   } else { ?>
            <img data-lazy="<?php echo esc_attr($slider['image'][0]); ?>" alt="" />
            <?php   } ?>
            <?php   if ($wrap_anchor) { ?>
        </a>
        <?php   } ?>
    </div><!-- END .item -->
    <?php
    endforeach;
    ?>
</div><!-- END #header_slider -->
<?php
// 動画
elseif (!empty($header_slider['slider_video'])) :
if (!wp_is_mobile()) : // if is pc
?>
<div id="header_slider" class="slider_video">
    <div class="slider_video_wrapper">
        <div id="slider_video" class="slider_video_container slider_video"></div>
    </div>
    <?php   if ($dp_options['use_slider_video_caption'] == 1) { ?>
    <div class="caption">
        <?php
    if ($dp_options['slider_video_headline']) {
        echo '<p class="headline rich_font">'.str_replace(array("\r\n", "\r", "\n"), '<br />', esc_html($dp_options['slider_video_headline'])).'</p>';
    }
    if ($dp_options['slider_video_caption']) {
        echo '<p class="catchphrase rich_font">'.str_replace(array("\r\n", "\r", "\n"), '<br />', esc_html($dp_options['slider_video_caption'])).'</p>';
    }
    if ($dp_options['show_slider_video_button'] == 1 && $dp_options['slider_video_button'] && $dp_options['slider_video_button_url']) {
        echo '<a class="button" href="'.esc_attr($dp_options['slider_video_button_url']).'"'.($dp_options['slider_video_button_target'] ? ' target="_blank"' : '').'>'.esc_html($dp_options['slider_video_button']).'</a>';
    } elseif ($dp_options['show_slider_video_button'] == 1 && $dp_options['slider_video_button']) {
        echo '<div class="button">'.esc_html($dp_options['slider_video_button']).'</div>';
    }
        ?>
    </div><!-- END .caption -->
    <?php   } ?>
</div><!-- END #header_slider -->
<?php elseif (!empty($header_slider['slider_video_image'][0])) : // if is mobile device ?>
<div id="header_slider" class="slider_video_mobile">
    <div class="item">
        <img src="<?php echo esc_attr($header_slider['slider_video_image'][0]); ?>" alt="" title="" />
        <?php   if ($dp_options['use_slider_video_caption'] == 1) { ?>
        <div class="caption">
            <?php
    if ($dp_options['slider_video_headline']) {
        echo '<p class="headline rich_font">'.str_replace(array("\r\n", "\r", "\n"), '<br />', esc_html($dp_options['slider_video_headline'])).'</p>';
    }
    if ($dp_options['slider_video_caption']) {
        echo '<p class="catchphrase rich_font">'.str_replace(array("\r\n", "\r", "\n"), '<br />', esc_html($dp_options['slider_video_caption'])).'</p>';
    }
    if ($dp_options['show_slider_video_button'] == 1 && $dp_options['slider_video_button'] && $dp_options['slider_video_button_url']) {
        echo '<a class="button" href="'.esc_attr($dp_options['slider_video_button_url']).'"'.($dp_options['slider_video_button_target'] ? ' target="_blank"' : '').'>'.esc_html($dp_options['slider_video_button']).'</a>';
    } elseif ($dp_options['show_slider_video_button'] == 1 && $dp_options['slider_video_button']) {
        echo '<div class="button">'.esc_html($dp_options['slider_video_button']).'</div>';
    }
            ?>
        </div><!-- END .caption -->
        <?php   } ?>
    </div><!-- END .item -->
</div><!-- END #header_slider -->
<?php
endif;

// youtube
elseif (!empty($header_slider['slider_youtube_url'])) :
if (!wp_is_mobile()) : // if is pc
?>
<div id="header_slider" class="slider_video">
    <div class="slider_video_wrapper">
        <div id="slider_video" class="slider_video_container slider_youtube"></div>
        <div id="slider_youtube" class="player youtube_video_player" data-property="{videoURL:'<?php echo esc_attr($header_slider['slider_youtube_url']); ?>',containment:'#slider_video',showControls:false,startAt:0,mute:true,autoPlay:true,loop:true,opacity:1,ratio:'16/9'}"></div>
    </div>
    <?php   if ($dp_options['use_slider_youtube_caption'] == 1) { ?>
    <div class="caption">
        <?php
    if ($dp_options['slider_youtube_headline']) {
        echo '<p class="headline rich_font">'.str_replace(array("\r\n", "\r", "\n"), '<br />', esc_html($dp_options['slider_youtube_headline'])).'</p>';
    }
    if ($dp_options['slider_youtube_caption']) {
        echo '<p class="catchphrase rich_font">'.str_replace(array("\r\n", "\r", "\n"), '<br />', esc_html($dp_options['slider_youtube_caption'])).'</p>';
    }
    if ($dp_options['show_slider_youtube_button'] == 1 && $dp_options['slider_youtube_button'] && $dp_options['slider_youtube_button_url']) {
        echo '<a class="button" href="'.esc_attr($dp_options['slider_youtube_button_url']).'"'.($dp_options['slider_youtube_button_target'] ? ' target="_blank"' : '').'>'.esc_html($dp_options['slider_youtube_button']).'</a>';
    } elseif ($dp_options['show_slider_youtube_button'] == 1 && $dp_options['slider_youtube_button']) {
        echo '<div class="button">'.esc_html($dp_options['slider_youtube_button']).'</div>';
    }
        ?>
    </div><!-- END .caption -->
    <?php   } ?>
</div><!-- END #header_slider -->
<?php elseif (!empty($header_slider['slider_youtube_image'][0])) : // if is mobile device ?>
<div id="header_slider" class="slider_video_mobile">
    <div class="item">
        <img src="<?php echo esc_attr($header_slider['slider_youtube_image'][0]); ?>" alt="" title="" />
        <?php   if ($dp_options['use_slider_youtube_caption'] == 1) { ?>
        <div class="caption">
            <?php
    if ($dp_options['slider_youtube_headline']) {
        echo '<p class="headline rich_font">'.str_replace(array("\r\n", "\r", "\n"), '<br />', esc_html($dp_options['slider_youtube_headline'])).'</p>';
    }
    if ($dp_options['slider_youtube_caption']) {
        echo '<p class="catchphrase rich_font">'.str_replace(array("\r\n", "\r", "\n"), '<br />', esc_html($dp_options['slider_youtube_caption'])).'</p>';
    }
    if ($dp_options['show_slider_youtube_button'] == 1 && $dp_options['slider_youtube_button'] && $dp_options['slider_youtube_button_url']) {
        echo '<a class="button" href="'.esc_attr($dp_options['slider_youtube_button_url']).'"'.($dp_options['slider_youtube_button_target'] ? ' target="_blank"' : '').'>'.esc_html($dp_options['slider_youtube_button']).'</a>';
    } elseif ($dp_options['show_slider_youtube_button'] == 1 && $dp_options['slider_youtube_button']) {
        echo '<div class="button">'.esc_html($dp_options['slider_youtube_button']).'</div>';
    }
            ?>
        </div><!-- END .caption -->
        <?php   } ?>
    </div><!-- END .item -->
</div><!-- END #header_slider -->
<?php
endif;
endif;
?>
<?php if ( is_home() || is_front_page() ) : ?>
<div class="l-headerMv">
    <img src="<?php echo get_template_directory_uri(); ?>/img/common/mv.jpg" width="1000" height="740" alt="" class="l-headerMv_img">
    <?php endif; ?>

    <?php
    if (is_show_custom_search_form(true)) :
    ?>
    <div id="index_header_search">
        <div class="inner">
            <?php get_template_part('custom_search_form'); ?>
        </div>
    </div>
    <?php
    endif;
    ?>
    <?php if ( is_home() || is_front_page() ) : ?>
</div>
<?php endif; ?>

<?php
if ($dp_options['show_index_news']) :
$args = array('post_type' => $dp_options['news_slug'], 'ignore_sticky_posts' => 1, 'orderby' => 'date', 'order' => 'DESC', 'posts_per_page' => $dp_options['index_news_num']);
$post_list = get_posts($args);
if ($post_list) :
?>
<div id="index_news">
    <div class="inner">
        <div class="newsticker">
            <ol class="newsticker-list">
                <?php     foreach ($post_list as $post) : setup_postdata($post); ?>
                <li class="newsticker-item">
                    <a href="<?php echo the_permalink(); ?>"><span><?php if ($dp_options['show_index_news_date']) : ?><time class="entry-date updated" datetime="<?php the_modified_time('c'); ?>"><?php the_time('Y.m.d'); ?></time><?php endif; ?><?php trim_title(50); ?></span></a>
                </li>
                <?php   endforeach; wp_reset_postdata(); ?>
            </ol>
            <?php   if ($dp_options['show_index_news_archive_link'] && $dp_options['index_news_archive_link_text']) : ?>
            <div class="archive_link">
                <a href="<?php echo get_post_type_archive_link($dp_options['news_slug']); ?>"><?php echo esc_html($dp_options['index_news_archive_link_text']); ?></a>
            </div>
            <?php   endif; ?>
        </div>
    </div>
</div>
<?php
endif;
endif;
if ($dp_options['show_index_news_mobile']) :
$args = array('post_type' => $dp_options['news_slug'], 'ignore_sticky_posts' => 1, 'orderby' => 'date', 'order' => 'DESC', 'posts_per_page' => $dp_options['index_news_num_mobile']);
$post_list = get_posts($args);
if ($post_list) :
?>
<div id="index_news_mobile">
    <div class="inner">
        <ol>
            <?php     foreach ($post_list as $post) : setup_postdata($post); ?>
            <li>
                <a href="<?php echo the_permalink(); ?>"><span><?php if ($dp_options['show_index_news_date_mobile']) : ?><time class="entry-date updated" datetime="<?php the_modified_time('c'); ?>"><?php the_time('Y.m.d'); ?></time><?php endif; ?><?php trim_title(50); ?></span></a>
            </li>
            <?php   endforeach; wp_reset_postdata(); ?>
        </ol>
        <?php   if ($dp_options['show_index_news_archive_link_mobile'] && $dp_options['index_news_archive_link_text_mobile']) : ?>
        <div class="archive_link">
            <a href="<?php echo get_post_type_archive_link($dp_options['news_slug']); ?>"><?php echo esc_html($dp_options['index_news_archive_link_text_mobile']); ?></a>
        </div>
        <?php   endif; ?>
    </div>
</div>
<?php
endif;
endif;
?>

<div id="main_col">
    <?php
    // コンテンツビルダー
    if ( ! empty( $dp_options['contents_builder'] ) ) :
    foreach ( $dp_options['contents_builder'] as $key => $content ) :
    $cb_index = 'cb_' . $key;
    if ( empty( $content['cb_content_select'] ) ) continue;
    if ( isset( $content['cb_display'] ) && ! $content['cb_display'] ) continue;

    // 紹介コンテンツ
    if ( $content['cb_content_select'] == 'introduce' ) :
    $args = array('post_type' => $dp_options['introduce_slug'], 'posts_per_page' => '-1', 'ignore_sticky_posts' => 1, 'orderby' => 'date', 'order' => 'DESC', 'meta_key' => 'show_front_page', 'meta_value' => '1');
    if ( $content['cb_order'] == 'random' ) :
    $args['orderby'] = 'rand';
    endif;

    $cb_posts = new WP_Query($args);
    if ($cb_posts->have_posts()) :
    ?>


    <div id="cb_<?php echo esc_attr($key); ?>" class="cb_content cb_content-<?php echo esc_attr($content['cb_content_select']); ?> inview-fadein">
        <div class="inner">
            <?php
            if ( $content['cb_headline'] ) :
            echo '   <h2 class="o-title h2">' . str_replace( array( "\r\n", "\r", "\n" ), '<br>', esc_html( $content['cb_headline'] ) ) . "</h2>\n";
            endif;
            if ( $content['cb_desc'] ) :
            echo '   <p class="cb_desc">' . str_replace( array( "\r\n", "\r", "\n" ), '<br>', esc_html( $content['cb_desc'] ) ) . "</p>\n";
            endif;
            ?>
            <div class="flex-wrap">
                <?php
                $i = 0;
                $row = 0;
                $b_id = "";
                $cb_posts_count = $cb_posts->post_count;

                while ($cb_posts->have_posts()) :
                $cb_posts->the_post();

                if ($i > 0 && $i % 3 == 0) :
                $row++;
                ?>
                    <?php
                    endif;

                    $col_class = '';
                    if ($row % 2 == 0) :
                    if ($i % 3 == 0) :
                    $col_class = ' show_info';
                    endif;
                    else :
                    if ($i % 3 == 2) :
                    $col_class = ' show_info';
                    endif;
                    endif;

                    $bukken_id = get_field('related_fudo', $post->ID);
                    $b_id = $bukken_id[0];

                    // var_dump($b_id);

                     /**
                     * single-fudoから読み込み
                     * single-roomからも活用
                     * アイキャッチ読み込み
                     */
                    $fudo_gaikan_images = get_field('bukken_gaikan_kyouyou', $b_id);
                    // print_r($fudo_gaikan_images[0]['sizes']);
                    if($fudo_gaikan_images != ''){
                        $fudoimg_data=$fudo_gaikan_images[0]['sizes']['large'];
                        // echo $fudoimg_data;
                    }else{
                        $fudoimg_data="https://koukyu-chintai.com/wp-content/uploads/cropped-cropped-main-1-2000x1200-1-1024x614.jpg";
                    }

                    // $room_images = get_field('room_situnai_senyu', $b_id);
                    // // print_r($room_images[0]['sizes']);
                    // if($room_images != ''){
                    //     $roomimg_data=$room_images[0]['sizes']['full'];
                    //     // echo $roomimg_data;
                    // }else{
                    //     $roomimg_data="https://koukyu-chintai.com/wp-content/uploads/cropped-cropped-main-1-2000x1200-1-1024x614.jpg";
                    // }

                    $img1 = @$fudo_gaikan_images[0]['sizes']['large'];
                    $img2 = @$fudo_gaikan_images[1]['sizes']['large'];
                    $img3 = @$fudo_gaikan_images[2]['sizes']['large'];
                    if ( is_null($img1) || is_null($img2) || is_null($img3) ){
                        $gai_images = stw_get_singlefudo_images($b_id,2);
                        $img1_path = '';
                        if ( 0 < count($gai_images) ) {
                            $img1_path = $gai_images[0]->image_path;
                            $img1 = stw_generate_imgurl($img1_path);
                        }
                        $h_images = stw_get_singlefudo_images($b_id);
                        $h_images_count_without_ban = count($h_images);

                        if ( 0 < count($h_images) ){
                            foreach ($h_images as $oh ){
                                if ( is_null($img1) ){
                                    if(!in_array(basename($oh->image_path), get_ban_image_file_name())){
                                    $img1 = stw_generate_imgurl($oh->image_path);
                                    continue;
                                    }else{
                                        $h_images_count_without_ban--;
                                        continue;
                                    }
                                }
                                if ( is_null($img2) && $oh->image_path != $img1_path ){
                                    if(!in_array(basename($oh->image_path), get_ban_image_file_name())){
                                    $img2 = stw_generate_imgurl($oh->image_path);
                                    continue;
                                    }else{
                                        $h_images_count_without_ban--;
                                        continue;
                                    }
                                }
                                if ( is_null($img3) && $oh->image_path != $img1_path ){
                                    if(!in_array(basename($oh->image_path), get_ban_image_file_name())){
                                    $img3 = stw_generate_imgurl($oh->image_path);
                                    continue;
                                    }else{
                                        $h_images_count_without_ban--;
                                        continue;
                                    }
                                }
                            }
                            /*
                            $img1 = ( isset($h_images[0]) ) ? stw_generate_imgurl($h_images[0]->image_path) : '';
                            $img2 = ( isset($h_images[1]) ) ? stw_generate_imgurl($h_images[1]->image_path) : '';
                            $img3 = ( isset($h_images[2]) ) ? stw_generate_imgurl($h_images[2]->image_path) : '';
                            */
                        }
                    }


                    ?>
                    <section class="o-post __4 vacant <?php echo esc_attr($col_class); ?>">
                        <div class="o-postThumb">
                            <?php if(get_the_post_thumbnail_url($post->ID)) { ?>
                                <img src="<?php echo get_the_post_thumbnail_url($post->ID); ?>" width="280" height="160">
                            <?php }elseif(isset($img1)) {?>
                                <img src="<?php echo $img1; ?>" width="280" height="160">
                            <?php  }elseif (has_post_thumbnail()) { the_post_thumbnail('size3'); } else { ?>
                            <img src="<?php echo get_template_directory_uri(); ?>/img/common/no_image3.gif" title="" alt="" />
                            <?php } ?>
                        </div>

                        <div class="o-postWrap">
                            <header class="o-postHeader __nop">
                                <div class="o-postCat"><?php echo esc_html(trim(get_post_meta($post->ID, 'shoulder_copy', true))); ?></div>
                                <h3 class="o-postTitle"><?php if ( is_mobile() ) { echo wp_trim_words( get_the_title(), 25, '...' ); } else { trim_title(32); } ?></h3>
                            </header>

                            <ul class="o-postDetail mb-1">
                                <li class="o-postDetail_item __price">
                                <span class="o-postDetail_name">賃料</span><?php echo get_kakaku_list_for_fudo($b_id);?>
                                </li>
                                <li class="o-postDetail_item __year"><span class="o-postDetail_name">築年月</span>
                                <?php
                                $tatemonochikunenn = get_post_meta($b_id, 'tatemonochikunenn', true);
                                $chikunen = get_post_meta($b_id, 'tatemonochikunenn', true);
                                // var_dump($tatemonochikunenn);
                            if(!empty($tatemonochikunenn)){

                                if(strpos($tatemonochikunenn, '月')){
                                    $tatemonochikunenn_data = $tatemonochikunenn . "01";
                                    $tatemonochikunenn_data = str_replace(array("年","月"), "/", $tatemonochikunenn_data);
                                }else{
                                    $tatemonochikunenn_data = $tatemonochikunenn . "/01";
                                }
                            }

                            echo $chikunen;
                            echo "(" . get_tikunensu($tatemonochikunenn_data) . ")";
                            // echo get_sintiku_mark($tatemonochikunenn_data);
                            ?>
                                </li>
                                <li class="o-postDetail_item __location"><span class="o-postDetail_name">所在地</span>
                                <?php echo str_replace("東京都", "", my_custom_shozaichi_print($b_id));?>
                                <?php echo get_post_meta($b_id, 'shozaichimeisho', true);?>
                                <?php echo get_post_meta($b_id, 'shozaichimeisho2', true);?>
                                </li>
                                <li class="o-postDetail_item __size">
                                <span class="o-postDetail_name">間取り</span><?php echo get_senyumenseki_list_for_fudo($b_id);?>/
                                <?php echo get_madori_list_for_fudo($b_id);?>
                                </li>
                            </ul>
                            <?php
                            if ($dp_options['show_introduce_categories']) {
                                $metas = array();
                                foreach(explode('-', $dp_options['show_introduce_categories']) as $cat) {
                                    if (!empty($dp_options['use_introduce_category'.$cat])) {
                                        $terms = get_the_terms($post->ID, $dp_options['introduce_category'.$cat.'_slug']);
                                        if ($terms && !is_wp_error($terms)) {
                                            $term = array_shift($terms);
                                            $metas[] = '<li class="o-postTag_item"><span data-href="'.get_term_link($term).'" title="'.esc_attr($term->name).'">'.esc_html($term->name).'</span></li>';
                                        }
                                    }
                                }
                                if ($metas) {
                                    echo '<ul class="o-postTag flex-wrap">'.implode('', $metas).'</ul>';
                                }
                            }
                            ?>


                        </div>
                        <a href="<?php the_permalink() ?>" class="o-postLink" title="<?php the_title_attribute(); ?>"></a>

                    </section>
                    <?php
                    $i++;
                    endwhile;
                    wp_reset_postdata();
                    ?>
                </div>

                <?php if($cb_posts_count > 7) { ?>
                <div class="inner">
                    <a href="<?php echo home_url('/introduce/');?>" class="o-btn __basic07 __sizem __p __radius01 __maxwidth01">物件一覧を見る</a>
                </div>
                <?php } ?>
            </div>
        </div>
    </div>

    <?php
    endif;

    //カルーセルスライダー
    elseif ( $content['cb_content_select'] == 'carousel' ) :
    $args = array('post_type' => 'post', 'posts_per_page' => $content['cb_post_num'], 'ignore_sticky_posts' => 1, 'orderby' => 'date', 'order' => 'DESC');

    if ($content['cb_post_type'] == 'introduce') :
    $args['post_type'] = $dp_options['introduce_slug'];
    // タクソノミーターム
    if ($content['cb_introduce_term']) :
    $term = get_term($content['cb_introduce_term']);
    if ($term && !is_wp_error($term)) :
    $args['tax_query'][] = array('taxonomy' => $term->taxonomy, 'field' => 'term_id', 'terms' => array($term->term_id), 'operator' => 'IN');
    endif;
    endif;
    else :
    if ($content['cb_list_type'] == 'recommend_post' || $content['cb_list_type'] == 'recommend_post2') :
    $args['orderby'] = 'rand';
    $args['meta_key'] = $content['cb_list_type'];
    $args['meta_value'] = 'on';
    endif;
    endif;

    $cb_posts = new WP_Query($args);
    if ($cb_posts->have_posts()) :
    ?>

    <div id="cb_<?php echo esc_attr($key); ?>" class="cb_content cb_content-<?php echo esc_attr($content['cb_content_select']); ?> inview-fadein" style="background-color:<?php echo esc_attr($content['cb_background_color']); ?>">
        <div class="inner">
            <?php
            if ( $content['cb_headline'] ) :
            echo '   <h2 class="o-title h2">' . str_replace( array( "\r\n", "\r", "\n" ), '<br>', esc_html( $content['cb_headline'] ) ) . "</h2>\n";
            endif;
            if ( $content['cb_desc'] ) :
            echo '   <p class="cb_desc">' . str_replace( array( "\r\n", "\r", "\n" ), '<br>', esc_html( $content['cb_desc'] ) ) . "</p>\n";
            endif;
            ?>
            <div class="carousel">
                <?php

                while ($cb_posts->have_posts()) :
                $cb_posts->the_post();
                ?>
                <div class="article item">
                    <a href="<?php the_permalink() ?>" title="<?php the_title_attribute(); ?>">
                        <div class="image">
                            <?php if ( has_post_thumbnail() ) { the_post_thumbnail( 'size2' ); } else { ?><img src="<?php echo get_template_directory_uri(); ?>/img/common/no_image2.gif" title="" alt="" /><?php } ?>
                            <h3 class="title"><?php trim_title( 34 ); ?></h3>
                        </div>
                        <p class="excerpt"><?php new_excerpt( 90 ); ?></p>
                    </a>
                </div>
                <?php
                endwhile;
                wp_reset_postdata();
                ?>
            </div>
        </div>
    </div>

    <?php
    endif;

    // カテゴリーリスト
    elseif ( $content['cb_content_select'] == 'category_list' ) :
    if ( $content['cb_categories'] && is_array( $content['cb_categories'] ) ) :
        $terms = array();
        foreach($content['cb_categories'] as $term_id) :
            $term = get_term($term_id);
            if ($term && !is_wp_error($term)) :
            $terms[] = $term;
            endif;
        endforeach;
    if ( $terms ) :
    ?>

    <div id="cb_<?php echo esc_attr($key); ?>" class="cb_content cb_content-<?php echo esc_attr($content['cb_content_select']); ?> inview-fadein">
        <div class="inner">
            <?php
            if ( $content['cb_headline'] ) :
            echo '   <h2 class="o-title h2">' . str_replace( array( "\r\n", "\r", "\n" ), '<br>', esc_html( $content['cb_headline'] ) ) . "</h2>\n";
            endif;
            if ( $content['cb_desc'] ) :
            echo '   <p class="cb_desc">' . str_replace( array( "\r\n", "\r", "\n" ), '<br>', esc_html( $content['cb_desc'] ) ) . "</p>\n";
            endif;

            echo '   <ul class="clearfix">';
            foreach( $terms as $term ) {
                // カテゴリー画像
                $image = null;
                $term_meta = get_option( 'taxonomy_' . $term->term_id );
                if ( !empty( $term_meta['image'] ) ) :
                $image = wp_get_attachment_image_src( $term_meta['image'], 'size3' );
                endif;

                if ( !empty( $image[0] ) ) :
                echo '    <li class="has_image">';
                echo '<a href="' . get_term_link($term) . '">';
                echo '<div class="image"><img src="' . esc_attr( $image[0] ) . '" alt="" width="240" height="160"></div>';
                else :
                echo '    <li>';
                echo '<a href="' . get_term_link($term) . '">';
                endif;

                echo '<div class="info"><h3>' . esc_html( $term->name ) . '</h3>';
                if ( $term->description ) :
                echo str_replace( array( "\r\n", "\r", "\n" ), '<br>', esc_html( $term->description ) );
                endif;
                echo "</div></a></li>\n";
            }
            echo "</ul>\n";
            ?>
        </div>
        <?php
        if (esc_attr($key) == 1){//エリア
            $link_text = 'カテゴリ一覧を見る';
            $archive_link = home_url('/category/');
        }elseif(esc_attr($key) == 2){
            $link_text = 'エリア一覧を見る';
            $archive_link = home_url('/area/');
        }else{//こだわり条件
            $link_text = '特徴一覧を見る';
            $archive_link = home_url('/condition/');
        }
        ?>

        <?php if(count($terms) > 9) { ?>
        <div class="inner">
            <a href="<?php echo $archive_link; ?>" class="o-btn __basic07 __sizem __p __radius01 __maxwidth01"><?php echo $link_text; ?></a>
        </div>
        <?php } ?>


    </div>

    <?php
    endif;
    endif;

    // 最新ブログ記事一覧
    elseif ( $content['cb_content_select'] == 'blog_list' ) :
    $args = array( 'post_type' => 'post', 'posts_per_page' => $content['cb_post_num'], 'ignore_sticky_posts' => 1, 'orderby' => 'date', 'order' => 'DESC' );
    if ( isset( $content['cb_list_type'] ) ) :
    if ( $content['cb_list_type'] == 'recommend_post' || $content['cb_list_type'] == 'recommend_post2' ) :
    $args['meta_key'] = $content['cb_list_type'];
    $args['meta_value'] = 'on';
    elseif ( $content['cb_post_term'] ) :
    $term = get_term( $content['cb_post_term'] );
    if ( $term && !is_wp_error( $term ) ) :
    $args['tax_query'][] = array( 'taxonomy' => $term->taxonomy, 'field' => 'term_id', 'terms' => array($term->term_id), 'operator' => 'IN' );
    endif;
    endif;
    if ( $content['cb_order'] == 'random' ) :
    $args['orderby'] = 'rand';
    elseif ( $content['cb_order'] == 'date2' ) :
    $args['order'] = 'ASC';
    endif;
    endif;

    $cb_posts = new WP_Query($args);
    if ($cb_posts->have_posts()) :
    ?>

    <div id="cb_<?php echo esc_attr($key); ?>" class="cb_content cb_content-<?php echo esc_attr($content['cb_content_select']); ?> inview-fadein">
        <div class="inner">
            <?php
            if ( $content['cb_headline'] ) :
            echo '   <h2 class="o-title h2">' . str_replace( array( "\r\n", "\r", "\n" ), '<br>', esc_html( $content['cb_headline'] ) ) . "</h2>\n";
            endif;
            if ( $content['cb_desc'] ) :
            echo '   <p class="cb_desc">' . str_replace( array( "\r\n", "\r", "\n" ), '<br>', esc_html( $content['cb_desc'] ) ) . "</p>\n";
            endif;
            ?>
            <ol id="post_list" class="inview-fadein clearfix">
                <?php
                while ($cb_posts->have_posts()) :
                $cb_posts->the_post();
                ?>
                <li class="article">
                    <a href="<?php the_permalink() ?>" title="<?php the_title_attribute(); ?>">
                        <div class="image">
                            <?php if ( has_post_thumbnail()) { the_post_thumbnail( 'size2' ); } else { ?><img src="<?php echo get_template_directory_uri(); ?>/img/common/no_image2.gif" title="" alt="" /><?php } ?>
                        </div>
                        <h3 class="title js-ellipsis"><?php the_title(); ?></h3>
                        <?php
                        $metas = array();
                        if ($dp_options['show_categories']) {
                            foreach(explode('-', $dp_options['show_categories']) as $cat) {
                                if ($cat == 1) {
                                    $terms = get_the_terms($post->ID, 'category');
                                    if ($terms && !is_wp_error($terms)) {
                                        $term = array_shift($terms);
                                        $metas[] = '<li class="cat"><span class="cat-category" data-href="'.get_term_link($term).'" title="'.esc_attr($term->name).'">'.esc_html($term->name).'</span></li>';
                                    }
                                } elseif (!empty($dp_options['use_category'.$cat])) {
                                    $terms = get_the_terms($post->ID, $dp_options['category'.$cat.'_slug']);
                                    if ($terms && !is_wp_error($terms)) {
                                        $term = array_shift($terms);
                                        $metas[] = '<li class="cat"><span class="cat-'.esc_attr($dp_options['category'.$cat.'_slug']).'" data-href="'.get_term_link($term).'" title="'.esc_attr($term->name).'">'.esc_html($term->name).'</span></li>';
                                    }
                                }
                            }
                        }
                        if ($dp_options['show_date']) {
                            $metas[] = '<li class="date"><time class="entry-date updated" datetime="'.get_the_modified_time('c').'">'.get_the_time('Y.m.d').'</time></li>';
                        }

                        if ($metas) {
                            echo '<ul class="meta clearfix">'.implode('', $metas).'</ul>';
                        }
                        ?>
                    </a>
                </li>
                <?php
                endwhile;
                wp_reset_postdata();
                ?>
            </ol>
            <div class="inner">
                <a href="<?php $terms = get_the_terms($post->ID, 'category'); ?><?php echo get_term_link($term) ;?>" class="o-btn __basic07 __sizem __p __radius01 __maxwidth01"><?php echo esc_html($content['cb_archive_link_text']); ?></a>
            </div>

            <?php
            if ( $content['cb_show_archive_link'] && $content['cb_archive_link_text'] && get_post_type_archive_link('post') != get_bloginfo( 'url' ) ) :
            ?>
            <div class="archive_link">
                <a href="<?php echo get_post_type_archive_link('post'); ?>"><?php echo esc_html($content['cb_archive_link_text']); ?></a>
            </div>
            <?php
            endif;
            ?>
        </div>
    </div>

    <?php
    endif;

    //フリースペース
    elseif ( $content['cb_content_select'] == 'wysiwyg' ) :
    $cb_wysiwyg_editor = apply_filters( 'the_content', $content['cb_wysiwyg_editor'] );
    if ( $cb_wysiwyg_editor ) :
    ?>

    <div id="cb_<?php echo esc_attr($key); ?>" class="cb_content cb_content-<?php echo esc_attr($content['cb_content_select']); ?>">
        <div class="inner">
            <div class="post_content clearfix">
                <?php echo $cb_wysiwyg_editor; ?>
            </div>
        </div>
    </div>

    <?php
    endif;
    endif;

    endforeach;
    endif;
    ?>

</div><!-- END #main_col -->

<?php get_footer(); ?>
