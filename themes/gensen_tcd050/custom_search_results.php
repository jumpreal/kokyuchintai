<?php
    // カスタム検索用グローバル変数
    global $custom_search_vars;

    get_header();
    $dp_options = get_desing_plus_option();

    // タグフィルター用ターム配列
    $tags = false;
    if ($custom_search_vars) {
        if ($dp_options['show_search_results_tag_filter'] && $dp_options['show_search_results_tag_filter'] != 'hide') {
            if ($dp_options['searcn_post_type'] == 'post') {
                $tags = get_terms('post_tag', array());
            } elseif ($dp_options['searcn_post_type'] == 'introduce') {
                $tags = get_terms($dp_options['introduce_tag_slug'], array());
            }
            if (!$tags || is_wp_error($tags)) $tags = false;
        }
    }

    // sort
    if (!empty($_REQUEST['sort']) && in_array($_REQUEST['sort'], array('date_asc', 'date_desc', 'views'))) {
        $sort = $_REQUEST['sort'];
    } else {
        $sort = 'date_desc';
    }
    $sort_base_url = remove_query_arg('sort');
    $sort_base_url = preg_replace('#/page/\d+#', '', $sort_base_url);
?>

<?php get_template_part('breadcrumb'); ?>

<?php
    if (is_category() || is_tax()) {
        $queried_object = get_queried_object();
?>
<div class="archive_header">
 <div class="inner">
  <h2 class="o-title h2"><?php echo esc_html($queried_object->name); ?></h2>
<?php
        if ($queried_object->description) {
?>
  <p class="desc"><?php echo str_replace(array("\r\n", "\r", "\n"), '<br>', esc_html($queried_object->description)); ?></p>
<?php
        }
?>
 </div>
</div>
<?php
    } elseif ($dp_options['search_results_headline']) {
?>
<div class="archive_header">
 <div class="inner">
  <h2 class="o-title h2" style="margin-bottom:0;"><?php echo esc_html($dp_options['search_results_headline']); ?></h2>
 </div>
</div>
<?php
    }
?>

<div id="main_col" class="clearfix">

 <div id="left_col" class="custom_search_results">

<?php
    if (have_posts() || !empty($_REQUEST['filter_tag'])) {
        // タグ絞り込み検索表示
        if ($tags) {
            // form action先を1ページ目に
            $archive_filter_action = explode('?', get_pagenum_link(1));
            $archive_filter_action = array_shift($archive_filter_action);

            // トグルあり オープン
            if ($dp_options['show_search_results_tag_filter'] === 'type2') {
                $toggle = 'open';
            // トグルあり クローズ
            } elseif ($dp_options['show_search_results_tag_filter'] === 'type3') {
                $toggle = 'close';
            } else {
                $toggle = '';
            }
            // トグルあり 絞り込み検索・ページ移動時にクッキー値でオープンクローズを上書き
            if ($toggle && (isset($_REQUEST['filter'])|| get_query_var('paged')) && !empty($_COOKIE['gensen_archive_filter_toggle']) && in_array($_COOKIE['gensen_archive_filter_toggle'], array('open', 'close'))) {
                $toggle = $_COOKIE['gensen_archive_filter_toggle'];
            }
?>
 <form action="<?php echo esc_attr($archive_filter_action); ?>" method="get" class="archive_filter<?php if ($toggle) echo ' is-'.esc_attr($toggle); ?>">
<?php
            foreach(array('post_type', 'page_id', 'cat', 'p', 'sort', 'search_keywords', 'search_keywords_operator', 'search_cat1', 'search_cat2', 'search_cat3') as $get_key) {
                if (!empty($_REQUEST[$get_key])) {
?>
   <input type="hidden" name="<?php echo esc_attr($get_key); ?>" value="<?php echo esc_attr(stripslashes($_REQUEST[$get_key])); ?>">
<?php
                }
            }
            // トグルあり 絞り込み検索を判別するためにarchive_filterを出力
            if ($toggle) {
?>
  <input type="hidden" name="filter" value="1">
<?php
            }
?>
  <div class="archive_filter_headline rich_font"><?php _e('Refine Search', 'tcd-w'); ?></div>
  <div class="archive_filter_toggle"<?php if ($toggle == 'close') echo ' style="display:none"'; ?>>
    <div class="archive_filter_tag clearfix">
<?php
            foreach($tags as $tag) {
                $checked = '';
                if (!empty($_REQUEST['filter_tag']) && in_array($tag->term_id, $_REQUEST['filter_tag'])) {
                    $checked = ' checked="checked"';
                } else {
                    $checked = '';
                }
                echo '     <label><input type="checkbox" name="filter_tag[]" value="'.esc_attr($tag->term_id).'"'.$checked.'><span>'.esc_html($tag->name).'</span></label>'."\n";
            }
?>
    </div>
    <div class="button">
     <input type="submit" value="<?php _e('Search for', 'tcd-w'); ?>">
    </div>
  </div>
 </form>
<?php
        }
    }
?>
<?php
if(is_user_logged_in()){
// echo '一般ユーザーには表示されません。';
// var_dump($posts);
}
?>
<?php if ( have_posts() ) : ?>
 <dl class="archive_sort clearfix">
  <dt><?php _e('Sort condition', 'tcd-w'); ?></dt>
  <dd><a href="<?php echo esc_attr(add_query_arg('sort', 'date_desc', $sort_base_url)); ?>"<?php if ($sort == 'date_desc') echo ' class="active"'; ?>><?php _e('Newest first', 'tcd-w'); ?></a></dd>
  <dd><a href="<?php echo esc_attr(add_query_arg('sort', 'date_asc', $sort_base_url)); ?>"<?php if ($sort == 'date_asc') echo ' class="active"'; ?>><?php _e('Oldest first', 'tcd-w'); ?></a></dd>
  <dd><a href="<?php echo esc_attr(add_query_arg('sort', 'views', $sort_base_url)); ?>"<?php if ($sort == 'views') echo ' class="active"'; ?>><?php _e('Large number of views', 'tcd-w'); ?></a></dd>
 </dl>

<?php get_template_part('navigation2'); ?>

 <div class="flex-wrap">

<?php   while ( have_posts() ) : the_post(); ?>
     <section class="o-post __3 vacant <?php echo esc_attr($col_class); ?>">
         <div class="o-postThumb">
             <?php

             $bukken_id = get_field('related_fudo', $post->ID);
             $b_id = $bukken_id[0];
             // var_dump($b_id);

              /**
              * single-fudoから読み込み
              * single-roomからも活用
              * アイキャッチ読み込み
              */
             $fudo_gaikan_images = get_field('bukken_gaikan_kyouyou', $b_id);
             // print_r($fudo_gaikan_images[0]['sizes']);
             if($fudo_gaikan_images != ''){
                 $fudoimg_data=$fudo_gaikan_images[0]['sizes']['large'];
                 // echo $fudoimg_data;
             }else{
                 $fudoimg_data="https://koukyu-chintai.com/wp-content/uploads/cropped-cropped-main-1-2000x1200-1-1024x614.jpg";
             }

             // $room_images = get_field('room_situnai_senyu', $b_id);
             // // print_r($room_images[0]['sizes']);
             // if($room_images != ''){
             //     $roomimg_data=$room_images[0]['sizes']['full'];
             //     // echo $roomimg_data;
             // }else{
             //     $roomimg_data="https://koukyu-chintai.com/wp-content/uploads/cropped-cropped-main-1-2000x1200-1-1024x614.jpg";
             // }

             $img1 = @$fudo_gaikan_images[0]['sizes']['large'];
             $img2 = @$fudo_gaikan_images[1]['sizes']['large'];
             $img3 = @$fudo_gaikan_images[2]['sizes']['large'];
             if ( is_null($img1) || is_null($img2) || is_null($img3) ){
                 $gai_images = stw_get_singlefudo_images($b_id,2);
                 $img1_path = '';
                 if ( 0 < count($gai_images) ) {
                     $img1_path = $gai_images[0]->image_path;
                     $img1 = stw_generate_imgurl($img1_path);
                 }
                 $h_images = stw_get_singlefudo_images($b_id);
                 $h_images_count_without_ban = count($h_images);

                 if ( 0 < count($h_images) ){
                     foreach ($h_images as $oh ){
                         if ( is_null($img1) ){
                             if(!in_array(basename($oh->image_path), get_ban_image_file_name())){
                             $img1 = stw_generate_imgurl($oh->image_path);
                             continue;
                             }else{
                                 $h_images_count_without_ban--;
                                 continue;
                             }
                         }
                         if ( is_null($img2) && $oh->image_path != $img1_path ){
                             if(!in_array(basename($oh->image_path), get_ban_image_file_name())){
                             $img2 = stw_generate_imgurl($oh->image_path);
                             continue;
                             }else{
                                 $h_images_count_without_ban--;
                                 continue;
                             }
                         }
                         if ( is_null($img3) && $oh->image_path != $img1_path ){
                             if(!in_array(basename($oh->image_path), get_ban_image_file_name())){
                             $img3 = stw_generate_imgurl($oh->image_path);
                             continue;
                             }else{
                                 $h_images_count_without_ban--;
                                 continue;
                             }
                         }
                     }
                     /*
                     $img1 = ( isset($h_images[0]) ) ? stw_generate_imgurl($h_images[0]->image_path) : '';
                     $img2 = ( isset($h_images[1]) ) ? stw_generate_imgurl($h_images[1]->image_path) : '';
                     $img3 = ( isset($h_images[2]) ) ? stw_generate_imgurl($h_images[2]->image_path) : '';
                     */
                 }
             }

            // if(isset($img1)){
            //     echo '<img src="' . $img1 . '">';
            // } else {
            //     if ( is_mobile() ) {
            //         echo '<img src="' . get_template_directory_uri() . '/img/common/no_image1.gif" title="" alt="" />';
            //     } else {
            //         echo '<img src="' . get_template_directory_uri() . '/img/common/no_image2.gif" title="" alt="" />';
            //     }
            // }
             ?>
            <?php if(get_the_post_thumbnail_url($post->ID)) { ?>
                                <img src="<?php echo get_the_post_thumbnail_url($post->ID); ?>" width="280" height="160">
                            <?php }elseif(isset($img1)) {?>
                                <img src="<?php echo $img1; ?>" width="280" height="160">
                            <?php  }elseif (has_post_thumbnail()) { the_post_thumbnail('size3'); } else { ?>
                            <img src="<?php echo get_template_directory_uri(); ?>/img/common/no_image3.gif" title="" alt="" />
                            <?php } ?>
         </div>
         <div class="o-postWrap">
             <a href="<?php echo get_the_permalink($post->ID);?>" class="o-postLink"></a>
             <header class="o-postHeader __nop">
                 <div class="o-postCat"><?php echo esc_html(trim(get_post_meta($post->ID, 'shoulder_copy', true))); ?></div>
                 <h3 class="o-postTitle">
                    <a href="<?php echo get_the_permalink($post->ID);?>">
                        <?php trim_title(38); ?>
                    </a>
                </h3>
             </header>

                <ul class="o-postDetail mb-1">
                                    <li class="o-postDetail_item __price">
                                    <span class="o-postDetail_name">賃料</span><?php echo get_kakaku_list_for_fudo($b_id);?>
                                    </li>
                                    <li class="o-postDetail_item __year"><span class="o-postDetail_name">築年月</span>
                                    <?php
                                    $tatemonochikunenn = get_post_meta($b_id, 'tatemonochikunenn', true);
                                    // var_dump($tatemonochikunenn);
                                if(!empty($tatemonochikunenn)){

                                    if(strpos($tatemonochikunenn, '月')){
                                        $tatemonochikunenn_data = $tatemonochikunenn . "01";
                                        $tatemonochikunenn_data = str_replace(array("年","月"), "/", $tatemonochikunenn_data);
                                    }else{
                                        $tatemonochikunenn_data = $tatemonochikunenn . "/01";
                                    }
                                }

                                echo $tatemonochikunenn;
                                echo "(" . get_tikunensu($tatemonochikunenn_data) . ")";
                                // echo get_sintiku_mark($tatemonochikunenn_data);
                                ?>
                                    </li>
                                    <li class="o-postDetail_item __location"><span class="o-postDetail_name">所在地</span>
                                    <?php echo str_replace("東京都", "", my_custom_shozaichi_print($b_id));?>
                                    <?php echo get_post_meta($b_id, 'shozaichimeisho', true);?>
                                    <?php echo get_post_meta($b_id, 'shozaichimeisho2', true);?>
                                    </li>
                                    <li class="o-postDetail_item __size">
                                    <span class="o-postDetail_name">間取り</span><?php echo get_senyumenseki_list_for_fudo($b_id);?>/
                                    <?php echo get_madori_list_for_fudo($b_id);?>
                                    </li>
                                    <li class="o-postDetail_item __size"><span class="o-postDetail_name">登録日：</span><?php echo get_the_date("Y-m-d"); ?></li>
                                </ul>
             <?php
             $metas = array();
             if ($post->post_type == 'post') {
                 if ($dp_options['show_categories']) {
                     foreach(explode('-', $dp_options['show_categories']) as $cat) {
                         if ($cat == 1) {
                             $terms = get_the_terms($post->ID, 'category');
                             if ($terms && !is_wp_error($terms)) {
                                 foreach ($terms as $term) {
                                     $metas['category'][] = '<li class="o-postTag_item"><a href="'.get_term_link($term).'" title="'.esc_attr($term->name).'">'.esc_html($term->name).'</a></li>';
                                 }
                                 $metas['category'] = ''.implode('', $metas['category']).'';
                             }
                         } elseif (!empty($dp_options['use_category'.$cat])) {
                             $terms = get_the_terms($post->ID, $dp_options['category'.$cat.'_slug']);
                             if ($terms && !is_wp_error($terms)) {
                                 foreach ($terms as $term) {
                                     $metas['category'.$cat][] = '<li class="o-postTag_item"><a href="'.get_term_link($term).'" title="'.esc_attr($term->name).'">'.esc_html($term->name).'</a></li>';
                                 }
                                 $metas['category'.$cat] = ''.implode('', $metas['category'.$cat]).'';
                             }
                         }
                     }
                 }

             } elseif ($post->post_type == $dp_options['introduce_slug']) {
                 if ($dp_options['show_introduce_categories']) {
                     foreach(explode('-', $dp_options['show_introduce_categories']) as $cat) {
                         if (!empty($dp_options['use_introduce_category'.$cat])) {
                             $terms = get_the_terms($post->ID, $dp_options['introduce_category'.$cat.'_slug']);
                             if ($terms && !is_wp_error($terms)) {
                                 foreach ($terms as $term) {
                                     $metas['introduce_category'.$cat][] = '<li class="o-postTag_item"><a href="'.get_term_link($term).'" title="'.esc_attr($term->name).'">'.esc_html($term->name).'</a></li>';
                                 }
                                 $metas['introduce_category'.$cat] = ''.implode('', $metas['introduce_category'.$cat]).'';
                             }
                         }
                     }
                 }
             }

             if ($metas) {
                 echo '    <ul class="o-postTag flex-wrap">'.implode('', $metas).'</ul>'."\n";
             }
             ?>
             <a href="<?php the_permalink() ?>" title="<?php the_title_attribute(); ?>" class="o-link over"></a>
         </div>
     </section>





<?php   endwhile; ?>

 </div><!-- END #post_list2 -->

<?php get_template_part('navigation2'); ?>

<?php else: ?>
 <p class="no_post"><?php _e('There is no registered post.', 'tcd-w'); ?></p>
<?php endif; ?>

</div><!-- END #left_col -->

<?php get_sidebar(); ?>

</div><!-- END #main_col -->

<?php get_footer(); ?>
