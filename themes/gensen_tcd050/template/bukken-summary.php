<?php

    $f_title = get_the_title($fudo_id);
    $bukken_gaiyo = my_custom_kyouyu_setsubi_print($fudo_id);
    $kakakushikikin = get_field('kakakushikikin', $fudo_id);
    $kakakureikin = get_field('kakakureikin', $fudo_id);
    $shuuhenshougaku = get_field('shuuhenshougaku', $fudo_id);
    $shuuhenchuugaku = get_field('shuuhenchuugaku', $fudo_id);
    $ken = my_custom_shozaichi_print($fudo_id);
    $address = get_field('shozaichimeisho', $fudo_id) . get_field('shozaichimeisho2', $fudo_id);
    $madori_list = get_madori_list_for_fudo($fudo_id);
    $moyori = my_custom_koutsu_all($fudo_id);
    $chinryo = get_kakaku_list_for_fudo($fudo_id);
    $chikunen = get_post_meta($fudo_id, 'tatemonochikunenn', true);
    $nensu = get_tikunensu(get_post_meta($fudo_id, 'tatemonochikunenn', true) . "/01");
    $madori = get_madori_list_for_fudo($fudo_id);
    $menseki = get_senyumenseki_list_for_fudo($fudo_id);
    $tourokubi = get_the_date('Y/m/d');
    $kousinbi = get_the_modified_date('Y/m/d');
    $soukosu = get_post_meta($fudo_id, 'bukkensoukosu', true);
    $kouzou = my_custom_tatemonokozo_print($fudo_id);
    if(get_post_meta($fudo_id, 'tatemonokaisu1', true)!="") $kaisu = '地上'.get_post_meta($fudo_id, 'tatemonokaisu1', true)."階";
    if(get_post_meta($fudo_id, 'tatemonokaisu2', true)!="") $kaisu .= '<br>地下'.get_post_meta($fudo_id, 'tatemonokaisu2', true)."階";

    $bukken_gaiyo_print=<<<EOF
    <h2>物件概要</h2>
    <table class="o-roomTable __style01">
        <tbody>
            <tr>
            <th>物件名</th>
                <td colspan="3">{$f_title}</td>
            </tr>
            <tr>
            <th>住所</th>
                <td colspan="3">{$ken}{$address}</td>
            </tr>
            <tr>
            <th>最寄駅</th>
            <td colspan="3">{$moyori}</td>
        </tr>
        <tr>
        <th>賃料</th>
        <td>{$chinryo}</td>
        <th>築年月</th>
        <td>{$chikunen}（{$nensu}）</td>
        </tr>
        <tr>
        <th>間取り</th>
        <td>{$madori}</td>
        <th>専有面積</th>
        <td>{$menseki}</td>
        </tr>
        <tr>
            <th>物件特徴</th>
            <td></td>
            <th>構造</th>
            <td>{$kouzou}</td>
        </tr>
        <tr>
            <th>総階数</th>
            <td>{$kaisu}</td>
            <th>総戸数</th>
            <td>{$soukosu}戸</td>
        </tr>
        <tr>
            <th>物件種別</th>
            <td>マンション</td>
            <th>登録日<br>更新日</th>
            <td>{$tourokubi}<br>{$kousinbi}</td>
        </tr>
        <tr>
                <th>設備</th>
        <td colspan="3">
                    <ul>
                        {$bukken_gaiyo}
                    </ul>
                </td>
        </tr>

        </tbody>
    </table>
EOF;


echo $bukken_gaiyo_print;
?>
