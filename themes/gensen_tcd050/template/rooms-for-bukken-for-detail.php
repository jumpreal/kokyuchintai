<?php
//件数表示
$connected_parent = get_posts($args);
$bukken_mansitu_fg = get_field('bukken_status');
$b_id = $args['connected_items'];
$kensu = count($connected_parent);


if($kensu > 0){
    ?>
<div class="o-partsMore">
    <h3>賃貸物件一覧 (<?php echo $kensu;?>件)</h3>
    <div class="o-room __horizontal">
<?php
/**
 * 物件に表示する部屋のループ処理PC
 */
// echo __FILE__;

if(count($connected_parent)>0){

    $i = 1;
    $room_image = '';
    $modal = '';
    $modal_script = '';

    foreach($connected_parent as $ck=>$cv){

    $room_post_ID = $cv->ID;

    //物件について質問するリンク
    $kuusitu_inquiry_link = get_permalink(1563) . "?bukken_id=" . $b_id . "&room_id=" . $room_post_ID;

    $room_meta=get_post_meta($room_post_ID);
    $str=$room_meta["kakaku"][0];
    $num = preg_replace('/[^0-9]/', '',$str);
    $tani=preg_replace('/'.$num.'/','',$str);
    $sr = get_field('kakakushikikin', $room_post_ID) ."/".get_field('kakakureikin', $room_post_ID);

    $source = get_post_meta($room_post_ID, 'source', true);

    global $wpdb;
    $rtn_arr = array();
    // postmetaよりuniq_idを取得
    $uniq_res = $wpdb->get_row($wpdb->prepare("select meta_value from ".$wpdb->prefix . "postmeta where post_id = %s and meta_key = 'uniq_id'", $room_post_ID ));

    $uniq_id = $uniq_res->meta_value;
    // スクレイピングした画像情報を取得
    $sql = $wpdb->prepare("select * from housing where uniq_id = %s", $uniq_id);
    $res = $wpdb->get_results($sql);
    // var_dump($res);
    $url = $res[0]->url;
    // $bosyu_juuko = get_field('madori_img', $room_post_ID);
    // if(is_array($bosyu_juuko))
    // $madori_img = $bosyu_juuko['url'];

    $bosyu_juuko = get_field('room_madori_images', $room_post_ID);
// print_r($bosyu_juuko);
    if ( isset($bosyu_juuko[0]['sizes']['large']) ) {
        $madori_img = $bosyu_juuko[0]['sizes']['large'];
    } else {
        $madori_img = stw_get_singlefudo_madori_image($room_post_ID);
    }

    if($madori_img !== ''){
        $room_image = $madori_img;
    }else{
        $room_image = "nowprinting.jpg";
    }

    $chinryo = get_mansitu_text($room_post_ID, $bukken_mansitu_fg, number_unit($num).$tani, '現在募集はございません');

    //部屋詳細へのリンク
    $room_detail_link = get_permalink($room_post_ID);

    //部屋への問合せリンク
    $room_inquiry_link = get_permalink($room_post_ID).'#toiawasesaki';

    //管理費
    $kanrihi = (get_field('kakakukyouekihi', $room_post_ID) ? number_format(get_field('kakakukyouekihi', $room_post_ID)) . "円" : "なし" );



    // echo $madori_img;

    // echo '<tr>';

    // //マイリスト登録用リンク生成
    // $link=wpfp_link_id($room_post_ID);

    // if($i > 3) {
    //     echo '<tr class="hide_area">';
    // }else{
    //     echo '<tr>';
    // }
    //     echo '<td><img style="width:150px;" src="' . $madori_img . '"></td>';
    //     //echo '<table class="rooms"><tr><th>部屋番号</th><td>'.$cv->post_title.'</td>';
    //     echo '<td>'. get_field('room_floor', $room_post_ID) .'階</td>';
    //     //echo '<td>'.$cv->post_title.'</td>';
    //     echo '<td>'. xls_custom_madorisu_print($room_post_ID) .'</td>';
    //     echo '<td>'. get_field('senyumenseki', $room_post_ID) .'㎡</td>';
    //     echo '<td>'. get_mansitu_text($room_post_ID, $bukken_mansitu_fg, number_unit($num).$tani.'円', '現在募集はございません') . '</td>';
    //     echo '<td>'. get_mansitu_text($room_post_ID, $bukken_mansitu_fg, $kanrihi, '-') .'</td>';
    //     echo '<td>'. get_mansitu_text($room_post_ID, $bukken_mansitu_fg, $sr, '-') .'</td>';
    //     // echo '<td>'. get_field('nyukyogenkyo', $room_post_ID).'</td>';
    //     echo '<td>'. get_mansitu_text($room_post_ID, $bukken_mansitu_fg, get_xls_custom_nyukyogenkyo_print($room_post_ID), '-') .'</td>';
    //     echo '<td><a href="'.get_permalink($room_post_ID).'">詳細</a></td>';
    //     echo '<td><a href="'.get_permalink($room_post_ID).'#toiawasesaki">お問合せ</a></td>';
    //     echo '<td>'.$link.'</td>';
    // echo '</tr>';

    if($i > 6) {
        $hide_class = 'hide_area';
    }else{
        $hide_class = '';
    }
?>
<section class="o-roomVacancy  flex-nowrap-no-lg ai-center <?php echo $hide_class;?>">
    <div class="flex-nowrap ai-center">
        <div class="o-roomVacancy_thumb js-modalImages<?php echo $nth; ?>-<?php echo $b_id; ?>-<?php echo $i;?>"><img src="<?php echo $room_image;?>" alt="" class="o-roomVacancy_img">
        </div>
        <div class="o-roomVacancy_info flex-nowrap-no-lg jc-start ai-center">
            <p class="o-roomVacancy_price"><?php echo $chinryo;?></p>
            <div class="o-roomVacancy_detailwrap">
            <div class="o-roomVacancy_cost flex-nowrap">
                <div class="o-roomVacancy_costItem __shikikin"><span class="o-roomVacancy_costItem_title">敷金</span>
                    <p class="o-roomVacancy_costItem_period"><?php
    if(get_field('bukken_status', $post_id) !== "2"){ //満室でなはい時表示
        the_field('kakakushikikin', $room_post_ID);
    }else{
        echo '-';
    }
                        ?></p>
                </div>
                <div class="o-roomVacancy_costItem __reikin"><span class="o-roomVacancy_costItem_title">礼金</span>
                    <p class="o-roomVacancy_costItem_period">
                        <?php
    if(get_field('bukken_status', $post_id) !== "2"){ //満室でなはい時表示
        the_field('kakakureikin', $room_post_ID);
    }else{
        echo '-';
    }
                        ?></p>
                </div>
                <div class="o-roomVacancy_costItem __kanri"><span class="o-roomVacancy_costItem_title">管理費</span>
                    <p class="o-roomVacancy_costItem_period"><?php
    if(get_field('bukken_status', $post_id) !== "2"){ //満室でなはい時表示
        if($kanrihi!=='円'){
            echo $kanrihi;
        }else{
            echo '-';
        }
    }else{
        echo '-';
    }
                        ?></p>
                </div>
            </div>
            <div class="o-roomVacancy_space flex-nowrap">
                <div class="o-roomVacancy_spaceItem __floor"><span class="o-roomVacancy_spaceItem_title">お部屋の階</span>
                    <p class="o-roomVacancy_spaceItem_period"><?php the_field('room_floor', $room_post_ID);?>階</p></div>
                <div class="o-roomVacancy_spaceItem __size">
                    <span class="o-roomVacancy_spaceItem_title">面積</span>
                    <p class="o-roomVacancy_spaceItem_period">
                <?php
                    echo get_post_meta($room_post_ID, 'senyumenseki', true);
                        ?>㎡</p>
                </div>
                <div class="o-roomVacancy_spaceItem __layout"><span class="o-roomVacancy_spaceItem_title">間取り</span>
                    <p class="o-roomVacancy_spaceItem_period"><?php echo xls_custom_madorisu_print($room_post_ID);?></p></div>
            </div>
            </div>
        </div>
    </div>
    <div class="o-roomVacancy_btn">
        <a href="<?php echo $kuusitu_inquiry_link;?>" class="o-roomVacancy_btnItem o-btn __basic01 __sizem __mid __navy01">お問い合わせ</a><br>
        物件ID:<?php echo $room_post_ID; ?>
    <?php if(is_user_logged_in()): ?>
        <div style="font-size:8pt;">
            読み込み元：<a target="_blank" href="<?php echo $url; ?>"><?php echo $source;?></a><br>
            <?php echo $uniq_id; ?>
        </div>
    <?php endif; ?>
    </div>
</section>

<?php

$modal=<<<EOF
<div class="o-modal __roomslick js-modalImages{$nth}_cont{$b_id}-{$i}">
    <div class="o-modalBg modal__bg js-modal-closeBg-{$b_id}-{$i}"></div>

    <div class="o-modalOverwrap">

        <a href="#" class="o-modalClose js-modal-close-{$b_id}-{$i}"><span class="o-modalClose_bar"></span><span class="o-modalClose_bar"></span></a>

        <div class="o-modalWrap">

            <div class="o-modalCont __noborder">
                <section class="o-roomSlick">
                <div class="o-roomSlick_wrap">

                <div class="o-roomSlick_main js-slideMain-{$i}">
EOF;

$modal.=get_bukken_images($room_post_ID, $room_image, $i);
$modal.=<<<EOF
                </div>

                <div class="o-roomSlick_sub js-slideThumb-{$i}">
                EOF;

                    $modal.=get_bukken_subimages($room_post_ID, $room_image, $i);
                    $modal.=<<<EOF

                        <a href="#" class="o-modalClose_btn js-modal-close-{$b_id}-{$i}">閉じる</a>
                </div>
                </div>
                </section>
            </div>
        </div>
    </div>
</div>
EOF;
echo $modal;

echo $modal_script=<<<EOF
<script>
    // js-modalImages_cont制御部
    jQuery(document).ready(function($) {
    $('.js-modalImages{$nth}-{$b_id}-{$i}').on('click', function () {
        $('html').css('overflow-y', 'hidden');  // 本文の縦スクロールを無効
        $('.js-modalImages{$nth}_cont{$b_id}-{$i}').fadeIn();
        $('.js-slideMain-{$i}').slick('setPosition');

        return false;
    });
    $('.js-modal-close-{$b_id}-{$i}').on('click', function () {
        $('.js-modalImages{$nth}_cont{$b_id}-{$i}').fadeOut();
        $('.slick-slide').show();
        $('html').css('overflow-y','auto');     // 本文の縦スクロールを有効
        return false;
    });
    $('.js-modal-closeBg-{$b_id}-{$i}').on('click', function () {
        $('.js-modalImages{$nth}_cont{$b_id}-{$i}').fadeOut();
        $('.slick-slide').show();
        return false;
    });
});
</script>

    <script>
    jQuery(function(){


        jQuery(function(){
            function sliderSetting{$i}(){

                var mainSlider{$i} = ".js-slideMain-{$i}"; //メインスライダーid
                var mainSliderItem{$i} = ".js-slideMain_item-{$i}"; //メインスライダーid
                var thumbnailSlider{$i} = ".js-slideThumb-{$i}"; //サムネイルスライダーid
                var thumbnailSliderItem{$i} = ".js-slideThumb_item-{$i}"; // サムネイル画像アイテム
                var width = jQuery(window).width();

                if(width <= 720){ //SPの設定 - 720より小さい時に
                    jQuery(mainSlider{$i}).slick({
                        autoplay: true,
                        slidesToScroll: 1,
                        speed: 1000,
                        autoplaySpeed: 10000,
                        arrows: true,
                        infinite: false,
                        swipeToSlide: true,
                        prevArrow: '<div class="o-modalArrow __prev"></div>',
                        nextArrow: '<div class="o-modalArrow __next"></div>',
                        asNavFor: thumbnailSlider{$i}
                    });
                    jQuery(thumbnailSlider{$i}).slick({
                        slidesToShow: 5,
                        slidesToScroll: 1,
                        speed: 1000,
                        autoplaySpeed: 10000,
                        arrows: false,
                        swipeToSlide: true,
                        infinite: false,
                        asNavFor: mainSlider{$i}
                    });
                    jQuery(thumbnailSlider{$i}+" .slick-slide").on('click',function(){
                        var index = jQuery(this).attr("data-slick-index");
                        jQuery(thumbnailSlider{$i}).slick("slickGoTo",index,false);
                    });
                } else {
                    //PCの設定
                    //slickスライダー初期化
                    jQuery(mainSlider{$i}).slick({
                        autoplay: false,
                        arrows: true,
                        fade: true,
                        infinite: true,
                        swipeToSlide: true,
                        prevArrow: '<div class="o-modalArrow __prev"></div>',
                        nextArrow: '<div class="o-modalArrow __next"></div>',
                    });


                    // メイン画像画像アイテムに data-index でindex番号を付与
                    jQuery(mainSliderItem{$i}).each(function(){
                        var index = jQuery(mainSliderItem{$i}).index(this);
                        // alert(index);1,3,5,7などとなる
                        jQuery(this).attr("data-slick-index", index);
                    });

                    // サムネイル画像アイテムに data-index でindex番号を付与
                    jQuery(thumbnailSliderItem{$i}).each(function(){
                        var index = jQuery(thumbnailSliderItem{$i}).index(this);
                        // alert("aa");
                        jQuery(this).attr("data-index", index);
                    });

                    // スライダー初期化後、カレントのサムネイル画像にクラス「thumbnail-current」を付ける
                    jQuery(mainSlider{$i}).on('init',function(slick){
                        var index = jQuery(".slide-item{$i}.slick-slide.slick-current").attr("data-slick-index");
                        // alert(index);//これがとれていない
                        jQuery(thumbnailSliderItem{$i}+'[data-index="'+index+'"]').addClass("thumbnail-current");
                        jQuery(mainSlider{$i}+'[data-index="' + index + '"]').addClass("thumbnail-current");
                    });


                    //サムネイル画像アイテムをクリックしたときにスライダー切り替え
                    //jQuery(thumbnailSliderItem{$i}).on('click',function(){
                    // ↑これだと i は賃貸物件一覧の数なので、例えば5件表示だが各物件に14まい画像ある場合などは動かない。
                    // i はあくまでmodalの数なので、それを条件にしつつ、class名の部分一致でイベントを取得するしかない。
                    jQuery('.js-slideThumb-{$i} [class*=js-slideThumb_item-]').on('click',function(){
                        //alert($i);
                        // var index = jQuery(this).attr("data-index");

                        // クリックされたサムネのclass群から、js-slideThumb-の後の数値だけ抽出する。
                        // この数値が、開きたいメインスライダーの画像index+1の値となる。
                        var siteiCon = jQuery(this).attr('class').replace(/[^0-9]/g, '');

                        // slickで画像の表示非表示は制御されるので、この後で明示的に処理する必要はない。
                        jQuery(mainSlider{$i}).slick("slickGoTo", siteiCon-1, false);
                    });

                    //サムネイル画像のカレントを切り替え
                    jQuery(mainSlider{$i}).on('beforeChange',function(event,slick, currentSlide,nextSlide){
                        jQuery(thumbnailSliderItem{$i}).each(function(){
                            jQuery(this).removeClass("thumbnail-current");
                        });
                        jQuery(thumbnailSliderItem{$i}+'[data-index="'+nextSlide+'"]').addClass("thumbnail-current");
                    });
                }
            }

            sliderSetting{$i}();

            jQuery(window).resize( function() {
                sliderSetting{$i}();
            });
        });
    });
        </script>
EOF;

$i++;
}//foreach


//3件以上の場合に隠す処理
if(count($connected_parent)>6){
    echo '<input id="o-partsMore_triggerRoom" class="o-partsMore_trigger __style02" type="checkbox" onclick="hideToggle(jQuery(\'.hide_area\'));">
    <label class="o-partsMore_btn __style02" for="o-partsMore_triggerRoom"><i class="o-partsMore_icon o-icon fas fa-angle-down"></i></label>';

echo $js=<<<EOF

<script>
// 初期表示
jQuery(function(){
    if ('1' != jQuery('#check').val()) {
        jQuery('.hide_area').hide();
    }
});
// 表示/非表示
var speed = 0; //表示アニメのスピード（ミリ秒）
var stateDeliv = 1;
function hideToggle(hidearea) {
    hidearea.toggle(speed);
}
</script>
EOF;
}
}
    ?>
</div>
</div>

<?php }else{?>

    <div class="o-partsMore">
    <h3>賃貸物件一覧 (<?php echo $kensu;?>件)</h3>
    </div>

<?php
}
?>




