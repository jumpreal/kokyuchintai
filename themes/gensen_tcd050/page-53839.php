<?php
    get_header();

    $display_title = get_post_meta($post->ID, 'display_title', true);
    if (!$display_title) $display_title = 'show';
    $display_side_content = get_post_meta($post->ID, 'display_side_content', true);
    if (!$display_side_content) $display_side_content = 'show';

    $image_id = get_post_meta($post->ID, 'page_image', true);
    if ($image_id) {
      $image = wp_get_attachment_image_src( $image_id, 'full' );
    }
    if (!empty($image[0])) {
      $headline = get_post_meta($post->ID, 'page_headline', true);
      $caption_style = 'font-size:'.get_post_meta($post->ID, 'page_headline_font_size', true).'px;';
      $caption_style .= 'color:'.get_post_meta($post->ID, 'page_headline_color', true).';';
      $shadow1 = get_post_meta($post->ID, 'page_headline_shadow1', true);
      $shadow2 = get_post_meta($post->ID, 'page_headline_shadow2', true);
      $shadow3 = get_post_meta($post->ID, 'page_headline_shadow3', true);
      $shadow4 = get_post_meta($post->ID, 'page_headline_shadow4', true);
      if (empty($shadow1)) $shadow1 = 0;
      if (empty($shadow2)) $shadow2 = 0;
      if (empty($shadow3)) $shadow3 = 0;
      if (empty($shadow4)) $shadow4 = '#333333';
      if ($shadow1 || $shadow2 || $shadow3) {
        $caption_style .= 'text-shadow:'.$shadow1.'px '.$shadow2.'px '.$shadow3.'px '.$shadow4.';';
      }
    }
?>

<?php get_template_part('breadcrumb'); ?>

<?php if (!empty($image[0])) { ?>
  <div id="header_image">
   <img src="<?php echo esc_attr($image[0]); ?>" alt="" />
   <?php if ($headline){ ?>
   <div class="caption rich_font" style="<?php echo esc_attr($caption_style); ?>">
    <?php echo str_replace(array("\r\n", "\r", "\n"), '<br />', esc_html($headline)); ?>
   </div>
   <?php } ?>
  </div>
<?php } ?>

<div id="main_col" class="clearfix">

<?php if ($display_side_content == 'show') { ?>
 <div id="left_col">
     <h2 class="o-title h2" style="user-select: auto;">エリア一覧</h2>
<?php } ?>
<div class="cb_content-category_list __3 __square">
   <ul>
<?php

$args = array('hide_empty' => false);
$terms = get_terms('property_area', $args);

  foreach ( $terms as $term ) {

      $term_meta = get_option( 'taxonomy_' . $term->term_id );
      if ( !empty( $term_meta['image'] ) ) :
      $image = wp_get_attachment_image_src( $term_meta['image'], 'size3' );
      endif;
      echo '<li class="has_image"><a href="'.get_term_link($term).'"><div class="image"><img src="' . $image[0] . '"></div><div class="info"><h3 id="' . $term->slug . '">' . $term->name . '</h3></div></a></li>';
  }


?>

    </ul>

     </div>

<?php
if ($display_side_content == 'show') {
?>
 </div><!-- END #left_col -->

<?php
  get_sidebar();
}
?>

</div><!-- END #main_col -->

<?php get_footer(); ?>
