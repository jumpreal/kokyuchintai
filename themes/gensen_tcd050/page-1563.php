<?php
    get_header();

    $display_title = get_post_meta($post->ID, 'display_title', true);
    if (!$display_title) $display_title = 'show';
    $display_side_content = get_post_meta($post->ID, 'display_side_content', true);
    if (!$display_side_content) $display_side_content = 'show';

    $image_id = get_post_meta($post->ID, 'page_image', true);
    if ($image_id) {
      $image = wp_get_attachment_image_src( $image_id, 'full' );
    }
    if (!empty($image[0])) {
      $headline = get_post_meta($post->ID, 'page_headline', true);
      $caption_style = 'font-size:'.get_post_meta($post->ID, 'page_headline_font_size', true).'px;';
      $caption_style .= 'color:'.get_post_meta($post->ID, 'page_headline_color', true).';';
      $shadow1 = get_post_meta($post->ID, 'page_headline_shadow1', true);
      $shadow2 = get_post_meta($post->ID, 'page_headline_shadow2', true);
      $shadow3 = get_post_meta($post->ID, 'page_headline_shadow3', true);
      $shadow4 = get_post_meta($post->ID, 'page_headline_shadow4', true);
      if (empty($shadow1)) $shadow1 = 0;
      if (empty($shadow2)) $shadow2 = 0;
      if (empty($shadow3)) $shadow3 = 0;
      if (empty($shadow4)) $shadow4 = '#333333';
      if ($shadow1 || $shadow2 || $shadow3) {
        $caption_style .= 'text-shadow:'.$shadow1.'px '.$shadow2.'px '.$shadow3.'px '.$shadow4.';';
      }
    }
?>

<?php get_template_part('breadcrumb'); ?>

<?php if (!empty($image[0])) { ?>
  <div id="header_image">
   <img src="<?php echo esc_attr($image[0]); ?>" alt="" />
   <?php if ($headline){ ?>
   <div class="caption rich_font" style="<?php echo esc_attr($caption_style); ?>">
    <?php echo str_replace(array("\r\n", "\r", "\n"), '<br />', esc_html($headline)); ?>
   </div>
   <?php } ?>
  </div>
<?php } ?>

<div id="main_col" class="clearfix">

<?php if ($display_side_content == 'show') { ?>
 <div id="left_col">
<?php } ?>

<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

 <div id="article">

<?php if ($display_title == 'show') { ?>
  <h2 id="post_title" class="rich_font"><?php the_title(); ?></h2>
<?php } ?>

  <div class="post_content clearfix">

<?php
        $bukken_id = htmlspecialchars($_GET['bukken_id']);
        $room_id = htmlspecialchars($_GET['room_id']);

        //
        $bukken_room_name = "物件\n:\n";

        
        if(!empty($room_id))
        $bukken_room_name .= get_the_title($bukken_id) . "&nbsp;" . get_post_meta($room_id, "room_floor", true) . "F\n";
        
        if(empty($room_id)){
          $bukken_room_name .= get_the_title($bukken_id) . "\n";
        }

        $room_post_ID = $room_id;

        $room_meta=get_post_meta($room_post_ID);
        $str=$room_meta["kakaku"][0];
        $num = preg_replace('/[^0-9]/', '',$str);
        $tani=preg_replace('/'.$num.'/','',$str);
        $sr = get_field('kakakushikikin', $room_post_ID) ."/".get_field('kakakureikin', $room_post_ID);
    
        // $bosyu_juuko = get_field('madori_img', $room_post_ID);
        // if(is_array($bosyu_juuko))
        // $madori_img = $bosyu_juuko['url'];
    
        $bosyu_juuko = get_field('room_madori_images', $room_post_ID);
    // print_r($bosyu_juuko);
        if ( isset($bosyu_juuko[0]['sizes']['large']) ) {
            $madori_img = $bosyu_juuko[0]['sizes']['large'];
        } else {
            $madori_img = stw_get_singlefudo_madori_image($room_post_ID);
        }
    
        if($madori_img !== ''){
            $room_image = $madori_img;
        }else{
            $room_image = "nowprinting.jpg";
        }
    
        $chinryo = get_mansitu_text($room_post_ID, $bukken_mansitu_fg, number_unit($num).$tani, '現在募集はございません');
    
        //部屋詳細へのリンク
        $room_detail_link = get_permalink($room_post_ID);
    
        //部屋への問合せリンク
        $room_inquiry_link = get_permalink($room_post_ID).'#toiawasesaki';
    
        //管理費
        $kanrihi = (get_field('kakakukyouekihi', $room_post_ID) ? number_format(get_field('kakakukyouekihi', $room_post_ID)) . "円" : "なし" );
    
    
        // echo $kanrihi . "aa";
        ?>  


<div class="mb-3">
        <div class="o-title __p __bold mb-1"><?php echo $bukken_room_name; ?></div>
        <?php if(!empty($room_id)){ ?>
        <div class="o-room __horizontal mb-0">
        <section class="o-roomVacancy  flex-nowrap-no-md ai-center ">
		    <div class="flex-nowrap ai-center">
		        <div class="o-roomVacancy_thumb __nomodal" id="js-modalImages-53659-1"><img src="<?php echo $room_image;?>" alt="" class="o-roomVacancy_img">
		        </div>
		        <div class="o-roomVacancy_info flex-nowrap-no-sm jc-start ai-center">
		            <p class="o-roomVacancy_price"><?php echo $chinryo; ?></p>
		            <div class="">
		            <div class="o-roomVacancy_cost flex-nowrap">
		                <div class="o-roomVacancy_costItem __shikikin"><span class="o-roomVacancy_costItem_title">敷金</span>
		                    <p class="o-roomVacancy_costItem_period">
                          <?php
                            if(get_field('bukken_status', $bukken_id) !== "2"){ //満室でなはい時表示
                                the_field('kakakushikikin', $room_post_ID);
                            }else{
                                echo '-';
                            }
                          ?>
                        </p>
		                </div>
		                <div class="o-roomVacancy_costItem __reikin"><span class="o-roomVacancy_costItem_title">礼金</span>
		                    <p class="o-roomVacancy_costItem_period">
                        <?php
                        if(get_field('bukken_status', $bukken_id) !== "2"){ //満室でなはい時表示
                            the_field('kakakureikin', $room_post_ID);
                        }else{
                            echo '-';
                        }
                        ?>
                        </p>
		                </div>
		                <div class="o-roomVacancy_costItem __kanri"><span class="o-roomVacancy_costItem_title">管理費</span>
		                    <p class="o-roomVacancy_costItem_period">
                        <?php
                          if(get_field('bukken_status', $bukken_id) !== "2" && !kanrihi == ''){ //満室でなはい時表示
                              echo $kanrihi;
                          }else{
                              echo '-';
                          }
                        ?>
                        </p>
		                </div>
		            </div>
		            <div class="o-roomVacancy_space flex-nowrap">
		                <div class="o-roomVacancy_spaceItem __floor"><span class="o-roomVacancy_spaceItem_title">お部屋の階</span>
		                    <p class="o-roomVacancy_spaceItem_period"><?php the_field('room_floor', $room_post_ID);?>階</p></div>
		                <div class="o-roomVacancy_spaceItem __size">
		                    <span class="o-roomVacancy_spaceItem_title">面積</span>
		                    <p class="o-roomVacancy_spaceItem_period">
                        <?php
                          echo get_post_meta($room_post_ID, 'senyumenseki', true);
                        ?>㎡</p>
		                </div>
		                <div class="o-roomVacancy_spaceItem __layout"><span class="o-roomVacancy_spaceItem_title">間取り</span>
		                    <p class="o-roomVacancy_spaceItem_period"><?php echo xls_custom_madorisu_print($room_post_ID);?></p></div>
		            </div>
	            </div>
	        </div>
	</section>
</div>
<?php } ?>
</div>



   <?php the_content(__('Read more', 'tcd-w')); ?>
   <?php custom_wp_link_pages(); ?>
  </div>

 </div><!-- END #article -->

<?php endwhile; endif; ?>

<?php
if ($display_side_content == 'show') {
?>
 </div><!-- END #left_col -->

<?php
  get_sidebar();
}
?>

</div><!-- END #main_col -->

<?php get_footer(); ?>