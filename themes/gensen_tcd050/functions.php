<?php


// 言語ファイル --------------------------------------------------------------------------------
load_textdomain('tcd-w', get_template_directory().'/languages/' . get_locale() . '.mo');


// hook wp_head --------------------------------------------------------------------------------
require get_template_directory() . '/functions/head.php';


// テーマオプション --------------------------------------------------------------------------------
require get_template_directory() . '/admin/theme-options.php';


// コンテンツビルダー --------------------------------------------------------------------------------
require get_template_directory() . '/admin/contents_builder.php';


// 更新通知 --------------------------------------------------------------------------------
require get_template_directory() . '/functions/update_notifier.php';


// Javascriptの読み込み -----------------------------------------------------------------------
function my_admin_scripts() {
    wp_enqueue_script('thickbox');
    wp_enqueue_script('media-upload');
    wp_enqueue_script('imgareaselect');
    wp_enqueue_script('jquery-ui-draggable');
    wp_enqueue_script('jquery-ui-sortable');
    wp_enqueue_script('ml-widget-js', get_template_directory_uri().'/widget/js/script.js', array(), '1.0.0', true);
    wp_enqueue_script('jquery.cookieTab', get_template_directory_uri().'/admin/js/jquery.cookieTab.js', array(), '1.0.0', true);
    wp_enqueue_script('my_script', get_template_directory_uri().'/admin/js/my_script.js', array(), '1.4.0', true);

    // 画像アップロード用
    wp_enqueue_media();
    wp_enqueue_script('cf-media-field', get_template_directory_uri().'/admin/js/cf-media-field.js', array(), '1.0.0', true);
    wp_localize_script( 'cf-media-field', 'cfmf_text', array(
        'image_title' => __('Please Select Image', 'tcd-w'),
        'image_button' => __('Use this Image', 'tcd-w'),
        'video_title' => __('Please Select Video', 'tcd-w'),
        'video_button' => __('Use this Video', 'tcd-w')
    ) );

    wp_enqueue_script( 'wp-color-picker'); // WPカラーピッカー
}
add_action('admin_print_scripts', 'my_admin_scripts');


// スタイルシートの読み込み -----------------------------------------------------------------------
function my_admin_styles() {
    wp_enqueue_style('thickbox');
    wp_enqueue_style('my_widget_css', get_template_directory_uri() . '/widget/css/style.css');
    wp_enqueue_style('my_admin_css', get_template_directory_uri() .'/admin/css/my_admin.css');
    wp_enqueue_style( 'wp-color-picker' ); // WPカラーピッカー
}
add_action('admin_print_styles', 'my_admin_styles');


// ビジュアルエディタ用スタイルシートの読み込み --------------------------------------------------------------------------------
function wpdocs_theme_add_editor_styles() {
    add_editor_style('editor-style-02.css');//管理画面用のスタイルシートを変更した場合は、ファイルの名前と番号を変える （キャッシュ対策）
}
add_action( 'admin_init', 'wpdocs_theme_add_editor_styles' );


// ページビルダー --------------------------------------------------------------------------------
require get_template_directory() . '/pagebuilder/pagebuilder.php';


// おすすめ記事 --------------------------------------------------------------------------------
require get_template_directory() . '/functions/recommend.php';


// meta title meta description --------------------------------------------------------------------------------
require get_template_directory() . '/functions/seo.php';


// 管理画面の記事一覧、クイック編集 --------------------------------------------------------------------------------
require get_template_directory() . '/functions/admin_column.php';
require get_template_directory() . '/functions/quick_edit.php';


// ページ用カスタムフィールド --------------------------------------------------------------------------------
require get_template_directory() . '/functions/page_cf.php';
require get_template_directory() . '/functions/page_cf2.php';


// 紹介用カスタムフィールド --------------------------------------------------------------------------------
require get_template_directory() . '/functions/introduce_cf.php';
require get_template_directory() . '/functions/introduce_cf2.php';


// カテゴリー用カスタムフィールド --------------------------------------------------------------------------------
require get_template_directory() . '/functions/category.php';


// 閲覧数カスタムフィールド・カウントアップ --------------------------------------------------------------------------------
require get_template_directory() . '/functions/view_count.php';


// カスタムCSS --------------------------------------------------------------------------------
require get_template_directory() . '/functions/custom_css.php';


// ビジュアルエディタにクイックタグを追加 --------------------------------------------------------------------------------
require get_template_directory() . '/functions/custom_editor.php';


// ショートコード --------------------------------------------------------------------------------
require get_template_directory() . '/functions/short_code.php';


// ウィジェット ------------------------------------------------------------------------
require get_template_directory() . '/widget/ad.php';
require get_template_directory() . '/widget/styled_post_list1.php';
require get_template_directory() . '/widget/google_search.php';
require get_template_directory() . '/widget/archive_list.php';
require get_template_directory() . '/widget/banner_list.php';
require get_template_directory() . '/widget/icon_link_list.php';
require get_template_directory() . '/widget/ranking_list.php';


// カスタムページリンク --------------------------------------------------------------------------------
require get_template_directory() . '/functions/custom_page_link.php';


// OGP tag -------------------------------------------------------------------------------------------
require get_template_directory() . '/functions/ogp.php';


// 次のページリンク --------------------------------------------------------------------------------
require get_template_directory() . '/functions/next_prev.php';


// ロゴ用関数 --------------------------------------------------------------------------------
require get_template_directory() . '/functions/logo.php';


// パスワード保護 --------------------------------------------------------------------------------
require ( get_template_directory() . '/functions/password_form.php' );


// カスタム検索 --------------------------------------------------------------------------------
require get_template_directory() . '/functions/custom_search.php';


// ビジュアルエディタに表(テーブル)の機能を追加 -----------------------------------------------
function mce_external_plugins_table($plugins) {
    $plugins['table'] = 'https://cdnjs.cloudflare.com/ajax/libs/tinymce/4.7.4/plugins/table/plugin.min.js';
    return $plugins;
}
add_filter( 'mce_external_plugins', 'mce_external_plugins_table' );

// tinymceのtableボタンにclass属性プルダウンメニューを追加
function mce_buttons_table($buttons) {
    $buttons[] = 'table';
    return $buttons;
}
add_filter( 'mce_buttons', 'mce_buttons_table' );

function bootstrap_classes_tinymce($settings) {
    $styles = array(
        array('title' => __('Default style', 'tcd-w'), 'value' => ''),
        array('title' => __('No border', 'tcd-w'), 'value' => 'table_no_border'),
        array('title' => __('Display only horizontal border', 'tcd-w'), 'value' => 'table_border_horizontal')
    );
    $settings['table_class_list'] = json_encode($styles);
    return $settings;
}
add_filter('tiny_mce_before_init', 'bootstrap_classes_tinymce');


// ユーザーエージェントを判定するための関数---------------------------------------------------------------------
function is_mobile() {
 //タブレットも含める場合はwp_is_mobile()

    $ua = array(
        'iPhone', // iPhone
        'iPod', // iPod touch
        'Android.*Mobile', // 1.5+ Android *** Only mobile
        'Windows.*Phone', // *** Windows Phone
        'dream', // Pre 1.5 Android
        'CUPCAKE', // 1.5+ Android
        'BlackBerry', // BlackBerry
        'BB10', // BlackBerry10
        'webOS', // Palm Pre Experimental
        'incognito', // Other iPhone browser
        'webmate' // Other iPhone browser
    );

    $pattern = '/' . implode( '|', $ua ) . '/i';
    $match = preg_match( $pattern, $_SERVER['HTTP_USER_AGENT'] );

    if ( $match ) {
        return TRUE;
    } else {
        return FALSE;
    }
}


// レスポンシブOFF機能を判定するための関数---------------------------------------------------------------------
function is_no_responsive() {
    global $dp_options;
    if ( ! $dp_options ) $dp_options = get_desing_plus_option();

    if ( $dp_options['responsive'] == 'no' ) {
        return TRUE;
    } else {
        return FALSE;
    }
}


// スクリプトのバージョン管理 ----------------------------------------------------------------------------------------------
function version_num() {
    if (function_exists('wp_get_theme')) {
        $theme_data = wp_get_theme();
    } else {
        $theme_data = get_theme_data(TEMPLATEPATH . '/style.css');
    }
    return $theme_data['Version'];
}


// ウィジェットの設定 ------------------------------------------------------------------------------
if ( function_exists('register_sidebar') ) {
    register_sidebar(array(
        'before_widget' => '<div class="widget side_widget clearfix %2$s" id="%1$s">'."\n",
        'after_widget' => "</div>\n",
        'before_title' => '<h3 class="o-title h2">',
        'after_title' => "</h3>\n",
        'name' => __('Basic side widget', 'tcd-w'),
        'description' => __('If there is no individual setting, this widget will be displayed.', 'tcd-w'),
        'id' => 'common_widget'
    ));
    register_sidebar(array(
        'before_widget' => '<div class="widget side_widget clearfix %2$s" id="%1$s">'."\n",
        'after_widget' => "</div>\n",
        'before_title' => '<h3 class="o-title h2">',
        'after_title' => "</h3>\n",
        'name' => __('Post side widget', 'tcd-w'),
        'id' => 'post_widget'
    ));
    register_sidebar(array(
        'before_widget' => '<div class="widget side_widget clearfix %2$s" id="%1$s">'."\n",
        'after_widget' => "</div>\n",
        'before_title' => '<h3 class="o-title h2">',
        'after_title' => "</h3>\n",
        'name' => __('Archive side widget', 'tcd-w'),
        'id' => 'archive_widget'
    ));
    register_sidebar(array(
        'before_widget' => '<div class="widget side_widget clearfix %2$s" id="%1$s">'."\n",
        'after_widget' => "</div>\n",
        'before_title' => '<h3 class="o-title h2">',
        'after_title' => "</h3>\n",
        'name' => __('Page side widget', 'tcd-w'),
        'id' => 'page_widget'
    ));
    register_sidebar(array(
        'before_widget' => '<div class="widget side_widget clearfix %2$s" id="%1$s">'."\n",
        'after_widget' => "</div>\n",
        'before_title' => '<h3 class="o-title h2">',
        'after_title' => "</h3>\n",
        'name' => __('News side widget', 'tcd-w'),
        'id' => 'news_widget'
    ));
    register_sidebar(array(
        'before_widget' => '<div class="widget side_widget clearfix %2$s" id="%1$s">'."\n",
        'after_widget' => "</div>\n",
        'before_title' => '<h3 class="o-title h2">',
        'after_title' => "</h3>\n",
        'name' => __('Introduce side widget', 'tcd-w'),
        'id' => 'introduce_widget'
    ));
    register_sidebar(array(
        'before_widget' => '<div class="widget side_widget clearfix %2$s" id="%1$s">'."\n",
        'after_widget' => "</div>\n",
        'before_title' => '<h3 class="o-title h2">',
        'after_title' => "</h3>\n",
        'name' => __('Search results side widget', 'tcd-w'),
        'id' => 'search_results_widget'
    ));
    register_sidebar(array(
        'before_widget' => '<div class="widget footer_widget %2$s" id="%1$s">'."\n",
        'after_widget' => "</div>\n",
        'before_title' => '<h3 class="footer_headline rich_font">',
        'after_title' => "</h3>\n",
        'name' => __('Footer widget', 'tcd-w'),
        'id' => 'footer_widget'
    ));
    register_sidebar(array(
        'before_widget' => '<div class="widget side_widget clearfix %2$s" id="%1$s">'."\n",
        'after_widget' => "</div>\n",
        'before_title' => '<h3 class="o-title h2">',
        'after_title' => "</h3>\n",
        'name' => __('Basic side widget (mobile)', 'tcd-w'),
        'description' => __('This widget will be replaced with normal widget when a user accesses the site by smartphone.', 'tcd-w'),
        'id' => 'common_widget_mobile'
    ));
    register_sidebar(array(
        'before_widget' => '<div class="widget side_widget clearfix %2$s" id="%1$s">'."\n",
        'after_widget' => "</div>\n",
        'before_title' => '<h3 class="o-title h2">',
        'after_title' => "</h3>\n",
        'name' => __('Post side widget (mobile)', 'tcd-w'),
        'description' => __('This widget will be replaced with normal widget when a user accesses the site by smartphone.', 'tcd-w'),
        'id' => 'post_widget_mobile'
    ));
    register_sidebar(array(
        'before_widget' => '<div class="widget side_widget clearfix %2$s" id="%1$s">'."\n",
        'after_widget' => "</div>\n",
        'before_title' => '<h3 class="o-title h2">',
        'after_title' => "</h3>\n",
        'name' => __('Archive side widget (mobile)', 'tcd-w'),
        'description' => __('This widget will be replaced with normal widget when a user accesses the site by smartphone.', 'tcd-w'),
        'id' => 'archive_widget_mobile'
    ));
    register_sidebar(array(
        'before_widget' => '<div class="widget side_widget clearfix %2$s" id="%1$s">'."\n",
        'after_widget' => "</div>\n",
        'before_title' => '<h3 class="o-title h2">',
        'after_title' => "</h3>\n",
        'name' => __('Page side widget (mobile)', 'tcd-w'),
        'description' => __('This widget will be replaced with normal widget when a user accesses the site by smartphone.', 'tcd-w'),
        'id' => 'page_widget_mobile'
    ));
    register_sidebar(array(
        'before_widget' => '<div class="widget side_widget clearfix %2$s" id="%1$s">'."\n",
        'after_widget' => "</div>\n",
        'before_title' => '<h3 class="o-title h2">',
        'after_title' => "</h3>\n",
        'name' => __('News side widget (mobile)', 'tcd-w'),
        'description' => __('This widget will be replaced with normal widget when a user accesses the site by smartphone.', 'tcd-w'),
        'id' => 'news_widget_mobile'
    ));
    register_sidebar(array(
        'before_widget' => '<div class="widget side_widget clearfix %2$s" id="%1$s">'."\n",
        'after_widget' => "</div>\n",
        'before_title' => '<h3 class="o-title h2">',
        'after_title' => "</h3>\n",
        'name' => __('Introduce side widget (mobile)', 'tcd-w'),
        'description' => __('This widget will be replaced with normal widget when a user accesses the site by smartphone.', 'tcd-w'),
        'id' => 'introduce_widget_mobile'
    ));
    register_sidebar(array(
        'before_widget' => '<div class="widget side_widget clearfix %2$s" id="%1$s">'."\n",
        'after_widget' => "</div>\n",
        'before_title' => '<h3 class="o-title h2">',
        'after_title' => "</h3>\n",
        'name' => __('Search results side widget (mobile)', 'tcd-w'),
        'description' => __('This widget will be replaced with normal widget when a user accesses the site by smartphone.', 'tcd-w'),
        'id' => 'search_results_widget_mobile'
    ));
    register_sidebar(array(
        'before_widget' => '<div class="widget footer_widget %2$s" id="%1$s">'."\n",
        'after_widget' => "</div>\n",
        'before_title' => '<h3 class="footer_headline rich_font">',
        'after_title' => "</h3>\n",
        'name' => __('Footer widget (mobile)', 'tcd-w'),
        'description' => __('This widget will be replaced with normal widget when a user accesses the site by smartphone.', 'tcd-w'),
        'id' => 'footer_widget_mobile'
    ));
}

// オリジナルの抜粋記事 --------------------------------------------------------------------------------
function new_excerpt($a, $echo = true) {
    if (has_excerpt()) {
        $base_content = get_the_excerpt();
        $base_content = str_replace(array("\r\n", "\r", "\n"), "", $base_content);
        $trim_content = mb_strimwidth($base_content, 0, $a, '…', 'utf-8');
    } else {
        $base_content = get_the_content();
        $base_content = preg_replace('!<style.*?>.*?</style.*?>!is', '', $base_content);
        $base_content = preg_replace('!<script.*?>.*?</script.*?>!is', '', $base_content);
        $base_content = preg_replace('/\[.+\]/','', $base_content);
        $base_content = str_replace(array("\r\n", "\r", "\n" , "&nbsp;"), "", $base_content);
        $base_content = strip_tags($base_content);
        $trim_content = mb_strimwidth($base_content, 0, $a, '…', 'utf-8');
        $trim_content = str_replace(array("\r\n", "\r", "\n" , "&nbsp;"), "", $trim_content);
        $trim_content = str_replace(']]>', ']]&gt;', $trim_content);
        $trim_content = htmlspecialchars($trim_content);
    }

    if ($echo) {
        echo $trim_content;
    } else {
        return $trim_content;
    }
}

//抜粋からPタグを取り除く
remove_filter( 'the_excerpt', 'wpautop' );


// 記事タイトルの文字数制限 --------------------------------------------------------------------------------
function trim_title($num) {
    $base_title = get_the_title();
    $trim_title = mb_substr($base_title, 0, $num ,"utf-8");
    $count_title = mb_strlen($trim_title,"utf-8");
    if($count_title > $num-1) {
        echo $trim_title . '…';
    } else {
        echo $trim_title;
    }
}


// タイトルをエンコード --------------------------------------------------------------------------------
function get_encoded_title($title){
    return urlencode(mb_convert_encoding($title, "UTF-8"));
}


// セルフピンバックを禁止する -------------------------------------------------------------------------------------
function no_self_ping( &$links ) {
    $home = home_url();
    foreach ( $links as $l => $link )
        if ( 0 === strpos( $link, $home ) )
            unset($links[$l]);
}
add_action( 'pre_ping', 'no_self_ping' );


// RSS用のフィードを追加 ---------------------------------------------------------------------------------------------------
add_theme_support( 'automatic-feed-links' );


//　ヘッダーから余分なMETA情報を削除 --------------------------------------------------------------------
remove_action( 'wp_head', 'wp_generator' );
remove_action( 'wp_head', 'rsd_link' );
remove_action( 'wp_head', 'wlwmanifest_link' );
remove_action( 'wp_head', 'index_rel_link' );
remove_action( 'wp_head', 'parent_post_rel_link', 10, 0 );
remove_action( 'wp_head', 'start_post_rel_link', 10, 0 );
remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0 );


// 固定ページからアイキャッチ用meta boxを削除 -----------------------------------------------------------
function remove_image_metabox_from_page() {
    remove_meta_box( 'postimagediv', 'page', 'side' );
}
add_action( 'do_meta_boxes' , 'remove_image_metabox_from_page' );


// インラインスタイルを取り除く --------------------------------------------------------------------------------
function remove_recent_comments_style() {
    global $wp_widget_factory;
    remove_action( 'wp_head', array( $wp_widget_factory->widgets['WP_Widget_Recent_Comments'], 'recent_comments_style' ) );
}
add_action( 'widgets_init', 'remove_recent_comments_style' );

function remove_adminbar_inline_style() {
    remove_action('wp_head', '_admin_bar_bump_cb');
}
add_action('get_header', 'remove_adminbar_inline_style');


//　サムネイルの設定 --------------------------------------------------------------------------------
if (function_exists('add_theme_support')) {
    add_theme_support('post-thumbnails');
    set_post_thumbnail_size( 860, 0, false );
    add_image_size( 'size1', 150, 150, true ); // styled_post_list1ウィジェットで利用 画質対策で100x100の1.5倍
    add_image_size( 'size2', 336, 216, true ); // 記事一覧・関連記事で利用 画質対策で280x180の1.2倍
    add_image_size( 'size3', 336, 336, true ); // トップページで利用 画質対策で280x280の1.2倍
    add_image_size( 'size4', 516, 192, true ); // 紹介詳細の紹介一覧バナーで利用 画質対策で430x160の1.2倍
    // imgタグのsrcsetを未使用に
    add_filter( 'wp_calculate_image_srcset', '__return_false' );
}


// カスタムメニューの設定 --------------------------------------------------------------------------------
if (function_exists('register_nav_menu')) {
    register_nav_menu( 'global-menu', __( 'Global menu', 'tcd-w' ) );
    register_nav_menu( 'footer-bottom-menu', __( 'Footer bottom menu', 'tcd-w' ) );
}


// ページナビ用 --------------------------------------------------------------------------------
function show_posts_nav() {
    global $wp_query;
    return ($wp_query->max_num_pages > 1);
}


// 絵文字を消す ------------------------------------------------------------------
function disable_emoji() {
    global $dp_options;
    if ( ! $dp_options ) $dp_options = get_desing_plus_option();
    if ( $dp_options['use_emoji'] == 0 ) {
        remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
        remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
        remove_action( 'wp_print_styles', 'print_emoji_styles' );
        remove_action( 'admin_print_styles', 'print_emoji_styles' );
        remove_filter( 'the_content_feed', 'wp_staticize_emoji' );
        remove_filter( 'comment_text_rss', 'wp_staticize_emoji' );
        remove_filter( 'wp_mail', 'wp_staticize_emoji_for_email' );
    }
}
add_action( 'init', 'disable_emoji' );


// bodyタグにclassを追加 --------------------------------------------------------------------------------
function ml_body_classes($classes) {
    global $dp_options, $header_slider, $custom_search_vars;
    if ( ! $dp_options ) $dp_options = get_desing_plus_option();

    if ($dp_options['header_fix'] == 'type2') {
        $classes[] = 'fix_top';
    }

    if ($dp_options['mobile_header_fix'] == 'type2') {
        $classes[] = 'mobile_fix_top';
    }

    if (is_singular($dp_options['news_slug']) && $dp_options['news_slug'] != 'news') {
        $classes[] = 'single-news';
    }

    if (is_singular($dp_options['introduce_slug']) && $dp_options['introduce_slug'] != 'introduce') {
        $classes[] = 'single-introduce';
    }

    if (is_mobile()) {
        $classes[] = 'mobile_device';
        if ($dp_options['footer_bar_display'] == 'type1' || $dp_options['footer_bar_display'] == 'type2') {
            $classes[] = 'mobile_footer_bar';
        }
    }

    if ($header_slider) {
        $classes[] = 'has_header_content';
    }

    if (is_front_page()) {
        // 設定によりトップページで検索結果が表示される対策
        if ($custom_search_vars) {
            $classes[] = 'home-search_results';
        } else {
            $classes[] = 'home-default';
        }
    } elseif ($custom_search_vars) {
        $classes[] = 'search-results';
    }

    return array_unique($classes);
}
add_filter('body_class','ml_body_classes');


// RGBをHEXに変換 ------------------------------------------------------------------
function hex2rgb($hex) {
    $hex = str_replace("#", "", $hex);

    if(strlen($hex) == 3) {
        $r = hexdec(substr($hex,0,1).substr($hex,0,1));
        $g = hexdec(substr($hex,1,1).substr($hex,1,1));
        $b = hexdec(substr($hex,2,1).substr($hex,2,1));
    } else {
        $r = hexdec(substr($hex,0,2));
        $g = hexdec(substr($hex,2,2));
        $b = hexdec(substr($hex,4,2));
    }

    return array($r, $g, $b);
}

// カテゴリーラベル変更
function change_post_menu_label() {
    $dp_options = get_desing_plus_option();
    global $submenu;
    $submenu['edit.php'][15][0] = $dp_options['category_label'];
}
function change_taxonomies_object_label() {
    $dp_options = get_desing_plus_option();
    global $wp_taxonomies;
    $labels = $wp_taxonomies['category']->labels;
    $labels->name = $dp_options['category_label'];
}
add_action( 'init', 'change_taxonomies_object_label' );
add_action( 'admin_menu', 'change_post_menu_label' );

// カスタム投稿タイプ・カスタムタクソノミー ------------------------------------------------------------------
if (function_exists('register_post_type')) {
    function gensen_register_post_type() {
        $dp_options = get_desing_plus_option();

        // カテゴリー2～3
        for ( $i = 2; $i <= 3; $i++ ) {
            if ($dp_options['use_category'.$i]) {
                register_taxonomy(
                    $dp_options['category'.$i.'_slug'],
                    'post',
                    array(
                        'label' => $dp_options['category'.$i.'_label'],
                        'labels' => array(
                            'name' => $dp_options['category'.$i.'_label'],
                            'singular_name' => $dp_options['category'.$i.'_label'],
                            'search_items' => __('Search Category', 'tcd-w'),
                            'popular_items' => __('Popular Category', 'tcd-w'),
                            'all_items' => __('All Category', 'tcd-w'),
                            'parent_item' => __('Parent Category', 'tcd-w'),
                            'edit_item' => __('Edit Category', 'tcd-w'),
                            'update_item' => __('Update Category', 'tcd-w'),
                            'add_new_item' => __('Add New Category', 'tcd-w'),
                            'new_item_name' => __('Name Of New Category', 'tcd-w'),
                        ),
                        'public' => true,
                        'show_in_rest' => true,
                        'show_ui' => true,
                        'show_admin_column' => true,
                        'hierarchical' => true,
                    )
                );
            }
        }

        // 管理画面のタグをカテゴリー2～3の後ろに表示させる
        if ( is_admin() ) {
            global $wp_taxonomies;
            if ( isset( $wp_taxonomies['post_tag'] ) ) {
                $post_tag = $wp_taxonomies['post_tag'];
                unset( $wp_taxonomies['post_tag'] );
                $wp_taxonomies['post_tag'] = $post_tag;
            }
        }

        // カスタム投稿 紹介
        register_post_type($dp_options['introduce_slug'], array(
            'label' => $dp_options['introduce_label'],
            'labels' => array(
                'name' => $dp_options['introduce_label'],
                'singular_name' => $dp_options['introduce_label'],
                'add_new' => __('Add New', 'tcd-w'),
                'add_new_item' => __('Add New Item', 'tcd-w'),
                'edit_item' => __('Edit', 'tcd-w'),
                'new_item' => __('New item', 'tcd-w'),
                'view_item' => __('View Item', 'tcd-w'),
                'search_items' => __('Search Items', 'tcd-w'),
                'not_found' => __('Not Found', 'tcd-w'),
                'not_found_in_trash' => __('Not found in trash', 'tcd-w'),
                'parent_item_colon' => ''
            ),
            'public' => true,
            'publicly_queryable' => true,
            'menu_position' => 5,
            'show_in_rest' => true,
            'show_ui' => true,
            'query_var' => true,
            'rewrite' => true,
            'capability_type' => 'post',
            'has_archive' => true,
            'hierarchical' => false,
            'supports' => array('title', 'editor', 'thumbnail')
        ));

        // 紹介カテゴリー1～3
        for ( $i = 1; $i <= 3; $i++ ) {
            if ($dp_options['use_introduce_category'.$i]) {
                register_taxonomy(
                    $dp_options['introduce_category'.$i.'_slug'],
                    $dp_options['introduce_slug'],
                    array(
                        'label' => $dp_options['introduce_category'.$i.'_label'],
                        'labels' => array(
                            'name' => $dp_options['introduce_category'.$i.'_label'],
                            'singular_name' => $dp_options['introduce_category'.$i.'_label'],
                            'search_items' => __('Search Category', 'tcd-w'),
                            'popular_items' => __('Popular Category', 'tcd-w'),
                            'all_items' => __('All Category', 'tcd-w'),
                            'parent_item' => __('Parent Category', 'tcd-w'),
                            'edit_item' => __('Edit Category', 'tcd-w'),
                            'update_item' => __('Update Category', 'tcd-w'),
                            'add_new_item' => __('Add New Category', 'tcd-w'),
                            'new_item_name' => __('Name Of New Category', 'tcd-w'),
                        ),
                        'public' => true,
                        'show_in_rest' => true,
                        'show_ui' => true,
                        'show_admin_column' => true,
                        'hierarchical' => true,
                    )
                );
            }
        }

        // 紹介タグ
        if ($dp_options['use_introduce_tag']) {
            register_taxonomy( $dp_options['introduce_tag_slug'], $dp_options['introduce_slug'], array(
                 'hierarchical' => false,
                'public' => true,
                'show_in_rest' => true,
                'show_ui' => true,
                'show_admin_column' => true,
                'capabilities' => array(
                    'manage_terms' => 'manage_post_tags',
                    'edit_terms'   => 'edit_post_tags',
                    'delete_terms' => 'delete_post_tags',
                    'assign_terms' => 'assign_post_tags',
                )
            ) );
        }

        // カスタム投稿 お知らせ
        register_post_type($dp_options['news_slug'], array(
            'label' => $dp_options['news_label'],
            'labels' => array(
                'name' => $dp_options['news_label'],
                'singular_name' => $dp_options['news_label'],
                'add_new' => __('Add New', 'tcd-w'),
                'add_new_item' => __('Add New Item', 'tcd-w'),
                'edit_item' => __('Edit', 'tcd-w'),
                'new_item' => __('New item', 'tcd-w'),
                'view_item' => __('View Item', 'tcd-w'),
                'search_items' => __('Search Items', 'tcd-w'),
                'not_found' => __('Not Found', 'tcd-w'),
                'not_found_in_trash' => __('Not found in trash', 'tcd-w'),
                'parent_item_colon' => ''
            ),
            'public' => true,
            'publicly_queryable' => true,
            'menu_position' => 5,
            'show_in_rest' => true,
            'show_ui' => true,
            'query_var' => true,
            'rewrite' => true,
            'capability_type' => 'post',
            'has_archive' => true,
            'hierarchical' => false,
            'supports' => array('title', 'editor', 'thumbnail')
        ));

        // 紹介 表示件数
        function pre_get_posts_introduce($wp_query) {
            $dp_options = get_desing_plus_option();
            if ( ! is_admin() && $wp_query->is_main_query() ) {
                if (
                    $wp_query->is_post_type_archive( $dp_options['introduce_slug'] ) ||
                    ( $wp_query->is_tax( $dp_options['introduce_category1_slug'] ) && $dp_options['use_introduce_category1'] && $dp_options['use_introduce_category1_introduce_archive'] ) ||
                    ( $wp_query->is_tax( $dp_options['introduce_category2_slug'] ) && $dp_options['use_introduce_category2'] && $dp_options['use_introduce_category2_introduce_archive'] ) ||
                    ( $wp_query->is_tax( $dp_options['introduce_category3_slug'] ) && $dp_options['use_introduce_category3'] && $dp_options['use_introduce_category3_introduce_archive'] )
                ) {
                    // echo 'hear'.$dp_options['archive_introduce_num'];
                    $wp_query->set( 'posts_per_page', $dp_options['archive_introduce_num'] );
                    // $wp_query->set( 'posts_per_page', 12 );
                }
            }
        }
        add_filter( 'pre_get_posts', 'pre_get_posts_introduce' );

        // ブログ 表示件数
        function pre_get_posts_staffblog($wp_query) {
            $dp_options = get_desing_plus_option();
            if ( ! is_admin() && $wp_query->is_main_query() && !is_post_type_archive('introduce') ) {
                $wp_query->set( 'posts_per_page', 300 );
            }
        }
        add_filter( 'pre_get_posts', 'pre_get_posts_staffblog' );

        
        // テンプレートファイル差し替え
        function custom_post_type_template_include( $template ) {
            // カスタム検索用グローバル変数
            global $custom_search_vars;

            global $dp_options;
            if ( ! $dp_options ) $dp_options = get_desing_plus_option();

            $template_name = null;

            if ( is_singular( $dp_options['news_slug'] ) ) {
                $template_name = 'single-news.php';
            } elseif ( is_singular( $dp_options['introduce_slug'] ) ) {
                $template_name = 'single-introduce.php';
            } elseif ( $custom_search_vars ) {
                // カスタム検索の場合はcustom_search.phpで処理されるため何もしない
            } elseif ( is_post_type_archive( $dp_options['news_slug'] ) ) {
                $template_name = 'archive-news.php';
            } elseif (
                is_post_type_archive( $dp_options['introduce_slug'] ) ||
                ( is_tax( $dp_options['introduce_category1_slug'] ) && $dp_options['use_introduce_category1'] && $dp_options['use_introduce_category1_introduce_archive'] ) ||
                ( is_tax( $dp_options['introduce_category2_slug'] ) && $dp_options['use_introduce_category2'] && $dp_options['use_introduce_category2_introduce_archive'] ) ||
                ( is_tax( $dp_options['introduce_category3_slug'] ) && $dp_options['use_introduce_category3'] && $dp_options['use_introduce_category3_introduce_archive'] )
            ) {
                $template_name = 'archive-introduce.php';
            }elseif( is_category(15)){//ここに必要カテゴリを設置
                // $wp_query->set( 'posts_per_page', 4);
                $template_name = 'archive-staffblog.php';
            } elseif ( is_category() || is_tax( array( $dp_options['category2_slug'], $dp_options['category3_slug'], $dp_options['introduce_category1_slug'], $dp_options['introduce_category2_slug'], $dp_options['introduce_category3_slug'] ) ) ) {
                $template_name = 'custom_search_results.php';
            }

            if ( $template_name ) {
                if ( file_exists( STYLESHEETPATH . '/' . $template_name ) ) {
                    return STYLESHEETPATH . '/' . $template_name;
                } elseif ( file_exists( TEMPLATEPATH . '/' . $template_name ) ) {
                    return TEMPLATEPATH . '/' . $template_name;
                }
            }

            return $template;
        }
        add_filter( 'template_include', 'custom_post_type_template_include' );
    }
    add_action( 'init', 'gensen_register_post_type' );
}



// カードリンクパーツ --------------------------------------------------------------------------------------
add_image_size( 'size-card', 150, 150, true );

function get_the_custom_excerpt($content, $length) {
    $length = ($length ? $length : 70);//デフォルトの長さを指定する
    $content = preg_replace('/<!--more-->.+/is',"",$content); //moreタグ以降削除
    $content = strip_shortcodes($content);//ショートコード削除
    $content = strip_tags($content);//タグの除去
    $content = str_replace("&nbsp;","",$content);//特殊文字の削除（今回はスペースのみ）
    $content = mb_substr($content,0,$length);//文字列を指定した長さで切り取る
    return $content.'...';
}

//カードリンクショートコード
function clink_scode($atts) {
    extract(shortcode_atts(array(
        'url'=>"",
        'title'=>"",
        'excerpt'=>""
    ),$atts));

    $id = url_to_postid($url);//URLから投稿IDを取得
    $post = get_post($id);//IDから投稿情報の取得
    $date = mysql2date('Y.m.d', $post->post_date);//投稿日の取得

    $img_width ="120";//画像サイズの幅指定
    $img_height = "120";//画像サイズの高さ指定
    $no_image = get_template_directory_uri().'/img/common/no_image1.gif';

    //抜粋を取得
    if(empty($excerpt)){
        if($post->post_excerpt){
            $excerpt = get_the_custom_excerpt($post->post_excerpt , 145);
        }else{
            $excerpt = get_the_custom_excerpt($post->post_content , 145);
        }
    }

    //タイトルを取得
    if(empty($title)){
        $title = esc_html(get_the_title($id));
    }
    //アイキャッチ画像を取得
    if(has_post_thumbnail($id)) {
        $img = wp_get_attachment_image_src(get_post_thumbnail_id($id),'size-card');
        $img_tag = "<img src='" . $img[0] . "' alt='{$title}' width=" . $img_width . " height=" . $img_height . " />";
    } else {
        $img_tag ='<img src="'.$no_image.'" alt="" width="'.$img_width.'" height="'.$img_height.'" />';
    }

    $clink ='<div class="cardlink"><a href="'. $url .'"><div class="cardlink_thumbnail">'. $img_tag .'</a></div><div class="cardlink_content"><span class="timestamp">'.$date.'</span><div class="cardlink_title"><a href="'. $url .'">'. $title .' </a></div><div class="cardlink_excerpt">' . $excerpt . '</div></div><div class="cardlink_footer"></div></div>';

    return $clink;
}
add_shortcode("clink", "clink_scode");


// カスタムコメント --------------------------------------------------------------------------------------

if (function_exists('wp_list_comments')) {
    // comment count
    add_filter('get_comments_number', 'comment_count', 0);
    function comment_count( $commentcount ) {
        global $id;
        $_commnets = get_comments('post_id=' . $id);
        $comments_by_type = separate_comments($_commnets);
        return count($comments_by_type['comment']);
    }
}


function custom_comments($comment, $args, $depth) {
    $GLOBALS['comment'] = $comment;
    global $commentcount;
    if(!$commentcount) {
        $commentcount = 0;
    }
?>

 <li class="comment <?php if($comment->comment_author_email == get_the_author_meta('email')) {echo 'admin-comment';} else {echo 'guest-comment';} ?>" id="comment-<?php comment_ID() ?>">
  <div class="comment-meta clearfix">
   <div class="comment-meta-left">
  <?php if (function_exists('get_avatar') && get_option('show_avatars')) { echo get_avatar($comment, 35); } ?>

    <ul class="comment-name-date">
     <li class="comment-name">
<?php if (get_comment_author_url()) : ?>
<a id="commentauthor-<?php comment_ID() ?>" class="url <?php if($comment->comment_author_email == get_the_author_meta('email')) {echo 'admin-url';} else {echo 'guest-url';} ?>" href="<?php comment_author_url() ?>" rel="nofollow">
<?php else : ?>
<span id="commentauthor-<?php comment_ID() ?>">
<?php endif; ?>

<?php comment_author(); ?>

<?php if(get_comment_author_url()) : ?>
</a>
<?php else : ?>
</span>
<?php endif; ?>
     </li>
     <li class="comment-date"><time datetime="<?php comment_time( 'c' ); ?>"><?php echo get_comment_time(__('F jS, Y', 'tcd-w')); ?></time></li>
    </ul>
   </div>

   <ul class="comment-act">
<?php if (function_exists('comment_reply_link')) {
        if ( get_option('thread_comments') == '1' ) { ?>
    <li class="comment-reply"><?php comment_reply_link(array_merge( $args, array('add_below' => 'comment-content', 'depth' => $depth, 'max_depth' => $args['max_depth'], 'reply_text' => '<span><span>'.__('REPLY','tcd-w').'</span></span>'))) ?></li>
<?php   } else { ?>
    <li class="comment-reply"><a href="javascript:void(0);" onclick="MGJS_CMT.reply('commentauthor-<?php comment_ID() ?>', 'comment-<?php comment_ID() ?>', 'comment');"><?php _e('REPLY', 'tcd-w'); ?></a></li>
<?php   }
      } else { ?>
    <li class="comment-reply"><a href="javascript:void(0);" onclick="MGJS_CMT.reply('commentauthor-<?php comment_ID() ?>', 'comment-<?php comment_ID() ?>', 'comment');"><?php _e('REPLY', 'tcd-w'); ?></a></li>
<?php } ?>
    <li class="comment-quote"><a href="javascript:void(0);" onclick="MGJS_CMT.quote('commentauthor-<?php comment_ID() ?>', 'comment-<?php comment_ID() ?>', 'comment-content-<?php comment_ID() ?>', 'comment');"><?php _e('QUOTE', 'tcd-w'); ?></a></li>
    <?php edit_comment_link(__('EDIT', 'tcd-w'), '<li class="comment-edit">', '</li>'); ?>
   </ul>

  </div>
  <div class="comment-content post_content" id="comment-content-<?php comment_ID() ?>">
  <?php if ($comment->comment_approved == '0') : ?>
   <span class="comment-note"><?php _e('Your comment is awaiting moderation.', 'tcd-w'); ?></span>
  <?php endif; ?>
  <?php comment_text(); ?>
  </div>

<?php
}

// ソート処理 pre_get_posts
function sort_pre_get_posts( $wp_query ) {
    global $dp_options, $custom_search_vars;
// echo '検索function';
    // 管理画面は終了
    if ( is_admin() ) return;

    // メインクエリー・アーカイブ以外は終了
    if ( ! $wp_query->is_main_query() && ! $wp_query->is_archive() ) return;

    // 検索フラグ
    $is_search = false;

    // ソート
    if ( isset( $_REQUEST['sort'] ) ) {
        // 先頭固定表示を無視
        $wp_query->set( 'ignore_sticky_posts', true );
// echo 'hear';
        // 閲覧数降順
        if ( $_REQUEST['sort'] === 'views' ) {
            $wp_query->set( 'meta_key', '_view_count' );
            $wp_query->set( 'orderby', 'meta_value_num' );
            $wp_query->set( 'order', 'DESC' );

        // 日時昇順
        } elseif ( $_REQUEST['sort'] === 'date_asc' ) {
            $wp_query->set( 'orderby', 'date' );
            $wp_query->set( 'order', 'ASC' );

        // 日時降順
        } else {
            $wp_query->set( 'orderby', 'date' );
            $wp_query->set( 'order', 'DESC' );
        }

    // 検索時
    } elseif ( $wp_query->is_search() || $custom_search_vars || is_category() || is_tax( array( $dp_options['category2_slug'], $dp_options['category3_slug'], $dp_options['introduce_category1_slug'], $dp_options['introduce_category2_slug'], $dp_options['introduce_category3_slug'] ) ) ) {
        // echo 'custom_searchです。';
        // 先頭固定表示を無視
        $wp_query->set( 'custom_orderby', true );
        $wp_query->set( 'ignore_sticky_posts', true );
        // $wp_query->set( 'orderby', 'post_title' );
        $wp_query->set( 'order', 'ASC' );      
    }

}
add_action( 'pre_get_posts', 'sort_pre_get_posts' );


function my_posts_orderby( $orderby, $query ) {
	global $wpdb;
	if ( $query->get( 'custom_orderby' ) ) {
        // echo 'abc';
		$orderby = "trim({$wpdb->posts}.post_title) = 'パークタワー高輪' DESC, {$wpdb->posts}.post_date DESC";
		// $orderby = "trim({$wpdb->posts}.post_title) = 'パークタワー高輪' DESC, {$wpdb->posts}.post_date DESC";
	}
	return $orderby;
}
// add_filter( 'posts_orderby', 'my_posts_orderby', 10, 2 );


/**
*サイトオプション
*/
if(function_exists('acf_add_options_page')){
    acf_add_options_page( array(
      'page_title' => '物件オプション',
      'menu_title' => '物件オプション',
      'menu_slug'  => 'bukken-option-settings',
      'capability' => 'manage_options',
    ) );
  }


//   add_action( 'pre_get_posts', 'sort_pre_get_posts' );

  // カスタム投稿タイプの紐付け　p2pプラグイン実行
  function custom_post_relation() {
      p2p_register_connection_type( array(
          'name' => 'room_relation', // 紐付けグループの名称
          'from' => 'room', // 出力されるアイテム
          'to' => 'fudo', // 出力する投稿タイプ
      ) );
  }
  add_action( 'p2p_init', 'custom_post_relation' );



/**
 * スクレイピングした間取り画像URLを取得する関数
 * 引数で渡されたroom_post_idよりuniq_idを取得。
 * housing_imagesから、該当uniq_idでis_madoriが1のレコードを1件取得し返す。
 * uniq_idが存在しないか、housing_imagesに該当レコードが無い場合は、空文字を返す
 * @param int $room_post_id
 * @return string
 */
function stw_get_singlefudo_madori_image( $room_post_id ){
    global $wpdb;
    $rtn_path = '';
    // postmetaよりuniq_idを取得
    $uniq_res = $wpdb->get_row($wpdb->prepare("select meta_value from ".$wpdb->prefix . "postmeta where post_id = %s and meta_key = 'uniq_id'", $room_post_id ));
    if ( $uniq_res->meta_value ){
        $uniq_id = $uniq_res->meta_value;
        // スクレイピングした画像情報を取得
        $images_res = $wpdb->get_results($wpdb->prepare("select * from " . $wpdb->prefix ."housing_images where uniq_id = %s and is_madori = 1", $uniq_id));
        if ( 0 < $wpdb->num_rows ){
            $rtn_path = stw_generate_imgurl($images_res[0]->image_path);
        }
    }

    return $rtn_path;
}


function stw_generate_imgurl( $path ){
    return home_url() . '/fudoimages/' . $path;
}

/**
 * 非表示画像オプション値取得
 */
function get_ban_image_file_name(){
    $ban_image_file_name = array();
    $ban_image_file_name = explode("\n", get_field('ban_image_file_name', 'option'));
    $ban_image_file_name = array_map('trim', $ban_image_file_name);
    $ban_image_file_name = array_filter($ban_image_file_name, 'strlen');
    $ban_image_file_name = array_values($ban_image_file_name);

    return $ban_image_file_name;

}


/**
 * 部屋IDから取得元のURLを表示
 * 管理者ログイン時のみ。
 */
function get_source_url($room_id){
    global $wpdb;


    $ret = $wpdb->get_row($wpdb->prepare("select A.ID, A.post_type, B.url as `url`, source from kctwp_posts A, kctwp_housing_transformed B where A.post_name = B.uniq_id and ID = " . $room_id . ";"));

    return '<p style="margin:20px 0;color:red;">取得元：<a href="' . $ret->url . '" target="_blank">' . $ret->source . "</a></p>";

}


function my_custom_koutsu_all($post_id) {
    global $wpdb;
    $moyori = '';
    $rosen_name = '';

    $koutsurosen_data = get_post_meta($post_id, 'koutsurosen1', true);
    $koutsueki_data = get_post_meta($post_id, 'koutsueki1', true);

    $shozaichiken_data = get_post_meta($post_id,'shozaichicode',true);
    $shozaichiken_data = myLeft($shozaichiken_data,2);

    if($koutsurosen_data !=""){
        $sql = "SELECT rosen_name FROM " . $wpdb->prefix . DB_ROSEN_TABLE . " WHERE rosen_id =".$koutsurosen_data."";
        //    $sql = $wpdb->prepare($sql,'');
        $metas = $wpdb->get_row( $sql );
        if(!empty($metas)){
            $rosen_name = $metas->rosen_name;
            $moyori .= ''.$rosen_name." ";
        }
    }

    if($koutsurosen_data !="" && $koutsueki_data !=""){
        $sql = "SELECT DTS.station_name";
        $sql .=  " FROM " . $wpdb->prefix . DB_ROSEN_TABLE . " AS DTR";
        $sql .=  " INNER JOIN " . $wpdb->prefix . DB_EKI_TABLE . " AS DTS ON DTR.rosen_id = DTS.rosen_id";
        $sql .=  " WHERE DTS.station_id=".$koutsueki_data." AND DTS.rosen_id=".$koutsurosen_data."";
        //    $sql = $wpdb->prepare($sql,'');
        $metas = $wpdb->get_row( $sql );
        if(!empty($metas)){
            if($metas->station_name != '＊＊＊＊'){
                if( $metas->station_name != '' ){
                    $moyori .= $metas->station_name.'駅 ';
                }
            }
        }
    }

    // if(get_post_meta($post_id, 'koutsutoho1', true) !="")
    //     $moyori .= ' 徒歩'.get_post_meta($post_id, 'koutsutoho1', true).'m';

        if(get_post_meta($post_id, 'koutsutoho1f', true) !="")
            $moyori .= ' 徒歩'.get_post_meta($post_id, 'koutsutoho1f', true).'分<br>';


            //バス路線
            /*
             * バス路線名、バス停名表示
             *
             * @since Fudousan Bus Plugin 1.0.0
             * For inc-single-fudo.php and inc-archive-fudo.php
             * admin_fudou.php fudo-widget.php fudo-widget3.php
             * apply_filters( 'fudoubus_buscorse_busstop_single', '', $post_id, 1 );
             *
             * @param int $post_id Post ID.
             * @param int $type 1 or 2.
             */
            $koutsubusstei = apply_filters( 'fudoubus_buscorse_busstop_single', '', $post_id, 1 );

            if( !$koutsubusstei ){
                $koutsubusstei = get_post_meta($post_id, 'koutsubusstei1', true);
            }
            $koutsubussfun = get_post_meta($post_id, 'koutsubussfun1', true);
            $koutsutohob1f = get_post_meta($post_id, 'koutsutohob1f', true);

            if( $koutsubusstei || $koutsubussfun ){

                if($rosen_name == 'バス'){
                    $moyori .= '(' . $koutsubusstei;
                    if( !empty( $koutsubussfun ) ) $moyori .= ' 乗'.$koutsubussfun.'分';
                }else{
                    $moyori .= '<br />';
                    $moyori .= ' バス(' . $koutsubusstei;
                    if( !empty( $koutsubussfun ) ) $moyori .= ' 乗'.$koutsubussfun.'分';
                }

                if($koutsutohob1f !="" )
                    $moyori .= ' 停歩'.$koutsutohob1f.'分';
                    $moyori .= ')';
            }


    $rosen_name = '';
    $koutsurosen_data = get_post_meta($post_id, 'koutsurosen2', true);
    $koutsueki_data = get_post_meta($post_id, 'koutsueki2', true);

    $shozaichiken_data = get_post_meta($post_id,'shozaichicode',true);
    $shozaichiken_data = myLeft($shozaichiken_data,2);


    if($koutsurosen_data !=""){
        $sql = "SELECT rosen_name FROM " . $wpdb->prefix . DB_ROSEN_TABLE . " WHERE rosen_id =".$koutsurosen_data."";
        //    $sql = $wpdb->prepare($sql,'');
        $metas = $wpdb->get_row( $sql );
        if(!empty($metas)){
            $rosen_name = $metas->rosen_name;
            $moyori .= $rosen_name." ";
        }
    }

    if($koutsurosen_data !="" && $koutsueki_data !=""){
        $sql = "SELECT DTS.station_name";
        $sql .=  " FROM " . $wpdb->prefix . DB_ROSEN_TABLE . " AS DTR";
        $sql .=  " INNER JOIN " . $wpdb->prefix . DB_EKI_TABLE . " AS DTS ON DTR.rosen_id = DTS.rosen_id";
        $sql .=  " WHERE DTS.station_id=".$koutsueki_data." AND DTS.rosen_id=".$koutsurosen_data."";
        //    $sql = $wpdb->prepare($sql,'');
        $metas = $wpdb->get_row( $sql );
        if(!empty($metas)){
            if($metas->station_name != '＊＊＊＊'){
                if( $metas->station_name != '' ){
                    $moyori .= $metas->station_name.'駅 ';
                }
            }
        }
    }

    // if(get_post_meta($post_id, 'koutsutoho2', true) !="")
    //     $moyori .= ' 徒歩'.get_post_meta($post_id, 'koutsutoho2', true).'m';

        if(get_post_meta($post_id, 'koutsutoho2f', true) !="")
            $moyori .= ' 徒歩'.get_post_meta($post_id, 'koutsutoho2f', true).'分<br>';


            /*
             * バス路線名、バス停名表示
             *
             * @since Fudousan Bus Plugin 1.0.0
             * For inc-single-fudo.php and inc-archive-fudo.php
             * admin_fudou.php fudo-widget.php fudo-widget3.php
             * apply_filters( 'fudoubus_buscorse_busstop_single', '', $post_id, 1 );
             *
             * @param int $post_id Post ID.
             * @param int $type 1 or 2.
             */
            $koutsubusstei = apply_filters( 'fudoubus_buscorse_busstop_single', '', $post_id, 2 );

            if( !$koutsubusstei ){
                $koutsubusstei = get_post_meta($post_id, 'koutsubusstei2', true);
            }
            $koutsubussfun = get_post_meta($post_id, 'koutsubussfun2', true);
            $koutsutohob2f = get_post_meta($post_id, 'koutsutohob2f', true);

            if( $koutsubusstei || $koutsubussfun ){

                if($rosen_name == 'バス'){
                    $moyori .= '(' . $koutsubusstei;
                    if( !empty( $koutsubussfun ) ) $moyori .= ' 乗'.$koutsubussfun.'分';
                }else{
                    $moyori .= '<br />';
                    $moyori .= ' バス(' . $koutsubusstei;
                    if( !empty( $koutsubussfun ) ) $moyori .= ' 乗'.$koutsubussfun.'分';
                }

                if($koutsutohob2f !="" )
                    $moyori .= ' 停歩'.$koutsutohob2f.'分';
                    $moyori .= ')';
            }


    $rosen_name = '';
    $koutsurosen_data = get_post_meta($post_id, 'koutsurosen3', true);
    $koutsueki_data = get_post_meta($post_id, 'koutsueki3', true);

    $shozaichiken_data = get_post_meta($post_id,'shozaichicode',true);
    $shozaichiken_data = myLeft($shozaichiken_data,2);


    if($koutsurosen_data !=""){
        $sql = "SELECT rosen_name FROM " . $wpdb->prefix . DB_ROSEN_TABLE . " WHERE rosen_id =".$koutsurosen_data."";
        //    $sql = $wpdb->prepare($sql,'');
        $metas = $wpdb->get_row( $sql );
        if(!empty($metas)){
            $rosen_name = $metas->rosen_name;
            $moyori .= $rosen_name." ";
            // $moyori .= '<span class="o-roomStation_name o-text __bold">'.$rosen_name."</span>";
        }
    }

    if($koutsurosen_data !="" && $koutsueki_data !=""){
        $sql = "SELECT DTS.station_name";
        $sql .=  " FROM " . $wpdb->prefix . DB_ROSEN_TABLE . " AS DTR";
        $sql .=  " INNER JOIN " . $wpdb->prefix . DB_EKI_TABLE . " AS DTS ON DTR.rosen_id = DTS.rosen_id";
        $sql .=  " WHERE DTS.station_id=".$koutsueki_data." AND DTS.rosen_id=".$koutsurosen_data."";
        //    $sql = $wpdb->prepare($sql,'');
        $metas = $wpdb->get_row( $sql );
        if(!empty($metas)){
            if($metas->station_name != '＊＊＊＊'){
                if( $metas->station_name != '' ){
                    $moyori .= $metas->station_name.'駅 ';
                }
            }
        }
    }

    // if(get_post_meta($post_id, 'koutsutoho3', true) !="")
    //     $moyori .= ' 徒歩'.get_post_meta($post_id, 'koutsutoho3', true).'m';

        if(get_post_meta($post_id, 'koutsutoho3f', true) !="")
            $moyori .= ' 徒歩'.get_post_meta($post_id, 'koutsutoho3f', true).'分';


            /*
             * バス路線名、バス停名表示
             *
             * @since Fudousan Bus Plugin 1.0.0
             * For inc-single-fudo.php and inc-archive-fudo.php
             * admin_fudou.php fudo-widget.php fudo-widget3.php
             * apply_filters( 'fudoubus_buscorse_busstop_single', '', $post_id, 1 );
             *
             * @param int $post_id Post ID.
             * @param int $type 1 or 2.
             */
            $koutsubusstei = apply_filters( 'fudoubus_buscorse_busstop_single', '', $post_id, 2 );

            if( !$koutsubusstei ){
                $koutsubusstei = get_post_meta($post_id, 'koutsubusstei3', true);
            }
            $koutsubussfun = get_post_meta($post_id, 'koutsubussfun3', true);
            $koutsutohob3f = get_post_meta($post_id, 'koutsutohob3f', true);

            if( $koutsubusstei || $koutsubussfun ){

                if($rosen_name == 'バス'){
                    $moyori .= '(' . $koutsubusstei;
                    if( !empty( $koutsubussfun ) ) $moyori .= ' 乗'.$koutsubussfun.'分';
                }else{
                    $moyori .= '<br />';
                    $moyori .= ' バス(' . $koutsubusstei;
                    if( !empty( $koutsubussfun ) ) $moyori .= ' 乗'.$koutsubussfun.'分';
                }

                if($koutsutohob3f !="" )
                    $moyori .= ' 停歩'.$koutsutohob3f.'分';
                    $moyori .= ')';
            }

            return $moyori;
}



//--円用単位関数
function number_unit($int=0){
    if(!empty($int)){
        return number_format($int);
    }
}//大外


/**
 * 物件に表示する特にオススメした特徴
 */

function the_recommend_speciality($post_id){
    $specialities = get_field('recommend_speciality', $post_id);

    if( have_rows('recommend_speciality', $post_id) ):

        ?>

        <div class="o-rsingleTag flex-w">
        <?php
        foreach( $specialities as $v ){//print_r($v);
            ?>
            <div class="o-rsingleTag_item"><i class="o-rsingleTag_icon o-icon __min flaticon-tag"></i>
                <a href="<?php echo $v->recommend_speciality_link;?>" class="o-rsingleTag_link o-link __nocolor"><?php echo $v->post_title;?></a></div>
        <?php
        }
        ?>
        </div>

        <?php
    endif;
}


/**
 * 建物構造
 *
 * @since Fudousan Plugin 1.0.0 *
 * @param int $post_id Post ID.
 */
function my_custom_tatemonokozo_print($post_id) {
    $tatemonokozo_data = get_post_meta($post_id,'tatemonokozo',true);
    $kouzou = '';

    if($tatemonokozo_data=="1")     $kouzou = '木造';
    if($tatemonokozo_data=="2")     $kouzou = 'ブロック';
    if($tatemonokozo_data=="3")     $kouzou = '鉄骨造';
    if($tatemonokozo_data=="4")     $kouzou = 'RC（鉄筋コンクリート）';
    if($tatemonokozo_data=="5")     $kouzou = 'SRC（鉄骨鉄筋コンクリート）';
    if($tatemonokozo_data=="6")     $kouzou = 'PC（プレキャストコンクリート）';
    if($tatemonokozo_data=="7")     $kouzou = 'HPC（鉄骨プレキャストコンクリート）';
    if($tatemonokozo_data=="9")     $kouzou = 'その他';
    if($tatemonokozo_data=="10")     $kouzou = '軽量鉄骨';
    if($tatemonokozo_data=="11")     $kouzou = 'ALC（軽量気泡コンクリート）';
    if($tatemonokozo_data=="12")     $kouzou = '鉄筋ブロック';
    if($tatemonokozo_data=="13")     $kouzou = 'CFT(コンクリート充填鋼管)';

    //text
    // if( $tatemonokozo_data !='' && !is_numeric($tatemonokozo_data) ) return $kouzou;
    return $kouzou;

}

function my_custom_tatemonokozo_print_br($post_id) {
    $tatemonokozo_data = get_post_meta($post_id,'tatemonokozo',true);
    if($tatemonokozo_data=="1")     echo '木造';
    if($tatemonokozo_data=="2")     echo 'ブロック';
    if($tatemonokozo_data=="3")     echo '鉄骨造';
    if($tatemonokozo_data=="4")     echo 'RC<br>（鉄筋コンクリート）';
    if($tatemonokozo_data=="5")     echo 'SRC<br>（鉄骨鉄筋コンクリート）';
    if($tatemonokozo_data=="6")     echo 'PC<br>（プレキャストコンクリート）';
    if($tatemonokozo_data=="7")     echo 'HPC<br>（鉄骨プレキャストコンクリート）';
    if($tatemonokozo_data=="9")     echo 'その他';
    if($tatemonokozo_data=="10")     echo '軽量鉄骨';
    if($tatemonokozo_data=="11")     echo 'ALC<br>（軽量気泡コンクリート）';
    if($tatemonokozo_data=="12")     echo '鉄筋ブロック';
    if($tatemonokozo_data=="13")     echo 'CFT<br>(コンクリート充填鋼管)';

    //text
    if( $tatemonokozo_data !='' && !is_numeric($tatemonokozo_data) ) return $tatemonokozo_data;

}



/**
 * 共有設備
 *
 * @since Fudousan Plugin 1.8.2
 * jumpshare
 * @param int $post_id Post ID.
 */
function my_custom_kyouyu_setsubi_print($post_id) {
    global $work_setsubi2;

    $setsubi_dat = '';
    $valu_dat = '';

    $setsubi_data = get_post_meta($post_id, 'setsubi2', true);

    foreach($work_setsubi2 as $meta_box){
        if( false !== strpos($setsubi_data, $meta_box['code']) ){
            //$setsubi_dat .= '<span class="setsubi_' . $meta_box['code'] . ' setsubi_dat">' . $meta_box['name']."</span> ";
            $setsubi_dat .= '<li class="o-listItem">' . $meta_box['name'] . '</li>';
        }
    }
    if( $setsubi_dat ){
    //    $valu_dat .= '<div class="setsubi_dat">';
        $valu_dat .=  $setsubi_dat;
    //    $valu_dat .= '</div>';
    }

    // if( get_post_meta($post_id,'setsubisonota',true) ){
    //     $valu_dat .= '<div class="setsubi_sonota">' . get_post_meta($post_id,'setsubisonota',true) . '</div>';
    // }
    return $valu_dat;
}



/**
 * 所在地
 *
 * @since Fudousan Plugin 1.5.0
 *
 * @param int $post_id Post ID.
 */
function my_custom_shozaichi_print($post_id) {
    global $wpdb;
    $ken = '';
    $shozaichiken_data = get_post_meta($post_id,'shozaichicode',true);
    $shozaichiken_data = myLeft($shozaichiken_data,2);

    if($shozaichiken_data=="")
        $shozaichiken_data = get_post_meta($post_id,'shozaichiken',true);

    if($shozaichiken_data != ""){
        /*
        $sql = "SELECT middle_area_name FROM " . $wpdb->prefix . DB_KEN_TABLE . " WHERE middle_area_id=".$shozaichiken_data."";
    //    $sql = $wpdb->prepare($sql,'');
        $metas = $wpdb->get_row( $sql );
        if( !empty($metas) ) echo $metas->middle_area_name;
        */
        $ken .= fudo_ken_name( $shozaichiken_data );
    }

    $shozaichicode_data = get_post_meta($post_id,'shozaichicode',true);
    $shozaichicode_data = myLeft($shozaichicode_data,5);
    $shozaichicode_data = myRight($shozaichicode_data,3);

    if($shozaichiken_data !="" && $shozaichicode_data !=""){
        $sql = "SELECT narrow_area_name FROM " . $wpdb->prefix . DB_SHIKU_TABLE . " WHERE middle_area_id=".$shozaichiken_data." and narrow_area_id =".$shozaichicode_data."";
    //    $sql = $wpdb->prepare($sql,'');
        $metas = $wpdb->get_row( $sql );
        if(!empty($metas)) $ken .= $metas->narrow_area_name;
    }

    return $ken;
}




/**
 * スクレイピングした画像情報を取得する関数
 * 引数で渡されたpost_idよりuniq_idを取得。
 * housing_imagesから、該当uniq_idの全件レコードを取得し返す。
 * uniq_idが存在しないか、housing_imagesに該当レコードが無い場合は、空配列を返す。
 * @param int $post_id
 * @param int $type 取得する画像タイプ（1:間取り画像2:外観）なしの場合は間取り以外の画像全て
 * @return array|object|NULL
 */
function stw_get_singlefudo_images( $post_id, $type=null ){
    global $wpdb;
    $rtn_arr = array();
    // postmetaよりuniq_idを取得
    $uniq_res = $wpdb->get_row($wpdb->prepare("select meta_value from ".$wpdb->prefix . "postmeta where post_id = %s and meta_key = 'uniq_id'", $post_id ));
    // echo $uniq_res->meta_value;
    if ( $uniq_res->meta_value ){
        $uniq_id = $uniq_res->meta_value;
        // スクレイピングした画像情報を取得
        if ( 1 == $type ){ // 間取り画像
            $images_res = $wpdb->get_results($wpdb->prepare("select * from " . $wpdb->prefix ."housing_images where uniq_id = %s and is_madori = 1", $uniq_id));

        } elseif ( 2 == $type ) { // 外観
            $images_res = $wpdb->get_results($wpdb->prepare("select * from " . $wpdb->prefix ."housing_images where uniq_id = %s and description like '外観%' ", $uniq_id));
            //echo '<pre>';var_dump($uniq_id);echo '</pre>';

        } else {
            $sql = $wpdb->prepare("select * from " . $wpdb->prefix ."housing_images where uniq_id = %s and is_madori = 0 group by description, file_size", $uniq_id);
            // echo $sql;select * from kctwp_housing_images where uniq_id = '82176727' and is_madori = 0 group by description, file_size
            $images_res = $wpdb->get_results($sql);
            // print_r($images_res);
        }
        if ( 0 < $wpdb->num_rows ){
            $rtn_arr = $images_res;
        }
    }

    // var_dump($rtn_arr);

    return $rtn_arr;
}




/**
 * 物件領域にショートコードを追加
 */
add_filter('manage_edit-fudo_columns', 'add_custom_post_columns_shortcode2');
function add_custom_post_columns_shortcode2($columns)
{
    $columns['post_modified'] = "ショートコード";
    return $columns;
}

add_action('manage_posts_custom_column', 'add_post_column_shortcode', 10, 2);
function add_post_column_shortcode($column_name, $post_id)
{
    if ($column_name == 'post_modified') {
        echo '[' . get_post_type() . ' id="' . $post_id . '"]<br>' . 'echo do_shortcode( \'[fudo id="' . $post_id . '"]\' );';
        echo '<br>[' . get_post_type() . '_room_list id="' . $post_id . '"]<br>' . 'echo do_shortcode( \'[fudo_room_list id="' . $post_id . '"]\' );';
    }
}



/**
 * 物件概要表示用ショートコード
 */
function get_bukken($atts)
{
    global $post;

    $bukken_id = shortcode_atts(array(
        'id' => ''
    ), $atts);

    $fudo_id = $bukken_id['id'];

    ob_start();
    include('template/bukken-summary.php');
    $summary = ob_get_contents();
    ob_end_clean();

    return $summary;

}
add_shortcode('fudo', 'get_bukken');


/**
 * 部屋一覧表示用ショートコード
 */
function get_bukken_room_list($atts){
    $bukken_id = shortcode_atts(array(
        'id' => '',
        'nth' => '',
        'per' => "50"
    ), $atts);

    $fudo_id = $bukken_id['id'];
    $nth     = $bukken_id['nth'];
    $per     = $bukken_id['per'];

    $per = ($per==0)? 50 : $per;
    //--roomに関連の親情報を集める
    $args = array(
        'connected_type' => 'room_relation',
        'connected_items' => $fudo_id,
        'posts_per_page' => $per,

        'meta_query' => array(
            // 'relation' => 'AND',
            // 'meta_nyukyogenkyo' => array(
            //         'relation' => 'OR',
            //         array(
            //             'relation' => 'OR',
            //             array(
            //             'key' => 'nyukyogenkyo',
            //             'type' => 'numeric',
            //             'value' => 3),//即入居可
            //             array(
            //                 'key' => 'nyukyogenkyo',
            //                 'type' => 'numeric',
            //                 'value' => 2),//居住中
            //             array(
            //                 'key' => 'nyukyogenkyo',
            //                 'type' => 'numeric',
            //                 'value' => 11),//空室
            //             array(
            //                 'key' => 'nyukyogenkyo',
            //                 'type' => 'numeric',
            //                 'value' => 1),//空室
            //             array(
            //                 'key' =>  'nyukyogenkyo',
            //                 'value' => '*'
            //             )
            //         ),
            //     ),
                'meta_kakaku' => array(
                    'key'=>'kakaku',
                    'type' => 'numeric',
                    'compare' => 'EXISTS',
                    )
        ),
          'orderby' => array( 'nyukyogenkyo' => 'DESC','meta_kakaku' => 'ASC'),
        //   'order' => 'DESC'
    );

    $connected_parent = get_posts($args);
    $bukken_mansitu_fg = get_field('bukken_status');

    $kensu = count($connected_parent);

    ob_start();
    include('template/rooms-for-bukken-for-detail.php');
    $rooms = ob_get_contents();
    ob_end_clean();

    return $rooms;
}
add_shortcode('fudo_room_list', 'get_bukken_room_list');



/**
 * モーダルで表示される物件画像生成
 * 左側の大きいブロック用
 */
function get_bukken_images($post_id, $room_image, $i){

    $j = 1;
    $modal_images = '';
    //間取り
    if($room_image){
        //        $modal_images .= '<h4 class="o-title o-modalTitle_sub">間取り</h4>';
        $modal_images .= '<div><img src="' . $room_image . '" alt="" class="o-modalRoom_img o-roomSlick_mainImg js-slideMain_item-' . $i . ' .slide-item' . $i . '.slick-slide.slick-current" loading="lazy">' . PHP_EOL . '</div>';
    }

    //棟の外観
    $bukken_madori_images = get_field('bukken_madori_images', $post_id);
    if($bukken_madori_images){
        //        $modal_images .= '<h4 class="o-title o-modalTitle_sub">間取り</h4>';
        foreach($bukken_madori_images as $v){
            $image_src = $v['sizes']['large'];
            $modal_images .= '<img src="' . $image_src . '" alt="" class="o-modalRoom_img o-roomSlick_mainImg js-slideMain_item-' . $i . '" loading="lazy">' . PHP_EOL;
        }
    }

    //棟その他
    $bukken_gaikan_kyouyou = get_field('bukken_gaikan_kyouyou', $post_id);
    if($bukken_gaikan_kyouyou){
        //        $modal_images .= '<h4 class="o-title o-modalTitle_sub">外観・共用部</h4>';
        foreach($bukken_gaikan_kyouyou as $v){
            $image_src = $v['sizes']['large'];
            $modal_images .= '<img src="' . $image_src . '" alt="" class="o-modalRoom_img o-roomSlick_mainImg js-slideMain_item-' . $i . '" loading="lazy">' . PHP_EOL;
        }
    }

    //リビング画像
    $bukken_situnai_senyu = get_field('bukken_situnai_senyu', $post_id);
    if($bukken_situnai_senyu){
        //        $modal_images .= '<h4 class="o-title o-modalTitle_sub">室内・専有部</h4>';
        foreach($bukken_situnai_senyu as $v){
            $image_src = $v['sizes']['large'];
            $modal_images .= '<img src="' . $image_src . '" alt="" class="o-modalRoom_img o-roomSlick_mainImg js-slideMain_item-' . $i . '" loading="lazy">' . PHP_EOL;
        }
    }
    // スクレイピングした画像
    $h_images = stw_get_singlefudo_images($post_id);
    if ( 0 < count($h_images) ){
        //        $modal_images .= '<h4 class="o-title o-modalTitle_sub">すべての画像</h4>';


        $i++;
        foreach($h_images as $oh){
            if(!in_array(basename($oh->image_path), get_ban_image_file_name())){
                $image_src = stw_generate_imgurl($oh->image_path);
                // 管理者ログインしていれば削除対象画像URLが出る
                // if(is_user_logged_in()){
                //     $modal_images .= '<div><input class="o-modalInput __hide" id="copyTarget' . $i . '" type="text" value="' . basename($oh->image_path) . '" style="width:600px;border:1px solid #444;" readonly><button style="background-color:#444;" onclick="copyToClipboard(' . $i . ')">Copy</button>';
                // }
                $modal_images .= '<div><img src="' . $image_src . '" alt="" class="o-modalRoom_img o-roomSlick_mainImg js-slideMain_item-' . $i . ' .slide-item' . $i . '.slick-slide.slick-current" loading="lazy">' . PHP_EOL . '</div>';
            }
            $i++;
        }
    }

    return $modal_images;

        // //棟の外観
        // $bukken_gaikan_images = get_field('bukken_gaikan_images', $post_id);
        // if($bukken_gaikan_images){
        //     echo '<h4 class="o-title o-modalTitle_sub">棟の外観</h4>';
        //     foreach($bukken_gaikan_images as $v){
        //         $image_src = $v['sizes']['large'];
    //         echo '<img src="' . $image_src . '" alt="" class="o-modalRoom_img o-roomSlick_mainImg" loading="lazy">' . PHP_EOL;
        //     }
        // }

        // //棟その他
        // $bukken_other_images = get_field('bukken_other_images', $post_id);
        // if($bukken_other_images){
        //     echo '<h4 class="o-title o-modalTitle_sub">棟その他</h4>';
        //     foreach($bukken_other_images as $v){
        //         $image_src = $v['sizes']['large'];
    //         echo '<img src="' . $image_src . '" alt="" class="o-modalRoom_img o-roomSlick_mainImg" loading="lazy">' . PHP_EOL;
        //     }
        // }

        // //リビング画像
        // $bukken_living_images = get_field('bukken_living_images', $post_id);
        // if($bukken_living_images){
        //     echo '<h4 class="o-title o-modalTitle_sub">リビング</h4>';
        //     foreach($bukken_living_images as $v){
        //         $image_src = $v['sizes']['large'];
    //         echo '<img src="' . $image_src . '" alt="" class="o-modalRoom_img o-roomSlick_mainImg" loading="lazy">' . PHP_EOL;
        //     }
        // }

        // //寝室
        // $bukken_bedroom_images = get_field('bukken_bedroom_images', $post_id);
        // if($bukken_bedroom_images){
        //     echo '<h4 class="o-title o-modalTitle_sub">寝室</h4>';
        //     foreach($bukken_bedroom_images as $v){
        //         $image_src = $v['sizes']['large'];
    //         echo '<img src="' . $image_src . '" alt="" class="o-modalRoom_img o-roomSlick_mainImg" loading="lazy">' . PHP_EOL;
        //     }
        // }

        // //浴室
        // $bukken_bathroom_images = get_field('bukken_bathroom_images', $post_id);
        // if($bukken_bathroom_images){
        //     echo '<h4 class="o-title o-modalTitle_sub">浴室</h4>';
        //     foreach($bukken_bathroom_images as $v){
        //         $image_src = $v['sizes']['large'];
    //         echo '<img src="' . $image_src . '" alt="" class="o-modalRoom_img o-roomSlick_mainImg" loading="lazy">' . PHP_EOL;
        //     }
        // }

        // //室内その他
        // $bukken_room_images = get_field('bukken_room_images', $post_id);
        // if($bukken_room_images){
        //     echo '<h4 class="o-title o-modalTitle_sub">室内その他</h4>';
        //     foreach($bukken_room_images as $v){
        //         $image_src = $v['sizes']['large'];
    //         echo '<img src="' . $image_src . '" alt="" class="o-modalRoom_img o-roomSlick_mainImg" loading="lazy">' . PHP_EOL;
        //     }
        // }

        // //物件間取り画像
        // $bukken_madori_images = get_field('bukken_madori_images', $post_id);
        // if($bukken_madori_images){
        //     echo '<h4 class="o-title o-modalTitle_sub">物件間取り</h4>';
        //     foreach($bukken_madori_images as $v){
        //         $image_src = $v['sizes']['large'];
    //         echo '<img src="' . $image_src . '" alt="" class="o-modalRoom_img o-roomSlick_mainImg" loading="lazy">' . PHP_EOL;
        //     }
        // }




    }

/**
 * モーダルで表示される物件画像生成
 * 右側の小さい画像郡
 */
function get_bukken_subimages($post_id, $room_image, $i){

    $j = 1;
    $modal_images = '';
    //間取り
    if($room_image){
        //        $modal_images .= '<h4 class="o-title o-modalTitle_sub">間取り</h4>';
        $modal_images .= '<div class="o-roomSlick_subItem js-slideThumb_item-' . $j . ' thumbnail-current"><img src="' . $room_image . '" alt="" class="o-modalRoom_img o-roomSlick_subImg" loading="lazy"></div>' . PHP_EOL;
    }

    //棟の外観
    $bukken_madori_images = get_field('bukken_madori_images', $post_id);
    if($bukken_madori_images){
        //        $modal_images .= '<h4 class="o-title o-modalTitle_sub">間取り</h4>';
        foreach($bukken_madori_images as $v){
            $image_src = $v['sizes']['large'];
            $modal_images .= '<div class="o-roomSlick_subItem js-slideThumb_item-' . $j . '"><img src="' . $image_src . '" alt="" class="o-modalRoom_img o-roomSlick_subImg" loading="lazy"></div>' . PHP_EOL;
        }
    }

    //棟その他
    $bukken_gaikan_kyouyou = get_field('bukken_gaikan_kyouyou', $post_id);
    if($bukken_gaikan_kyouyou){
        //        $modal_images .= '<h4 class="o-title o-modalTitle_sub">外観・共用部</h4>';
        foreach($bukken_gaikan_kyouyou as $v){
            $image_src = $v['sizes']['large'];
            $modal_images .= '<div class="o-roomSlick_subItem js-slideThumb_item-' . $j . '"><img src="' . $image_src . '" alt="" class="o-modalRoom_img o-roomSlick_subImg" loading="lazy"></div>' . PHP_EOL;
        }
    }

    //リビング画像
    $bukken_situnai_senyu = get_field('bukken_situnai_senyu', $post_id);
    if($bukken_situnai_senyu){
        //        $modal_images .= '<h4 class="o-title o-modalTitle_sub">室内・専有部</h4>';
        foreach($bukken_situnai_senyu as $v){
            $image_src = $v['sizes']['large'];
            $modal_images .= '<div class="o-roomSlick_subItem js-slideThumb_item-' . $j . '"><img src="' . $image_src . '" alt="" class="o-modalRoom_img o-roomSlick_subImg" loading="lazy"></div>' . PHP_EOL;
        }
    }
    // スクレイピングした画像
    $h_images = stw_get_singlefudo_images($post_id);
    if ( 0 < count($h_images) ){
        //        $modal_images .= '<h4 class="o-title o-modalTitle_sub">すべての画像</h4>';


        $j++;
        foreach($h_images as $oh){
            if(!in_array(basename($oh->image_path), get_ban_image_file_name())){
                $image_src = stw_generate_imgurl($oh->image_path);
                // print_r($oh);
                // 管理者ログインしていれば削除対象画像URLが出る
                // if(is_user_logged_in()){
                //     $modal_images .= '<input class="o-modalInput __hide" id="copyTarget' . $j . '" type="text" value="' . basename($oh->image_path) . '" style="width:600px;border:1px solid #444;" readonly><button style="background-color:#444;" onclick="copyToClipboard(' . $j . ')">Copy</button>';
                // }
                $modal_images .= '<div class="o-roomSlick_subItem js-slideThumb_item-' . $j . '"><img src="' . $image_src . '" alt="" class="o-modalRoom_img o-roomSlick_subImg" alt="' . $oh->description . '" loading="lazy"></div>' . PHP_EOL;
            }
            $j++;
        }
    }

    return $modal_images;
}


/**
 * 各問い合わせに、対象post_idを渡す
 * CF7のフック
 */
function kaiza_form_tag_filter($tag){
    if ( ! is_array( $tag ) )
    return $tag;

    if(isset($_GET['bukken_id'])){ //投稿ID
        $bukken_id = htmlspecialchars($_GET['bukken_id']);
        $room_id = htmlspecialchars($_GET['room_id']);

        //
        // $bukken_room_name = "問い合わせ対象物件\n";
        $bukken_room_name = "";

        
        if(!empty($bukken_id)){
            $bukken_room_name .= '物件ID: ' . $bukken_id . "\n" . '【' . get_the_title($bukken_id) . "】";
            
            if(!empty($room_id)){
                $bukken_room_name .= get_post_meta($room_id, "room_floor", true) . "F\n";
                $bukken_room_name .= '部屋ID: ' . $room_id;
            }
        }


        //CF7の名称変数を修得
        $name = $tag['name'];

    //こちらにCF7上で値をいれたいfield名を指定
    if($name == 'taisyou'){
        $tag['values'] = (array)$bukken_room_name;
    }

    }
    return $tag;
    }
add_filter('wpcf7_form_tag', 'kaiza_form_tag_filter', 11);


?>

