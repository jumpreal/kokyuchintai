<?php

/*

 * 不動産プラグインウィジェット

 * @package WordPress 5.0

 * @subpackage Fudousan Plugin

 * Version: 5.0.0

*/





//物件詳細関連物件表示

function fudo_widgetInit_syousai() {

	register_widget('fudo_widget_syousai');

}

add_action('widgets_init', 'fudo_widgetInit_syousai');



/**

 * 物件詳細 関連物件表示

 *

 * Version: 5.0.0

 */

class fudo_widget_syousai extends WP_Widget {



	/**

	 * Register widget with WordPress 4.3.

	 */

	function __construct() {

		parent::__construct(

			'fudo_syousai', 			// Base ID

			'関連物件表示(物件詳細ページ)' ,		// Name

			array( 'description' => '物件詳細ページに関連物件を表示します。', )	// Args

		);

	}



	/** @see WP_Widget::form */	

	function form($instance) {



		global $is_fudoukaiin,$is_fudoubus;



		$title = isset($instance['title']) ? esc_attr($instance['title']) : '';

		$item  = isset($instance['item']) ? esc_attr($instance['item']) : '';

		$sort  = isset($instance['sort']) ? esc_attr($instance['sort']) : '';



		$view1 = isset($instance['view1']) ? esc_attr($instance['view1']) : '';

		$view2 = isset($instance['view2']) ? esc_attr($instance['view2']) : '';

		$view3 = isset($instance['view3']) ? esc_attr($instance['view3']) : '';

		$view4 = isset($instance['view4']) ? esc_attr($instance['view4']) : '';

		$view5 = isset($instance['view5']) ? esc_attr($instance['view5']) : '';



		$view6 = isset($instance['view6']) ? esc_attr($instance['view6']) : '';	//バス路線



		$kaiinview = isset($instance['kaiinview']) ? esc_attr($instance['kaiinview']) : '';



		if($item=='') $item = 5;



		?>

		<p><label for="<?php echo $this->get_field_id('title'); ?>">

		タイトル <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo $title; ?>" /></label></p>





		<p><label for="<?php echo $this->get_field_id('item'); ?>">

		最大表示数 <input class="widefat" id="<?php echo $this->get_field_id('item'); ?>" name="<?php echo $this->get_field_name('item'); ?>" type="text" value="<?php echo $item; ?>" /></label></p>





		<p><label for="<?php echo $this->get_field_id('sort'); ?>">

		優先絞込 <select class="widefat" id="<?php echo $this->get_field_id('sort'); ?>" name="<?php echo $this->get_field_name('sort'); ?>">

			<option value="2"<?php if($sort == 2){echo ' selected="selected"';} ?>>地域</option>

			<option value="3"<?php if($sort == 3){echo ' selected="selected"';} ?>>駅</option>

			<option value="1"<?php if($sort == 1){echo ' selected="selected"';} ?>>地図座標(近い順)</option>

		</select></label></p>





		表示項目

		<p><label for="<?php echo $this->get_field_id('view1'); ?>">

		タイトル<select class="widefat" id="<?php echo $this->get_field_id('view1'); ?>" name="<?php echo $this->get_field_name('view1'); ?>">

			<option value="1"<?php if($view1 == 1){echo ' selected="selected"';} ?>>表示する</option>

			<option value="2"<?php if($view1 == 2){echo ' selected="selected"';} ?>>表示しない</option>

		</select></label></p>



		<p><label for="<?php echo $this->get_field_id('view2'); ?>">

		価格 <select class="widefat" id="<?php echo $this->get_field_id('view2'); ?>" name="<?php echo $this->get_field_name('view2'); ?>">

			<option value="1"<?php if($view2 == 1){echo ' selected="selected"';} ?>>表示する</option>

			<option value="2"<?php if($view2 == 2){echo ' selected="selected"';} ?>>表示しない</option>

		</select></label></p>



		<p><label for="<?php echo $this->get_field_id('view3'); ?>">

		間取り・土地面積 <select class="widefat" id="<?php echo $this->get_field_id('view3'); ?>" name="<?php echo $this->get_field_name('view3'); ?>">

			<option value="1"<?php if($view3 == 1){echo ' selected="selected"';} ?>>表示する</option>

			<option value="2"<?php if($view3 == 2){echo ' selected="selected"';} ?>>表示しない</option>

		</select></label></p>



		<p><label for="<?php echo $this->get_field_id('view4'); ?>">

		地域 <select class="widefat" id="<?php echo $this->get_field_id('view4'); ?>" name="<?php echo $this->get_field_name('view4'); ?>">

			<option value="1"<?php if($view4 == 1){echo ' selected="selected"';} ?>>表示する</option>

			<option value="2"<?php if($view4 == 2){echo ' selected="selected"';} ?>>表示しない</option>

		</select></label></p>



		<p><label for="<?php echo $this->get_field_id('view5'); ?>">

		路線・駅 <select class="widefat" id="<?php echo $this->get_field_id('view5'); ?>" name="<?php echo $this->get_field_name('view5'); ?>">

			<option value="1"<?php if($view5 == 1){echo ' selected="selected"';} ?>>表示する</option>

			<option value="2"<?php if($view5 == 2){echo ' selected="selected"';} ?>>表示しない</option>

		</select></label></p>



	<?php if( $is_fudoubus ) { ?>

		<p><label for="<?php echo $this->get_field_id('view6'); ?>">

		バス路線 <select class="widefat" id="<?php echo $this->get_field_id('view6'); ?>" name="<?php echo $this->get_field_name('view6'); ?>">

			<option value="2"<?php if($view6 == 2){echo ' selected="selected"';} ?>>表示しない</option>

			<option value="1"<?php if($view6 == 1){echo ' selected="selected"';} ?>>表示する</option>

		</select></label></p>

	<?php } ?>



	<?php if($is_fudoukaiin && get_option('kaiin_users_can_register') == '1' ){ ?>

		<p><label for="<?php echo $this->get_field_id('kaiinview'); ?>">

		会員物件 <select class="widefat" id="<?php echo $this->get_field_id('kaiinview'); ?>" name="<?php echo $this->get_field_name('kaiinview'); ?>">

			<option value="1"<?php if($kaiinview == 1){echo ' selected="selected"';} ?>>会員・一般物件を表示する</option>

			<option value="2"<?php if($kaiinview == 2){echo ' selected="selected"';} ?>>会員物件を表示しない</option>

			<option value="3"<?php if($kaiinview == 3){echo ' selected="selected"';} ?>>会員物件だけ表示</option>

		</select></label></p>

	<?php } ?>



		<?php 

	}





	/** @see WP_Widget::update */

	function update($new_instance, $old_instance) {

		return $new_instance;

	}



	/** @see WP_Widget::widget */

	function widget($args, $instance) {



		global $is_fudoukaiin,$is_fudoubus;

		global $wpdb;

		global $post;

		$post_ID = $post->ID;



		if($post_ID !=''){



			// outputs the content of the widget



		        extract( $args );

			$title = isset($instance['title']) ? $instance['title'] : '';

			$item  = isset($instance['item']) ?  $instance['item'] : '';

			$sort  = isset($instance['sort']) ?  $instance['sort'] : '';



			$view1 = isset($instance['view1']) ? $instance['view1'] : '';

			$view2 = isset($instance['view2']) ? $instance['view2'] : '';

			$view3 = isset($instance['view3']) ? $instance['view3'] : '';

			$view4 = isset($instance['view4']) ? $instance['view4'] : '';

			$view5 = isset($instance['view5']) ? $instance['view5'] : '';



			$view6 = isset($instance['view6']) ? $instance['view6']: '';	//バス路線



			$kaiinview = isset($instance['kaiinview']) ? $instance['kaiinview'] : '';

			if($kaiinview == '') $kaiinview = 1;



			if( !$is_fudoukaiin || get_option('kaiin_users_can_register') != '1' ){

				$kaiinview = 1;

			}



			if($item =="") 	$item=5;



			$newup_mark = get_option('newup_mark');

			if($newup_mark == '') $newup_mark=14;





			//map

			$ido_data = floatval(get_post_meta($post_ID,'bukkenido',true));

			$keido_data = floatval(get_post_meta($post_ID,'bukkenkeido',true));



			//種別

			$bukkenshubetsu_data = get_post_meta($post_ID,'bukkenshubetsu',true);



			//価格

			$kakaku_data = intval(get_post_meta($post_ID,'kakaku',true));



			//面積

			$tatemonomenseki_data = floatval(get_post_meta($post_ID,'tatemonomenseki',true));



			//面積

			$tochikukaku_data = floatval(get_post_meta($post_ID,'tochikukaku',true));



			//所在地

			$shozaichicode_data = get_post_meta($post_ID,'shozaichicode',true);



			//路線・駅

			$koutsurosen1_data = get_post_meta($post_ID,'koutsurosen1',true);

			$koutsueki1_data = get_post_meta($post_ID,'koutsueki1',true);



			$koutsurosen2_data = get_post_meta($post_ID,'koutsurosen2',true);

			$koutsueki2_data = get_post_meta($post_ID,'koutsueki2',true);





			$sql = "SELECT DISTINCT P.ID,P.post_title,P.post_modified,P.post_date";

			$sql .=  " FROM (((((((((($wpdb->posts AS P";

			$sql .=  " INNER JOIN $wpdb->postmeta AS PM   ON P.ID = PM.post_id) ";		

			$sql .=  " INNER JOIN $wpdb->postmeta AS PM_1 ON P.ID = PM_1.post_id) ";



			//面積

			if ($tatemonomenseki_data >1 ){

			$sql .=  " INNER JOIN $wpdb->postmeta AS PM_2 ON P.ID = PM_2.post_id) ";

			}else{

			$sql .=  ")";

			}



			//面積

			if ($tochikukaku_data >1 ){

			$sql .=  " INNER JOIN $wpdb->postmeta AS PM_3 ON P.ID = PM_3.post_id) ";

			}else{

			$sql .=  ")";

			}



			//地域

			if ($sort == 2 || $sort == ''){

			$sql .=  " INNER JOIN $wpdb->postmeta AS PM_10 ON P.ID = PM_10.post_id) "; 

			}else{

			$sql .=  ")";

			}



			//路線・駅

			if ($sort == 3 && $koutsurosen1_data !='' && $koutsueki1_data != '' ){

			$sql .=  " INNER JOIN $wpdb->postmeta AS PM_12 ON P.ID = PM_12.post_id) ";

			$sql .=  " INNER JOIN $wpdb->postmeta AS PM_13 ON P.ID = PM_13.post_id) ";

			}else{

			$sql .=  "))";

			}



			//座標

			if ($sort == 1 && $ido_data > 1 && $keido_data > 1 ){

			$sql .=  " INNER JOIN $wpdb->postmeta AS PM_15 ON P.ID = PM_15.post_id) ";

			$sql .=  " INNER JOIN $wpdb->postmeta AS PM_16 ON P.ID = PM_16.post_id) ";

			}else{

			$sql .=  "))";

			}



			//会員

			if( $kaiinview != 1 ){

				$sql = $sql . " INNER JOIN $wpdb->postmeta AS PM_4 ON P.ID = PM_4.post_id) ";

			}else{

				$sql = $sql . " )";

			}



			/*

			 * 関連物件表示 org追加SQL条件 WHERE INNER JOIN

			 *

			 * Version: 1.7.4

			 */

			$sql =  apply_filters( 'fudo_widget_syousai_inner_join_data', $sql, $kaiinview );





			$sql .=  " WHERE ";



			$sql .=  " P.post_status='publish' AND P.post_password = '' AND  (P.post_type ='fudo')";
			// $sql .=  " P.post_status='publish' AND P.post_password = '' AND  (P.post_type ='fudo' or P.post_type ='room')";



			$sql .=  " AND P.ID !=".$post_ID."";



			//種別

			$sql .=  " AND PM.meta_key='bukkenshubetsu' AND PM.meta_value = '".$bukkenshubetsu_data."'";



			//価格

			$sql .=  " AND PM_1.meta_key='kakaku' ";



			if ($tatemonomenseki_data >1 ){

			$sql .=  " AND PM_2.meta_key='tatemonomenseki'";

			}

			if ($tochikukaku_data >1 ){

			$sql .=  " AND PM_3.meta_key='tochikukaku' ";

			}



			//地域

			if ($sort == 2 || $sort == ''){

			$sql .=  " AND PM_10.meta_key='shozaichicode' AND PM_10.meta_value = '".$shozaichicode_data."'";

			}



			//路線・駅

			if ($sort == 3 && $koutsurosen1_data !='' && $koutsueki1_data != '' ){

			$sql .=  " AND PM_12.meta_key='koutsurosen1' AND PM_12.meta_value = '".$koutsurosen1_data."'";

			$sql .=  " AND PM_13.meta_key='koutsueki1' AND PM_13.meta_value = '".$koutsueki1_data."'";

			}



			//座標

			if ($sort == 1 && $ido_data > 1 && $keido_data > 1 ){

			$sql .=  " AND PM_15.meta_key='bukkenido' AND PM_16.meta_key ='bukkenkeido'";

			}



			//会員物件を表示しない

			if( $kaiinview == 2 ){

				$sql = $sql . " AND PM_4.meta_key='kaiin' AND PM_4.meta_value < 1 ";

			}



			//会員物件だけ表示

			if( $kaiinview == 3 ){

				$sql = $sql . " AND PM_4.meta_key='kaiin' AND PM_4.meta_value > 0 ";

			}



			/*

			 * 関連物件表示 org追加SQL条件 WHERE

			 *

			 * Version: 1.7.4

			 */

			$sql =  apply_filters( 'fudo_widget_syousai_where_data', $sql, $kaiinview );





			$order_by = '';



			//地域

			if ($sort == 2 || $sort == ''){

				$order_by .=  " ORDER BY PM_10.meta_value ASC";

			}



			//路線・駅

			if ($sort == 3 && $koutsurosen1_data !='' && $koutsueki1_data != '' ){

				$order_by .=  " ORDER BY PM_12.meta_value ASC";

			}



			//座標

			if ($sort == 1 && $ido_data > 1 && $keido_data > 1 ){

				$order_by .=  " ORDER BY ABS( PM_15.meta_value  - ".$ido_data.") + ABS( PM_16.meta_value - ".$keido_data.") ASC";

			}





			//面積

			if ($tatemonomenseki_data > 1 ){

				if($order_by == ''){

					$order_by .=  "  ORDER BY ABS( PM_2.meta_value - $tatemonomenseki_data) ASC";

				}else{

					$order_by .=  " , ABS( PM_2.meta_value - $tatemonomenseki_data) ASC";

				}

			}

			if ($tochikukaku_data > 1 ){

				if($order_by == ''){

					$order_by .=  " ORDER BY ABS( PM_3.meta_value - $tochikukaku_data) ASC";

				}else{

					$order_by .=  " , ABS( PM_3.meta_value - $tochikukaku_data) ASC";

				}

			}





			//価格に近い物件

				if($order_by == ''){

					$order_by .=  " ORDER BY ABS( PM_1.meta_value - $kakaku_data) ASC";

				}else{

					$order_by .=  " , ABS( PM_1.meta_value - $kakaku_data) ASC";

				}







			$sql .=  $order_by . " limit ".$item."";


// echo $sql;
		//	$sql = $wpdb->prepare($sql,'');

			$metas = $wpdb->get_results( $sql, ARRAY_A );



			//ユーザー別会員物件リスト

			$kaiin_users_rains_register = get_option('kaiin_users_rains_register');



			//物件詳細リンク

			if ( defined('FUDOU_BUKKEN_TARGET') && FUDOU_BUKKEN_TARGET== 1 ){

				$target_link = ' target="_blank" rel="noopener noreferrer"';

			}else{

				$target_link = '';

			}


//　■　□　■　□　■　□　■　□　■　□　■　□　■　□　■　□　■　□　■　□　■　□　■　□　■　□　■　□　■　□　■　□　


			if(!empty($metas)) {



				// echo $before_widget;



				if ( $title ){

					echo $before_title . $title . $after_title; 

				}





				$img_path = get_option('upload_path');

				if ($img_path == '')

					$img_path = 'wp-content/uploads';





				// echo '<div id="syousai_box">';

				// echo '<ul id="'.$args['widget_id'].'_1" class="syousai-content kanren">';



				//grid_count css class

				$grid_count = 1;

				?>
	<section class="l-contPopular l-cont __section __garter">
    <h2 class="o-title __title mb-3">関連している物件</h2>

    <div class="o-carousel swiper-container __carouselType01">
        <div class="l-contPopular_cont o-room o-carouselWrap swiper-wrapper flex-nw">
<?php


				foreach ( $metas as $meta ) {



					$rosen_bus = false;	//路線を「バス」に設定している?



					$bukken_id =  $meta['ID'];
					include('bukken-card.php');

				}//forech

?>
</div>
        <div class="o-carouselPagenation swiper-pagination"></div>
    </div>
</section>
<?php
				// echo '</ul>';

				// echo '</div>';



				// echo $after_widget;



				$syousai_box_widget_id = str_replace( '-' , '_' ,$args['widget_id']);



				//jquery.matchHeight.js

				?>

				<script type="text/javascript">



					<?php if( apply_filters( 'fudou_imagesloaded_use', true ) ){  ?>

						imagesLoaded( '#<?php echo $args['widget_id']; ?>_1', function(){

							setTimeout('syousai_box<?php echo $syousai_box_widget_id; ?>()', 1000); 

						});

					<?php }else{ ?>

						setTimeout('syousai_box<?php echo $syousai_box_widget_id; ?>()', 1000); 

					<?php } ?>



					function syousai_box<?php echo $syousai_box_widget_id; ?>() { 

						jQuery.noConflict();

						var j$ = jQuery;

						j$(function() {

						    j$('#<?php echo $args['widget_id']; ?>_1 > li').matchHeight();



						});

					}

				</script>

				<?php



			}

		}

	}

} // Class fudo_widget_syousai



