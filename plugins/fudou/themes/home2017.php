<?php
/**
 * The front page template file
 *
 * If the user has selected a static page for their homepage, this is what will
 * appear.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

/*
 * Fudou TopPage ver1.7.8
 */
get_header(); ?>


<div id="top">
  <div class="kv">
    <!-- キービジュアル -->
    <?php the_custom_header_markup(); ?>
    
    <div class="catch">
     <p class="mincho">厳選した高級物件だけを提案する <br><span>「高級賃貸東京」</span></p>
    </div>
    
    <!-- 検索枠 -->
    <div class="search-box1 wrap-s">
      <?php if(is_active_sidebar('widget-1')) : ?>
        <ul id="my_sidebar_widget">
          <?php dynamic_sidebar('widget-1'); ?>
        </ul>
      <?php endif; ?>
    </div>
  </div>


  <div id="top" class="wrap">
    <div id="primary" class="content-area">
      <main id="main" class="site-main" role="main">
      
  
        <div id="top_fbox" class="hentry">
          <?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('top_widgets') ) : ?>
          <?php endif; ?>
        </div><!-- #top_fbox -->
        <p>aaaaaaaaa</p>
  
  
      </main><!-- #main -->
    </div><!-- #primary -->
  </div><!-- .wrap -->
</div><!-- #top -->

<?php get_footer(); ?>
