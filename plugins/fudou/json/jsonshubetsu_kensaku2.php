<?php
/**
 * Front to the WordPress application. This file doesn't do anything, but loads
 * wp-blog-header.php which does and tells WordPress to load the theme.
 *
 * @package WordPress4.5
 * @subpackage Fudousan Plugin
 * Version: 1.7.4
 */

/**
 * Tells WordPress to load the WordPress theme and output it.
 *
 * @var bool
 */
define('WP_USE_THEMES', false);

/** Loads the WordPress Environment and Template */
require_once '../../../../wp-blog-header.php';

//$wpdb->show_errors();

	global $wpdb;
	global $work_setsubi2;
	
	$setsubi_dat = '';

	status_header( 200 );
	header("Content-Type: text/plain; charset=utf-8");
	header("X-Content-Type-Options: nosniff");


	$shu_data = isset($_POST['shu']) ? myIsNum_f($_POST['shu']) : '';
	if(empty($shu_data))
		$shu_data = isset($_GET['shu']) ? myIsNum_f($_GET['shu']) : '';

	if($shu_data == '1') 
		$shu_data = '< 3000' ;
	if($shu_data == '2') 
		$shu_data = '> 3000' ;

	if(intval($shu_data) > 3) 
		$shu_data = '= ' .$shu_data ;

// print_r($_GET);
	//共用チェックボックス
	$set_id2 = isset($_POST['set2']) ? $_POST['set2'] : '';
	if(empty($set_id2))
		$set_id2 = isset($_GET['set2']) ? $_GET['set2'] : '';
// print_r($set_id2);
	//num check
	$array_set_id2 = $set_id2;
	$set_id2 = array();
	if(!empty($array_set_id2)){
		foreach( $array_set_id2 as $data ) {
			if( (int)$data ){
				$set_id2[] = $data;
			}
		}
	}

//http://kokyuchintai-tokyo.com/?bukken=jsearch&shu=2&ros=0&eki=0&ken=0&sik=0&kalc=0&kahc=0&kalb=0&kahb=0&hof=0&tik=0&mel=0&meh=0&set2%5B%5D=26004&set%5B%5D=35001
	// print_r($set_id2);

	if( !empty($shu_data) ){
		//共用設備
		//このオプション値はadmin_fudou.php
		$widget_seach_setsubi2 = maybe_unserialize( get_option('widget_seach_setsubi2') );
// print_r($widget_seach_setsubi);//ここにはオプション設定画面の値がきちんと入ってきている。

//このSQLはなに？
		$sql  =  " SELECT DISTINCT PM.meta_value AS setsubi2";
		$sql .=  " FROM (($wpdb->posts AS P";
		$sql .=  " INNER JOIN $wpdb->postmeta AS PM   ON P.ID = PM.post_id) ";
		$sql .=  " INNER JOIN $wpdb->postmeta AS PM_1 ON P.ID = PM_1.post_id) ";

		//検索 SQL 表示制限 INNER JOIN
		$sql .=  apply_filters( 'inc_archive_kensaku_sql_inner_join', '' );

		$sql .=  " WHERE P.post_status='publish' AND P.post_password = '' AND P.post_type ='room' ";
		$sql .=  " AND PM_1.meta_key='bukkenshubetsu'";
		$sql .=  " AND CAST( PM_1.meta_value AS SIGNED ) ".$shu_data."";
		$sql .=  " AND PM.meta_key='setsubi2'";

		//検索 SQL 表示制限 WHERE
		$sql .=  apply_filters( 'inc_archive_kensaku_sql_where', '' );

		$sql .=  " ORDER BY CAST( PM.meta_value AS SIGNED )";
		//$sql = $wpdb->prepare($sql,'');
// echo $sql;
		$metas = $wpdb->get_results( $sql,  ARRAY_A );
		$array_setsubi2 = array();
// print_r($work_setsubi2);
// print_r($metas);
		if(!empty($metas)) {
			foreach($work_setsubi2 as $meta_box){
				foreach ( $metas as $meta ) {
					
					$setsubi_data = $meta['setsubi2'];

					if( strpos($setsubi_data,$meta_box['code']) ){

						$setsubi_code = $meta_box['code'];
						$setsubi_name = $meta_box['name'];

						$data = array( $setsubi_code => array("code" => $setsubi_code,"name" => $setsubi_name));

						foreach($array_setsubi2 as $meta_box2){
							if ( $setsubi_code == $meta_box2['code'])
								$data = '';
						}
						if(!empty($data))
						$array_setsubi2 = array_merge( $data , $array_setsubi2);
					}
				}
			}
		}
// print_r($array_setsubi2);
		if(!empty($array_setsubi2)) {

			krsort($array_setsubi2);
			$setsubi_dat ='<span class="jsearch_setsubi">共用設備</span><br />';

			foreach($array_setsubi2 as $meta_box3){

				//$widget_seach_setsubi2
				if(is_array($widget_seach_setsubi2)) {
					$k=0;
					foreach($widget_seach_setsubi2 as $meta_box5){
						if($widget_seach_setsubi2[$k] == $meta_box3['code']){

							$setsubi_dat .= '<span style="display: inline-block">';
							$setsubi_dat .= '<input type="checkbox" name="set2[]"  value="'.$meta_box3['code'].'" id="'.$meta_box3['code'].'"';
								if(is_array($set_id2)) {
									foreach($set_id2 as $meta_box4)
										if( $meta_box4 == $meta_box3['code'] ) $setsubi_dat .= ' checked="checked"';
								}
							$setsubi_dat .= '">';
							$setsubi_dat .= '<label for="'.$meta_box3['code'].'">'.$meta_box3['name'].'</label>';
							$setsubi_dat .= '</span>';


						}
						$k++;
					}
				}else{

							$setsubi_dat .= '<span style="display: inline-block">';
							$setsubi_dat .= '<input type="checkbox" name="set2[]"  value="'.$meta_box3['code'].'" id="'.$meta_box3['code'].'"';
								if(is_array($set_id2)) {
									foreach($set_id2 as $meta_box4)
										if( $meta_box4 == $meta_box3['code'] ) $setsubi_dat .= ' checked="checked"';
								}
							$setsubi_dat .= '">';
							$setsubi_dat .= '<label for="'.$meta_box3['code'].'">'.$meta_box3['name'].'</label>';
							$setsubi_dat .= '</span>';


				}

			}
		}
		echo $setsubi_dat;
	}

//$wpdb->print_error();
