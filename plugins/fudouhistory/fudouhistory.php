<?php
/*
Plugin Name: Fudousan Browsing history
Plugin URI: http://nendeb.jp/
Description: Browsing history Plugin for Fudousan Plugin
Version: 5.3.4
Author: nendeb
Author URI: http://nendeb.jp/
License: GPLv2
*/

// Define current version constant
define( 'FUDOU_HISTORY_VERSION', '5.3.4' );

/*  Copyright 2020 nendeb (email : nendeb@gmail.com )

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

*/


if( !defined('WP_CONTENT_URL') )	define('WP_CONTENT_URL', get_option('siteurl').'/wp-content');
if( !defined('WP_CONTENT_DIR') )	define('WP_CONTENT_DIR', ABSPATH.'wp-content');
if( !defined('WP_PLUGIN_URL') )		define('WP_PLUGIN_URL', WP_CONTENT_URL.'/plugins');
if( !defined('WP_PLUGIN_DIR') )		define('WP_PLUGIN_DIR', WP_CONTENT_DIR.'/plugins');

require_once 'fudo-widget5.php';

require_once 'admin_fudouhistory.php';



/**
 *
 * ヘッダーに CSSを 追加
 *
 * @since Fudousan Plugin 1.5.0
 */
function add_header_history_css_fudou() {
	if ( !is_multisite() && !is_admin() ) {
		$plugin_url = plugin_dir_url( __FILE__ );
		wp_enqueue_style("fudouhistory", $plugin_url .'fudouhistory.css' );
	}
}
add_action( 'wp_enqueue_scripts', 'add_header_history_css_fudou', 12 );


/**
 *
 * 不動産プラグインチェック
 *
 * @since Fudousan Plugin 1.7.0
 */
function fudou_active_plugins_check_fudouhistory(){
	global $is_fudouhistory;
	$is_fudouhistory=true;
}
add_action('init', 'fudou_active_plugins_check_fudouhistory');


/**
 *
 * View Version in Footer
 *
 */
function fudou_history_footer_version() {
    echo "<!-- FUDOU HISTORY VERSION " . FUDOU_HISTORY_VERSION . " -->\n";
}
add_filter( 'wp_footer', 'fudou_history_footer_version' );

function database_initialization_fudouhistory() {
	$fudou_version = '';
	if ( defined('FUDOU_VERSION') ){
		$fudou_version = FUDOU_VERSION;
	}	
	if ( version_compare( $fudou_version , '1.5', '<') ) { 
		if( !defined('DB_KEN_TABLE') )		define('DB_KEN_TABLE'	,'area_middle_area');
		if( !defined('DB_SHIKU_TABLE') )	define('DB_SHIKU_TABLE'	,'area_narrow_area');
		if( !defined('DB_ROSENKEN_TABLE') )	define('DB_ROSENKEN_TABLE','train_area_rosen');
		if( !defined('DB_ROSEN_TABLE') )	define('DB_ROSEN_TABLE'	,'train_rosen');
		if( !defined('DB_EKI_TABLE') )		define('DB_EKI_TABLE'	,'train_station');
	} 
}
add_action('plugins_loaded', 'database_initialization_fudouhistory');



/**
 * Asynk History Widget
 * 
 * If you want to change Asynk Widget.
 * @since Fudousan Browsing history 1.8.1
 */
function fudou_asynk_fudouhistory_widget(){
	//Case WP Fastest Cache
	if ( defined('WPFC_MAIN_PATH' ) && !is_user_logged_in() ) {
		add_filter( 'fudou_history_asynk_views', '__return_true' ) ;
	}
}
add_action('init', 'fudou_asynk_fudouhistory_widget');



/**
 * Asynk Single Count
 * 
 * If you want to change Asynk Widget.
 * @since Fudousan Browsing history 1.7.2
 */
function fudou_history_asynk_single_count(){
	global $wp_query;

	//Asynk Single Count
	if ( apply_filters( 'fudou_history_asynk_views', false ) ) {

		$object = $wp_query->get_queried_object();
		$post_id = isset( $_GET['p'] ) ? myIsNum_f( $_GET['p'] ) : '';
		if( empty($post_id) ){
			$post_id = $object->ID;
		}
		if( $post_id ){
			?>
			<script type="text/javascript">
				;
				setTimeout('fudou_history_asynk_single_count()', 2000 ); 
				function fudou_history_asynk_single_count() { 
					var getsite_check_history = '<?php echo plugins_url(); ?>/fudouhistory/json/';
					var postDat_check_history = encodeURI("p=<?php echo $post_id;?>");
					//request = new XMLHttpRequest();
					request_check_history = new createXmlHttpRequest(); 
					request_check_history.open("POST", getsite_check_history + "check_history_bukken.php", true);
					request_check_history.setRequestHeader("Content-Type","application/x-www-form-urlencoded; charset=utf-8");
					request_check_history.send( postDat_check_history );
					request_check_history.onreadystatechange = function() {
					};
				};

			</script>
			<?php
		}
	}
}
add_action( 'single-fudo6', 'fudou_history_asynk_single_count' );


/**
 * Lazy-Loading Images 
 *
 * @since Fudousan Plugin 5.3.4
 */
if ( !function_exists( 'fudou_lazy_loading' )  ) {
	function fudou_lazy_loading() {

		global $fudou_lazy_loading;

		if( false !== apply_filters( 'fudou_lazy_loading_enabled', true ) ){
			$fudou_lazy_loading = 'loading="lazy"';
		}else{
			$fudou_lazy_loading = '';
		}
	}
	add_action( 'init', 'fudou_lazy_loading' );
}



/**
 *
 * 正規表現 URL アドレス の判別
 *
 * @since Fudousan Plugin 1.5.0
 * @param string $value.
 * @return bool $value
 */
if (!function_exists('checkurl_fudou')) {
	function checkurl_fudou( $url ){
		if( preg_match('/^(http|https):\/\/([A-Z0-9][A-Z0-9_-]*(?:\.[A-Z0-9][A-Z0-9_-]*)+):?(\d+)?\/?/i' , $url )){
			return true;
		}
		return false;
	}
}

