=== fudouhistory ===

Contributors: nendeb
Tags: fudousan,csv,bukken,estate,fudou
Requires at least: 4.7
Tested up to: 5.3
Stable tag: 5.3.4

Browsing history Plugin

== Description ==

Browsing history Plugin for Fudousan Plugin

== Installation ==

Installing the plugin:
1. Download the zipped plugin file to your local machine.
2. Unzip the file.
3. Upload the `fudouhistory` folder to the `/wp-content/plugins/` directory.
4. Activate the plugin through the 'Plugins' menu in WordPress.

== Requirements ==

* WordPress 4.7 or later
* PHP 5.6 or later (NOT support PHP 4.x!!)

== Credits ==

This plugin uses  Fudousan Browsing history  Plugin

== Upgrade Notice ==

The required WordPress version has been changed and now requires WordPress 4.7 or higher

== Frequently Asked Questions ==

Use these support channels appropriately.

1. [Docs](http://nendeb.jp/)


= Other Notes =

/**
 * Asynk History Widget
 * If you want to change Asynk Widget.
 */
function fudou_do_asynk_fudouhistory_widget(){
	if ( !is_user_logged_in() ) {
		add_filter( 'fudou_history_asynk_views', '__return_true' ) ;
	}
}
add_action( 'init', 'fudou_do_asynk_fudouhistory_widget' );


== Changelog ==

= 5.3.4 =
* Fixed dat_history_bukken.php
* Fixed fudo-widget5.php

= 5.3.3 =
* Fixed dat_history_bukken.php
* Fixed fudo-widget5.php

= 5.3.0 =
* Fixed fudo-widget5.php

= 5.2.4 =
* Fixed fudo-widget5.php

= 5.2.3 =
* Fixed dat_history_bukken.php
* Fixed fudo-widget5.php

= 5.2.0 =
* Fixed fudouhistory.php
* Fixed fudo-widget5.php
* Fixed dat_history_bukken.php
* Fixed dat_history_kensaku.php
* Add   admin_fudouhistory.php

= 5.1.0 =
* Fixed dat_history_bukken.php

= 5.0.0 =
* Fixed dat_history_bukken.php

= 1.9.2 =
* Fixed dat_history_bukken.php

= 1.9.0 =
* Fixed dat_history_bukken.php

= 1.8.1 =
* Fixed fudo-widget5.php
* Fixed fudouhistory.php

= 1.8.0 =
* Fixed dat_history_bukken.php

= 1.7.14 =
* Fixed fudo-widget5.php

= 1.7.12 =
* Fixed dat_history_bukken.php

= 1.7.8 =
* Fixed fudo-widget5.php

= 1.7.7 =
* Fixed dat_history_bukken.php

= 1.7.6 =
* Fixed dat_history_bukken.php
* Fixed dat_history_kensaku.php
* Fixed fudo-widget5.php

= 1.7.5 =
* Fixed WordPress4.6 beta
* Fixed dat_history_bukken.php
* Fixed Multi Kaiin Support.

= 1.7.4 =
* Fixed dat_history_bukken.php
* Add Multi Kaiin Support.

= 1.7.2 =
* Fixed WordPress4.5
* Support Cache Plugin.

= 1.6.8 =
* Fixed WordPress4.4
* Add add_filter

= 1.6.5 =
* Fixed WordPress4.3

= 1.6.4 =
* Fixed fudo-widget5.php

= 1.6.0 =
* Support Bus-Lines Plugin.

= v1.5.5 =
* Fixed Permalink_structure Check.

= v1.5.3 =
* Fixed Permalinks Check.

= v1.5.0 =
* Fixed WordPress4.0.

= v1.0.3 =
* Fixed WordPress3.9a

= v1.0.2 =
* Fixed WordPress3.8.
* Add Item Views.

= v1.0.1 =
* Fixed WordPress3.7.

= v1.0.0 =
* Initial version of the plugin.
