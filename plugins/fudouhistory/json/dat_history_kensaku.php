<?php
/**
 * Front to the WordPress application. This file doesn't do anything, but loads
 * wp-blog-header.php which does and tells WordPress to load the theme.
 *
 * @package WordPress5.2
 * @subpackage Fudousan Browsing history
 * Version: 5.2.0
 */

/**
 * Tells WordPress to load the WordPress theme and output it.
 *
 * @var bool
 */
if( isset( $_POST['title'] ) ){
if( !defined( 'WP_USE_THEMES') ){
	define('WP_USE_THEMES', false);

	/** Loads the WordPress Environment and Template */
	require_once '../../../../wp-blog-header.php';

	status_header( 200 );
	header("Content-Type: text/plain; charset=utf-8");
	header("X-Content-Type-Options: nosniff");

	$views		= isset( $_POST['views'] ) ? myIsNum_f($_POST['views']) : 4 ;
	$title		= isset( $_POST['title'] ) ? $_POST['title'] : '' ;
	$widget_area	= isset( $_POST['widget_area'] ) ? $_POST['widget_area'] : '' ;
	$before_widget	= isset( $_POST['before_widget'] ) ? $_POST['before_widget'] : '' ;
	$after_widget	= isset( $_POST['after_widget'] )  ? $_POST['after_widget'] : '' ;
	$before_title	= isset( $_POST['before_title'] ) ? $_POST['before_title'] : '' ;
	$after_title	= isset( $_POST['after_title'] )  ? $_POST['after_title'] : '' ;
	$cl_botton	= isset( $_POST['cl_botton'] )    ? $_POST['cl_botton'] : '' ;
	$locale_user	= isset( $_POST['locale_user'] )    ? $_POST['locale_user'] : '' ;
	$token		= isset( $_POST['token'] )    ? $_POST['token'] : '' ;

	//&対策
	$cl_botton = str_replace( "%26", "&", $cl_botton );

	if ( !wp_verify_nonce( $token, 'fudo_jsearch_history_nonce') ){
		exit();
	}

	$before_widget = str_replace( '\\', '', $before_widget );
	$before_title  = str_replace( '\\', '', $before_title );
	$cl_botton     = str_replace( '\\', '', $cl_botton );

	echo dat_history_kensaku( $views, $title, $widget_area, $before_widget, $after_widget, $before_title, $after_title, $cl_botton, $locale_user );
}
}

function dat_history_kensaku( $views, $title, $widget_area, $before_widget, $after_widget, $before_title, $after_title, $cl_botton, $locale_user ){

	global $new_fudou_jsearch;
	$value_dat = '';

	//クッキー取得
	$cookie_fudou_jsearch =	$new_fudou_jsearch;
	if( empty( $cookie_fudou_jsearch ) )
		$cookie_fudou_jsearch = isset($_COOKIE['fudou_jsearch']) ? $_COOKIE['fudou_jsearch'] : '';

	if( !empty( $cookie_fudou_jsearch ) ){
		$cookie_fudou_jsearch = str_replace( "\\" ,"" , $cookie_fudou_jsearch);
		$cookie_fudou_jsearch = maybe_unserialize( $cookie_fudou_jsearch );
	}

	//クッキーがある場合
	if( !empty( $cookie_fudou_jsearch ) ){

		$value_dat .= $before_widget;

		//タイトル
		if ( $title ) {
			$value_dat .= $before_title . $cl_botton  . $title . $after_title; 
		}

		$i=1;
		$value_dat .= '<ul class="cookie_fudou_jsearch">';
		foreach ( $cookie_fudou_jsearch as $id => $meta ) {


			foreach ( $meta as $title => $value ) {

				if( $views < $i ){
					break;
				}
				if( $title == 'url' ){
					$value_dat .= '<li><a href="' . $value . '">';
				}else{
					$value_dat .=  $value . '</a></li>';
				}
			}

				$i++;
		}
		$value_dat .= '</ul>';
		$value_dat .= $after_widget;

	}else{
	//クッキーが無い場合

		//ブロックエディタの場合
		if( $locale_user ){
			$value_dat .= '<div class="components-placeholder">';
			$value_dat .= '<div class="components-placeholder__fieldset">';
			$value_dat .= 'データがありませんでした。';
			$value_dat .= '</div>';
			$value_dat .= '</div>';
		}
	}

	return $value_dat;
}
