<?php
/**
 * Front to the WordPress application. This file doesn't do anything, but loads
 * wp-blog-header.php which does and tells WordPress to load the theme.
 *
 * @package WordPress 5.3
 * @subpackage Fudousan Browsing history
 * Version: 5.3.4
 */

/**
 * Tells WordPress to load the WordPress theme and output it.
 *
 * @var bool
 */

if( isset( $_POST['title'] ) ){
if( !defined( 'WP_USE_THEMES') ){

	define( 'WP_USE_THEMES', false );

	// Loads the WordPress Environment and Template
	require_once '../../../../wp-blog-header.php';

	status_header( 200 );
	header("Content-Type: text/plain; charset=utf-8");
	header("X-Content-Type-Options: nosniff");

	$views		= isset( $_POST['views'] ) ? myIsNum_f($_POST['views']) : 4 ;
	$title		= isset( $_POST['title'] ) ? $_POST['title'] : '' ;
	$widget_area	= isset( $_POST['widget_area'] ) ? $_POST['widget_area'] : '' ;
	$before_widget	= isset( $_POST['before_widget'] ) ? $_POST['before_widget'] : '' ;
	$after_widget	= isset( $_POST['after_widget'] )  ? $_POST['after_widget'] : '' ;
	$before_title	= isset( $_POST['before_title'] ) ? $_POST['before_title'] : '' ;
	$after_title	= isset( $_POST['after_title'] )  ? $_POST['after_title'] : '' ;
	$cl_botton	= isset( $_POST['cl_botton'] )    ? $_POST['cl_botton'] : '' ;
	$locale_user	= isset( $_POST['locale_user'] )  ? $_POST['locale_user'] : '' ;
	$token		= isset( $_POST['token'] )    ? $_POST['token'] : '' ;
	$widget_id	= isset( $_POST['widget_id'] )    ? $_POST['widget_id'] : '' ;

	//&対策
	$cl_botton = str_replace( "%26", "&", $cl_botton );

	if ( !wp_verify_nonce( $token, 'fudo_single_history_nonce') ){
		exit();
	}

	$before_widget = str_replace( '\\', '', $before_widget );
	$before_title  = str_replace( '\\', '', $before_title );
	$cl_botton     = str_replace( '\\', '', $cl_botton );

	echo dat_history_bukken( $views, $title, $widget_area, $before_widget, $after_widget, $before_title, $after_title, $cl_botton, $locale_user, $widget_id );
}
}

function dat_history_bukken( $views, $title, $widget_area, $before_widget, $after_widget, $before_title, $after_title, $cl_botton, $locale_user, $widget_id='' ){

	global $wpdb;
	global $new_fudou_single;
	global $is_fudoubus;
	global $fudou_lazy_loading;

	$value_dat = '';

	//クッキー取得
	$cookie_fudou_single =	$new_fudou_single;
	if( empty( $cookie_fudou_single ) ){
		$cookie_fudou_single = isset($_COOKIE['fudou_single']) ? $_COOKIE['fudou_single'] : '';
	}

	if( !empty( $cookie_fudou_single ) ){
		$cookie_fudou_single = str_replace( "\\" ,"" , $cookie_fudou_single);
		$cookie_fudou_single = maybe_unserialize( $cookie_fudou_single );
	}

	//クッキーがある場合
	if( !empty( $cookie_fudou_single ) ){

		$newup_mark = get_option('newup_mark');
		if($newup_mark == '') $newup_mark=14;


		//ユーザー別会員物件リスト
		$kaiin_users_rains_register = get_option('kaiin_users_rains_register');

		$view1=1;
		$view2=1;
		$view3=1;
		$view4=1;
		$view5=1;
		$view6=1;

		//before_widget
		// $value_dat .= $before_widget;

		//物件詳細リンク 5.0.0
		if ( defined('FUDOU_BUKKEN_TARGET') && FUDOU_BUKKEN_TARGET== 1 ){
			$target_link = ' target="_blank" rel="noopener"';
		}else{
			$target_link = '';
		}


		//タイトル
		if ( $title ) {
			// $value_dat .= $before_title . $cl_botton  . $title . $after_title;
		}

		// $value_dat .= '<ul class="grid-content cookie_fudou_single">';

		$i=1;

		//grid_count css class
		$grid_count = 1;
		$disp_value_dat = "";

		foreach ( $cookie_fudou_single as $id =>$value  ) {

			if( $views < $i ){
				break;
			}

			$rosen_bus = false;	//路線を「バス」に設定している?

			$post_id =  $value;
			$post_id_array = get_post( $post_id );

			/*
			 * VIP 物件詳細ページ非表示 404
			 *
			 * apply_filters( 'fudou_kaiin_vip_single_seigen', $view, $post_id );
			 *
			 * single_fudoXXXX.php
			 * dat_history_bukken.php
			 *
			 * @param bool $view	true:表示  false: 非表示(404)
			 * @subpackage Fudousan Kaiin Vip Plugin Version: 1.0.0
			 *
			 */
			if( apply_filters( 'fudou_kaiin_vip_single_seigen', true, $post_id ) ){

				if( !empty($post_id_array) ){

					if( $post_id_array->post_status == 'publish' ){

						$post_title = $post_id_array->post_title;
						$post_url =  str_replace('&p=','&amp;p=', get_permalink($post_id) );

						//newup_mark
						$post_modified = $post_id_array->post_modified;
						$post_date =    $post_id_array->post_date;
						$post_modified_date =  vsprintf("%d-%02d-%02d", sscanf($post_modified, "%d-%d-%d"));
						$post_date =  vsprintf("%d-%02d-%02d", sscanf($post_date, "%d-%d-%d"));

						$newup_mark_img=  '';
						if( $newup_mark != 0 && is_numeric($newup_mark) ){
							if( ( abs(strtotime($post_modified_date) - strtotime(date("Y/m/d"))) / (60 * 60 * 24) ) < $newup_mark ){
								if($post_modified_date == $post_date ){
									$newup_mark_img = '<div class="new_mark">new</div>';
								}else{
									$newup_mark_img =  '<div class="new_mark">up</div>';
								}
							}
						}

						//会員2
						$kaiin = 0;
						if( !is_user_logged_in() && get_post_meta( $post_id, 'kaiin', true ) > 0 ) $kaiin = 1;
						//ユーザー別会員物件リスト
						$kaiin2 = users_kaiin_bukkenlist( $post_id, get_option( 'kaiin_users_rains_register' ), get_post_meta( $post_id, 'kaiin', true ) );


						$value_dat .= '<li class="box1 grid_count'. $grid_count . '">';

						//grid_count css class
						$grid_count++;
						if( $grid_count > 4 ){
							$grid_count = 1;
						}

						$value_dat .= $newup_mark_img;

						//会員項目表示判定
						if ( !my_custom_kaiin_view('kaiin_gazo',$kaiin,$kaiin2) ){
							$value_dat .= '<img ' . $fudou_lazy_loading . ' class="members_only box1image" src="'.plugins_url().'/fudou/img/kaiin.jpg" alt="" />';
						}else{

							//サムネイル画像
							$fudoimg_data = '';
							$fudoimg1_data = get_post_meta($post_id, 'fudoimg1', true);
							if($fudoimg1_data != '')	$fudoimg_data=$fudoimg1_data;
							$fudoimg2_data = get_post_meta($post_id, 'fudoimg2', true);
							if($fudoimg2_data != '')	$fudoimg_data=$fudoimg2_data;

							/*
							 * fudoimg_data Filter
							 *
							 * Version: 5.2.3
							 **/
							$fudoimg_data = apply_filters( 'pre_fudoimg_data', $fudoimg_data, $post_id, 'fudou_single_history', $widget_id );


							$fudoimg_alt = str_replace("　"," ",$post_title);

							//物件詳細リンク
							$value_dat .= '<a href="' . $post_url . '" ' . $target_link . '>';

							if($fudoimg_data !="" ){

								/*
								 * Add url fudoimg_data Pre
								 *
								 * Version: 1.7.12
								 *
								 **/
								$fudoimg_data = apply_filters( 'pre_fudoimg_data_add_url', $fudoimg_data, $post_id );

								//Check URL
								if ( checkurl_fudou( $fudoimg_data )) {
									$value_dat .= '<img ' . $fudou_lazy_loading . ' class="box1image" src="' . $fudoimg_data . '" alt="' . $fudoimg_alt . '" title="' . $fudoimg_alt . '" />';
								}else{
									//Check attachment
									$sql  = "SELECT P.ID,P.guid";
									$sql .= " FROM $wpdb->posts AS P";
									$sql .= " WHERE P.post_type ='attachment' AND P.guid LIKE '%/$fudoimg_data' ";
								//	$sql = $wpdb->prepare($sql,'');
									$metas = $wpdb->get_row( $sql );
									$attachmentid = '';
									if ( $metas != '' ){
										$attachmentid  =  $metas->ID;
									}

									/*
									 * fudoimg_data to attachmentid
									 *
									 * Version: 1.9.2
									 **/
									$attachmentid = apply_filters( 'fudoimg_data_to_attachmentid', $attachmentid, $fudoimg_data, $post_id );

									if($attachmentid !=''){
										/*
										 * thumbnail_type
										 * thumbnail、medium、large、full
										 *
										 * Version: 5.2.3
										 **/
										$fudo_top_thumbnail_type = apply_filters( 'fudo_top_thumbnail_type', 'thumbnail', 'fudou_single_history', $widget_id );

										$fudoimg_data1 = wp_get_attachment_image_src( $attachmentid, $fudo_top_thumbnail_type );
										$fudoimg_url = $fudoimg_data1[0];

										//light-box v1.7.9 SSL
										$large_guid_url = wp_get_attachment_image_src( $attachmentid, 'large' );
										if( $large_guid_url[0] ){
											$guid_url = $large_guid_url[0];
										}else{
											$guid_url = get_the_guid( $attachmentid );
											if( is_ssl() ){
												$guid_url= str_replace( 'http://', 'https://', $guid_url );
											}
										}

										if($fudoimg_url !=''){
											$value_dat .= '<img ' . $fudou_lazy_loading . ' class="box1image" src="' . $fudoimg_url.'" alt="'.$fudoimg_alt.'" title="'.$fudoimg_alt.'" />';
										}else{
											$value_dat .= '<img ' . $fudou_lazy_loading . ' class="box1image" src="' . $guid_url . '" alt="'.$fudoimg_alt.'" title="'.$fudoimg_alt.'" />';
										}
									}else{

										/*
										 * Add url fudoimg_data
										 *
										 * Version: 1.7.12
										 *
										 **/
										$fudoimg_data = apply_filters( 'fudoimg_data_add_url', $fudoimg_data, $post_id );

										if ( checkurl_fudou( $fudoimg_data )) {
											$value_dat .= '<img ' . $fudou_lazy_loading . ' class="box1image" src="' . $fudoimg_data . '" alt="' . $fudoimg_alt . '" title="' . $fudoimg_alt . '" />';
										}else{
											$value_dat .= '<img ' . $fudou_lazy_loading . ' class="box1image" src="' . plugins_url() . '/fudou/img/nowprinting.jpg" alt="'.$fudoimg_data.'" />';
										}
									}
								}

							}else{
								$value_dat .= '<img ' . $fudou_lazy_loading . ' class="box1image" src="'.plugins_url().'/fudou/img/nowprinting.jpg" alt="" />';
							}
							$value_dat .= '</a>';
						}


						/*
						 * 閲覧履歴 追加項目
						 *
						 * Version: 1.7.12
						 */
						$value_dat .= apply_filters( 'fodou_history_bukken0', '', $post_id, $kaiin, $kaiin2 );

						//タイトル
						if ( my_custom_kaiin_view('kaiin_title',$kaiin,$kaiin2) ){
							if($view1=="1" && $post_title !=''){
									$value_dat .= '<span class="top_title">';
									$value_dat .= str_replace("　"," ",$post_title).'';
									$value_dat .= '</span><br />';
							}
						}


						$value_dat .= apply_filters( 'fodou_history_bukken1', '', $post_id, $kaiin, $kaiin2 );


						//会員2
						if ( !my_custom_kaiin_view( 'kaiin_kakaku',$kaiin,$kaiin2 )
							&& !my_custom_kaiin_view( 'kaiin_madori', $kaiin, $kaiin2 )
							&& !my_custom_kaiin_view( 'kaiin_menseki', $kaiin, $kaiin2 )
							&& !my_custom_kaiin_view( 'kaiin_shozaichi', $kaiin, $kaiin2 )
							&& !my_custom_kaiin_view( 'kaiin_kotsu', $kaiin, $kaiin2) ){
							$value_dat .=  '<span class="top_kaiin">この物件は 会員様限定で公開している物件です</span>';
						}


						$value_dat .= apply_filters( 'fodou_history_bukken2', '', $post_id, $kaiin, $kaiin2 );


						//価格
						if ( my_custom_kaiin_view('kaiin_kakaku',$kaiin,$kaiin2) ){

								if($view2=="1"){

									//ver5.3.3
									$value_dat = apply_filters( 'fodou_history_bukken_view2a_filter', $value_dat, $post_id, $kaiin, $kaiin2 );

									$value_dat .= '<span class="top_price">';
									if( get_post_meta($post_id, 'seiyakubi', true) != "" ){
										$value_dat .= 'ご成約済　';
									}else{
										//非公開の場合
										if(get_post_meta($post_id,'kakakukoukai',true) == "0"){
											$kakakujoutai_data = get_post_meta($post_id,'kakakujoutai',true);
											if($kakakujoutai_data == "1")	$value_dat .= '相談';
											if($kakakujoutai_data == "2")	$value_dat .= '確定';
											if($kakakujoutai_data == "3")	$value_dat .= '入札';

											//価格状態 v1.9.0
											$value_dat = apply_filters( 'fudou_add_kakakujoutai_filter', $value_dat, $kakakujoutai_data, $post_id );
											$value_dat .= '　';

										}else{
											$kakaku_data = get_post_meta($post_id,'kakaku',true);

											if( is_numeric( $kakaku_data ) ){
												if ( function_exists( 'fudou_money_format_ja' ) ) {
													// Money Format 億万円 表示
													$value_dat .=  apply_filters( 'fudou_money_format_ja', $kakaku_data );
												}else{
													$value_dat .= floatval($kakaku_data)/10000;
													$value_dat .= "万円 ";
												}
											}
										}
									}
									$value_dat .= '</span>';

									//ver5.3.3
									$value_dat = apply_filters( 'fodou_history_bukken_view2b_filter', $value_dat, $post_id, $kaiin, $kaiin2 );

								}
						}


						$value_dat .= apply_filters( 'fodou_history_bukken3', '', $post_id, $kaiin, $kaiin2 );


						//間取り・土地面積
						if($view3=="1"){

							//ver5.3.3
							$value_dat = apply_filters( 'fodou_history_bukken_view3a_filter', $value_dat, $post_id, $kaiin, $kaiin2 );

							//間取り
							if ( my_custom_kaiin_view('kaiin_madori',$kaiin,$kaiin2) ){
									$value_dat .= '<span class="top_madori">';
									$madorisyurui_data = get_post_meta($post_id,'madorisyurui',true);
									$value_dat .= get_post_meta($post_id,'madorisu',true);
									if($madorisyurui_data=="10")	$value_dat .= 'R ';
									if($madorisyurui_data=="20")	$value_dat .= 'K ';
									if($madorisyurui_data=="25")	$value_dat .= 'SK ';
									if($madorisyurui_data=="30")	$value_dat .= 'DK ';
									if($madorisyurui_data=="35")	$value_dat .= 'SDK ';
									if($madorisyurui_data=="40")	$value_dat .= 'LK ';
									if($madorisyurui_data=="45")	$value_dat .= 'SLK ';
									if($madorisyurui_data=="50")	$value_dat .= 'LDK ';
									if($madorisyurui_data=="55")	$value_dat .= 'SLDK ';
									$value_dat .= '</span>';
							}

							//ver5.3.3
							$value_dat = apply_filters( 'fodou_history_bukken_view3b_filter', $value_dat, $post_id, $kaiin, $kaiin2 );

							//土地面積
							if ( my_custom_kaiin_view('kaiin_menseki',$kaiin,$kaiin2) ){
									$value_dat .= '<span class="top_menseki">';
									if ( get_post_meta($post_id,'bukkenshubetsu',true) < 1200 ) {
										if( get_post_meta($post_id, 'tochikukaku', true) !="" )
											$value_dat .= ' '.get_post_meta($post_id, 'tochikukaku', true).'m&sup2;';
									}
									$value_dat .= '</span>';
							}

							//ver5.3.3
							$value_dat = apply_filters( 'fodou_history_bukken_view3c_filter', $value_dat, $post_id, $kaiin, $kaiin2 );
						}


						$value_dat .= apply_filters( 'fodou_history_bukken4', '', $post_id, $kaiin, $kaiin2 );


						//所在地
						if ( my_custom_kaiin_view('kaiin_shozaichi',$kaiin,$kaiin2) ){

								if($view4=="1"){

									//ver5.3.3
									$value_dat = apply_filters( 'fodou_history_bukken_view4a_filter', $value_dat, $post_id, $kaiin, $kaiin2 );

									$value_dat .= '<span class="top_shozaichi">';
									$shozaichiken_data = get_post_meta($post_id,'shozaichicode',true);
									$shozaichiken_data = myLeft($shozaichiken_data,2);
									$shozaichicode_data = get_post_meta($post_id,'shozaichicode',true);
									$shozaichicode_data = myLeft($shozaichicode_data,5);
									$shozaichicode_data = myRight($shozaichicode_data,3);

									if($shozaichiken_data !="" && $shozaichicode_data !=""){
										$sql = "SELECT narrow_area_name FROM " . $wpdb->prefix . DB_SHIKU_TABLE . " WHERE middle_area_id=".$shozaichiken_data." and narrow_area_id =".$shozaichicode_data."";

									//	$sql = $wpdb->prepare($sql,'');
										$metas = $wpdb->get_row( $sql );
										if( !empty($metas) ) $value_dat .= "<br />".$metas->narrow_area_name."";
									}
									$value_dat .= get_post_meta($post_id, 'shozaichimeisho', true);
									$value_dat .= '</span>';

									//ver5.3.3
									$value_dat = apply_filters( 'fodou_history_bukken_view4b_filter', $value_dat, $post_id, $kaiin, $kaiin2 );
								}
						}


						$value_dat .= apply_filters( 'fodou_history_bukken5', '', $post_id, $kaiin, $kaiin2 );


						//交通路線
						if ( my_custom_kaiin_view('kaiin_kotsu',$kaiin,$kaiin2) ){

								if($view5=="1"){

									//ver5.3.3
									$value_dat = apply_filters( 'fodou_history_bukken_view5a_filter', $value_dat, $post_id, $kaiin, $kaiin2 );

									$value_dat .= '<span class="top_kotsu">';
									$koutsurosen_data = get_post_meta($post_id, 'koutsurosen1', true);
									$koutsueki_data = get_post_meta($post_id, 'koutsueki1', true);
									$shozaichiken_data = get_post_meta($post_id,'shozaichicode',true);
									$shozaichiken_data = myLeft($shozaichiken_data,2);

									if($koutsurosen_data !=""){
										$sql = "SELECT rosen_name FROM " . $wpdb->prefix . DB_ROSEN_TABLE . " WHERE rosen_id =".$koutsurosen_data."";
									//	$sql = $wpdb->prepare($sql,'');
										$metas = $wpdb->get_row( $sql );
										if( !empty( $metas ) ){
											if( $metas->rosen_name == 'バス' ){
												$rosen_bus = true;
											}
											$value_dat .= "<br />".$metas->rosen_name;
										}
									}

									//交通駅
									if( $koutsurosen_data && $koutsueki_data && !$rosen_bus ){
										$sql = "SELECT DTS.station_name";
										$sql = $sql . " FROM " . $wpdb->prefix . DB_ROSEN_TABLE . " AS DTR";
										$sql = $sql . " INNER JOIN " . $wpdb->prefix . DB_EKI_TABLE . " AS DTS ON DTR.rosen_id = DTS.rosen_id";
										$sql = $sql . " WHERE DTS.station_id=".$koutsueki_data." AND DTS.rosen_id=".$koutsurosen_data."";
									//	$sql = $wpdb->prepare($sql,'');
										$metas = $wpdb->get_row( $sql );
										if( !empty($metas) ){
											if($metas->station_name != '＊＊＊＊') 	$value_dat .= $metas->station_name.'駅';
										}
									}
									$value_dat .= '</span>';

									//ver5.3.3
									$value_dat = apply_filters( 'fodou_history_bukken_view5b_filter', $value_dat, $post_id, $kaiin, $kaiin2 );
								}
						}


						$value_dat .= apply_filters( 'fodou_history_bukken6', '', $post_id, $kaiin, $kaiin2 );


						//交通バス路線
						if ( my_custom_kaiin_view( 'kaiin_kotsu', $kaiin,$kaiin2 ) ){
								if( $view6=="1" &&  $is_fudoubus ){
									if( $rosen_bus ){
										$value_dat .= ' ';
									}else{
										$value_dat .= '<br />';
									}
									$value_dat .= '<span class="top_kotsubus">';
									$value_dat .= apply_filters( 'fudoubus_buscorse_busstop_single', '', $post_id, 1 );
									$value_dat .= '</span>';
								}
						}


						$value_dat .= apply_filters( 'fodou_history_bukken7', '', $post_id, $kaiin, $kaiin2 );


						//$value_dat .= '<br />更新日:';
						//$value_dat .= $post_modified_date;

						$value_dat .= '<div>';

						//会員ロゴ
						if( get_post_meta( $post_id, 'kaiin', true ) == 1 ){
							$kaiin_logo = '<span class="fudo_kaiin_type_logo"><img ' . $fudou_lazy_loading . ' src="' . plugins_url() . '/fudou/img/kaiin_s.jpg" alt="会員物件" /></span>';
							$value_dat .= apply_filters( 'fudou_kaiin_logo_view', $kaiin_logo );
						}
						$value_dat .= apply_filters( 'fudo_kaiin_type_logo_filter', '', $post_id  );	//会員ロゴ


						$value_dat .= apply_filters( 'fodou_history_bukken8', '', $post_id, $kaiin, $kaiin2 );


						$value_dat .= '<span style="float:right;" class="box1low"><a href="' . $post_url . '" ' . $target_link . '>→物件詳細</a></span>';
						$value_dat .= '</div>';


						$value_dat .= apply_filters( 'fodou_history_bukken9', '', $post_id, $kaiin, $kaiin2 );


						$value_dat .= '</li>';

						//賃料
						if(get_field('chinryos', $post_id)){
							//the_field('chinryos', $post_id);
						}else{
							$price = get_kakaku_list_for_fudo($post_id);
						}

						$link = get_permalink($post_id);

						$i++;

// if(is_page('mylist')){



// }elseif(is_page('myhistory')){

// 	ob_start();
// 	$bukken_id = $post_id;
// 	include( get_parent_theme_file_path('template/bukken-for-myhistory.php') ) ;
// 	$value_dat = ob_get_clean();

// }else{//シンプル表示
$img_url = '';
if (has_post_thumbnail($post_id)) {
    $thumbnail_id = get_post_thumbnail_id($post_id);
    $eye_image_array = wp_get_attachment_image_src( $thumbnail_id , 'medium' );
    $img_url = $eye_image_array[0];
} else {
    $rtn_arr = stw_get_singlefudo_images($post_id,2);
    if ( 0 < count($rtn_arr) ){
        $img_url = stw_generate_imgurl($rtn_arr[0]->image_path);
    } else {
        $img_url = "https://koukyu-chintai.com/wp-content/uploads/cropped-cropped-main-1-2000x1200-1-1024x614.jpg";
    }
}
$value_dat=<<<EOF

        <article class="l-contRecent_room o-roomItem">
            <div class="o-roomWrap">
                <div class="o-roomThumb"><img src="{$img_url}" alt="" class="o-roomThumb_img"></div>
            </div>
            <div class="o-roomHead">
                <h3 class="o-roomTitle o-title __min __nobold">{$post_title}</h3>
                <p class="o-roomPrice o-text __en __min __bold">{$price}</p>
            </div>
            <a href="{$link}" class="o-link __over"></a>
        </article>

EOF;
// }

$disp_value_dat .= $value_dat;


					} //publish

				} //!empty($post_id_array

			} //fudou_kaiin_vip_single_seigen

		} //loop
		// $value_dat .= '</ul>';

		// $value_dat .= $after_widget;

	}else{
	//クッキーが無い場合

		//ブロックエディタの場合
		if( $locale_user ){
			$value_dat .= '<div class="components-placeholder">';
			$value_dat .= '<div class="components-placeholder__fieldset">';
			$value_dat .= 'データがありませんでした。';
			$value_dat .= '</div>';
			$value_dat .= '</div>';
		}

	}	//!empty( $cookie_fudou_single

if(is_page('mylist') ||  is_page('myhistory')){

}else{
$value_dat_head=<<<EOF
EOF;

$value_dat_foot=<<<EOF
EOF;
}

	return $disp_value_dat;
	// return $value_dat_head . $disp_value_dat . $value_dat_foot;
}

