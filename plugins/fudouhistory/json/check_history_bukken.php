<?php
/**
 * Front to the WordPress application. This file doesn't do anything, but loads
 * wp-blog-header.php which does and tells WordPress to load the theme.
 *
 * @package WordPress4.5
 * @subpackage Fudousan Browsing history
 * Version: 1.7.2
 */

/**
 * Tells WordPress to load the WordPress theme and output it.
 *
 * @var bool
 */
if( !defined( 'WP_USE_THEMES') ){
	define( 'WP_USE_THEMES', false );
}

/** Loads the WordPress Environment and Template */
require_once '../../../../wp-blog-header.php';

status_header( 200 );
header("Content-Type: text/plain; charset=utf-8");

$post_id = isset( $_POST['p'] ) ? myIsNum_f( $_POST['p'] ) : '';
//$post_id = isset( $_GET['p'] ) ? myIsNum_f( $_GET['p'] ) : '';

fudou_add_jsearch_single_history_2( $post_id );


function fudou_add_jsearch_single_history_2( $post_id ){

	global $new_fudou_single;

	//クッキー用ドメイン・フォルダ
	$str = home_url();

	//フォルダ
	$folder = '';
	//ドメイン名
	$domain_name ='';

	$str = str_replace( "https://" , "" , $str );
	$str = str_replace( "http://" , "" , $str );

	//ドメイン直後の / 位置
	$pos = mb_strpos( $str , '/' );
	if( $pos ){
		//ドメイン名
		$domain_name = myLeft( $str, $pos );

		//全体の文字数
		$all = mb_strlen( $str, "utf-8");
		//ドメイン以下のフォルダ
		$folder = myRight( $str, $all - $pos );
		$folder = $folder . '/';
	}

	//物件閲覧履歴
	if( $post_id ){

			$single_history_days = get_option('single_history_days');
			if( empty( $single_history_days ) ) $single_history_days = 30;

			$single_history_views = get_option('single_history_views');
			if( empty( $single_history_views ) ) $single_history_views = 10;


			//クッキー読み込み
			$fudou_single = isset($_COOKIE['fudou_single']) ? $_COOKIE['fudou_single'] : '';

			if(!empty($fudou_single)){
				$fudou_single = str_replace( "\\" ,"" , $fudou_single);
				$fudou_single = maybe_unserialize( $fudou_single );
			}else{
				$fudou_single = array();
			}

			array_unshift($fudou_single, $post_id);
			$fudou_single = array_unique($fudou_single);

			//ログ最大値
			if(!empty($fudou_single)) {
				$new_fudou_single = array();
				$i=1;
				foreach( $fudou_single as $name => $value ) {
					$new_fudou_single[] = $value;
					if( $i >= $single_history_views ) break;
					$i++;
				}
			}

			//クッキー用ドメイン・フォルダ
			if( $folder ){
				setcookie( 'fudou_single' , maybe_serialize( $new_fudou_single ) , time()+60*60*24*$single_history_days , $folder  );
			}else{
				setcookie( 'fudou_single' , maybe_serialize( $new_fudou_single ) , time()+60*60*24*$single_history_days , '/' );
			}

		echo '';
	}
}

