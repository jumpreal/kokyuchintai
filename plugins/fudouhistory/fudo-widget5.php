<?php
/*
 * 不動産プラグインウィジェット
 * @package WordPress5.3
 * @subpackage Fudousan Browsing history
 * Version: 5.3.4
 */

//物件閲覧履歴ウィジェット
function fudou_widgetInit_single_history() {
	if( defined( 'FUDOU_VERSION' ) ){
		register_widget('fudou_widget_single_history');
	}
}
add_action('widgets_init', 'fudou_widgetInit_single_history');


//条件検索閲覧履歴
function fudou_widgetInit_jsearch_history() {
	if( defined( 'FUDOU_VERSION' ) ){
		register_widget('fudou_widget_jsearch_history');
	}
}
add_action('widgets_init', 'fudou_widgetInit_jsearch_history');




/*
 * 物件閲覧履歴ウィジェット
 *
 * @package WordPress5.3
 * @subpackage Fudousan Browsing history
 * Version: 5.3.4
*/
class fudou_widget_single_history extends WP_Widget {

	/**
	 * Register widget with WordPress 4.3.
	 */
	function __construct() {
		parent::__construct(
			'fudou_single_history', 	// Base ID
			'閲覧履歴(物件)' ,		// Name
			array( 'description' => '物件詳細の閲覧履歴を表示します。', )	// Args
		);

		//テーマカスタマイザー option に登録
		add_filter( 'customize_save_after', array( $this, 'customize_save_after_widget_fudou_single_history' ) );
	}

	/** option */
	function fudo_widget_option($name,$value) {

		$stored_value = get_option($name);
		if ($stored_value === false) {
			add_option($name, $value);
		} else {
			update_option($name, $value);
		}
		wp_cache_set( $name, $value, 'options' );
	}


	//@see WP_Widget::form
	function form($instance) {

		//走った回数
		global $single_history_count;
		$single_history_count++;

		$title = isset( $instance['title'] ) ? esc_attr( $instance['title'] ) : '';
		$views = isset( $instance['views'] ) ? myIsNum_f( $instance['views'] ) : 4;

	?>
		<p><label for="<?php echo $this->get_field_id('title'); ?>">
		タイトル <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo $title; ?>" /></label></p>
		<p><label for="<?php echo $this->get_field_id('views'); ?>">
		表示数         <input class="widefat" id="<?php echo $this->get_field_id('views'); ?>" name="<?php echo $this->get_field_name('views'); ?>" type="text" value="<?php echo $views; ?>" /></label></p>
	<?php
	}

	// @see WP_Widget::update
	function update($new_instance, $old_instance) {
		return $new_instance;
	}

	// @see WP_Widget::widget
	function widget($args, $instance) {

		global $wpdb;
		global $new_fudou_single;
		global $fudou_lazy_loading;

		//for block
		if( !isset( $args['widget_id'] ) ){
			$args['widget_id'] = 'history_bukken_' . mt_rand();
		}
		//id が無い場合追加する (id=fudou_single_history-X)
		if( false === strpos( $args['before_widget'], 'id=' ) ){
			$args['before_widget'] = str_replace( 'class=' , 'id="' . $args['widget_id'] . '" class=' , $args['before_widget'] );
		}
		//ブロックエディタ判別
		$locale_user = isset( $_GET['_locale'] ) ? $_GET['_locale'] : '';


		$title = isset($instance['title']) ? apply_filters('widget_title', $instance['title']) : '';
		$history_widget_id = str_replace( '-' , '_' , $args['widget_id'] );

		$views = isset( $instance['views'] ) ? $instance['views'] : 4 ;
		$hstb  = isset($_GET['hstb'])        ? myIsNum_f($_GET['hstb']) : 0 ;


		$widget_id = $this->str_cut_r( $args['widget_id'], '-' );

		//for block
		if( !isset( $args['id'] ) ){
			$args['id'] = 'block';
		}
		$widget_area = $args['id'];	//設置場所 'top_widgets'


		if( $hstb != 1 ){

			//ユーザー別会員物件リスト
			$kaiin_users_rains_register = get_option('kaiin_users_rains_register');

			//履歴クリア
			if(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on'){
				$history_url = esc_url( "https://" . $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"] );
			} else {
				$history_url = esc_url( "http://" . $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"] );
			}

			//パーマリンクチェック
			$permalink_structure = get_option('permalink_structure');
			if ( $permalink_structure != '' ) {

				if ( is_front_page() ){
					$cl_botton = '<span class="cl_botton"><a href="?hstb=1" rel="nofollow"><img ' . $fudou_lazy_loading . ' src="'.plugins_url().'/fudouhistory/clearbn1.png" alt="履歴クリア" title="履歴クリア" /></a></span>';
				}else{

					$pos = strpos( $history_url , '?');
					if ($pos === false) {
						$cl_botton = '<span class="cl_botton"><a href="'.$history_url.'?hstb=1" rel="nofollow"><img ' . $fudou_lazy_loading . ' src="'.plugins_url().'/fudouhistory/clearbn1.png" alt="履歴クリア" title="履歴クリア" /></a></span>';
					}else{
						$cl_botton = '<span class="cl_botton"><a href="'.$history_url.'&hstb=1" rel="nofollow"><img ' . $fudou_lazy_loading . ' src="'.plugins_url().'/fudouhistory/clearbn1.png" alt="履歴クリア" title="履歴クリア" /></a></span>';
					}
				}
			}else{
				//デフォルト
				if ( is_front_page() && !isset( $_GET['page'] ) ){
					$cl_botton = '<span class="cl_botton"><a href="?hstb=1" rel="nofollow"><img ' . $fudou_lazy_loading . ' src="'.plugins_url().'/fudouhistory/clearbn1.png" alt="履歴クリア" title="履歴クリア" /></a></span>';
				}else{
					$cl_botton = '<span class="cl_botton"><a href="'.$history_url.'&hstb=1" rel="nofollow"><img ' . $fudou_lazy_loading . ' src="'.plugins_url().'/fudouhistory/clearbn1.png" alt="履歴クリア" title="履歴クリア" /></a></span>';
				}
			}

			//&対策
			$cl_botton = str_replace( "&", "%26", $cl_botton );

			/**
			 * Asynk History Widget
			 *
			 * If you want to change Asynk Widget.
			 * @since Fudousan Browsing history 1.7.2
			 * Add $locale_user v5.2.0
			 */
			if( apply_filters( 'fudou_history_asynk_views', false ) && !$locale_user ){

				//ウィジェット数
				global $wp_registered_widgets;
				$i = 0;
				$widget_no = 1;
				foreach( $wp_registered_widgets as $wp_registered_widget){

					$pos = strpos( $wp_registered_widget['id'], 'fudou_single_history' );
					if( $pos !== false ){
						$i++;
						if( $wp_registered_widget['id']  == $args['widget_id'] ){
							$widget_no = $i;
						}
					}
				}

				//非同期
				echo '<div id="history'. $history_widget_id .'_cb" class="' . $widget_area . ' widget_fudou_single_history_cb"></div>';

				$history_delay = $widget_no * 300 + 2000;
				?>
				<script type="text/javascript">

					;setTimeout( 'history_bukken<?php echo $history_widget_id; ?>()', <?php echo $history_delay; ?> );
					function history_bukken<?php echo $history_widget_id; ?>() {

						var getsitehistry = '<?php echo plugins_url(); ?>/fudouhistory/json/';
						var views_<?php echo $history_widget_id;?>  = '<?php echo $views;?>';
						var title_<?php echo $history_widget_id;?>  = '<?php echo $title;?>';
						var widget_area_<?php echo $history_widget_id;?>  = '<?php echo $widget_area;?>';
						var before_widget_<?php echo $history_widget_id;?>  = '<?php echo $args["before_widget"];?>';
						var after_widget_<?php echo $history_widget_id;?>   = '<?php echo $args["after_widget"];?>';
						var before_title_<?php echo $history_widget_id;?>   = '<?php echo $args["before_title"];?>';
						var after_title_<?php echo $history_widget_id;?>    = '<?php echo $args["after_title"];?>';
						var cl_botton_<?php echo $history_widget_id;?>      = '<?php echo $cl_botton;?>';
						var token_<?php echo $history_widget_id;?>          = '<?php echo wp_create_nonce( 'fudo_single_history_nonce' );?>';
						var widget_id_<?php echo $history_widget_id;?>      = '<?php echo $history_widget_id;?>';


						var postDat_<?php echo $history_widget_id;?> = encodeURI( "&views=" + views_<?php echo $history_widget_id;?> )
							+ encodeURI( "&title=" + title_<?php echo $history_widget_id;?> )
							+ encodeURI( "&widget_area="   + widget_area_<?php echo $history_widget_id;?> )
							+ encodeURI( "&before_widget=" + before_widget_<?php echo $history_widget_id;?> )
							+ encodeURI( "&after_widget="  + after_widget_<?php echo $history_widget_id;?> )
							+ encodeURI( "&before_title="  + before_title_<?php echo $history_widget_id;?> )
							+ encodeURI( "&after_title="   + after_title_<?php echo $history_widget_id;?> )
							+ encodeURI( "&cl_botton="     + cl_botton_<?php echo $history_widget_id;?> )
							+ encodeURI( "&locale_user="   + "<?php echo $locale_user;?>" )
							+ encodeURI( "&token="         + token_<?php echo $history_widget_id;?> )
							+ encodeURI( "&widget_id="     + widget_id_<?php echo $history_widget_id;?> );

						//request<?php echo $widget_id;?> = new XMLHttpRequest();
						request<?php echo $widget_id;?> = new createXmlHttpRequest();
						request<?php echo $widget_id;?>.open("POST", getsitehistry +"dat_history_bukken.php", true);
						request<?php echo $widget_id;?>.setRequestHeader("Content-Type","application/x-www-form-urlencoded; charset=utf-8");
						request<?php echo $widget_id;?>.send( postDat_<?php echo $history_widget_id;?> );
						request<?php echo $widget_id;?>.onreadystatechange = function() {
							if (request<?php echo $widget_id;?>.readyState == 4 && request<?php echo $widget_id;?>.status == 200) {
								var jsDat<?php echo $widget_id;?> = request<?php echo $widget_id;?>.responseText;
								if(jsDat<?php echo $widget_id;?> !=''){
									document.getElementById('history<?php echo $history_widget_id;?>_cb').innerHTML = jsDat<?php echo $widget_id;?>;
									topbukken<?php echo $history_widget_id; ?>();
								}else{
									document.getElementById('history<?php echo $history_widget_id;?>_cb').innerHTML = '';
								};
							};
						};
					};

					function topbukken<?php echo $history_widget_id; ?>() {

					<?php
					/*
					 * use_matchHeight
					 * ver 5.3.3
					 */
					if( apply_filters( 'fudo_use_matchHeight', true ) ){ ?>
						<?php if(  $views > 1  ){ ?>
							jQuery(function() {
							    jQuery('#<?php echo $args['widget_id'];?> li').matchHeight();
							});
						<?php } ?>
					<?php } ?>

						<?php
						/**
						 * add after js
						 *
						 * @since UNPC 1.8.1
						 */
						do_action( 'fudou_history_bukken_after_js' )
						?>

					};
				</script>
				<?php

			}else{

				require_once 'json/dat_history_bukken.php';

				//&対策
				$cl_botton = str_replace( "%26", "&", $cl_botton );

				echo dat_history_bukken( $views, $title, $widget_area, $args['before_widget'], $args['after_widget'], $args['before_title'], $args['after_title'], $cl_botton, $locale_user, $history_widget_id );

				?>
				<script type="text/javascript">
					<?php if( apply_filters( 'fudou_imagesloaded_use', true ) ){  ?>
						imagesLoaded( '#box<?php echo $args['widget_id']; ?>', function(){
							setTimeout('topbukken<?php echo $history_widget_id; ?>()', 1000);
						});
					<?php }else{ ?>
						setTimeout('topbukken<?php echo $history_widget_id; ?>()', 2000);
					<?php } ?>

					function topbukken<?php echo $history_widget_id; ?>() {
					<?php
					/*
					 * use_matchHeight
					 * ver 5.3.3
					 */
					if( apply_filters( 'fudo_use_matchHeight', true ) ){ ?>
						<?php if(  $views > 1  ){ ?>
							jQuery(function() {
							    jQuery('#<?php echo $args['widget_id'];?> li').matchHeight();
							});
						<?php } ?>
					<?php } ?>
					};
				</script>
				<?php

			}


		} // $hstb
	}

	function str_cut_r( $str, $options ) {
		//$all = mb_strlen( $str, "utf-8");
		$all = 0;
		$pos = mb_strpos($str , $options);
		if($pos !== false )
		$str= myRight($str,$all - $pos -1 );
	        return $str;
	}

}	//class fudou_widget_single_history


















/*
 * 条件検索閲覧履歴ウィジェット
 *
 * @package WordPress5.3
 * @subpackage Fudousan Browsing history
 * Version: 5.3.4
 *
*/
class fudou_widget_jsearch_history extends WP_Widget {

	/**
	 * Register widget with WordPress 4.3.
	 */
	function __construct() {
		parent::__construct(
			'fudou_jsearch_history', 		// Base ID
			'検索履歴(条件検索)' ,			// Name
			array( 'description' => '検索履歴を表示します。', )	// Args
		);

		//テーマカスタマイザー option に登録
		add_filter( 'customize_save_after', array( $this, 'customize_save_after_widget_fudou_jsearch_history' ) );
	}


	/** option */
	function fudo_widget_option($name,$value) {
		$stored_value = get_option($name);
		if ($stored_value === false) {
			add_option($name, $value);
		} else {
			update_option($name, $value);
		}
		wp_cache_set( $name, $value, 'options' );
	}


	//@see WP_Widget::form
	function form($instance) {

		//走った回数
		global $jsearch_history_count;
		$jsearch_history_count++;

		$title = isset($instance['title']) ? esc_attr($instance['title']) : '';
		$views = isset( $instance['views'] ) ? myIsNum_f( $instance['views'] ) : 4;

	?>
		<p><label for="<?php echo $this->get_field_id('title'); ?>">
		タイトル <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo $title; ?>" /></label></p>
		<p><label for="<?php echo $this->get_field_id('views'); ?>">
		表示数         <input class="widefat" id="<?php echo $this->get_field_id('views'); ?>" name="<?php echo $this->get_field_name('views'); ?>" type="text" value="<?php echo $views; ?>" /></label></p>

	<?php
	}

	// @see WP_Widget::update
	function update($new_instance, $old_instance) {
		return $new_instance;
	}

	// @see WP_Widget::widget
	function widget($args, $instance) {

		global $new_fudou_jsearch;
		global $fudou_lazy_loading;

		//for block
		if( !isset( $args['widget_id'] ) ){
			$args['widget_id'] = 'jsearch_history_' . mt_rand();
		}
		//ブロックエディタ判別
		$locale_user = isset( $_GET['_locale'] ) ? $_GET['_locale'] : '';

		$title = isset($instance['title']) ? apply_filters('widget_title', $instance['title']) : '';
		$history_widget_id = str_replace( '-' , '_' ,$args['widget_id']);

		$views = isset( $instance['views'] ) ? $instance['views'] : 4 ;

		$hstj = isset($_GET['hstj']) ? myIsNum_f($_GET['hstj']) : 0 ;

		$widget_id = $this->str_cut_r( $args['widget_id'], '-' );

		//for block
		if( !isset( $args['id'] ) ){
			$args['id'] = 'block';
		}
		$widget_area = $args['id'];	//設置場所


		if( $hstj != 1 ){

			//履歴クリア
			if(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on'){
				$history_url = esc_url( "https://" . $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"] );
			} else {
				$history_url = esc_url( "http://" . $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"] );
			}

			//パーマリンクチェック
			$permalink_structure = get_option('permalink_structure');
			if ( $permalink_structure != '' ) {

				if ( is_front_page() ){
					$cl_botton = '<span class="cl_botton2"><a href="?hstj=1" rel="nofollow"><img ' . $fudou_lazy_loading . ' src="'.plugins_url().'/fudouhistory/clearbn1.png" alt="履歴クリア" title="履歴クリア" /></a></span>';
				}else{

					$pos = strpos( $history_url , '?');
					if ($pos === false) {
						$cl_botton = '<span class="cl_botton2"><a href="'.$history_url.'?hstj=1" rel="nofollow"><img ' . $fudou_lazy_loading . ' src="'.plugins_url().'/fudouhistory/clearbn1.png" alt="履歴クリア" title="履歴クリア" /></a></span>';
					}else{
						$cl_botton = '<span class="cl_botton2"><a href="'.$history_url.'&hstj=1" rel="nofollow"><img ' . $fudou_lazy_loading . ' src="'.plugins_url().'/fudouhistory/clearbn1.png" alt="履歴クリア" title="履歴クリア" /></a></span>';
					}
				}
			}else{
				//デフォルト
				if ( is_front_page() && !isset( $_GET['page'] ) ){
					$cl_botton = '<span class="cl_botton2"><a href="?hstj=1" rel="nofollow"><img ' . $fudou_lazy_loading . ' src="'.plugins_url().'/fudouhistory/clearbn1.png" alt="履歴クリア" title="履歴クリア" /></a></span>';
				}else{
					$cl_botton = '<span class="cl_botton2"><a href="'.$history_url.'&hstj=1" rel="nofollow"><img ' . $fudou_lazy_loading . ' src="'.plugins_url().'/fudouhistory/clearbn1.png" alt="履歴クリア" title="履歴クリア" /></a></span>';
				}
			}

			//&対策
			$cl_botton = str_replace( "&", "%26", $cl_botton );


			/**
			 * Asynk History Widget
			 *
			 * If you want to change Asynk Widget.
			 * @since Fudousan Browsing history 1.7.2
			 * Add $locale_user v5.2.0
			 */
			if( apply_filters( 'fudou_history_asynk_views', false ) && !$locale_user ){

				//ウィジェット数
				global $wp_registered_widgets;
				$i = 0;
				$widget_no = 1;
				foreach( $wp_registered_widgets as $wp_registered_widget){

					$pos = strpos( $wp_registered_widget['id'], 'fudou_jsearch_history' );
					if( $pos !== false ){
						$i++;
						if( $wp_registered_widget['id']  == $args['widget_id'] ){
							$widget_no = $i;
						}
					}
				}

				//非同期
				echo '<div id="history'. $history_widget_id .'_cb" class="widget_fudo_jsearch_history_cb"></div>';

				$history_delay = $widget_no * 100 + 1000;

				?>
				<script type="text/javascript">

					;setTimeout('history_jsearch<?php echo $history_widget_id; ?>()', <?php echo $history_delay; ?> );
					function history_jsearch<?php echo $history_widget_id; ?>() {

						var getsitehistry = '<?php echo plugins_url(); ?>/fudouhistory/json/';
						var views_<?php echo $history_widget_id;?>  = <?php echo $views;?>;
						var title_<?php echo $history_widget_id;?>  = '<?php echo $title;?>';
						var widget_area_<?php echo $history_widget_id;?>   = '<?php echo $widget_area;?>';
						var before_widget_<?php echo $history_widget_id;?> = '<?php echo $args["before_widget"];?>';
						var after_widget_<?php echo $history_widget_id;?>  = '<?php echo $args["after_widget"];?>';
						var before_title_<?php echo $history_widget_id;?>  = '<?php echo $args["before_title"];?>';
						var after_title_<?php echo $history_widget_id;?>   = '<?php echo $args["after_title"];?>';
						var cl_botton_<?php echo $history_widget_id;?>     = '<?php echo $cl_botton;?>';
						var token_<?php echo $history_widget_id;?>         = '<?php echo wp_create_nonce( 'fudo_jsearch_history_nonce' );?>';


						var postDat_<?php echo $history_widget_id;?> = encodeURI( "&views=" + views_<?php echo $history_widget_id;?> )
							+ encodeURI( "&title=" + title_<?php echo $history_widget_id;?> )
							+ encodeURI( "&widget_area=" + widget_area_<?php echo $history_widget_id;?> )
							+ encodeURI( "&before_widget=" + before_widget_<?php echo $history_widget_id;?> )
							+ encodeURI( "&after_widget="  + after_widget_<?php echo $history_widget_id;?> )
							+ encodeURI( "&before_title="  + before_title_<?php echo $history_widget_id;?> )
							+ encodeURI( "&after_title="   + after_title_<?php echo $history_widget_id;?> )
							+ encodeURI( "&cl_botton="     + cl_botton_<?php echo $history_widget_id;?> )
							+ encodeURI( "&locale_user="   + "<?php echo $locale_user;?>" )
							+ encodeURI( "&token="         + token_<?php echo $history_widget_id;?> );

						//request<?php echo $widget_id;?> = new XMLHttpRequest();
						request<?php echo $widget_id;?> = new createXmlHttpRequest();
						request<?php echo $widget_id;?>.open("POST", getsitehistry +"dat_history_kensaku.php", true);
						request<?php echo $widget_id;?>.setRequestHeader("Content-Type","application/x-www-form-urlencoded; charset=utf-8");
						request<?php echo $widget_id;?>.send( postDat_<?php echo $history_widget_id;?> );
						request<?php echo $widget_id;?>.onreadystatechange = function() {
							if (request<?php echo $widget_id;?>.readyState == 4 && request<?php echo $widget_id;?>.status == 200) {
								var jsDat<?php echo $widget_id;?> = request<?php echo $widget_id;?>.responseText;
								if(jsDat<?php echo $widget_id;?> !=''){
									document.getElementById('history<?php echo $history_widget_id;?>_cb').innerHTML = jsDat<?php echo $widget_id;?>;

									<?php
									/**
									 * add after js
									 *
									 * @since UNPC 1.8.1
									 */
									do_action( 'fudou_history_bukken_after_js' )
									?>

								}else{
									document.getElementById('history<?php echo $history_widget_id;?>_cb').innerHTML = '';
								};
							};
						};
					};
				</script>
				<?php
			}else{

				require_once 'json/dat_history_kensaku.php';

				//&対策
				$cl_botton = str_replace( "%26", "&", $cl_botton );

				echo dat_history_kensaku( $views, $title, $widget_area, $args['before_widget'], $args['after_widget'], $args['before_title'], $args['after_title'], $cl_botton, $locale_user );
			}
		}

	}

	function str_cut_r( $str, $options ) {
		//$all = mb_strlen( $str, "utf-8");
		$all = 0;
		$pos = mb_strpos($str , $options);
		if($pos !== false )
		$str= myRight($str,$all - $pos -1 );
	        return $str;
	}

}	//class fudou_widget_jsearch_history








/*
 * クッキー閲覧履歴 物件閲覧履歴/条件検索閲覧履歴
 *
 * @package WordPress4.8
 * @subpackage Fudousan Browsing history
 * Version: 1.8.0
 *
*/
function fudou_add_jsearch_single_history( $template = '' ){

	global $wp_query;
	global $new_fudou_single;
	global $new_fudou_jsearch;


	//クッキー用ドメイン・フォルダ
	$str = home_url();

	//フォルダ
	$folder = '';
	//ドメイン名
	$domain_name ='';

	$str = str_replace( "https://" , "" , $str );
	$str = str_replace( "http://" , "" , $str );

	//ドメイン直後の / 位置
	$pos = mb_strpos( $str , '/' );
	if( $pos ){
		//ドメイン名
		$domain_name = myLeft( $str, $pos );

		//全体の文字数
		$all = mb_strlen( $str, "utf-8");
		//ドメイン以下のフォルダ
		$folder = myRight( $str, $all - $pos );
		$folder = $folder . '/';
	}

	//キャッシュ利用する?
	$asynk_single_count =  apply_filters( 'fudou_history_asynk_views', false ) ;

	if ( !$asynk_single_count ) {

		//物件閲覧履歴
		$object = $wp_query->get_queried_object();
		if( !empty( $object->post_type ) ){

			if( $object->post_type == 'fudo' ){

				$post_id = isset( $_GET['p'] ) ? myIsNum_f( $_GET['p'] ) : '';
				if( empty($post_id) ){
					$post_id = $object->ID;
				}

				$single_history_days = get_option('single_history_days');
				if( empty( $single_history_days ) ) $single_history_days = 30;

				$single_history_views = get_option('single_history_views');
				if( empty( $single_history_views ) ) $single_history_views = 10;


				//クッキー読み込み
				$fudou_single = isset($_COOKIE['fudou_single']) ? $_COOKIE['fudou_single'] : '';

				if(!empty($fudou_single)){
					$fudou_single = str_replace( "\\" ,"" , $fudou_single);
					$fudou_single = maybe_unserialize( $fudou_single );
				}else{
					$fudou_single = array();
				}

				array_unshift($fudou_single, $post_id);
				$fudou_single = array_unique($fudou_single);

				//ログ最大値
				if(!empty($fudou_single)) {
					$new_fudou_single = array();
					$i=1;
					foreach( $fudou_single as $name => $value ) {
						$new_fudou_single[] = $value;
						if( $i >= $single_history_views ) break;
						$i++;
					}
				}

				//クッキー用ドメイン・フォルダ
				if( $folder ){
					setcookie( 'fudou_single' , maybe_serialize( $new_fudou_single ) , time()+60*60*24*$single_history_days , $folder  );
				}else{
					setcookie( 'fudou_single' , maybe_serialize( $new_fudou_single ) , time()+60*60*24*$single_history_days , '/' );
				}

			}
		}
	}







	//条件検索閲覧履歴
	if ( isset( $_GET['bukken'] ) &&  $_GET['bukken'] == 'jsearch' ) {
		if ( !isset( $_GET['paged'] ) || (  isset( $_GET['paged'] ) && $_GET['paged'] == '1' ) ) {

		//タイトル生成

			global $wpdb;
			global $work_setsubi2;


			//路線・県ID
				$mid_id = isset($_GET['mid']) ? myIsNum_f($_GET['mid']) : '';
				if($mid_id=='')	$mid_id = "99999";

			//駅・市区ID
				$nor_id = isset($_GET['nor']) ? myIsNum_f($_GET['nor']) : '';
				if($nor_id=='')	$nor_id = "99999";

			//カテゴリ
				$bukken = isset($_GET['bukken']) ? $_GET['bukken'] : '';
				$bukken_slug_data = esc_attr( stripslashes($bukken));
				$taxonomy_name = '';
				if( $bukken != 'ken' && $bukken != 'shiku' && $bukken != 'rosen' && $bukken != 'station' && $bukken != 'jsearch' && $bukken != 'search')
					$taxonomy_name = 'bukken';

			//インカテゴリ
				$cat_data = '';
				$cat_id = isset($_GET['ct']) ? myIsNum_f($_GET['ct']) : '';
				if ( is_array( $cat_id ) ) {
					$i=0;
					$cat_data = ' IN ( 0 ';
					foreach( $cat_id as $meta_set ){
						if( (int) $cat_id[$i] ){
							$cat_data .= ','. (int) $cat_id[$i] . '';
						}
						$i++;
					}
					$cat_data .= ') ';
				} else {
					if( $cat_id ){
						$cat_data = ' = ' . (int) $cat_id;
					}
				}

			//投稿タグ
				if($bukken_slug_data == ''){
					$bukken_tag = isset($_GET['bukken_tag']) ? $_GET['bukken_tag'] : '';
					$bukken_slug_data = esc_attr( stripslashes($bukken_tag));
					$taxonomy_name = 'bukken_tag';
				}

				$bukken_slug_data = str_replace(" ","",$bukken_slug_data);
				$bukken_slug_data = str_replace(";","",$bukken_slug_data);
				$bukken_slug_data = str_replace(",","",$bukken_slug_data);
				$bukken_slug_data = str_replace("'","",$bukken_slug_data);

				$slug_data = utf8_uri_encode($bukken_slug_data,0);			//エンコード


			//物件キーワード検索  $s st

				$s = isset($_GET['s']) ? esc_sql(esc_attr($_GET['s'])) : '';

				$s = str_replace(" ","",$s);
				$s = str_replace(";","",$s);
				$s = str_replace(",","",$s);
				$s = str_replace("'","",$s);
				$s = str_replace("\\","",$s);

				$searchtype = isset($_GET['st']) ? esc_sql(esc_attr($_GET['st'])) : '';


			//ページ
				$bukken_page_data = isset($_GET['paged']) ? myIsNum_f($_GET['paged']) : '';
				if($bukken_page_data < 2) $bukken_page_data = "";


			//種別
				$bukken_shubetsu = isset($_GET['shu']) ? myIsNum_f($_GET['shu']) : '';

			//複数種別用 売買・賃貸判別
				$shub = isset($_GET['shub']) ? myIsNum_f($_GET['shub']) : '';


			//複数種別
				if (is_array($bukken_shubetsu)) {
					$i=0;
					$shu_data = ' IN ( 0 ';
					foreach($bukken_shubetsu as $meta_set){
						if( (int)$bukken_shubetsu[$i] ){
							$shu_data .= ','. (int)$bukken_shubetsu[$i] . '';
						}
						$i++;
					}
					$shu_data .= ') ';

				} else {
					$shu_data = " > 0 ";
					if($bukken_shubetsu == '1')
						$shu_data = '< 3000' ;	//売買
					if($bukken_shubetsu == '2')
						$shu_data = '> 3000' ;	//賃貸

					if(intval($bukken_shubetsu) > 3 && $bukken_slug_data == 'jsearch')
						$shu_data = '= ' . (int)$bukken_shubetsu ;
				}

			//複数種別用 売買・賃貸判別
				if($bukken_shubetsu == '' && $shub != ''){
					if ($shub == '1' )
						$shu_data = '< 3000' ;	//売買
					if ($shub == '2' )
						$shu_data = '> 3000' ;	//賃貸
				}

			//条件検索用

			$rosen_eki = isset($_GET['re']) ? myIsNum_f($_GET['re']) : '';		//路線駅 array
			$ksik_id   = isset($_GET['ksik']) ? myIsNum_f($_GET['ksik']) : '';	//県市区 array

			if($bukken_slug_data == "jsearch"){

				$ros_id = isset($_GET['ros']) ? myIsNum_f($_GET['ros']) : '';	//路線
				$eki_id = isset($_GET['eki']) ? myIsNum_f($_GET['eki']) : '';	//駅
				$ken_id = isset($_GET['ken']) ? myIsNum_f($_GET['ken']) : '';	//県
				$sik_id = isset($_GET['sik']) ? myIsNum_f($_GET['sik']) : '';	//市区

				$ken_id = sprintf( "%02d", $ken_id );

				//複数市区
				$ksik_data = '';
				if (is_array($ksik_id)) {
					$i=0;
					$ksik_data = " IN ( '99999' ";
					foreach($ksik_id as $meta_set){
						if( myIsNum_f($ksik_id[$i]) ){
							$ksik_data .= ", '". myIsNum_f($ksik_id[$i]) . "000000'";
						}
						$i++;
					}
					$ksik_data .= ") ";
				}


				//複数駅
				$eki_data = '';
				if(is_array( $rosen_eki )  ){
					$i=0;
					$eki_data = ' IN ( 0 ';
					foreach($rosen_eki as $meta_set){
						if( intval(myLeft($rosen_eki[$i],6)) ){
							$eki_data .= ',' . intval(myLeft($rosen_eki[$i],6)) . intval(myRight($rosen_eki[$i],6));
						}
						$i++;
					}
					$eki_data .= ') ';
				}


				//設備条件
				$set_id = isset($_GET['set2']) ? myIsNum_f($_GET['set2']) : '';
				echo $set_id;
				$setsubi_name = '';
				if(!empty($set_id)) {
					$setsubi_name = '設備条件： ';
					$i=0;//print_r($word_setsubi2);
					foreach($set_id as $meta_set){
						foreach($work_setsubi2 as $meta_setsubi){
							if($set_id[$i] == $meta_setsubi['code'] )
								$setsubi_name .= $meta_setsubi['name'] . ' ';
						}
						$i++;
					}
				}else{
					//print_r($work_setsubi2);
				}


				//間取り
				$madori_id = isset($_GET['mad']) ? myIsNum_f($_GET['mad']) : '';
				$madori_name = '';
				if(!empty($madori_id)) {
					$madori_name = '間取り：';

					$i=0;
					foreach($madori_id as $meta_box){
						$madorisu_data = $madori_id[$i];
						$madorisyurui_data = myRight($madorisu_data,2);
						$madori_name .= myLeft($madorisu_data,1);

						if($madorisyurui_data=="10")	$madori_name .= 'R ';
						if($madorisyurui_data=="20")	$madori_name .= 'K ';
						if($madorisyurui_data=="25")	$madori_name .= 'SK ';
						if($madorisyurui_data=="30")	$madori_name .= 'DK ';
						if($madorisyurui_data=="35")	$madori_name .= 'SDK ';
						if($madorisyurui_data=="40")	$madori_name .= 'LK ';
						if($madorisyurui_data=="45")	$madori_name .= 'SLK ';
						if($madorisyurui_data=="50")	$madori_name .= 'LDK ';
						if($madorisyurui_data=="55")	$madori_name .= 'SLDK ';
						$i++;
					}
				}



				//価格
				$kalb_data = isset($_GET['kalb']) ? myIsNum_f($_GET['kalb']) : '';	//価格下限
				$kahb_data = isset($_GET['kahb']) ? myIsNum_f($_GET['kahb']) : '';	//価格上限
				$kalc_data = isset($_GET['kalc']) ? myIsNum_f($_GET['kalc']) : '';	//賃料下限
				$kahc_data = isset($_GET['kahc']) ? myIsNum_f($_GET['kahc']) : '';	//賃料上限
				$kakaku_name = '';



					$kal_data =0 ;
					$kah_data =0 ;

					//売買
					if($bukken_shubetsu == '1' || ( intval($bukken_shubetsu) < 3000 && intval($bukken_shubetsu) > 1000 ) || $shub == '1') {

						$kal_data = intval( $kalb_data ) * 10000 ;
						$kah_data = intval( $kahb_data ) * 10000 ;

							//価格条件
							if($kalb_data > 0 || $kahb_data > 0 ){
								$kakaku_name = '価格：';
								if($kalb_data > 0 )
									$kakaku_name .= $kalb_data.'万円';
								$kakaku_name .= '～';
								if($kahb_data > 0)
									$kakaku_name .= $kahb_data . '万円 ' ;
							}
					}

					//賃貸
					if($bukken_shubetsu == '2' || intval($bukken_shubetsu) > 3000  || $shub == '2') {

						$kal_data = intval( $kalc_data ) * 10000 ;
						$kah_data = intval( $kahc_data ) * 10000 ;

							//賃料条件
							if($kalc_data > 0 || $kahc_data > 0 ){
								$kakaku_name = '賃料：';
								if($kalc_data > 0 )
									$kakaku_name .= $kalc_data.'万円';
								$kakaku_name .= '～';
								if($kahc_data > 0)
									$kakaku_name .= $kahc_data . '万円 ' ;
							}
					}


				//表面利回り
				$rimaw_name = '';
				$rimaw_data = isset($_GET['rim']) ? myIsNum_f($_GET['rim']) : ''; 	//表面利回り
					//利回り条件
					if($rimaw_data > 0 )
						$rimaw_name = '利回り'.$rimaw_data.'%以上 ';


				//築年数
				$tiku_name = '';
				$tik_data = isset($_GET['tik']) ? myIsNum_f($_GET['tik']) : '';
					$tik_data = intval($tik_data);

					//築年数条件
					if($tik_data > 0 )
						$tiku_name = '築'.$tik_data.'年以内 ';


				//歩分
				$hofun_name = '';
				$hof_data = isset($_GET['hof']) ? myIsNum_f($_GET['hof']) : '';

					//歩分条件
					if($hof_data > 0 )
						$hofun_name = '駅徒歩'.$hof_data.'分以内 ';




				//面積下限
				$mel_data = isset($_GET['mel']) ? myIsNum_f($_GET['mel']) : '';
					$mel_data = intval($mel_data);

				//面積上限
				$meh_data = isset($_GET['meh']) ? myIsNum_f($_GET['meh']) : '';
					$meh_data = intval($meh_data);

					//面積条件
					$menseki_name = '';
					if($mel_data > 0 || $meh_data > 0 ){
						$menseki_name = '面積';

						if($mel_data > 0 )
							$menseki_name .= $mel_data.'m&sup2;';

						$menseki_name .= '～';

						if($meh_data > 0)
							$menseki_name .= $meh_data . 'm&sup2; ' ;
					}

			} 	//条件検索用



			//タイトル
				$org_title = '';
				if($s != ''){
					$org_title = '検索: '.$s ;

					if($searchtype == 'id'){
						$org_title = '検索 物件番号: '.$s ;
					}

					if($searchtype == 'chou'){
						$org_title = '検索 町名: '.$s ;
					}
				}else{
					if ($taxonomy_name !=''){

						if( $taxonomy_name == 'bukken' ){
							$org_title = 'カテゴリ: ';
							$joken_url  = $site .'?bukken='.$slug_data.'';
						}
						if( $taxonomy_name == 'bukken_tag'){
							$org_title = 'タグ: ';
							$joken_url  = $site .'?bukken_tag='.$slug_data.'';
						}

						$sql  = "SELECT T.name";
						$sql .= " FROM $wpdb->terms AS T ";
						$sql .= " INNER JOIN $wpdb->term_taxonomy AS TT ON T.term_id = TT.term_id ";
						$sql .= " WHERE TT.taxonomy  = '".$taxonomy_name."' ";
						$sql .= " AND T.slug   = '".$slug_data."' ";
					//	$sql = $wpdb->prepare($sql,'');
						$metas = $wpdb->get_row( $sql );
						if(!empty($metas))
							$org_title .= $metas->name;

					}
				}


			//種別タイトル
				global $work_bukkenshubetsu;
				//複数種別
				$shu_name = '';
				if (is_array($bukken_shubetsu)) {
					$i=0;
					foreach($bukken_shubetsu as $meta_set){
						foreach($work_bukkenshubetsu as $meta_box){
							if( $bukken_shubetsu[$i] ==  $meta_box['id'] ){
								$shu_name .= ' '. $meta_box['name'] . ' ';
							}
						}
						$i++;
					}
				} else {
					//種別
					$bukken_shubetsu = isset($_GET['shu']) ? myIsNum_f($_GET['shu']) : '';

						foreach($work_bukkenshubetsu as $meta_box){
							if( $bukken_shubetsu ==  $meta_box['id'] ){
								$shu_name = ' '. $meta_box['name'] . ' ';
							}
						}
				}




			//路線タイトル
				$rosen_name = '';
				if( ($bukken_slug_data=="rosen" && $mid_id !="") || ($bukken_slug_data=="jsearch" && $ros_id != '' && $eki_id == 0 ) ){
					$rosen_id = $mid_id;
					if($bukken_slug_data=="jsearch" && $ros_id != '')
						$rosen_id = $ros_id;

					if( !is_array( $rosen_id ) && $rosen_id != '' ){
						$sql = "SELECT rosen_name FROM " . $wpdb->prefix . DB_ROSEN_TABLE . " WHERE rosen_id =".$rosen_id."";
					//	$sql = $wpdb->prepare($sql,'');
						$metas = $wpdb->get_row( $sql );
						if(!empty($metas)) $org_title = $metas->rosen_name;
						$rosen_name = $org_title;
					}
				}

			//駅タイトル
				$eki_name = '';
				if( ($bukken_slug_data=="station" && $mid_id !="" && $nor_id !="") || ($bukken_slug_data=="jsearch" && $ros_id != '' && $eki_id != '' ) ){
					$rosen_id = $mid_id;
					$ekin_id  = $nor_id;
					if($bukken_slug_data=="jsearch" && $ros_id != '' && $eki_id != ''){
						$rosen_id = $ros_id;
						$ekin_id  = $eki_id;
					}

					if( !is_array( $rosen_id ) && $rosen_id != '' && !is_array( $ekin_id ) && $ekin_id != ''){
						$sql = "SELECT DTS.station_name,DTR.rosen_name";
						$sql .=  " FROM " . $wpdb->prefix . DB_ROSEN_TABLE . " AS DTR";
						$sql .=  " INNER JOIN " . $wpdb->prefix . DB_EKI_TABLE . " AS DTS ON DTR.rosen_id = DTS.rosen_id";
						$sql .=  " WHERE DTS.rosen_id=".$rosen_id." AND DTS.station_id=".$ekin_id."";
					//	$sql = $wpdb->prepare($sql,'');
						$metas = $wpdb->get_row( $sql );
						if(!empty($metas)) {
							$org_title = $metas->rosen_name.''.$metas->station_name.'駅';
							$eki_name = $org_title . ' ';
						}
					}
				}


			//バス路線タイトル
				if( $bukken_slug_data == "bus" ){
					$org_title = apply_filters( 'fudoubus_title_archive', '' );
				}


			//県タイトル
				$ken_name = '';
				if( ($bukken_slug_data=="ken" && $mid_id !="") || ($bukken_slug_data=="jsearch" && $ken_id != '') ){
					$kenn_id = $mid_id;
					if($bukken_slug_data=="jsearch" && $ken_id != '')
						$kenn_id = $ken_id;

					if( !is_array( $kenn_id ) && $kenn_id != '' ){
						$sql = "SELECT middle_area_name FROM " . $wpdb->prefix . DB_KEN_TABLE . " WHERE middle_area_id=".$kenn_id."";
					//	$sql = $wpdb->prepare($sql,'');
						$metas = $wpdb->get_row( $sql );
						if(!empty($metas)){
							$org_title = $metas->middle_area_name;
							$ken_name = $org_title;
						}
					}
				}

			//市区タイトル
				$siku_name = '';
				if( ($bukken_slug_data=="shiku" && $mid_id !="" && $nor_id !="") || ($bukken_slug_data=="jsearch" && $ken_id != '' && $sik_id !='') ){
					$kenn_id = $mid_id;
					$sikn_id = $nor_id;
					if($bukken_slug_data=="jsearch" && $ken_id != '' && $sik_id != ''){
						$kenn_id = $ken_id;
						$sikn_id = $sik_id;
					}

					if( $kenn_id != '' && !is_array( $sikn_id ) && $sikn_id !='' ){
						$sql = "SELECT narrow_area_name FROM " . $wpdb->prefix . DB_SHIKU_TABLE . " WHERE middle_area_id=".$kenn_id." and narrow_area_id =".$sikn_id."";
					//	$sql = $wpdb->prepare($sql,'');
						$metas = $wpdb->get_row( $sql );
						if(!empty($metas)){
							$org_title = $metas->narrow_area_name;
							$siku_name = $org_title . ' ';
						}
					}
				}


			//複数駅タイトル
				if(is_array( $rosen_eki )  ){
					$i=0;
					foreach($rosen_eki as $meta_set){
						$f_rosen_id =  intval(myLeft($rosen_eki[$i],6));
						$f_eki_id   =  intval(myRight($rosen_eki[$i],6));

						$sql = "SELECT DTS.station_name";
						$sql .=  " FROM " . $wpdb->prefix . DB_EKI_TABLE . " AS DTS";
						$sql .=  " WHERE DTS.rosen_id=".$f_rosen_id." AND DTS.station_id=".$f_eki_id."";
					//	$sql = $wpdb->prepare($sql,'');
						$metas = $wpdb->get_row( $sql );
						if(!empty($metas)) $eki_name .= $metas->station_name . '駅 ';

						$i++;
					}
				}


			//複数市区タイトル $ksik_id = $_GET['ksik']; //県市区
				$ksik_name = '';
				if (is_array($ksik_id)) {
					$sql  = "SELECT narrow_area_name FROM " . $wpdb->prefix . DB_SHIKU_TABLE . "";
					$sql .= " WHERE ";
					$i=0;
					$j=0;
					foreach($ksik_id as $meta_set){
						$tmp_kenn_id = $ksik_id[$i];
						$kenn_id = myLeft($tmp_kenn_id,2);
						$sikn_id = myRight($tmp_kenn_id,3);
						if($kenn_id != '' && $sikn_id != ''){
							if($j > 0 ) $sql .= " OR ";
							$sql .= "( middle_area_id=".$kenn_id." and narrow_area_id =".$sikn_id.")";
							$j++;
						}
						$i++;
					}
					if ($j > 0 ){
					//	$sql = $wpdb->prepare($sql,'');
						$metas = $wpdb->get_results( $sql, ARRAY_A );
						if(!empty($metas)) {

							foreach ( $metas as $meta ) {
								$ksik_name .= '' . $meta['narrow_area_name'] .' ';

							}
						}
					}
				}


			//条件検索タイトル生成
				if($bukken_slug_data == "jsearch"){
					// $org_title = '検索 ';
					$org_title .= $shu_name;
					$org_title .= $rosen_name;
					$org_title .= $eki_name;
					$org_title .= apply_filters( 'fudoubus_title_archive', '' );
					$org_title .= apply_filters( 'fudoubus_title_archive2', '' );
					$org_title .= $ken_name;
					$org_title .= $siku_name;
					$org_title .= $ksik_name;
					$org_title .= $tiku_name;
					$org_title .= $hofun_name;
					$org_title .= $menseki_name;
					$org_title .= $kakaku_name;
					$org_title .= $rimaw_name;
					$org_title .= $setsubi_name;
					$org_title .= $madori_name;
				}

			// //売買
			// if($bukken_shubetsu == '1' || (intval($bukken_shubetsu) < 3000 && intval($bukken_shubetsu) > 1000) || $shub == '1' ) {
			// 	$org_title =  '売買 > '.$org_title.' ';
			// }

			// //賃貸
			// if($bukken_shubetsu == '2' || intval($bukken_shubetsu) > 3000 || $shub == '2' ) {
			// 	$org_title =  '賃貸 > '.$org_title.' ';
			// }


		//オリジナルフィルター $org_title
			$org_title = apply_filters( 'fudou_org_title_archive', $org_title );


		//クッキー書き込み

			$jsearch_history_days = get_option('jsearch_history_days');
			if( empty( $jsearch_history_days ) ) $jsearch_history_days = 30;

			$jsearch_history_views = get_option('jsearch_history_views');
			if( empty( $jsearch_history_views ) ) $jsearch_history_views = 10;



			//クッキー読み込み
			$cookie_fudou_jsearch = isset($_COOKIE['fudou_jsearch']) ? $_COOKIE['fudou_jsearch'] : '';
			if( !empty( $cookie_fudou_jsearch ) ){
				$cookie_fudou_jsearch = str_replace( "\\" ,"" , $cookie_fudou_jsearch);
				$cookie_fudou_jsearch = maybe_unserialize( $cookie_fudou_jsearch );
			}else{
				$cookie_fudou_jsearch = array();
			}

			//URL
			if(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on'){
				$jsearch_url = esc_url( "https://" . $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"] );
			} else {
				$jsearch_url = esc_url( "http://" . $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"] );
			}


			//ログ追加
			$add_fudou_jsearch = array();
			$add_fudou_jsearch[count($cookie_fudou_jsearch)]['url'] = esc_url( $jsearch_url );
			$add_fudou_jsearch[count($cookie_fudou_jsearch)]['title'] = $org_title ;
			$fudou_jsearch = $add_fudou_jsearch + $cookie_fudou_jsearch  ;

			//ログ最大値
			if( !empty( $fudou_jsearch ) ) {
				$new_fudou_jsearch = array();
				$i=1;
				foreach( $fudou_jsearch as $id => $meta ) {
					//重複削除
					$copy_ok = true;
					foreach( $new_fudou_jsearch as $meta2 ) {
						if( $meta2 == $meta ){
							$copy_ok = false;
							break;
						}
					}
					if( $copy_ok )	$new_fudou_jsearch[] = $meta;
					if( $i >= $jsearch_history_views ) break;
					$i++;
				}
			}

			//クッキー用ドメイン・フォルダ
			if( $folder ){
				setcookie( 'fudou_jsearch' , maybe_serialize( $new_fudou_jsearch ) , time()+60*60*24*$jsearch_history_days , $folder );
			}else{
				setcookie( 'fudou_jsearch' , maybe_serialize( $new_fudou_jsearch ) , time()+60*60*24*$jsearch_history_days , '/' );
			}
		}
	}


	$hstb = isset($_GET['hstb']) ? myIsNum_f($_GET['hstb']) : 0 ;
	$hstj = isset($_GET['hstj']) ? myIsNum_f($_GET['hstj']) : 0 ;

	if( $hstb == 1 ){

		//クッキー用ドメイン・フォルダ
		if( $folder ){
			setcookie( 'fudou_single'  , '' , time() - 3600 , $folder );
		}else{
			setcookie( 'fudou_single'  , '' , time() - 3600 , '/' );
		}

		$new_fudou_single = array();
	}

	if( $hstj == 1 ){

		//クッキー用ドメイン・フォルダ
		if( $folder ){
			setcookie( 'fudou_jsearch' , '' , time() - 3600 , $folder );
		}else{
			setcookie( 'fudou_jsearch' , '' , time() - 3600 , '/' );
		}

		$new_fudou_jsearch = array();
	}
	return $template;
}
add_action('template_include', 'fudou_add_jsearch_single_history');

