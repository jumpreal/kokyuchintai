<?php
/*
 * 不動産閲覧履歴プラグイン管理画面設定
 * @package WordPress5.2
 * @subpackage Fudousan Plugin
 * Fudousan Browsing history
 * Version: 5.2.0
*/




/**
 * 不動産閲覧履歴プラグイン基本設定
 *
 * Version: 5.2.0
 */
if( is_admin() ){
	new FudouBrowsingHistoryPlugin();
}
class FudouBrowsingHistoryPlugin {

	/**
	 * Start up
	 */
	function __construct() {
		add_action( 'admin_menu', array( $this, 'admin_menu' ) );
	}

	/**
	 * Add options page
	 */
	function admin_menu () {
		add_options_page( 'admin_fudou_history_menu', '不動産閲覧履歴設定', 'edit_pages', __FILE__, array( $this, 'form' ) );
	}

	function process_option($name, $default, $params) {
		$value = '';
		if( isset( $_POST['single_history_days']) ){
			$value = stripslashes($params[$name]);
			$stored_value = get_option($name);

				if ($stored_value === false) {
					add_option($name, $value);
				} else {
					update_option($name, $value);
				}
		}else{
			$value = get_option($name);;
		}
		return $value;
	}

	//プラグイン設定フォーム
	function form() {

		global $post;

		$single_history_days		= $this->process_option( 'single_history_days',  '', $_POST );
		$single_history_views		= $this->process_option( 'single_history_views', '', $_POST );

		$jsearch_history_days		= $this->process_option( 'jsearch_history_days',  '', $_POST );
		$jsearch_history_views		= $this->process_option( 'jsearch_history_views', '', $_POST );

		if( empty( $single_history_days ) ) $single_history_days = 30;
		if( empty( $single_history_views ) ) $single_history_views = 10;

		if( empty( $jsearch_history_days ) ) $jsearch_history_days = 30;
		if( empty( $jsearch_history_views ) ) $jsearch_history_views = 10;

		?>

		<div class="wrap">
			<div id="icon-tools" class="icon32"><br /></div>
			<h2>不動産閲覧履歴設定</h2>
			<div id="poststuff">

			<div id="post-body">
			<div id="post-body-content">

				<?php if ( !empty($_POST ) ) : ?>
				<div id="message" class="updated fade"><p><strong><?php _e('Options saved.') ?></strong></p></div>
				<?php endif; ?> 

				<br />

				<form class="add:the-list: validate" method="post">


				<h3>閲覧履歴(物件)設定</h3>
				<div style="margin:0 0 0 20px;">

					<table>
					<tr>
						<td>保持期間 (日数)</td><td> <input name="single_history_days" type="text" value="<?php echo $single_history_days; ?>" /></td> 
					</tr>
					<tr>
						<td>保持数</td><td> <input name="single_history_views" type="text" value="<?php echo $single_history_views; ?>" /></td> 
					</tr>
					</table>
				</div>
				<br />


				<h3>検索履歴(条件検索)設定</h3>
				<div style="margin:0 0 0 20px;">

					<table>
					<tr>
						<td>保持期間 (日数)</td><td> <input name="jsearch_history_days" type="text" value="<?php echo $jsearch_history_days; ?>" /></td> 
					</tr>
					<tr>
						<td>保持数</td><td> <input name="jsearch_history_views" type="text" value="<?php echo $jsearch_history_views; ?>" /></td> 
					</tr>
					</table>
				</div>
				<br />

				<p class="submit"><input type="submit" name="submit" id="submit" class="button-primary" value="変更を保存"  /></p>

			</div>
			</div>

		    </form>
		</div>

		<?php
	}
}
